<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nusoap_lib {
	function __construct() {
		require_once('nusoap/lib/nusoap.php');
	}
}

/* End of file Nusoap_lib.php */
/* Location: ./system/application/libraries/Nusoap_lib.php */