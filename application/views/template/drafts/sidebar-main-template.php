<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"> </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                    <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                    </a>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <a href="javascript:;" class="btn submit">
                                <i class="icon-magnifier"></i>
                            </a>
                        </span>
                    </div>
                </form>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
            <li class="nav-item start <?=in_array($this->uri->segment(1), array('reports','open_reports')) ? 'active open' : ''?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-laptop"></i>
                    <span class="title">Report Module</span>
                    <span class="arrow <?=in_array($this->uri->segment(1), array('reports','open_reports')) ? 'open' : ''?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?=in_array($this->uri->segment(1), array('reports','open_reports')) ? 'active open' : ''?> ">
                        <a href="<?=base_url('reports/template')?>" class="nav-link nav-toggle">
                            <span class="title">Template</span>
                            <?=($this->uri->segment(1) == 'reports') ? '<span class="selected"></span>' : ''?>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item <?=($this->uri->segment(1) == 'config') ? 'active open' : ''?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-gears"></i>
                    <span class="title">System Configuration</span>
                    <?php if($this->uri->segment(1) == 'config'): ?><span class="selected"></span><?php endif; ?>
                    <span class="arrow <?=$this->uri->segment(1) == 'config' ? 'open' : ''?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?=($this->uri->segment(2) == 'database') ? 'active open' : ''?>">
                        <a href="<?=base_url('config/database')?>" class="nav-link nav-toggle">
                            <span class="title">Database</span>
                            <?=($this->uri->segment(2) == 'database') ? '<span class="selected"></span>' : ''?>
                        </a>
                    </li>
                    <!-- <li class="nav-item <?=($this->uri->segment(2) == 'database_type') ? 'active open' : ''?>">
                        <a href="<?=base_url('config/database_type')?>" class="nav-link nav-toggle">
                            <span class="title">Types of Database</span>
                            <?=($this->uri->segment(2) == 'database_type') ? '<span class="selected"></span>' : ''?>
                        </a>
                    </li> -->
                </ul>
            </li>
            <li class="heading">
                <h3 class="uppercase">User Management</h3>
            </li>
            <li class="nav-item start <?=($this->uri->segment(1) == 'users') ? 'active open' : ''?>">
                <a href="<?=base_url('users')?>" class="nav-link nav-toggle">
                    <i class="fa fa-user"></i>
                    <span class="title">User Accounts</span>
                    <?=($this->uri->segment(1) == 'users') ? '<span class="selected"></span>' : ''?>
                </a>
            </li>
            <li class="nav-item <?=($this->uri->segment(1) == 'permissions') ? 'active open' : ''?>">
                <a href="<?=base_url('permissions')?>" class="nav-link nav-toggle">
                    <i class="fa fa-unlock-alt"></i>
                    <span class="title">Permissions</span>
                    <?=($this->uri->segment(1) == 'permissions') ? '<span class="selected"></span>' : ''?>
                </a>
            </li>
            <li class="nav-item <?=($this->uri->segment(1) == 'roles') ? 'active open' : ''?>">
                <a href="<?=base_url('roles')?>" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Roles</span>
                    <?=($this->uri->segment(1) == 'roles') ? '<span class="selected"></span>' : ''?>
                </a>
            </li>
            <li class="nav-item <?=($this->uri->segment(1) == 'others') ? 'active open' : ''?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-gears"></i>
                    <span class="title">Others</span>
                    <?php if($this->uri->segment(1) == 'others'): ?><span class="selected"></span><?php endif; ?>
                    <span class="arrow <?=$this->uri->segment(1) == 'others' ? 'open' : ''?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?=($this->uri->segment(2) == 'questions') ? 'active open' : ''?>">
                        <a href="<?=base_url('others/questions')?>" class="nav-link nav-toggle">
                            <span class="title">Security Questions</span>
                            <?=($this->uri->segment(2) == 'questions') ? '<span class="selected"></span>' : ''?>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>