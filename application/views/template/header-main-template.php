<!-- BEGIN HEADER -->
<header class="page-header">
    <nav class="navbar mega-menu" role="navigation">
        <div class="container-fluid">
            <div class="clearfix navbar-fixed-top">
                <!-- Brand and toggle get grouped for better mobile display -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="toggle-icon">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                </button>
                <!-- End Toggle Button -->
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <span class="logo-default"><span class="logo-title">BI </span> <span class="logo-subtitle">RTOOL</span></span>
                    <div class="menu-toggler sidebar-toggler"> </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN SEARCH -->
                <form class="search" action="extra_search.html" method="GET">
                    <input type="name" class="form-control" name="query" placeholder="Search...">
                    <a href="javascript:;" class="btn submit">
                        <i class="fa fa-search"></i>
                    </a>
                </form>
                <!-- END SEARCH -->
                <!-- BEGIN TOPBAR ACTIONS -->
                <div class="topbar-actions">
                    <!-- BEGIN GROUP NOTIFICATION -->
                    <div class="btn-group-notification btn-group" id="header_notification_bar">
                        <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-bell"></i>
                            <span class="badge"><?=count(notifications(1))?></span>
                        </button>
                        <ul class="dropdown-menu-v2">
                            <li class="external">
                                <h3>
                                    <span class="bold"><?=count(notifications(1))?> pending</span> notifications</h3>
                                <a href="<?=base_url('settings/users/notification')?>">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px; padding: 0;" data-handle-color="#637283">
                                    <?php foreach(notifications(1) as $notif) { $notif_message = strip_tags($notif['notif_message']); ?>
                                        <li title="<?=$notif_message?>">
                                            <a href="javascript:;">
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-<?=$notif['alert_type']?>">
                                                        <i class="<?=$notif['type_icon']?>"></i>
                                                    </span> <?=strlen($notif_message) > 22 ? substr($notif_message,0,22).'...' : $notif_message?> </span>
                                                <span class="time"><?=$notif['time_passed']?></span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- END GROUP NOTIFICATION -->
                    <!-- BEGIN USER PROFILE -->
                    <div class="btn-group-img btn-group">
                        <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span>Hi, <?=$_SESSION['user_name']?></span>
                            <img src="<?=base_url('uploads/profiles/'.$_SESSION['image'])?>" alt=""> </button>
                        <ul class="dropdown-menu-v2" role="menu">
                            <li>
                                <a href="<?=base_url('settings/users/profile')?>">
                                    <i class="icon-user"></i> My Profile
                                </a>
                            </li>
                            <li>
                                <a href="<?=base_url('settings/users/group')?>">
                                    <i class="icon-users"></i> My Group
                                </a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="<?=base_url('user_login/login/logout')?>">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END USER PROFILE -->
                </div>
                <!-- END TOPBAR ACTIONS -->
            </div>
            <!-- BEGIN HEADER MENU -->
            <?php $fpage = $this->uri->segment(1); $spage = $this->uri->segment(2); $tpage = $this->uri->segment(3); ?>
            <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown dropdown-fw <?=$fpage=='reports' ? 'active open selected' : ''?>">
                        <a href="<?=base_url('reports/report_all')?>" class="text-uppercase" id="ahome">
                            <i class="glyphicon glyphicon-home"></i> Home </a>
                        <ul class="dropdown-menu dropdown-menu-fw">
                            <li class="<?=$spage == 'report_all' ? 'active' : ''?>">
                                <a href="<?=base_url('reports/report_all')?>">
                                    <i class="icon-bar-chart"></i> All </a>
                            </li>
                            <li class="<?=$spage == 'report_public' ? 'active' : ''?>">
                                <a href="<?=base_url('reports/report_public')?>">
                                    <i class="fa fa-cloud"></i> Public </a>
                            </li>
                            <li class="<?=$spage == 'report_shared' ? 'active' : ''?>">
                                <a href="<?=base_url('reports/report_shared')?>">
                                    <i class="fa fa-share-alt"></i> Shared </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-fw <?=$fpage=='projects' ? 'active open selected' : ''?>">
                        <a href="<?=base_url('projects/project_all')?>" class="text-uppercase" id="aprojects">
                            <i class="fa fa-bar-chart-o"></i> Projects </a>
                        <ul class="dropdown-menu dropdown-menu-fw">
                            <li class="<?=$spage == 'project_all' || $spage == 'project_all_owned' || $spage == 'project_all_shared' ? 'active' : ''?>">
                                <a href="<?=base_url('projects/project_all')?>">
                                    <i class="fa fa-folder-open-o"></i> All </a>
                            </li>
                            <li class="<?=$spage == 'new_project' ? 'active' : ''?>">
                                <a href="<?=base_url('projects/new_project')?>">
                                    <i class="fa fa-plus"></i> Add new Project </a>
                            </li>
                            <li class="<?=$spage == 'inactive_projects' || $spage == 'inactive_projects_owned' || $spage == 'inactive_projects_shared' ? 'active' : ''?>">
                                <a href="<?=base_url('projects/inactive_projects')?>">
                                    <i class="fa fa-ban"></i> Inactive Projects </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-fw <?=$fpage=='datasources' ? 'active open selected' : ''?>">
                        <a href="<?=base_url('datasources/datasources_all')?>" class="text-uppercase" id="adsources">
                            <i class="fa fa-database"></i> Data Sources </a>
                        <ul class="dropdown-menu dropdown-menu-fw">
                            <li class="<?=$spage == 'datasources_all' ? 'active' : ''?>">
                                <a href="<?=base_url('datasources/datasources_all')?>">
                                    <i class="fa fa-database"></i> All </a>
                            </li>
                            <li class="<?=$spage == 'new_source' ? 'active' : ''?>">
                                <a href="<?=base_url('datasources/new_source')?>">
                                    <i class="fa fa-plus"></i> Add new Connections </a>
                            </li>
                            <li class="<?=$spage == 'inactive_datasources' ? 'active' : ''?>">
                                <a href="<?=base_url('datasources/inactive_datasources')?>">
                                    <i class="fa fa-ban"></i> Inactive Connections </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-fw <?=$fpage=='settings' ? 'active open selected' : ''?>">
                        <a href="<?=base_url('settings/users')?>" class="text-uppercase" id="asettings">
                            <i class="fa fa-gears"></i> Settings </a>
                        <ul class="dropdown-menu dropdown-menu-fw">
                            <li class="dropdown more-dropdown-sub <?=$spage == 'users' || $spage == 'add_newuser' ? 'active' : ''?>">
                                <a href="javascript:;">
                                    <i class="icon-users"></i> Users </a>
                                <ul class="dropdown-menu">
                                    <li class="<?=$tpage == '' || $tpage == 'index' ? 'active' : ''?>">
                                        <a href="<?=base_url('settings/users')?>">
                                             <i class="fa fa-users"></i> All </a>
                                    </li>
                                    <li class="<?=$tpage == 'new_user' ? 'active' : ''?>">
                                        <a href="<?=base_url('settings/users/new_user')?>">
                                             <i class="fa fa-plus"></i> Add New </a>
                                    </li>
                                    <li class="<?=$tpage == 'inactive_users' ? 'active' : ''?>">
                                        <a href="<?=base_url('settings/users/inactive_users')?>">
                                             <i class="fa fa-ban"></i> Inactive </a>
                                    </li>
                                    <li class="<?=$tpage == 'deleted_users' ? 'active' : ''?>">
                                        <a href="<?=base_url('settings/users/deleted_users')?>">
                                             <i class="fa fa-user-times"></i> Deleted </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- <li class="<?=$spage == 'permissions' || $spage == 'add_newpermission' ? 'active' : ''?>">
                                <a href="<?=base_url('settings/permissions')?>">
                                    <i class="fa fa-key"></i> Permissions </a>
                            </li> -->
                            <li class="<?=$spage == 'roles' || $spage == 'add_newrole' ? 'active' : ''?>">
                                <a href="<?=base_url('settings/roles')?>">
                                    <i class="icon-user-following"></i> Roles </a>
                            </li>
                            <li class="dropdown more-dropdown-sub <?=$spage == 'add_question' || $spage == 'security_questions' ? 'active' : ''?>">
                                <a href="javascript:;">
                                    <i class="fa fa-lock"></i> Security Questions </a>
                                <ul class="dropdown-menu">
                                    <li class="<?=$spage == 'security_questions' ? 'active' : ''?>">
                                        <a href="<?=base_url('settings/security_questions')?>">
                                             <i class="fa fa-list"></i> All </a>
                                    </li>
                                    <li class="<?=$spage == 'add_questions' ? 'active' : ''?>">
                                        <a href="<?=base_url('settings/add_question')?>"> 
                                             <i class="fa fa-plus"></i> Add New </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?=$spage == 'dbsourcetype' ? 'active' : ''?>">
                                <a href="<?=base_url('settings/dbsourcetype')?>">
                                    <i class="fa fa-database"></i> Datasource Type</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- END HEADER MENU -->
        </div>
        <!--/container-->
    </nav>
</header>
<!-- END HEADER -->