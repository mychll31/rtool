<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('set_custom_userid'))
{
    function set_custom_userid($userid)
    {
        return 'RTL-'.date('Y').'-'.(str_pad($userid,5,'0',STR_PAD_LEFT));
    }
}

if ( ! function_exists('default_password'))
{
    function default_password()
    {
        $strpass = substr(str_shuffle(str_repeat($x='0123456789qwertyuiopasdfghjklzxcvbnm', ceil(7/strlen($x)))), 1,7);
        return $strpass;
    }
}

if ( ! function_exists('include_datatables'))
{
    function include_datatables()
    {
    	$CI =& get_instance();
    	if(in_array($CI->uri->segment(1), array('libraries'))):
    		return true;
    	else:
    		return false;
    	endif;
	}
}

if ( ! function_exists('getFullname'))
{
    function getFullname($fname, $lname, $mname)
    {
        $mname = $mname == '' ? '' : ucfirst($mname[0]);
    	return ucfirst($fname).', '.ucfirst($lname).' '.$mname.'.';
	}
}

if ( ! function_exists('fixfullname'))
{
    function fixfullname($fname, $lname, $mname)
    {
        $midname = $mname != '' ? $mname[0].'.' : '';
        return ucfirst($fname).' '.ucfirst($midname).' '.ucfirst($lname);
    }
}

if ( ! function_exists('unique_multidimensional_array'))
{
    function unique_multidimensional_array($array, $key)
    {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }
}

if ( ! function_exists('set_title'))
{
    function set_title($title)
    {
        echo '<title>RTOOL | '.ucfirst($title).'</title>';
    }
}

if ( ! function_exists('getDatabase_type'))
{
    function getDatabase_type($type=null)
    {
        $dbtypes = array('1' => 'MSSQL',
                         '2' => 'MySQL', 
                         '3' => 'SQLite');
        if($type!=null){
            return $dbtypes[$type];
        }else{
            return $dbtypes;
        }
    }
}

if ( ! function_exists('where_operator'))
{
    function where_operator($id=null)
    {
        $operators = array('1' => 'where',
                           '2' => 'and', 
                           '3' => 'or',
                           '4' => 'not');
        if($id!=null){
            return $operators[$id];
        }else{
            return $operators;
        }
    }
}

if ( ! function_exists('conditional_operator'))
{
    function conditional_operator($id=null)
    {
        $operators = array('1' => '=',
                           '2' => '!=', 
                           '3' => '>',
                           '4' => '>=',
                           '5' => '<',
                           '6' => '<=',
                           '7' => 'like');
        if($id!=null){
            return $operators[$id];
        }else{
            return $operators;
        }
    }
}

if ( ! function_exists('getprimary_key'))
{
    function getprimary_key($json)
    {
        if($json!='')
        {
            $arrdata = json_decode($json,true);
            $key = array_search(1,array_column($arrdata,'primary_key'));
            
            return $arrdata[$key]['name'];
        } else {
            return '';
        }
    }
}

if ( ! function_exists('file_ext_icon'))
{
    function file_ext_icon($filename,$actual_filename)
    {
        if($filename == '') {
            return '';
        }
        $file_ext = '';
        $ext = explode('.',$filename);
        $ext = $ext[count($ext)-1];

        # excels
        if(in_array($ext,array('xla','xlam','xls','xlsb','xlsm','xlsx','xml'))) {
            $file_ext = 'fa fa-file-excel-o font-green';
        #csv
        } else if($ext == 'csv') {
            $file_ext = 'fa fa-file-o';
        # json
        } else if($ext == 'json') {
            $file_ext = 'fa fa-file-text-o font-blue';
        # sqlite
        } else if(in_array($ext,array('sqlite','db','db3','sqlite3'))) {
            $file_ext = 'fa fa-file font-red';
        } else {
            $file_ext = 'fa fa-file-text';
        }

        return '<i class="'.$file_ext.'"></i> <font color="gray">'.$actual_filename.'</font>';
    }
}

if ( ! function_exists('print_rd'))
{
    function print_rd($data)
    {
        echo '<pre>';
        print_r($data);
    }
}