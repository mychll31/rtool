<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('log_action'))
{
    function log_action($strSQL,$strDescription,$strjson='',$status)
    {
		$CI =& get_instance();
		$arrLogData = array(
			'log_usr_id' => $CI->session->userdata('userid'),
			'log_sql' => $strSQL,
			'log_desc' => $strDescription,
			'log_json' => $strjson,
			'log_status' => $status,
			'log_date' => date('Y-m-d H:i:s')
			);
		$CI->db->insert('tbllogs', $arrLogData);
		return $CI->db->insert_id();	
	}
}

if ( ! function_exists('getProfileName'))
{
    function getProfileName($userid)
    {
		$CI =& get_instance();
        $CI->load->database();
    	if($userid!=null):	
	    	$result = $CI->db->get_where('tblprofiles', array('userid' => $userid))->result_array();
	    	$result = $result[0];
	    	return ucfirst($result['surname']).', '.ucfirst($result['firstname']).' '.ucfirst($result['middlename'][0]).'.';
	    else:
	    	return '';
	    endif;
	}
}