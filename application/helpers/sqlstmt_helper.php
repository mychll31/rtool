<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('selectallfrom'))
{
    function selectfrom($dbname,$tblname,$fields,$str=0)
    {
    	if($str):
            $statement = '<span class="font-purple">SELECT</span> '.$fields.' 
                        <span class="font-purple">FROM</span> <span class="font-blue">`'.$dbname.'`.`'.$tblname.'`</span>';
        else:
            $statement = 'SELECT '.$fields.' FROM `'.$dbname.'`.`'.$tblname.'`';
        endif;

        return $statement;
	}
}

if ( ! function_exists('jointbl'))
{
    function jointbl($jname,$db1,$tbl1,$fld1,$db2,$tbl2,$fld2,$str=0)
    {
        if($str):
            $statement = '<span class="font-purple">'.strtoupper($jname).' </span>
                        <span class="font-blue">`'.$db2.'`.`'.$tbl2.'`</span>
                            <span class="font-purple"> ON </span>
                            <span class="font-blue">`'.$db1.'`.`'.$tbl1.'`.`'.$fld1.'`</span>
                            =
                            <span class="font-blue">`'.$db2.'`.`'.$tbl2.'`.`'.$fld2.'`</span>';
        else:
            $statement = ' '.strtoupper($jname).' `'.$db2.'`.`'.$tbl2.'` ON `'.$db1.'`.`'.$tbl1.'`.`'.$fld1.'` = `'.$db2.'`.`'.$tbl2.'`.`'.$fld2.'`';
        endif;

        return $statement;
    }
}

if ( ! function_exists('mysqlfunction'))
{
    function mysqlfunction()
    {
        $list = array(
                    array('func' => 'Group By',
                            'desc' => 'The MySQL GROUP BY statement is used along with the SQL aggregate functions like SUM to provide means of grouping the result dataset by certain database table column(s).'),
                    array('func' => 'IN',
                            'desc' => 'This is a clause, which can be used along with any MySQL query to specify a condition.'),
                    array('func' => 'BETWEEN',
                            'desc' => 'This is a clause, which can be used along with any MySQL query to specify a condition.'),
                    array('func' => 'UNION',
                            'desc' => 'Use a UNION operation to combine multiple result sets into one.'),
                    array('func' => 'COUNT',
                            'desc' => 'The MySQL COUNT aggregate function is used to count the number of rows in a database table.'),
                    array('func' => 'MAX',
                            'desc' => 'The MySQL MAX aggregate function allows us to select the highest (maximum) value for a certain column.'),
                    array('func' => 'MIN',
                            'desc' => 'The MySQL MIN aggregate function allows us to select the lowest (minimum) value for a certain column.'),
                    array('func' => 'AVG',
                            'desc' => 'The MySQL AVG aggregate function selects the average value for certain table column.'),
                    array('func' => 'SUM',
                            'desc' => 'The MySQL SUM aggregate function allows selecting the total for a numeric column.'),
                    array('func' => 'SQRT',
                            'desc' => 'This is used to generate a square root of a given number.'),
                    array('func' => 'RAND',
                            'desc' => 'This is used to generate a random number using MySQL command.'),
                    array('func' => 'CONCAT',
                            'desc' => 'This is used to concatenate any string inside any MySQL command.'),
                    array('func' => 'DATE and Time',
                            'desc' => 'Complete list of MySQL Date and Time related functions.'),
                    array('func' => 'Numeric',
                            'desc' => 'Complete list of MySQL functions required to manipulate numbers in MySQL.'),
                    array('func' => 'String',
                            'desc' => 'Complete list of MySQL functions required to manipulate strings in MySQL.'),
                    array('func' => 'WHERE',
                            'desc' => 'WHERE Clause allows you to specify a search condition for the rows returned by a query.')
        );
        
        return $list;
        
    }
}

if ( ! function_exists('format_query'))
{
    function format_query($query,$tablelist)
    {
        $stmt = strtolower($query);
        $stmt = str_replace('`','',$stmt);
        
        $arr_func = array('select','from');
        foreach($arr_func as $func) {
            $stmt = str_replace($func, '<span class="font-purple">'.strtoupper($func).'</span>', $stmt);
        }

        $CI =& get_instance();
        $arrjoins = array_column($CI->db->get('tbljoins')->result_array(), 'join_name');
        foreach($arrjoins as $fjoin) {
            $stmt = str_replace($fjoin, '<br><span class="font-purple">'.strtoupper($fjoin).'</span>', $stmt);
        }

        # format dbname
        foreach(array_unique(array_column($tablelist,'dbname')) as $dbname) {
            $stmt = str_replace($dbname, '<span class="font-blue">`'.$dbname.'`</span>', $stmt);
        }
        # format table_name
        foreach(array_unique(array_column($tablelist,'table_name')) as $table_name) {
            $stmt = str_replace($table_name, '<span class="font-blue">`'.$table_name.'`</span>', $stmt);
        }

        $stmt = str_replace(' on ',' ON ',$stmt);
        return $stmt;
        
    }
}