<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('set_notification'))
{
    function set_notification($typeid,$recipient_id,$link,$message)
    {
		$CI =& get_instance();
		$notifdata = array(
			'sender_id' 	=> $CI->session->userdata('userid'),
			'notif_type' 	=> $typeid,
			'recipient_id' 	=> $recipient_id,
			'notif_link' 	=> $link,
			'notif_message'	=> $message,
			'datetime' 		=> date('Y-m-d H:i:s')
			);
		$CI->db->insert('tblnotifications', $notifdata);
		return $CI->db->insert_id();	
	}
}

if ( ! function_exists('notification_message'))
{
    function notification_message($typeid)
    {
		switch ($typeid) {
			case 1:
				return 'shared project with you.';
			case 2:
				return 'remove you form the project.';
			case 3:
				return 'change your access to the project.';
			case 4:
				return 'Connection name already exists, but in inactive status. Please set the connection to active status or create new one.';
			case 5:
				return 'Connection name already exists. Please select the source from existing connection or create a new one.';
			case 6:
				return 'Report name is already exists.';
			default:
				return '';
		}
	}
}

if ( ! function_exists('notifications'))
{
    function notifications($unread=2)
    {
		$CI =& get_instance();
		$CI->db->order_by('datetime','desc');
		if($unread<2) {
			$CI->db->where('tblnotifications.is_unread',$unread);
		}
		$CI->db->where('tblnotifications.recipient_id',$CI->session->userdata('userid'));
		$CI->db->join('tblnotification_types','tblnotification_types.type_id = tblnotifications.notif_type');
		$arrnotif = $CI->db->get('tblnotifications')->result_array();
		
		foreach ($arrnotif as $key=>$notif) {
			$timepassed = strtotime(date('Y-m-d H:i:s')) - strtotime($notif['datetime']);
			$days = floor($timepassed / (60*60*24));
			$hrs = round(($timepassed-$days*60*60*24) / (60*60));
			$mins = round(($timepassed-$days*60*60*24) / (60));

			if($days < 1) {
				if($hrs < 1) {
					if($mins < 10) {
						$arrnotif[$key]['time_passed'] = 'Just now';
					} else {
						$arrnotif[$key]['time_passed'] = $mins.' mins';
					}
				} else {
					$arrnotif[$key]['time_passed'] = $hrs.' hrs';
				}
			} else {
				$strdays = $days < 2 ? ' day ' : ' days ';
				if($hrs < 2) {
					$arrnotif[$key]['time_passed'] = $days.$strdays;
				}else{
					$arrnotif[$key]['time_passed'] = $days.$strdays.$hrs.' hrs';
				}
			}
		}
		
		return $arrnotif;
	}
}