<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('load_plugin'))
{
    function load_plugin($type, $include)
    {
        $str_include = '';
        if($type=='css'):
            foreach($include as $inc):
                switch ($inc) {
                    case 'datatables':
                        $str_include.=
                                '<link href="'.base_url('assets/global/plugins/datatables/datatables.min.css').'" rel="stylesheet" type="text/css" />'.
                                '<link href="'.base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'" rel="stylesheet" type="text/css" />';
                        break;
                    case 'dropzone':
                        $str_include.=
                                '<link href="'.base_url('assets/global/plugins/dropzone/dropzone.min.css').'" rel="stylesheet" type="text/css" />'.
                                '<link href="'.base_url('assets/global/plugins/dropzone/basic.min.css').'" rel="stylesheet" type="text/css" />';
                        break;
                    case 'error':
                        $str_include.=
                                '<link href="'.base_url('assets/pages/css/error.min.css').'" rel="stylesheet" type="text/css" />';
                        break;
                    case 'select':
                        $str_include.=
                                '<link href="'.base_url('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css').'" rel="stylesheet" type="text/css" />';
                        break;
                    case 'select2':
                        $str_include.=
                                '<link href="'.base_url('assets/global/plugins/select2/css/select2.min.css').'" rel="stylesheet" type="text/css" />'.
                                '<link href="'.base_url('assets/global/plugins/select2/css/select2-bootstrap.min.css').'" rel="stylesheet" type="text/css" />';
                        break;
                    case 'datetime-picker':
                        $str_include.=
                                '<link href="'.base_url('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'" rel="stylesheet" type="text/css" />'.
                                '<link href="'.base_url('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css').'" rel="stylesheet" type="text/css" />'.
                                '<link href="'.base_url('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css').'" rel="stylesheet" type="text/css" />'.
                                '<link href="'.base_url('assets/global/plugins/clockface/css/clockface.css').'" rel="stylesheet" type="text/css" />';
                        break;
                    case 'jquery-fileupload':
                        $str_include.=
                                '<link href="'.base_url('assets/global/plugins/fancybox/source/jquery.fancybox.css').'" rel="stylesheet" type="text/css" />'.
                                '<link href="'.base_url('assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css').'" rel="stylesheet" type="text/css" />'.
                                '<link href="'.base_url('assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css').'" rel="stylesheet" type="text/css" />'.
                                '<link href="'.base_url('assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css').'" rel="stylesheet" type="text/css" />';
                        break;
                    case 'profile':
                        $str_include.=
                                '<link href="'.base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css').'" rel="stylesheet" type="text/css" />'.
                                '<link href="'.base_url('assets/pages/css/profile.min.css').'" rel="stylesheet" type="text/css" />';
                        break;

                    default:
                        break;
                }
            endforeach;
            echo $str_include;
        endif;


        # BEGIN JS PLUGINS
        $page_level_plugins = '';
        $page_level_plugins2 = '';
        if($type=='js'):
            # BEGIN CORE PLUGINS
            $core_plugins =
                        '<script src="'.base_url('assets/global/plugins/jquery.min.js').'" type="text/javascript"></script>'.
                        '<script src="'.base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js').'" type="text/javascript"></script>'.
                        '<script src="'.base_url('assets/global/plugins/js.cookie.min.js').'" type="text/javascript"></script>'.
                        '<script src="'.base_url('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js').'" type="text/javascript"></script>'.
                        '<script src="'.base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js').'" type="text/javascript"></script>'.
                        '<script src="'.base_url('assets/global/plugins/jquery.blockui.min.js').'" type="text/javascript"></script>'.
                        '<script src="'.base_url('assets/global/plugins/uniform/jquery.uniform.min.js').'" type="text/javascript"></script>'.
                        '<script src="'.base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js').'" type="text/javascript"></script>'.
                        '<script src="'.base_url('assets/global/plugins/bootstrap-toastr/toastr.min.js').'" type="text/javascript"></script>'.
                        '<script src="'.base_url('assets/global/plugins/bootstrap-toastr/toastr.min.js').'" type="text/javascript"></script>';
            # END CORE PLUGINS

            # BEGIN PAGE LEVEL PLUGINS
            foreach($include as $inc):
                switch ($inc) {
                    case 'datatables':
                        $page_level_plugins.= 
                                    '<script src="'.base_url('assets/global/scripts/datatable.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/datatables/datatables.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js').'" type="text/javascript"></script>';
                        break;
                    case 'dropzone':
                        $page_level_plugins.= 
                                    '<script src="'.base_url('assets/global/plugins/dropzone/dropzone.min.js').'" type="text/javascript"></script>';
                        break;
                    case 'charts':
                        $page_level_plugins.= 
                                    '<script src="'.base_url('assets/global/plugins/moment.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/morris/morris.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/morris/raphael-min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/counterup/jquery.waypoints.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/counterup/jquery.counterup.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/amcharts/amcharts/amcharts.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/amcharts/amcharts/serial.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/amcharts/amcharts/pie.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/amcharts/amcharts/radar.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/amcharts/amcharts/themes/light.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/amcharts/amcharts/themes/patterns.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/amcharts/amcharts/themes/chalk.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/amcharts/ammap/ammap.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/amcharts/amstockcharts/amstock.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/fullcalendar/fullcalendar.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/flot/jquery.flot.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/flot/jquery.flot.resize.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/flot/jquery.flot.categories.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jquery.sparkline.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js').'" type="text/javascript"></script>';
                        break;
                    case 'select':
                        $page_level_plugins.= 
                                    '<script src="'.base_url('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js').'" type="text/javascript"></script>';
                        break;
                    case 'select2':
                        $page_level_plugins.= 
                                    '<script src="'.base_url('assets/global/plugins/select2/js/select2.full.min.js').'" type="text/javascript"></script>';
                        break;
                    case 'modals':
                        $page_level_plugins.= 
                                    '<script src="'.base_url('assets/global/plugins/jquery-ui/jquery-ui.min.js').'" type="text/javascript"></script>';
                        break;
                    case 'datetime-picker':
                        $page_level_plugins.= 
                                    '<script src="'.base_url('assets/global/plugins/moment.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/clockface/js/clockface.js').'" type="text/javascript"></script>';
                        break;
                    case 'jquery-fileupload':
                        $page_level_plugins.= 
                                    // '<script src="'.base_url('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js').'" type="text/javascript"></script>'.
                                    // '<script src="'.base_url('assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js').'" type="text/javascript"></script>'.
                                    // '<script src="'.base_url('assets/global/plugins/jquery-file-upload/js/vendor/load-image.min.js').'" type="text/javascript"></script>'.
                                    // '<script src="'.base_url('assets/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js').'" type="text/javascript"></script>'.
                                    // '<script src="'.base_url('assets/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js').'" type="text/javascript"></script>'.
                                    // '<script src="'.base_url('assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js').'" type="text/javascript"></script>'.
                                    // '<script src="'.base_url('assets/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js').'" type="text/javascript"></script>'.
                                    // '<script src="'.base_url('assets/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js').'" type="text/javascript"></script>'.
                                    // '<script src="'.base_url('assets/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js').'" type="text/javascript"></script>'.
                                    // '<script src="'.base_url('assets/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js').'" type="text/javascript"></script>';
                        break;

                    default:
                        break;
                }
            endforeach;
            # END PAGE LEVEL PLUGINS

            # BEGIN THEME GLOBAL SCRIPTS
            $global_scripts = '<script src="'.base_url('assets/global/scripts/app.min.js').'" type="text/javascript"></script>'.
                              '<script src="'.base_url('assets/pages/scripts/ui-toastr.js').'" type="text/javascript"></script>';
            # END THEME GLOBAL SCRIPTS

            # BEGIN PAGE LEVEL SCRIPTS 2
            foreach($include as $inc):
                switch ($inc) {
                    case 'datatables':
                        $page_level_plugins2.= 
                                    '<script src="'.base_url('assets/pages/scripts/table-datatables-managed.min.js').'" type="text/javascript"></script>';
                        break;
                    case 'dropzone':
                        $page_level_plugins2.= 
                                    '<script src="'.base_url('assets/pages/scripts/form-dropzone.min.js').'" type="text/javascript"></script>';
                        break;
                    case 'charts':
                        $page_level_plugins2.= 
                                    '<script src="'.base_url('assets/pages/scripts/dashboard.min.js').'" type="text/javascript"></script>';
                        break;
                    case 'modals':
                        $page_level_plugins2.= 
                                    '<script src="'.base_url('assets/pages/scripts/ui-modals.min.js').'" type="text/javascript"></script>';
                        break;
                    case 'select':
                        $page_level_plugins2.= 
                                    '<script src="'.base_url('assets/pages/scripts/components-bootstrap-select.min.js').'" type="text/javascript"></script>';
                        break;
                    case 'select2':
                        $page_level_plugins2.= 
                                    '<script src="'.base_url('assets/pages/scripts/components-select2.min.js').'" type="text/javascript"></script>';
                        break;
                    case 'datetime-picker':
                        $page_level_plugins2.= 
                                    '<script src="'.base_url('assets/pages/scripts/components-date-time-pickers.min.js').'" type="text/javascript"></script>';
                        break;
                    case 'jquery-fileupload':
                        $page_level_plugins2.= 
                                    '<script src="'.base_url('assets/pages/scripts/form-fileupload.min.js').'" type="text/javascript"></script>';
                        break;
                    case 'jquery-validation':
                        $page_level_plugins2.= 
                                    '<script src="'.base_url('assets/js/validation.js').'" type="text/javascript"></script>';
                        break;
                    case 'profile':
                        $page_level_plugins2.= 
                                    '<script src="'.base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/layouts/layout/scripts/layout.min.js').'" type="text/javascript"></script>'.
                                    '<script src="'.base_url('assets/layouts/layout/scripts/demo.min.js').'" type="text/javascript"></script>';
                        break;
                    
                    default:
                        break;
                }
            endforeach;
            # END PAGE LEVEL PLUGINS 2

            echo $core_plugins.$page_level_plugins.$global_scripts.$page_level_plugins2;
        endif;
        
        
    }

    
}