<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('c_encrypt'))
{
    function c_encrypt($data)
    {
    	$key = "rt007-0031-tim";
    	$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    	$iv = openssl_random_pseudo_bytes($ivlen);
    	$ciphertext_raw = openssl_encrypt($data, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    	$hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
    	$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
    	return $ciphertext;
    	
	}
}

if ( ! function_exists('c_decrypt'))
{
    function c_decrypt($data)
    {
    	$key = "rt007-0031-tim";
    	$c = base64_decode($data);
    	$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    	$iv = substr($c, 0, $ivlen);
    	$hmac = substr($c, $ivlen, $sha2len=32);
    	$ciphertext_raw = substr($c, $ivlen+$sha2len);
    	$original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    	$calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
    	if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
    	{
    	    return $original_plaintext;
    	}
    	
	}
}

if ( ! function_exists('checkSession'))
{
    function checkSession()
    {
        $session_exist = isset($_SESSION) ? isset($_SESSION['BoolLoggedIn']) ? $_SESSION['BoolLoggedIn']==true ? true : false : false : false;
        if(!$session_exist):
            redirect('');
        endif;
    }
}

if ( ! function_exists('template_footer'))
{
    function template_footer()
    {
        return '</div>
                <!-- BEGIN FOOTER -->
                <p class="copyright">2015 © Metronic by keenthemes.
                    <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
                </p>
                <a href="#index" class="go2top">
                    <i class="icon-arrow-up"></i>
                </a>
                <!-- END FOOTER -->
            </div>
        </div>
        <!-- END CONTAINER -->';
    }
}

if ( ! function_exists('filtercond'))
{
    function filtercond()
    {
        return '<select id="selcond">
                    <option value="and">AND</option>
                    <option value="or">OR</option>
                </select>';
    }
}

if ( ! function_exists('filterElement'))
{
    function filterElement($fieldType)
    {
        return '<select id="selfilter">
                    <option value="equal"> = </option>
                    <option value="higher"> > </option>
                    <option value="higher_equal"> >= </option>
                    <option value="less"> < </option>
                    <option value="less_equal"> <= </option>
                    <option value="not_equal"> != </option>
                    <option value="like"> LIKE </option>
                    <option value="w_like"> LIKE %...</option>
                    <option value="like_w"> LIKE ...%</option>
                    <option value="w_like_w"> LIKE %...%</option>
                    <option value="not_like"> NOT LIKE </option>
                    <option value="in_arr"> IN (...) </option>
                    <option value="not_in_arr"> NOT IN (...) </option>
                    <option value="between"> BETWEEN </option>
                    <option value="not_between"> NOT BETWEEN </option>
                    <option value="is_null"> IS NULL </option>
                    <option value="is_not_null"> IS NOT NULL </option>
                </select>';
    }
}