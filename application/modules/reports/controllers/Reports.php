<?php 
class Reports extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->data = array();
        checkSession();
        $this->load->model(array('projects/Project_model','Report_model'));
    }

    public function report_all()
    {
        $this->data['report_status'] = 'All Reports';
        $this->data['reports'] = $this->Report_model->allreports($_SESSION['userid']);
        $this->template->load('template/main-template', 'report/view_all', $this->data);
    }

    public function view_report()
    {
        $this->template->load('template/main-template', 'report/view_report', $this->data);
    }

    public function report_public()
    {
        $this->data['report_status'] = 'Public Reports';
        $this->data['reports'] = $this->Report_model->reports_public();
        $this->template->load('template/main-template', 'report/view_all', $this->data);
    }

    public function report_shared()
    {
        $this->data['report_status'] = 'Confidential Reports';
        $this->data['reports'] = $this->Report_model->reports_shared($_SESSION['userid']);
        $this->template->load('template/main-template', 'report/view_all', $this->data);
    }

    public function download_report()
    {
        // reports/download_csv/1.csv/reportname
        $filetype = $this->uri->segment(3);
        $filename = $this->uri->segment(4);
        $reportname = $this->uri->segment(5);
        
        if(in_array($filetype,array('csv','json'))) {
            $generated_file = FCPATH.'/data/'.$filetype.'/'.$filename;

            if(!file_exists($generated_file)) {
                die("File doesn't seem to exist.");
            } else {
                header("Content-Description: File Transfer");
                header("Content-Disposition: attachment; filename = ".$reportname.".".$filetype);
                header("Content-Type: application/".$filetype);
                header("Content-Transfer-Encoding: binary");
                readfile($generated_file);
            }
        }

    }

    
}