<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->projects = 'tblprojects';
		$this->shared = 'tblprojects_shared';
		$this->user_group = 'tbluser_group';
		$this->profiles = 'tblprofiles';
	}

	function allreports($userid)
	{
		$this->db->select($this->projects.'.report_id,'.$this->projects.'.report_name,'.$this->projects.'.added_by,'.$this->shared.'.shared_to,'.$this->shared.'.shared_to_groupid,'.$this->profiles.'.firstname,'.$this->profiles.'.surname,'.$this->profiles.'.middlename');
		$this->db->join($this->shared,$this->shared.'.report_id = '.$this->projects.'.report_id','left');
		$this->db->join($this->profiles,$this->profiles.'.userid = '.$this->projects.'.added_by','left');
		$this->db->where($this->projects.'.added_by',$userid);
		$this->db->or_where($this->shared.'.shared_to',$userid);
		$this->db->or_where($this->shared.'.shared_to_groupid IN (SELECT '.$this->user_group.'.group_id FROM '.$this->user_group.' WHERE '.$this->user_group.'.user_id = '.$userid.')');
		$this->db->or_where($this->projects.'.is_public',1);
		$this->db->where($this->projects.'.is_active',1);
		$this->db->where($this->projects.'.is_published',1);
		$this->db->where($this->projects.'.is_delete',0);
		$this->db->group_by($this->projects.'.report_id');

		return $this->db->get($this->projects)->result_array();
	}

	function reports_public()
	{
		$this->db->join($this->profiles,$this->profiles.'.userid = '.$this->projects.'.added_by','left');
		return $this->db->get_where($this->projects,array(
									$this->projects.'.is_public' => 1,
									$this->projects.'.is_active' => 1,
									$this->projects.'.is_published' => 1,
									$this->projects.'.is_delete' => 0))->result_array();
	}

	function reports_shared($userid)
	{
		$this->db->select($this->projects.'.report_id,'.$this->projects.'.report_name,'.$this->projects.'.added_by,'.$this->shared.'.shared_to,'.$this->shared.'.shared_to_groupid,'.$this->profiles.'.firstname,'.$this->profiles.'.surname,'.$this->profiles.'.middlename');
		$this->db->join($this->shared,$this->shared.'.report_id = '.$this->projects.'.report_id','left');
		$this->db->join($this->profiles,$this->profiles.'.userid = '.$this->projects.'.added_by','left');
		$this->db->where($this->shared.'.shared_to',$userid);
		$this->db->or_where($this->shared.'.shared_to_groupid IN (SELECT '.$this->user_group.'.group_id FROM '.$this->user_group.' WHERE '.$this->user_group.'.user_id = '.$userid.')');
		$this->db->where($this->projects.'.is_active',1);
		$this->db->where($this->projects.'.is_published',1);
		$this->db->where($this->projects.'.is_delete',0);
		$this->db->group_by($this->projects.'.report_id');

		return $this->db->get($this->projects)->result_array();
	}

	function report_details($reportid)
	{
		$res = $this->db->get_where($this->projects,array('report_id' => $reportid))->result_array();
		return count($res) > 0 ? $res[0] : array();
	}


}