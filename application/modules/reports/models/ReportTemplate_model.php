<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ReportTemplate_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'templates';
		$this->table2 = 'tmpl_tables';
	}

	function add($arrData)
	{
		$this->db->insert($this->table, $arrData);
		$id = $this->db->insert_id();
		log_action($this->db->last_query(), 'Add template','',1);
		return $id;		
	}

	function edit($arrData, $id)
	{
		$json = json_encode($this->db->get_where($this->table, array('id' => $id))->result_array());
		$this->db->where('id',$id);
		$this->db->update($this->table, $arrData);
		log_action($this->db->last_query(), 'Update template', $json, 1);
		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$json = json_encode($this->db->get_where($this->table, array('id' => $id))->result_array());
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		log_action($this->db->last_query(), 'Delete template', $json, 2);
		return $this->db->affected_rows();	
	}

	function getData($id=0)
	{
		if($id==0):
			return $this->db->get_where($this->table, array('isactive' => 1))->result_array();
		else:
			$result = $this->db->get_where($this->table, array('id' => $id, 'isactive' => 1))->result_array();
			return $result[0];
		endif;
	}

	function isExists($code, $id=0)
	{
		$result = $this->db->get_where($this->table, array('name' => $code, 'id !=' => $id))->result_array();
		if(count($result) > 0):
			return true;
		else:
			return false;
		endif;
	}

	function check_tmpltables_exists($tmplid, $dbname, $tblname)
	{
		$result = $this->db->get_where($this->table2, array('tmpl_id' => $tmplid, 'dbname' => $dbname, 'tablename' => $tblname))->result_array();
		if(count($result) > 0):
			return true;
		else:
			return false;
		endif;
	}

	function add_tmpltables_exists($arrData)
	{
		$this->db->insert($this->table2, $arrData);
		$id = $this->db->insert_id();
		log_action($this->db->last_query(), 'Add template table', '' ,1);
		return $id;		
	}

	function getConditionsByField($arrfield)
	{
		return $this->db->order_by('created_date desc')->get_where('tmpl_conditions', $arrfield)->result_array();
	}

	function add_condition($arrData)
	{
		$this->db->insert('tmpl_conditions', $arrData);
		$id = $this->db->insert_id();
		log_action($this->db->last_query(), 'Add condition', '' ,1);
		return $id;		
	}

}