<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ReportTemplateTables_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'tmpl_tables';
	}

	function add($arrData)
	{
		$this->db->insert($this->table, $arrData);
		$id = $this->db->insert_id();
		log_action($this->db->last_query(), 'Add template table','',1);
		return $id;		
	}

	function check_exists($tmplid, $dbname, $tblname)
	{
		$result = $this->db->get_where($this->table, array('tmpl_id' => $tmplid, 'dbname' => $dbname, 'tablename' => $tblname))->result_array();
		if(count($result) > 0):
			return true;
		else:
			return false;
		endif;
	}

	function getDataByField($arrfield)
	{
		return $this->db->order_by('created_date desc')->get_where($this->table, $arrfield)->result_array();
	}

	function deleteByField($arrfield)
	{
		$delData = $this->db->get_where($this->table, $arrfield)->result_array();
		$json = json_encode($delData);
		$this->db->where($arrfield);
		$this->db->delete($this->table);
		log_action($this->db->last_query(), 'Delete template table details', $json,2);
		return count($delData);	
	}

 //    function setPrimaryTable($arrData, $id, $tmplid)
	// {
	// 	$json = json_encode($this->db->get_where($this->table, array('tmpl_id' => $tmplid))->result_array());
	// 	$this->db->where('tmpl_id',$tmplid);
	// 	$this->db->update($this->table, array('is_select' => 0));

	// 	$json = json_encode($this->db->get_where($this->table, array('id' => $id))->result_array());
	// 	$this->db->where('id',$id);
	// 	$this->db->update($this->table, $arrData);
		
	// 	log_action($this->db->last_query(), 'Update database', $json,1);
	// 	return $this->db->affected_rows();
	// }

}