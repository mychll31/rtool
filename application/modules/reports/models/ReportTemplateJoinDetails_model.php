<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ReportTemplateJoinDetails_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'tmpl_join_details';
	}

	function add($arrData)
	{
		$this->db->insert($this->table, $arrData);
		$id = $this->db->insert_id();
		log_action($this->db->last_query(), 'Add template table join details','',1);
		return $id;		
	}

	function getDataByField($arrfield)
	{
		return $this->db->order_by('join_order asc')->get_where($this->table, $arrfield)->result_array();
	}

	function deleteByField($arrfield)
	{
		$delData = $this->db->get_where($this->table, $arrfield)->result_array();
		$json = json_encode($delData);
		$this->db->where($arrfield);
		$this->db->delete($this->table);
		log_action($this->db->last_query(), 'Delete template table join details', $json,2);
		return count($delData);	
	}

	function updateJoin_order($id, $tmplid, $order_join, $new_order, $stat)
	{
		$json = json_encode($this->db->get_where($this->table, array('tmpl_id' => $tmplid))->result_array());
		if($stat):
			$this->db->where('id',$id);
		else:
			$this->db->where('id !=',$id);
			$this->db->where('join_order',$order_join);
		endif;

		$this->db->where('tmpl_id',$tmplid);
		$this->db->update($this->table, array('join_order' => $new_order));
		
		log_action($this->db->last_query(), 'Update database', $json,1);
		return $this->db->affected_rows();
	}
	
	function getjoinTables($tmplid)
	{
		$jointables = $this->getDataByField(array('tmpl_id' => $tmplid));
		$arrtables = array();
		foreach($jointables as $table):
			$arrtables[] = array('table_name' => $table['tbl1'], 'db_name' => $table['db1'], 'table_id' => $table['tmpl_table1_id']);
			$arrtables[] = array('table_name' => $table['tbl2'], 'db_name' => $table['db2'], 'table_id' => $table['tmpl_table2_id']);
		endforeach;
		return array_map('unserialize',array_unique(array_map('serialize', $arrtables)));
	}


}