<?php 
$route['reports/template'] = 'reports/reportTemplate/index';
$route['reports/template/add'] = 'reports/reportTemplate/add';
$route['reports/template/edit/(:any)'] = 'reports/reportTemplate/edit/$1';
$route['reports/template/view/(:any)'] = 'reports/reportTemplate/view/$1';
$route['reports/template/select-tables/(:any)'] = 'reports/reportTemplate/report_setup/$1';

// open reports
$route['open_reports/(:num)'] = 'reports/OpenReport/index/$1';

// open dashboards
$route['dashboards/(:num)'] = 'reports/OpenDashboard/index/$1';