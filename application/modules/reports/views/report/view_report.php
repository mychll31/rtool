<?=load_plugin('css',array('datatables','charts'))?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1>View Report</h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?=base_url('reports/report_all')?>">Home</a>
        </li>
        <li>
            <a href="<?=base_url('reports/report_all')?>">All</a>
        </li>
        <li class="active">View Report</li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-red-sunglo hide"></i>
                    <span class="caption-subject font-red-sunglo bold uppercase">Revenue</span>
                    <span class="caption-helper">monthly stats...</span>
                </div>
                <div class="actions">
                	<div class="btn-group" style="margin-right: 10px;">
                        <a href="" class="btn blue btn-outline btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-close-others="true"> Filter Range
                            <span class="fa fa-angle-down"> </span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;"> Q1 2014
                                    <span class="label label-sm label-default"> past </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;"> Q2 2014
                                    <span class="label label-sm label-default"> past </span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="javascript:;"> Q3 2014
                                    <span class="label label-sm label-success"> current </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;"> Q4 2014
                                    <span class="label label-sm label-warning"> upcoming </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <a href="" class="btn green btn-outline btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-close-others="true"> Chart Type
                            <span class="fa fa-angle-down"> </span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;"> Bar chart </a>
                            </li>
                            <li>
                                <a href="javascript:;"> Bar Chart </a>
                            </li>
                            <li class="active">
                                <a href="javascript:;"> Line Chart </a>
                            </li>
                            <li>
                                <a href="javascript:;"> Bar Chart </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div id="site_activities_loading">
                    <img src="../assets/global/img/loading.gif" alt="loading" /> </div>
                <div id="site_activities_content" class="display-none">
                    <div id="site_activities" style="height: 228px;"> </div>
                </div>
                <div style="margin: 20px 0 10px 30px">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-sm label-success"> Revenue: </span>
                            <h3>$13,234</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-sm label-info"> Tax: </span>
                            <h3>$134,900</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-sm label-danger"> Shipment: </span>
                            <h3>$1,134</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-sm label-warning"> Orders: </span>
                            <h3>235090</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
            	<div class="caption font-dark">
            	    <i class="icon-settings font-dark"></i>
            	    <span class="caption-subject bold uppercase"> [Report Name] </span>
            	</div>
                <div class="actions">
                    <div class="btn-group">
                        <a href="" class="btn green btn-outline btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-close-others="true"> Download
                            <span class="fa fa-angle-down"> </span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;"> CSV </a>
                            </li>
                            <li>
                                <a href="javascript:;"> HTML </a>
                            </li>
                            <li>
                                <a href="javascript:;"> PDF </a>
                            </li>
                            <li>
                                <a href="javascript:;"> PDF </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
            	<div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblall-reports" style="visibility: hidden;">
                    <thead>
                        <tr>
                        	<th> No </th>
                            <th> Report Name </th>
                            <th> Status </th>
                            <th> Author </th>
                            <th> Test </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> shuxer </td>
                            <td> 120 </td>
                            <td class="center"> 12 Jan 2012 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> looper </td>
                            <td> 120 </td>
                            <td class="center"> 12.12.2011 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> userwow </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> user1wow </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> restest </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> foopl </td>
                            <td> 20 </td>
                            <td class="center"> 19.11.2010 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> weep </td>
                            <td> 20 </td>
                            <td class="center"> 19.11.2010 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> coop </td>
                            <td> 20 </td>
                            <td class="center"> 19.11.2010 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> pppol </td>
                            <td> 20 </td>
                            <td class="center"> 19.11.2010 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> test </td>
                            <td> 20 </td>
                            <td class="center"> 19.11.2010 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> userwow </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> test </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> goop </td>
                            <td> 20 </td>
                            <td class="center"> 12.11.2010 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> weep </td>
                            <td> 20 </td>
                            <td class="center"> 15.11.2011 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> toopl </td>
                            <td> 20 </td>
                            <td class="center"> 16.11.2010 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> userwow </td>
                            <td> 20 </td>
                            <td class="center"> 9.12.2012 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> tes21t </td>
                            <td> 20 </td>
                            <td class="center"> 14.12.2012 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> fop </td>
                            <td> 20 </td>
                            <td class="center"> 13.11.2010 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> kop </td>
                            <td> 20 </td>
                            <td class="center"> 17.11.2010 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> vopl </td>
                            <td> 20 </td>
                            <td class="center"> 19.11.2010 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> userwow </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> wap </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> test </td>
                            <td> 20 </td>
                            <td class="center"> 19.12.2010 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> toop </td>
                            <td> 20 </td>
                            <td class="center"> 17.12.2010 </td>
                            <td> Test </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> weep </td>
                            <td> 20 </td>
                            <td class="center"> 15.11.2011 </td>
                            <td> Test </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?=template_footer()?>
<?=load_plugin('js',array('datatables','charts'))?>

<script>
    $(document).ready(function() {
        $('#tblall-reports').dataTable( {
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblall-reports').css('visibility', 'visible');
            }} );
    });
</script>