<?=set_title('Home');load_plugin('css',array('datatables'))?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><?=$report_status?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li class="active"><?php echo str_replace('reports', '', strtolower($report_status)) ?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Report List</span>
                </div>
            </div>
            <div class="portlet-body">
            	<div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblall-reports" style="visibility: hidden;">
                    <thead>
                        <tr>
                        	<th width="10px;"> No </th>
                            <th> Report Name </th>
                            <th> Author </th>
                            <td width="10px;"></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach($reports as $report) { ?>
                        <tr>
                        	<td> <?=$no++?> </td>
                            <td> <?=$report['report_name']?> </td>
                            <td> <?=getFullname($report['firstname'], $report['surname'], $report['middlename'])?> </td>
                            <td align="center">
                                <a href="<?=base_url('projects/view_report/'.$report['report_id'])?>" class="btn blue btn-sm">
                                    <i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?=template_footer()?>
<?=load_plugin('js',array('datatables'))?>

<script>
    $(document).ready(function() {
        $('#tblall-reports').dataTable( {
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblall-reports').css('visibility', 'visible');
            } } );
    });
</script>