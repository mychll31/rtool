<h3 class="form-title font-green">Sign In</h3>
<div class="alert alert-danger" <?=isset($err_uname) ? '' : 'hidden'?>>
    <button class="close" data-close="alert"></button>
    <span> <?=isset($err_uname) ? $err_uname : 'Enter username and password'?> </span>
</div>
<?=form_open('',array('class' => 'login-form'))?>
	<div class="form-group <?=isset($err_uname) ? 'has-error' : ''?>">
    	<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    	<label class="control-label">Username</label>
    	<input class="form-control" type="text" autocomplete="off" 
    		name="username" id="username" value="<?=set_value('username')?>" required />
    </div>
	<div class="form-group <?=isset($err_uname) ? 'has-error' : ''?>">
    	<label class="control-label">Password</label>
    	<input class="form-control" type="password" autocomplete="off" 
    		name="password" id="password" required />
    </div>
    <div>
        <a href="<?=base_url('forget_password')?>">Forgot Password ?</a><br>
        <a href="<?=base_url('registration')?>" class="text-info">Create new account</a>
    </div>
	<div class="form-actions">
    	<button type="submit" class="btn green uppercase" id="btnlogin">Login</button>
    	<label class="rememberme check">
        	<input type="checkbox" name="remember" value="1" />Remember </label>
	</div>
	<div class="create-account">
		<p><span class="uppercase font-white"><?=date('Y')?> @ MIT Project</span></p>
	</div>
<?=form_close()?>

<?=load_plugin('js', array('select','datetime_picker','login-validation')) ?>