<h3 class="form-title font-green">Retype Password</h3>
<form class="login-form" action="" method="post">
    <?php if(isset($error)) : ?>
        <p class="text-danger" <?=isset($error) ? '' : 'hidden'?>>
            Your confirmation code is expired, please try again.</p>
        <div><a href="<?=base_url('forget_password')?>">Forgot Password ?</a></div>
    <?php else: ?>
    	<div class="form-group <?=isset($err_uname) ? 'has-error' : ''?>">
            <label class="control-label">Password</label>
            <input class="form-control" type="password" autocomplete="off" 
                name="txtpass" id="txtpass" />
            <span class="help-block"></span>
        </div>
        <div class="form-group <?=isset($err_uname) ? 'has-error' : ''?>">
            <label class="control-label">Retype Password</label>
            <input class="form-control" type="password" autocomplete="off" 
                name="txtretpass" id="txtretpass" />
            <span class="help-block"></span>
        </div>
    	<div class="form-actions">
        	<button type="submit" class="btn green uppercase" id="btnsubmit">Submit</button>
            &nbsp;
            <a class="btn white uppercase" href="<?=base_url('login')?>">Cancel</a>
    	</div>
    <?php endif; ?>
</form>

<script>
    $(document).ready(function () {
        $('#txtpass').on('keyup',function () {
            if(!checknull($(this))) {
                validate_password($('#txtpass'),$('#txtretpass'));
            }
        });
        $('#txtretpass').on('keyup',function () {
            if(!checknull($(this))) {
                validate_password($('#txtpass'),$('#txtretpass'));
            }
        });

        $('#btnsubmit').click(function (d) {
            if(checknull($('#txtpass')) + checknull($('#txtretpass')) > 0) {
                d.preventDefault();
            }else{
                if(validate_password($('#txtpass'),$('#txtretpass'))) {
                    d.preventDefault();
                }
            }
        });

    });
</script>