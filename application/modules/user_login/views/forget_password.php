<h3 class="form-title font-green">Forget Password</h3>
<form class="login-form" action="" method="post">
    <p class="text-success" <?=isset($success) ? '' : 'hidden'?>>
        Confirmation link has been sent to your email.</p>
	<div class="form-group <?=isset($error) ? 'has-error' : ''?>">
    	<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    	<label class="control-label">Email</label>
    	<input class="form-control" type="text" autocomplete="off" 
    		name="txtemail" id="txtemail" value="<?=set_value('txtemail')?>" />
        <span class="help-block"><?=isset($error) ? $error : ''?></span>
    </div>
	<div class="form-actions">
    	<button type="submit" class="btn green uppercase" id="btnsubmit">Submit</button>
        &nbsp;
        <a class="btn white uppercase" href="<?=base_url('login')?>">Back</a>
	</div>
</form>

<script>
    function validate(e){
        if (e.val() == '') {
            e.parent().addClass('has-error')
            errmsg = 'This field is required.'
            res = 1
        } else {
            var re = /\S+@\S+.\S+/
            if (re.test(e.val())) {
                e.parent().removeClass('has-error')
                errmsg = ''
                res = 0
            } else {
                e.parent().addClass('has-error')
                errmsg = 'Invalid input.'
                e.parent().find('.help-block').html(errmsg)
                res = 1
            }
        }
        e.parent().find('.help-block').html(errmsg)
        return res
    }
    $(document).ready(function () {
        $('#txtemail').on('keyup',function () {
            validate($(this));
        });

        $('#btnsubmit').click(function (d) {
            if (validate($('#txtemail')) == 1) {
                d.preventDefault()
            }
        });

    });
</script>