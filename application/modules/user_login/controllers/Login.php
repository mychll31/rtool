<?php 
class Login extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->data = array();
        $this->load->model(array('Login_model','users/User_model'));
    }

    public function index()
    {
        // check if session exist redirect to homepage else redirect to login
        $session_exist = isset($_SESSION) ? isset($_SESSION['BoolLoggedIn']) ? $_SESSION['BoolLoggedIn']==true ? true : false : false : false;
        if($session_exist):
            redirect('reports/report_all');
        endif;

        $arrpost = $this->input->post();
        if(!empty($arrpost)):
            $user = $this->Login_model->getUserByUsername($arrpost['username']);
            if($user != null):
                if($user['is_delete'] == 1):
                    $this->data['err_uname'] = 'Deleted user please contact administrator';
                else:
                    if($user['is_active'] == 0):
                        $this->data['err_uname'] = 'Inactive user please contact administrator';
                    else:
                        if($user['is_confirmed'] == 0):
                            $this->data['err_uname'] = 'Unconfirmed user please check your email or contact administrator';
                        else:
                            if(password_verify($arrpost['password'], $user['password'])):
                                // ACCESS GRANTED
                                $this->Login_model->add_session($user['userid'], $user['access_level'], $user['username'], $user['surname'], $user['firstname'], $user['middlename'], $user['custom_userid'], $user['image_fname']);
                                $this->User_model->edit(array('last_login' => date('Y-m-d H:i:s')), $user['userid']);
                                redirect('reports/report_all');
                            else:
                                $this->data['err_uname'] = 'Wrong Password';
                            endif;
                        endif;
                    endif;
                endif;
            else:
                $this->data['err_uname'] = 'Username not exists';
            endif;
        endif;
        $this->template->load('template/user-template', 'user_login/login', $this->data);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}