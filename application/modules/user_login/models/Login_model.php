<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->user = 'tblusers';
		$this->profile = 'tblprofiles';
	}

	function getUserByUsername($username)
	{
		$this->db->join($this->profile,$this->profile.'.userid = '.$this->user.'.userid');
		$result = $this->db->get_where($this->user, array('username' => $username))->result_array();
		return count($result) > 0 ? $result[0] : null;
	}

	public function add_session($userid, $user_level, $user_name, $lname, $fname, $mname, $user_cid, $image)
    {
        $sessData = array(
             'userid' => $userid,
             'user_level' => $user_level,
             'user_name' => $user_name,
             'lname' => $lname,
             'fname' => $fname,
             'mname' => $mname,
             'user_cid' => $user_cid,
             'image' => $image,
             'BoolLoggedIn' => TRUE
            );
        
        $this->session->set_userdata($sessData);
    }
    
}
/* End of file Login_model.php
 * Location: ./application/modules/libraries/models/Login_model.php */