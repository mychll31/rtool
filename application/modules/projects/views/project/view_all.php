<?=set_title('Projects');load_plugin('css',array('datatables'))?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><?=$project_status?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li class="active"><?php echo str_replace('projects', '', strtolower($project_status)) ?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Project List</span>
                </div>
                <div class="actions" <?=$project_status == 'All Projects' ? '' : 'hidden'?>>
                    <div class="btn-group">
                        <a href="" class="btn blue btn-outline btn-sm dropdown-toggle" data-toggle="dropdown" data-close-others="true"> <?=ucfirst($data_status)?>
                            <span class="fa fa-angle-down"> </span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li <?=strtolower($data_status) == 'all' ? 'hidden' : ''?>>
                                <a href="<?=base_url('projects/project_all')?>"> All </a>
                            </li>
                            <li <?=strtolower($data_status) == 'owned' ? 'hidden' : ''?>>
                                <a href="<?=base_url('projects/project_all_owned')?>"> Owned </a>
                            </li>
                            <li <?=strtolower($data_status) == 'shared with you' ? 'hidden' : ''?>>
                                <a href="<?=base_url('projects/project_all_shared')?>"> Shared with you </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblall-reports" style="visibility: hidden;">
                    <thead>
                        <tr>
                            <th width="10px;"> No </th>
                            <th> Report Name </th>
                            <th style="text-align: center;"> Author </th>
                            <th style="text-align: center;"> Date Created </th>
                            <td width="150px;"> </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach($arrProjects as $project):?>
                            <tr class="odd gradeX">
                                <td> <?=$no++?> </td>
                                <td> <?=$project['report_name']?> </td>
                                <td align="center"> <?=getFullname($project['firstname'], $project['surname'], $project['middlename'])?> </td>
                                <td align="center"> <?=date('M d, Y', strtotime($project['added_date']))?> </td>
                                <td align="center" nowrap>
                                    <a href="<?=base_url('projects/edit_project/').$project['report_id']?>" class="btn green btn-sm">
                                        <i class="icon-magnifier"></i> <i class="icon-pencil"></i>&nbsp; View and Edit
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?=template_footer()?>
<?=load_plugin('js',array('datatables'))?>

<script>
    $(document).ready(function() {
        $('#tblall-reports').dataTable( {
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblall-reports').css('visibility', 'visible');
            }} );
    });
</script>