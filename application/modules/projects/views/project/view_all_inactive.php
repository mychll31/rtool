<?=load_plugin('css',array('datatables'))?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><?=$project_status?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li class="active"><?php echo str_replace('projects', '', strtolower($project_status)) ?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Project List</span>
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <a href="" class="btn blue btn-outline btn-sm dropdown-toggle" data-toggle="dropdown" data-close-others="true"> <?=ucfirst($data_status)?>
                            <span class="fa fa-angle-down"> </span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li <?=strtolower($data_status) == 'all' ? 'hidden' : ''?>>
                                <a href="<?=base_url('projects/inactive_projects')?>"> All </a>
                            </li>
                            <li <?=strtolower($data_status) == 'owned' ? 'hidden' : ''?>>
                                <a href="<?=base_url('projects/inactive_projects_owned')?>"> Owned </a>
                            </li>
                            <li <?=strtolower($data_status) == 'shared with you' ? 'hidden' : ''?>>
                                <a href="<?=base_url('projects/inactive_projects_shared')?>"> Shared with you </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
            	<div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblall-reports" style="visibility: hidden;">
                    <thead>
                        <tr>
                        	<th> No </th>
                            <th> Report Name </th>
                            <th> Status </th>
                            <th> Author </th>
                            <td> </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> shuxer </td>
                            <td> 120 </td>
                            <td class="center"> 12 Jan 2012 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> looper </td>
                            <td> 120 </td>
                            <td class="center"> 12.12.2011 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> userwow </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> user1wow </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> restest </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> foopl </td>
                            <td> 20 </td>
                            <td class="center"> 19.11.2010 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> weep </td>
                            <td> 20 </td>
                            <td class="center"> 19.11.2010 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> coop </td>
                            <td> 20 </td>
                            <td class="center"> 19.11.2010 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> pppol </td>
                            <td> 20 </td>
                            <td class="center"> 19.11.2010 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> test </td>
                            <td> 20 </td>
                            <td class="center"> 19.11.2010 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> userwow </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> test </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> goop </td>
                            <td> 20 </td>
                            <td class="center"> 12.11.2010 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> weep </td>
                            <td> 20 </td>
                            <td class="center"> 15.11.2011 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> toopl </td>
                            <td> 20 </td>
                            <td class="center"> 16.11.2010 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> userwow </td>
                            <td> 20 </td>
                            <td class="center"> 9.12.2012 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> tes21t </td>
                            <td> 20 </td>
                            <td class="center"> 14.12.2012 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> fop </td>
                            <td> 20 </td>
                            <td class="center"> 13.11.2010 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> kop </td>
                            <td> 20 </td>
                            <td class="center"> 17.11.2010 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> vopl </td>
                            <td> 20 </td>
                            <td class="center"> 19.11.2010 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> userwow </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> wap </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> test </td>
                            <td> 20 </td>
                            <td class="center"> 19.12.2010 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> toop </td>
                            <td> 20 </td>
                            <td class="center"> 17.12.2010 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                        	<td> 1 </td>
                            <td> weep </td>
                            <td> 20 </td>
                            <td class="center"> 15.11.2011 </td>
                            <td>
                                <a href="<?=base_url('projects/view_project')?>" class="btn blue btn-sm">
                                	<i class="icon-magnifier"> </i> View
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?=template_footer()?>
<?=load_plugin('js',array('datatables'))?>

<script>
    $(document).ready(function() {
        $('#tblall-reports').dataTable( {
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblall-reports').css('visibility', 'visible');
            }} );
    });
</script>