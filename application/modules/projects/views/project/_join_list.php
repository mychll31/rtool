<?=set_title('Project');load_plugin('css',array('select','datatables'))?>
<input type="hidden" id="txtreport_id" value="<?=$this->uri->segment(3)?>">
<input type="hidden" value="<?=base_url()?>" id="txtbaseurl">

<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><a data-toggle="modal" href="#edit_projectname-modal" title="click to edit title">
        <i class="fa fa-pencil"></i> <span id="project-name"><?=$arrProject['report_name']?></span></a> </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li class="active"><?=$project_status?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"> Join</span>
                </div>
                <div class="actions">
                    <a href="<?=base_url('projects/select_tables/').$this->uri->segment(3)?>" class="btn sbold green" style="margin-right: 10px;">
                        <i class="icon-control-rewind"></i> BACK
                    </a>
                    <a id="btnfinish" class="btn sbold blue">
                        <i class="fa fa-floppy-o"></i> SAVE AND FINISH
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <label id="lblstatus" class="label label-default"><?=$query['added']==''?'Generated':'Custom'?></label>
                <div class="well" id="div-query-format" <?=$sql_is_show?'hidden':''?>>
                    <?=format_query($custom_query,$arrTableList);?>
                </div>
                <textarea id="txtsql" class="form-control" style="display:none;"><?=$custom_query?></textarea>
                <div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <div class="row">
                    <!-- begin custom query -->
                    <div class="col-md-12" id="custom_query-div" <?=$sql_is_show?'':'hidden'?>>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark bold uppercase">Custom Query</span>
                                </div>
                            </div>
                            <div class="portlet-body util-btn-margin-bottom-5">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="txtquery-added" hidden><?=$query['added']?></div><br>
                                        <div id="txtquery-suggested" hidden><?=$query['suggested']?></div>
                                        <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
                                        <textarea class="form-control" rows="13" id="txtcustom_query"><?=$custom_query?></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="clearfix">
                                            <br>
                                            <a class="btn default red-stripe btn-cls" data-cls="select"> SELECT </a>
                                            <a class="btn default blue-stripe btn-cls" data-cls="select_all"> SELECT * </a>
                                            <a class="btn default green-stripe btn-cls" data-cls="from"> FROM </a>
                                            <a class="btn default yellow-stripe" data-toggle="modal" href="#custom-query-modal"> JOIN </a>
                                            <a class="btn default purple-stripe" data-toggle="modal" href="#mysql_functions-modal"> FUNCTIONS </a>
                                            <a class="btn grey-cascade" data-toggle="modal" href="#clear_custom_query-modal"> Clear </a>
                                            <a class="btn yellow" id="btn-query-check"> Check </a>
                                            <a class="btn blue" id="btn-query-save"> Save </a>
                                            <a class="btn green pull-right bold" id="btn-query-generate">Generate Query </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end custom query -->
                </div>
                <div class="row">
                    <div class="col-md-12" style="margin-bottom: 20px;">
                        <a href="javascript:;" id="btncustom-query" class="btn btn-sm blue" data-value=<?=$sql_is_show?'':'hide'?>> <?=$sql_is_show?'Show':'Hide'?> Custom Query</a>
                    </div>
                </div>

                <div class="row">
                    <!-- begin tables -->
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark bold uppercase">Table List</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tbltables" style="visibility: hidden;">
                                    <thead>
                                        <tr>
                                            <th hidden> No </th>
                                            <th> Host </th>
                                            <th> Database name </th>
                                            <th> Table name </th>
                                            <th> JOIN </th>
                                            <th> To Table </th>
                                            <td style="width:100px"></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(count($arrTableList)>0): foreach($arrTableList as $k => $table): ?>
                                        <tr class="odd gradeX" data-tblid="<?=$table['table_id']?>">
                                            <td hidden><span class="tdindex"><?=$no++?></span></td>
                                            <td><?=$table['dbhost']?></td>
                                            <td><?=$table['dbname']?></td>
                                            <td nowrap><?=$table['table_name'].'.'.(count($table['join_details']) ? $table['join_details'][0]['table1_pk'] : getprimary_key($table['field_names']))?></td>
                                            <td style="text-transform: uppercase;">
                                                <?=count($table['join_details']) ? $table['join_details'][0]['join_name'] : ''?></td>
                                            <td><?=count($table['join_details']) ? $table['join_details'][0]['table_name'].'.'.$table['join_details'][0]['table2_pk'] : ''?></td>
                                            <td align="center" nowrap>
                                                <a class="btn blue btn-sm tooltips" 
                                                    data-original-title="View Fields" 
                                                    data-id="<?=$table['table_id']?>" 
                                                    id="btnviewfields">
                                                        <i class="icon-list"></i></a>
                                                <a class="btn green-meadow btn-sm tooltips <?=$k==(count($arrTableList)-1)?'disabled':''?>"
                                                    data-original-title="Join"
                                                    id="btnjoin">
                                                        <i class="icon-share"></i></a>
                                                <a class="btn default btn-sm tooltips <?=$k==0?'disabled':''?>" data-placement="left"
                                                    data-original-title="Move Up" 
                                                    id="btnmove-up">
                                                        <i class="glyphicon glyphicon-arrow-up bold"></i></a>
                                                <a class="btn default btn-sm tooltips <?=$k==(count($arrTableList)-1)?'disabled':''?>"
                                                    data-original-title="Move Down" 
                                                    id="btnmove-down">
                                                        <i class="glyphicon glyphicon-arrow-down bold"></i></a>
                                            </td>
                                        </tr>
                                        <?php endforeach; endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- joins -->
                </div>

            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?php
    $this->load->view('_join_table-modal');
    $this->load->view('sql_info');
    echo template_footer();
    echo load_plugin('js',array('select','datatables','jquery-validation'));
?>

<script src="<?=base_url('assets/js/custom/_join_list-js.js')?>"></script>