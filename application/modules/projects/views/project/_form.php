<?=set_title('Projects');load_plugin('css',array('select','jquery-fileupload'))?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><?=$project_status?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li class="active"><?php echo str_replace('project', '', strtolower($project_status)) ?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-blue sbold uppercase"><?=$action?> Project</span>
                </div>
                <?php if($action=='view'): ?>
                <div class="actions">
                    <a href="<?=base_url('datasources/edit_source')?>" class="btn green" style="margin-right: 3px;">
                        <i class="fa fa-edit"></i> Edit</a>
                    <a class="btn red" data-toggle="modal" href="#delete-modal">
                        <i class="icon-trash"></i> Delete</a>
                </div>
                <?php endif;?>
            </div>

            <div class="portlet-body form">
                <?=form_open_multipart('projects/new_project',array('class' => 'form-horizontal'))?>
                    <input type="hidden" id="txtbase_url" value="<?=base_url()?>"><br>
                    <input type="hidden" id="txtsource_id" name="txtsource_id"><br>
                    <input type="hidden" id="txtfileloc" name="txtfileloc"><br>
                    <input type="hidden" id="txtfilename" name="txtfilename"><br>
                    <input type="hidden" id="dbwfupload" name="dbwfupload"><br>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Report name <span class="required">*</span> </label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtreportname" id="txtreportname">
                                <span class="help-block"> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Select from existing connection</label>
                            <div class="col-md-7">
                                <select class="form-control bs-select" id="selexconn" name="selexconn">
                                    <?php if(count($arrconnections)>0):?>
                                        <option value="">-- SELECT CONNECTION --</option>
                                        <?php foreach($arrconnections as $conn): ?>
                                        <option value="<?=$conn['source_id']?>"><?=$conn['connname']?></option>
                                        <?php endforeach; else: ?>
                                        <option value="">-- NO EXISTING CONNECTIONS --</option>
                                    <?php endif; ?>
                                </select>
                                <span class="help-block"> </span>
                            </div>
                        </div>

                        <!-- begin new connection -->
                        <div id="new-connection">
                            <hr>
                            <h5 class="font-green uppercase">Create new connection</h5>

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Connection name <span class="required">*</span> </label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="selconnname" id="selconnname">
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Database type <span class="required">*</span> </label>
                                    <div class="col-md-7">
                                        <select class="form-control bs-select" <?=$action=='view'?'disabled':''?> id="seldbtype" name="seldbtype">
                                            <option value="">-- SELECT DB TYPE --</option>
                                            <?php foreach($arrdbTypes as $dbtype): ?>
                                                <option value="<?=$dbtype['type_id']?>"
                                                        data-ftype="<?=$dbtype['file_type']?>"
                                                        data-wfupload="<?=$dbtype['wfileupload']?>">
                                                        <?=$dbtype['name']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="form-group div-host" hidden>
                                    <label class="col-md-3 control-label">Host</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txthost" id="txthost">
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="div-form" hidden>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Database name</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtdbname" id="txtdbname">
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Database username</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtuname" id="txtuname">
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Database password</label>
                                        <div class="col-md-7">
                                            <input type="password" class="form-control" <?=$action=='view'?'disabled':''?> name="txtpass" id="txtpass">
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Database re-type password</label>
                                        <div class="col-md-7">
                                            <input type="password" class="form-control" <?=$action=='view'?'disabled':''?> name="txtretpass" id="txtretpass">
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group div-upload" hidden>
                                    <label class="col-md-3 control-label">File Upload <span class="required">*</span> </label>
                                    <div class="col-md-7">
                                        <div class="row fileupload-buttonbar">
                                            <div class="col-lg-9">
                                                <span class="btn dark fileinput-button">
                                                    <i class="fa fa-upload"></i>
                                                    <span> Select files... </span>
                                                    <input type="file" name="files" id="btnfupload"> </span>
                                                    <span class="span-fname"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end new connection -->
                    </div>

                    <div class="form-actions testconnection" hidden>
                        <div class="row">
                            <div class="col-md-offset-3 col-md-7">
                                <button type="button" class="btn red" id="btntestconn">Test Connection</button>
                                <div class="alert alert-danger col-md-12" style="margin-top: 20px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions addconnection">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green" id="btnaddconn">Add Connection</button>
                                <a href="<?=base_url('projects/new_project')?>" class="btn default btncancel">Cancel</a>
                                <div class="alert alert-danger col-md-9" style="margin-top: 20px;"></div>
                            </div>
                        </div>
                    </div>
                <?=form_close()?>

            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?=template_footer()?>
<?=load_plugin('js',array('select','jquery-validation'))?>

<script src="<?=base_url('assets/js/custom/_project-js.js')?>"></script>