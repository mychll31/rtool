<!-- begin add modal -->
<div class="modal fade bs-modal" id="addTable-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Table</h4>
            </div>
            <div class="modal-body" style="padding-top: 0px !important;">
                <div class="loading-image" id="loading-image2"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <div class="row table-list-modal">
                    <?php if(count($arrconnections)>0): foreach($arrconnections as $conn): ?>
                        <div class="col-md-6">
                            <br>
                            <!-- BEGIN TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <span class="caption-subject font-green-sharp bold"> <?=$conn['connname']?></span><br>
                                    <small style="color: #999;">
                                        <b>host</b>:<i><u><?=$conn['dbhost']?></u></i>&nbsp;
                                        <b>db</b>:<?=$conn['dbname']?>
                                    </small>
                                    <br>
                                </div>
                                <div class="portlet-body" data-conn="<?=$conn['conn_id']?>">
                                    <?php if($conn['table_info']['message'] == 'Connected'): ?>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column tbltable-list" data-sourceid="<?=$conn['source_id']?>">
                                            <thead>
                                                <tr>
                                                    <th width="10px;">
                                                        <input type="checkbox" class="group-checkable" id="chkall" value="<?=$conn['conn_id']?>"/> </th>
                                                    <th> Table name </th>
                                                </tr>
                                            </thead>
                                            <tbody class="tblist">
                                                <?php foreach($conn['table_info']['tables'] as $tblname): ?>
                                                    <tr>
                                                        <td width="10px;">
                                                            <input type="checkbox" data-source="<?=$conn['source_id']?>" value="<?=$tblname?>"/> </td>
                                                        <td> <?=$tblname?> </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    <?php else: ?>
                                        <div class="alert alert-danger"><strong>Error!</strong> <?=$conn['table_info']['message']?> </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <!-- END TABLE PORTLET-->
                        </div>
                    <?php endforeach; endif; ?>
                </div>
            </div>
            <div class="modal-footer table-list-modal">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green" id="btnaddTable">Add Table</button>
            </div>
        </div>
    </div>
</div>
<!-- end add modal -->

<!-- begin view fields for each table modal -->
<div class="modal fade" id="viewfield-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title sbold uppercase" id="vfield-tablename">tablename</h5>
                <small>
                    <b>Host:</b> <u><i id="vfield-hostname"></i></u>&nbsp;
                    <b>Database:</b> <span id="vfield-databasename"></span></small>
            </div>
            <div class="modal-body">
                <div class="scroller" style="height:300px" data-rail-visible="1" data-rail-color="red" data-handle-color="green">
                    <table class="table table-striped table-bordered" id="tblfields">
                        <thead>
                            <tr>
                                <th style="text-align:center;">name</th>
                                <th style="text-align:center;">type</th>
                                <th style="text-align:center;">max_length</th>
                                <th style="text-align:center;">primary_key</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end view fields for each table modal -->

<!-- begin edit project name modal -->
<div class="modal fade bs-modal-sm" id="edit_projectname-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Name</h4>
            </div>
            <div class="modal-body">
                <input type="text" class="form-control" id="txtprojname" value="<?=$arrProject['report_name']?>">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn green" id="btnsubmit-name">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- end edit project name modal -->

<!-- begin delete modal -->
<div class="modal fade bs-modal-sm" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <?=form_open('',array('id'=>'frmconfirm')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title title-conf"></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="txtdelmode" id="txtdelmode" value="table">
                <input type="hidden" name="txtdelid" id="txtdelid">
                <input type="hidden" id="txtaction" value="delete-connection">
                <span class="modal-conf"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green btn-confirm">Yes</button>
            </div>
        </div>
        <?=form_close()?>
    </div>
</div>
<!-- end delete modal