<!-- begin add connection modal -->
<div class="modal fade bs-modal" id="connection-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Connection</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Select from existing connection</label>
                                    <div class="col-md-7">
                                        <select class="form-control bs-select" id="selexconn" name="selexconn">
                                            <?php if(count($arrsources)>0):?>
                                                <option value="">-- SELECT CONNECTION --</option>
                                                <?php foreach($arrsources as $source): ?>
                                                    <option value="<?=$source['source_id']?>"><?=$source['connname']?></option>
                                                <?php endforeach; else: ?>
                                                <option value="">-- NO EXISTING CONNECTIONS --</option>
                                            <?php endif; ?>
                                        </select>
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                            </div>
                            <!-- begin new connection -->
                            <div class="row new-connection" style="padding: 15px 0;border-top: 1px solid #efefef;margin-top: 11px;">
                                <label class="col-md-4 control-label font-green uppercase">Create new connection</label>
                            </div>
                            <div class="row new-connection">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Connection Name</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="selconnname" id="selconnname">
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Database Type</label>
                                    <div class="col-md-7">
                                        <select class="form-control bs-select" <?=$action=='view'?'disabled':''?> id="seldbtype" name="seldbtype">
                                            <option value="">-- SELECT DB TYPE --</option>
                                            <?php foreach($arrdbTypes as $dbtype): ?>
                                                    <option value="<?=$dbtype['type_id']?>"
                                                        data-ftype="<?=$dbtype['file_type']?>"
                                                        data-wfupload="<?=$dbtype['wfileupload']?>">
                                                        <?=$dbtype['name']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="form-group div-host" hidden>
                                    <label class="col-md-4 control-label">Host</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txthost" id="txthost">
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="div-form" hidden>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Database Name</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtdbname" id="txtdbname">
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Database Username</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtuname" id="txtuname">
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Database Password</label>
                                        <div class="col-md-7">
                                            <input type="password" class="form-control" <?=$action=='view'?'disabled':''?> name="txtpass" id="txtpass">
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Database Re-type Password</label>
                                        <div class="col-md-7">
                                            <input type="password" class="form-control" <?=$action=='view'?'disabled':''?> name="txtretpass" id="txtretpass">
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group div-upload" hidden>
                                    <label class="col-md-4 control-label">File Upload <span class="required">*</span> </label>
                                    <div class="col-md-7">
                                        <div class="row fileupload-buttonbar">
                                            <div class="col-lg-9">
                                                <span class="btn dark fileinput-button">
                                                    <i class="fa fa-upload"></i>
                                                    <span> Select files... </span>
                                                    <input type="file" name="files" id="btnfupload"> </span>
                                                    <span class="span-fname"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end new connection -->
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <div class="form-group testconnection">
                    <label class="col-md-4"></label>
                    <div class="col-md-7">
                        <button class="btn red pull-left" id="btntestconn">Test Connection</button>
                        <button type="button" class="btn default pull-left btn-close" data-id="1">Close</button>
                        <div class="alert alert-danger" style="margin-top: 50px;text-align: left;padding: 10px;"></div>
                    </div>
                </div>
                <div class="form-group newdata-source" hidden>
                    <label class="col-md-4"></label>
                    <div class="col-md-7">
                        <button class="btn blue pull-left" id="btnadd-newsource">Add Data Source</button>
                        <button type="button" class="btn default pull-left btn-close" data-id="2">Cancel</button>
                        <div class="alert alert-danger" style="margin-top: 50px;text-align: left;padding: 10px;"></div>
                    </div>
                </div>
                <div class="form-group addconnection" hidden>
                    <label class="col-md-4"></label>
                    <div class="col-md-7">
                        <button class="btn green pull-left" id="btnaddconn">Add Connection</button>
                        <button type="button" class="btn default pull-left btn-close" data-id="2">Cancel</button>
                        <div class="alert alert-danger" style="margin-top: 50px;text-align: left;padding: 10px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end add connection modal -->

<!-- begin edit project name modal -->
<div class="modal fade bs-modal-sm" id="edit_projectname-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Name</h4>
            </div>
            <div class="modal-body">
                <input type="text" class="form-control" id="txtprojname" value="<?=$arrProject['report_name']?>">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn green" id="btnsubmit-name">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- end edit project name modal -->

<!-- begin delete modal -->
<div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title title-conf">Delete Connection</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="txtdelid" id="txtdelid">
                <input type="hidden" id="txtsourceid">
                <input type="hidden" id="txtaction" value="delete-connection">
                <span class="modal-conf">Are you sure you want to delete this connection?</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="button" class="btn green btn-confirm">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- end delete modal -->

<!-- begin delete modal -->
<div class="modal fade bs-modal-sm" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <?=form_open('',array('id'=>'frmconfirm')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title title-conf"></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="txtdelmode" id="txtdelmode" value="table">
                <input type="hidden" name="txtdelid" id="txtdelid">
                <input type="hidden" id="txtaction" value="delete-connection">
                <span class="modal-conf"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green btn-confirm">Yes</button>
            </div>
        </div>
        <?=form_close()?>
    </div>
</div>
<!-- end delete modal