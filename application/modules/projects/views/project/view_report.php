<?=set_title('Projects');load_plugin('css',array('datatables','charts','select2'))?>
<style type="text/css">.select2-container { z-index: 999999; }</style>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <?php if($access): ?>
        <h1><span><?=$arrProject['report_name']?></span></h1>
    <?php else: ?>
        <h1><a data-toggle="modal" href="#edit_projectname-modal" title="click to edit title">
            <i class="fa fa-pencil"></i> <span id="project-name"><?=$arrProject['report_name']?></span></a> </h1>
    <?php endif; ?>
    <ol class="breadcrumb" <?=$access ? 'hidden' : ''?>>
        <li>
            <a href="#publish-modal" data-toggle="modal" class="btn btn-sm default bold <?=$arrProject['is_published']?'':'hidden'?>" id="btnunpublish"><i class="icon-ban"></i> UNPUBLISH</a>
            <a href="#publish-modal" data-toggle="modal" class="btn btn-sm green bold <?=$arrProject['is_published']?'hidden':''?>" id="btnpublish"><i class="icon-share-alt"></i> PUBLISH</a>
            <a href="#share-with-modal" data-toggle="modal" class="btn btn-sm blue-hoki bold" id="btnshare_with">Shared with</a>
        </li>
    </ol>
    <br>
    <div align="right" id="share-with-container" <?=$access ? 'hidden' : ''?>>
        <?php 
            foreach ($shared_with as $user) {
                if($user['group_name'] == '') {
                    echo '<small class="font-blue">'.getFullname($user['firstname'], $user['surname'], $user['middlename']).', </small>';
                } else {
                    echo '<small class="font-blue">'.$user['group_name'].', </small>';
                }
            }
        ?>
    </div>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-red-sunglo hide"></i>
                    <span class="caption-subject font-red-sunglo bold uppercase">Revenue</span>
                    <span class="caption-helper">monthly stats...</span>
                </div>
                <div class="actions">
                    <div class="btn-group" id="btnchart-layout">
                        <a href="" class="btn blue btn-outline btn-sm dropdown-toggle" data-toggle="dropdown" data-close-others="true"> Chart Layout
                            <span class="fa fa-angle-down"> </span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li class="active">
                                <a href="javascript:;" class="chrt-layout" data-id="amchart"> amChart </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="chrt-layout" data-id="flotchart"> Flotchart </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="chrt-layout" data-id="googlecharts"> Google Charts </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="chrt-layout" data-id="echarts"> eCharts </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="chrt-layout" data-id="morrischarts"> Morris Charts </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="chrt-layout" data-id="highcharts"> HighCharts </a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group btn-chart">
                        <a href="javascript:;" class="btn green btn-outline btn-sm dropdown-toggle" data-toggle="dropdown" data-close-others="true"> Chart Type
                            <span class="fa fa-angle-down" id="btnchart-type"> </span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <?php foreach($arrcharts as $chart):
                                    echo '<li class="lichart '.$chart['layout_code'].'">
                                              <a href="javascript:;" data-id="'.$chart['id'].'"> '.$chart['type'].' </a>
                                          </li>';
                                  endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div id="site_activities_loading">
                    <img src="../assets/global/img/loading.gif" alt="loading" /> </div>
                <div id="site_activities_content" class="display-none">
                    <div id="site_activities" style="height: 228px;"> </div>
                </div>
                <div style="margin: 20px 0 10px 30px">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-sm label-success"> Revenue: </span>
                            <h3>$13,234</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-sm label-info"> Tax: </span>
                            <h3>$134,900</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-sm label-danger"> Shipment: </span>
                            <h3>$1,134</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-sm label-warning"> Orders: </span>
                            <h3>235090</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-footer">
                <div class="row">
                    <div class="col-md-12">
                        <hr>
                        <a href="" class="btn btn-default pull-right"><i class="fa fa-download"></i> Download Chart</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<?=$this->load->view('_report-table')?>
<!-- END PAGE BASE CONTENT -->
<?php
    $this->load->view('_report-modal');
    echo template_footer();
    echo load_plugin('js',array('datatables','charts','select2'));?>

<script src="<?=base_url('assets/js/custom/_condition.js')?>"></script>
<script src="<?=base_url('assets/js/custom/_report-js.js')?>"></script>
