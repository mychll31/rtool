<!-- begin where clause modal -->
<div class="modal fade bs-modal-sm" id="where_clause-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Clear Custom Query</h4>
            </div>
            <div class="modal-body">
                Here is the list of operators, which can be used with the WHERE clause.

                <small>Source: <a href="https://www.tutorialspoint.com/mysql/mysql-where-clause.htm">https://www.tutorialspoint.com/mysql/mysql-where-clause.htm</a></small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- end where clause modal -->