<?=set_title('Project');load_plugin('css',array('datatables','select','jquery-fileupload'))?>
<input type="hidden" id="txtreport_id" value="<?=$this->uri->segment(3)?>">
<input type="hidden" value="<?=base_url()?>" id="txtbase_url">
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><a data-toggle="modal" href="#edit_projectname-modal" title="click to edit title">
        <i class="fa fa-pencil"></i> <span id="project-name"><?=$arrProject['report_name']?></span></a> </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li class="active"><?=$project_status?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<div class="clearfix" style="margin-bottom: 15px;" <?=$arrProject['added_by'] == $_SESSION['userid'] ? '' : 'hidden'?>>
    <a class="btn grey-cascade <?=$arrProject['is_active'] == 1 ? '' : 'hidden'?>" id="btndeactivate">
        <i class="icon-ban"></i> Deactivate</a>
    <a class="btn yellow <?=$arrProject['is_active'] == 1 ? 'hidden' : ''?>" id="btnactivate">
        <i class="icon-check"></i> Activate</a>
    <a class="btn red" id="btndelete-project">
        <i class="icon-trash"></i> Delete</a>
</div>
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"> Connection List</span>
                </div>
                <div class="actions">
                    <a href="<?=base_url('projects/select_tables/').$this->uri->segment(3)?>" class="btn sbold green">
                        <i class="icon-control-forward"></i> NEXT
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group">
                                <a id="btn-add-conn-list" class="btn sbold blue">
                                    <i class="icon-share"></i> &nbsp;Add Connection
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                
            	<div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblconnection" style="visibility: hidden;">
                    <thead>
                        <tr>
                            <th> Host </th>
                            <th> Database name </th>
                            <th style="text-align: center;"> Datasource Type </th>
                            <th> Connection name </th>
                            <th> Filename </th>
                            <td style="width: 100px;"> </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($arrconnections as $conns): ?>
                            <tr data-id="<?=$conns['source_id']?>" data-connid="<?=$conns['conn_id']?>">
                                <td><?=$conns['dbhost']?></td>
                                <td><?=$conns['dbname']?></td>
                                <td align="center"><?=$conns['name']?></td>
                                <td><?=$conns['connname']?></td>
                                <td><?=$conns['actual_filename']?></td>
                                <td align="center">
                                    <a class="btn red btn-sm delete-conn">
                                        <i class="icon-trash"> </i> Remove
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?php
    $this->load->view('_connection-modal');
    echo template_footer();
    echo load_plugin('js',array('datatables','select','jquery-validation'));
?>
<script src="<?=base_url('assets/js/custom/_connection-js.js')?>"></script>
<!-- <script src="<?=base_url('assets/js/custom/_connection_filesources-js.js')?>"></script> -->