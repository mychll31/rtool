<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> <?=$arrProject['report_name']?> </span>
                </div>
                <div class="actions">
                    <div class="btn-group" style="margin-right: 10px;">
                        <a href="" class="btn green btn-outline btn-sm dropdown-toggle" data-toggle="dropdown" data-close-others="true"> Download Table
                            <span class="fa fa-angle-down"> </span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="<?=base_url('reports/download_report/csv/'.$arrProject['file_name'].'.csv/'.$report_fname)?>"> CSV </a>
                            </li>
                            <li>
                                <a href="<?=base_url('reports/download_report/json/'.$arrProject['file_name'].'.json/'.$report_fname)?>"> JSON </a>
                            </li>
                            <li>
                                <a href="javascript:;"> HTML </a>
                            </li>
                            <li>
                                <a href="javascript:;"> PDF </a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <a data-toggle="modal" href="#select-field-modal" class="btn blue btn-sm">
                            Select Field </a>
                    </div>
                    <div class="btn-group">
                        <a data-toggle="modal" href="#filter-field-modal" class="btn green btn-sm">
                            Filter Field </a>
                    </div>
                </div>
            </div>
            <!-- begin table body -->
            <div class="portlet-body">
                <div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column"
                        id="tblall-reports" style="visibility: hidden;">
                    <thead>
                        <tr>
                        <?php foreach($fields as $header) {
                                echo '<th class="th-'.$header.'">'.$header.'</th>';
                        } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($arrdata as $row) {
                                echo '<tr>';
                                foreach($row as $key => $dt){
                                    echo '<td class="td-'.$key.'">'.$dt.'</td>';
                                }
                                echo '</tr>';
                        } ?>
                    </tbody>
                </table>
            </div>
            <!-- end table body -->
        </div>
    </div>
</div>