<?=set_title('Project');load_plugin('css',array('datatables'))?>
<input type="hidden" id="txtreport_id" value="<?=$this->uri->segment(3)?>">
<input type="hidden" value="<?=base_url()?>" id="txtbaseurl">
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><a data-toggle="modal" href="#edit_projectname-modal" title="click to edit title">
        <i class="fa fa-pencil"></i> <span id="project-name"><?=$arrProject['report_name']?></span></a> </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li class="active"><?=$project_status?></li>
    </ol>
</div>
<div class="clearfix" style="margin-bottom: 15px;" <?=$arrProject['added_by'] == $_SESSION['userid'] ? '' : 'hidden'?>>
    <a class="btn grey-cascade <?=$arrProject['is_active'] == 1 ? '' : 'hidden'?>" id="btndeactivate">
        <i class="icon-ban"></i> Deactivate</a>
    <a class="btn yellow <?=$arrProject['is_active'] == 1 ? 'hidden' : ''?>" id="btnactivate">
        <i class="icon-check"></i> Activate</a>
    <a class="btn red" id="btndelete-project">
        <i class="icon-trash"></i> Delete</a>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"> Table List</span>
                </div>
                <div class="actions">
                    <a href="<?=base_url('projects/connection_list/').$this->uri->segment(3)?>" class="btn sbold green" style="margin-right: 10px;">
                        <i class="icon-control-rewind"></i> BACK
                    </a>
                    <a href="<?=base_url('projects/join_list/').$this->uri->segment(3)?>" class="btn sbold green">
                        <i class="icon-control-forward"></i> NEXT
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group">
                                <a class="btn sbold blue" id="btnselect-tables">
                                    <i class="icon-notebook"></i>&nbsp; Add Table
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            	<div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tbldb-tables" style="visibility: hidden;">
                    <thead>
                        <tr>
                        	<th hidden style="width:80px;"> No </th>
                            <th> Connection name </th>
                            <th> Host </th>
                            <th> Database name </th>
                            <th> Table name </th>
                            <td style="width:200px;"> </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($arrTables) > 0): foreach($arrTables as $table): ?>
                        <tr class="odd gradeX" data-id="<?=$table['table_id']?>" data-sourceid="<?=$table['source_id']?>">
                        	<td hidden><?=$no++?></td>
                            <td><?=$table['connname']?></td>
                            <td><?=$table['dbhost']?></td>
                            <td><?=$table['dbname']?></td>
                            <td><?=$table['table_name']?></td>
                            <td align="center" data-fields='<?=$table["field_names"]?>'>
                                <a class="btn blue btn-sm" id="btnviewfields">
                                	<i class="icon-list"></i>&nbsp; View Fields </a>
                                <a class="btn red btn-sm" id="btndelete">
                                    <i class="icon-trash"></i>&nbsp; Remove </a>
                            </td>
                        </tr>
                        <?php endforeach; endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?php
    $this->load->view('_select_tables-modal');
    echo template_footer();
    echo load_plugin('js',array('datatables'));
?>
<script src="<?=base_url('assets/js/custom/_select_tables-js.js')?>"></script>