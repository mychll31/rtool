<!-- begin join table modal -->
<div class="modal fade bs-modal" id="join-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Join</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="txttableid"><br>
                    <input type="hidden" id="txtjoinid"><br>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Selected table <br><small>(Table 1)</small></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="txtseltable" disabled>
                                        <input type="hidden" value="" id="txtseltableid">
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Join</label>
                                    <div class="col-md-7">
                                        <select class="form-control bs-select" id="seljoin">
                                            <option value="">-- SELECT JOIN --</option>
                                            <?php if(count($arrJoins)>0): foreach($arrJoins as $join): ?>
                                                <option value="<?=$join['join_id']?>"><?=strtoupper($join['join_name'])?></option>
                                            <?php endforeach; endif; ?>
                                        </select>
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Join to table <br><small>(Table 2)</small></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="txtseltable2" disabled>
                                        <input type="hidden" value="" id="txtseltableid2">
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <br>
                                <h5 class="sbold font-green">Select Primary Key</h5>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <div class="portlet box default">
                                            <div class="portlet-title" style="background-color: #fff !important;font-weight: 500 !important;border-bottom: 1px solid #ccc !important;">
                                                <div class="caption" style="font-size: medium !important;">Table 1</div></div>
                                            <div class="portlet-body">
                                                <label id="alert_pkrad1" class="font-red"></label>
                                                <div class="scroller" style="height:150px" data-always-visible="1" data-rail-visible="1" data-rail-color="gray" data-handle-color="green">
                                                    <div class="radio-list" id="radtbl1"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="portlet box default">
                                            <div class="portlet-title" style="background-color: #fff !important;font-weight: 500 !important;border-bottom: 1px solid #ccc !important;">
                                                <div class="caption" style="font-size: medium !important;">Table 2</div></div>
                                            <div class="portlet-body">
                                                <label id="alert_pkrad2" class="font-red"></label>
                                                <div class="scroller" style="height:150px" data-always-visible="1" data-rail-visible="1" data-rail-color="gray" data-handle-color="green">
                                                    <div class="radio-list" id="radtbl2"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12"><a href="javascript:;" id="btnremovejoin" class="btn red">Remove Join</a></div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <a class="btn green" id="btnproceed">Proceed</a>
            </div>
        </div>
    </div>
</div>
<!-- end join table modal -->

<!-- begin confirm modal -->
<div class="modal fade bs-modal-sm" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><span id="spntitle">Delete Join</span></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="txtdelete" name="txtdelete">
                <span id="spncaption">Are you sure you want to delete this join table?</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green" id="submit-confirm-modal">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- end confirm modal -->

<!-- begin view fields for each table modal -->
<div class="modal fade" id="viewfield-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title sbold uppercase" id="vfield-tablename">tablename</h5> 
                <small>
                    <b>Host:</b> <u><i id="vfield-hostname">Hostname</i></u>&nbsp;
                    <b>Database:</b> <span id="vfield-databasename">databasename</span></small>
            </div>
            <div class="modal-body">
                <div class="scroller" style="height:300px" data-rail-visible="1" data-rail-color="red" data-handle-color="green">
                    <table class="table table-striped table-bordered" id="tblfields">
                        <thead>
                            <tr>
                                <th style="text-align:center;">name</th>
                                <th style="text-align:center;">type</th>
                                <th style="text-align:center;">max_length</th>
                                <th style="text-align:center;">primary_key</th>
                                <th class="tblaction" style="text-align:center;">Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end view fields for each table modal -->

<!-- begin edit project name modal -->
<div class="modal fade bs-modal-sm" id="edit_projectname-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Name</h4>
            </div>
            <div class="modal-body">
                <input type="text" class="form-control" id="txtprojname" value="<?=$arrProject['report_name']?>">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn green" id="btnsubmit-name">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- end edit project name modal -->

<!-- begin custom query modal -->
<div class="modal fade bs-modal-sm" id="custom-query-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Join</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" value="<?=$this->uri->segment(2)?>" name="txtaction">
                <ul class="list-group">
                    <?php if(count($arrJoins)>0): foreach($arrJoins as $join): ?>
                        <li class="list-group-item"> <?=strtoupper($join['join_name'])?>
                            <a class="btn btn-xs default pull-right btnselect-join" data-joinname="<?=$join['join_name']?>"> Select </a>
                        </li>
                    <?php endforeach; endif; ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end custom query modal -->

<!-- begin clear custom query modal -->
<div class="modal fade bs-modal-sm" id="clear_custom_query-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Clear Custom Query</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to clear custom query? This cannot be undone, we suggest to save the query first.
                <br>However, if you want to have the suggested SQL Query based on your table list, click the Generate Query Button.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn green" id="btn-clear-custom-query">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- end clear custom query modal -->

<!-- begin mysql function modal -->
<div class="modal fade bs-modal-sm" id="mysql_functions-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Functions</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" value="<?=$this->uri->segment(2)?>" name="txtaction">
                <ul class="list-group list-function">
                    <?php foreach(mysqlfunction() as $func): ?>
                        <li class="list-group-item"> <?=strtoupper($func['func'])?>
                            <a class="btn btn-xs default pull-right btnfunction" data-name="<?=$func['func']?>"> Select </a>
                            <a class="font-blue small btnfunc_desc"> ? </a>
                            <div class="small font-blue-madison divfunc_detail"> <?=$func['desc']?> </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end mysql function modal -->

<!-- begin error message modal -->
<div class="modal fade bs-modal-sm" id="error-message-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="error-message-modal-title"></h4>
            </div>
            <div class="modal-body" id="error-message-modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Okay</button>
            </div>
        </div>
    </div>
</div>
<!-- end error message modal