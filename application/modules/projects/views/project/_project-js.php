<script type="text/javascript">
    $(document).ready(function() {
        $('#div-fileupload,.addconnection').hide();
        $('#seldbtype').change(function() {
            var dbval = $(this).find(':selected').data('wfupload');
            if(dbval == 1){
                $('#div-fileupload').show();
                $('#dbase-connection-section,.form-actions').hide();
            }else{
                $('#dbase-connection-section,.form-actions').show();
                $('#div-fileupload').hide();
            }
        });

        $('#selexconn').change(function() {
            if($(this).val()!=''){
                $('#new-connection').hide();
            }else{
                $('#new-connection').show();
            }
        });

    });
</script>