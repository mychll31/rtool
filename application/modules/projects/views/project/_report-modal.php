<!-- begin project select field modal -->
<div class="modal fade bs-modal" id="select-field-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog <?=count($arrTableList) > 1 ? 'modal-lg' : ''?>">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none !important;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Select Field</h4>
            </div>
            <div class="modal-body" style="padding-top: 0px !important;">
                <div class="row">
                    <?php if(count($arrTableList)>0): foreach($arrTableList as $table): ?>
                    <?php 
                    $class = '';
                    switch (count($arrTableList)) {
                        case 1:
                            $class = 'col-md-12';
                            break;
                        case 2:
                            $class = 'col-md-6';
                            break;
                        default:
                            $class = 'col-md-4';
                            break;
                    } ?>
                    <div class="<?=$class?>">
                        <div class="portlet box portlet-db" style="border: 1px solid #e4e3e3;">
                            <div class="portlet-title font-dark">
                                <div class="db-caption">
                                    <small>
                                        <b>host</b>: <i><u><?=$table['dbhost']?></u></i><br>
                                        <b>db</b>: <?=$table['dbname']?><br>
                                        <b>Table</b>: <i><?=$table['table_name']?></i>
                                    </small>
                                    <br>
                                </div>
                                <label style="margin-left: 5px !important;">
                                    <input type="checkbox" name="chkall" id="chkall" <?=isset($chkpost) ? isset($chkpost['chkall']) == 'on' ? 'checked' : '' : 'checked'?>> Check All</label>
                            </div>
                            <div class="portlet-body">
                                <div class="scroller" style="height:100px" <?=count(json_decode($table['field_names'], true))>5?'data-always-visible="1"':''?> data-rail-visible="1" data-rail-color="red" data-handle-color="green">
                                    <div class="list-checkbox" data-conn="">
                                        <input type="hidden" name="chkfield[]">
                                        <?php foreach(json_decode($table['field_names'], true) as $field): ?>
                                            <label><input type="checkbox" class="chktbl" name="chkfield[]" value="<?=$field['name']?>"
                                                <?=isset($chkpost) ? in_array($field['name'], $chkpost['chkfield']) ? 'checked' : '' : 'checked'?>> <?=$field['name']?></label><br>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; endif; ?>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green" id="btnselect-field">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- end project select field modal -->

<!-- begin project filter field modal -->
<div class="modal fade bs-modal" id="filter-field-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Filter Field</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if(count($arrTableList)>0): foreach($arrTableList as $table): ?>
                            <a href="javascript:;" class="btn btn-default" id="btnfilter-field" data-id="<?=$table['table_name']?>"><i class="fa fa-table"></i> <?=$table['table_name']?></a>
                        <?php endforeach; endif; ?>

                        <br><br>
                        <!-- <div class="well small" id="condition-stmt" style="padding: 5px 5px;margin-top: 10px;margin-bottom: 15px;">
                            WHERE 1
                        </div> -->
                    </div>
                </div>
                <?php if(count($arrTableList)>0): foreach($arrTableList as $table): ?>
                    <div class="row filter-container hidden" data-id="<?=$table['table_name']?>">
                        <div class="col-md-12">
                            <div class="portlet-body">
                                <span>
                                    <b>Host</b>: <i><u><?=$table['dbhost']?></u></i>&nbsp;
                                    <b>Database Name</b>: <?=$table['dbname']?>&nbsp;
                                    <b>Table Name</b>: <i><?=$table['table_name']?></i>
                                </span>
                                <!-- <hr> -->
                                <table class="table table-bordered">
                                    <tr>
                                        <th style="line-height: 0.75;">Name</th>
                                        <th style="line-height: 0.75;">Type</th>
                                        <th style="line-height: 0.75;">Filter By</th>
                                        <th style="line-height: 0.75;">Value</th>
                                    </tr>
                                    <?php foreach(json_decode($table['field_names'],true) as $field) { ?>
                                    <tr>
                                        <td><?=$field['name']?></td>
                                        <td><?=$field['type']?></td>
                                        <td nowrap><?=filtercond().' '.filterElement($field['type'])?></td>
                                        <td><input type="text" data-field="<?=$field['name']?>" data-table="<?=$table['table_name']?>" class="form-control txtfilter" style="height: 20px;padding-left: 4px;"></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php endforeach; endif; ?>
                <div class="row add-condition hidden">
                    <div class="col-md-12">
                        <button class="btn btn-sm blue pull-right" id="btn-add-condition"> Add Condition</button>
                        <button class="btn btn-sm default pull-right" id="clear-condition" style="margin-right: 10px;">Clear Condition</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green" id="btnaddTable">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- end project filter field modal -->

<!-- begin project share with modal -->
<style type="text/css">
    .btn-round {
        border-radius: 30px 30px !important;
        height: 25px;
        width: 25px;
        padding: 3px 5px;
        font-size: smaller;
    }
</style>
<div class="modal fade bs-modal" id="share-with-modal" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <input type="hidden" id="txtbaseurl" value="<?=base_url()?>">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Share With</h4>
            </div>
            <div class="modal-body" style="padding-top: 0px !important;">
                <!-- select users -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light portlet-db">
                            <div class="portlet-title">
                                <div class="db-caption">
                                    <span class="caption-subject bold"> Select User</span><br>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <select class="select2" id="sel-sharewith-user">
                                    <option value="0">--Add User--</option>
                                    <?php foreach($users as $user){ if(!in_array($user['userid'],array_column($shared_with,'shared_to'))) {
                                        echo '<option value="'.$user['userid'].'">'.getFullname($user['firstname'], $user['surname'], $user['middlename']).'</option>';
                                    }
                                    } ?>
                                </select>
                                <div style="padding: 10px 0px;">
                                    <a href="javascript:;" class="btn btn-xs default bold btn-remove-all-user"><i class="fa fa-remove"></i> Remove All</a>
                                    <a href="javascript:;" class="btn btn-xs default bold btn-can-edit-all-user"><i class="fa fa-pencil"></i> All Users Can Edit Report</a>
                                </div>
                                <div class="scroller" style="height:200px" data-rail-color="red" data-handle-color="green">
                                    <table class="table table-bordered" id="tblsharewith-user">
                                        <tr>
                                            <th>Name</th>
                                            <th width="10px;"><center>Access</center></th>
                                            <th width="10px;"><center>Action</center></th>
                                        </tr>
                                        <?php foreach($shared_with as $user): if($user['group_name'] == ''):?>
                                            <tr data-userid='<?=$user['shared_to']?>'>
                                                <td class="tds-user"><?=getFullname($user['firstname'], $user['surname'], $user['middlename'])?></td>
                                                <td align="center" nowrap>
                                                    <a href="javascript:;" title="This User Can View Report" class="btn btn-round blue"><i class="fa fa-eye"></i></a>
                                                    <a href="javascript:;" title="This User <?=$user['edit_report'] ? 'Can' : 'Can\'t'?> Edit Report" class="btn btn-round <?=$user['edit_report'] ? 'green' : 'btn-default'?> btnedit-sw-user"><i class="fa fa-pencil"></i></a></td>
                                                <td align="center" nowrap>
                                                    <a href="javascript:;" title="Delete" class="btn btn-xs red btnremove-sw-user"><i class="fa fa-remove"></i></a></td>
                                            </tr>
                                        <?php endif; endforeach; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- select group -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light portlet-db">
                            <div class="portlet-title">
                                <div class="db-caption">
                                    <span class="caption-subject bold"> Select Group</span><br>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="scroller" style="height:200px" data-rail-color="red" data-handle-color="green">
                                    <table class="table table-bordered" id="tblsharewith-group">
                                        <tr>
                                            <th>Group Name</th>
                                            <th width="10px;"><center>Access</center></th>
                                            <th width="10px"><center>Action</center></th>
                                        </tr>
                                        <?php
                                        foreach($groups as $group):
                                            $shared_id = array_search($group['group_id'], array_column($shared_with, 'shared_to_groupid'));
                                            $isactive = in_array($group['group_id'],array_column($shared_with,'shared_to_groupid')) ? 1 : 0;
                                            $can_edit = $shared_id!='' ? $shared_with[$shared_id]['edit_report'] : 0;?>
                                            <tr>
                                                <td><?=$group['group_name']?></td>
                                                <td align="center" nowrap>
                                                        <a href="javascript:;" title="" class="btn btn-round <?=$isactive?'blue':'btn-default disabled'?> btnview-sw-grp"><i class="fa fa-eye"></i></a>
                                                        <a href="javascript:;" title="" class="btn btn-round <?=$can_edit ? 'green' : 'btn-default'?> <?=$isactive?'':'disabled'?> btnedit-sw-grp"><i class="fa fa-pencil"></i></a></td>
                                                <td align="center" nowrap>
                                                    <a href="javascript:;" class="btn btn-xs <?=$isactive?'green':'default'?> btn-grp-add" data-grpid="<?=$group['group_id']?>"><i class="fa fa-check"></i></a>
                                                    <a href="javascript:;" class="btn btn-xs <?=$isactive?'red':'default'?> btn-grp-rem" data-grpid="<?=$group['group_id']?>"><i class="fa fa-remove"></i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green" id="btnshare_with">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- end project share with modal -->

<!-- begin publish/unpublish modal -->
<style type="text/css">.toast.toast-success { width: 200px !important;}</style>
<div class="modal fade bs-modal-sm" id="publish-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title publish-title"></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="txtpublish">
                <div class="unpublish-body">
                    Are you sure you want to unpublish this project?
                </div>
                <div class="publish-body">
                    Are you sure you want to publish this project?<br><br>
                    <label class="lblpublish"><input type="checkbox" id="chkpublish"> Publish in Public?</label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn green" id="btnsubmit-publish">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- end publish/unpublish modal -->

<!-- begin edit project name modal -->
<div class="modal fade bs-modal-sm" id="edit_projectname-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Name</h4>
            </div>
            <div class="modal-body">
                <input type="text" class="form-control" name="txtprojname" id="txtprojname" value="<?=$arrProject['report_name']?>">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn green" id="btnsubmit-name">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- end edit project name modal