<script>
    $(document).ready(function() {
        $('#tblconnection').dataTable( {
            "pageLength": 5,
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblconnection').css('visibility', 'visible');
            }} );

        // hide connections
        $('#connclose, #connadd').hide();
    	
        $('#selexconn').change(function() {
            if($(this).val()!=''){
                $('#new-connection').hide();
            }else{
                $('#new-connection').show();
            }
        });

        $('#conntest').click(function() {
            // if test connection is success
            $(this).hide();
            $('#connclose, #connadd').show();
        });

        $('table#tblconnection').on('click', 'a', function() {
            $('#txtdelid').val($(this).data('id'));
        });

    });
</script>