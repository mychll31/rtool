<?=load_plugin('css',array('datatables','select','jquery-fileupload'))?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><a data-toggle="modal" href="#edit_projectname-modal" title="click to edit title">
        <i class="fa fa-pencil"></i> <?=$arrProject['report_name']?></a></h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li class="active"><?php echo str_replace('project', '', strtolower($project_status)) ?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"> Connection List</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <pre><?php print_r($arrProject) ?></pre>
                            <pre><?php print_r($arrconnections) ?></pre>
                            <pre><?php print_r($arrsources) ?></pre>
                            <div class="btn-group">
                                <a data-toggle="modal" href="#connection-modal" class="btn sbold blue">
                                    <i class="icon-share"></i>&nbsp;
                                        <?=$arrconnections[0]['is_file'] != 1 ? 'Add Connection' : 'Add File Source'?>
                                </a>
                            </div>
                            <div class="btn-group pull-right">
                                <a href="<?=base_url('projects/select_tables/').$this->uri->segment(3)?>" class="btn sbold green">
                                    <i class="icon-control-forward"></i> NEXT
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            	<div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblconnection" style="visibility: hidden;">
                    <thead>
                        <tr>
                        	<th style="width: 80px;"> No </th>
                            <?php
                            if($arrconnections[0]['is_file'] != 1):
                                echo '<th> Host </th>
                                      <th> Database name </th>';
                            else:    
                                echo '<th> Connection name </th>
                                      <th> Filename </th>
                                      <th> File Type </th>';
                            endif;?>
                            <td style="width: 100px;"> </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($arrconnections as $conns): ?>
                        <tr class="odd gradeX">
                        	<td> <?=$no++?> </td>
                            <?php
                            if($arrconnections[0]['is_file'] != 1):
                                echo '<td>'.$conns['dbhost'].'</td>
                                      <td>'.$conns['dbname'].'</td>';
                            else:    
                                echo '<td>'.$conns['connname'].'</td>
                                      <td>'.$conns['filename'].'</td>
                                      <td>'.$conns['filetype'].' ('.str_replace('.','',$conns['fileext']).')</td>';
                            endif;?>
                            <td align="center">
                                <a data-toggle="modal" href="#delete-modal" class="btn red btn-sm" data-id="<?=$conns['conn_id']?>">
                                	<i class="icon-trash"> </i> Remove
                                </a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?php
print_r($arrconnections[0]['is_file']);
    if($arrconnections[0]['is_file'] != 1):
        $this->load->view('_connection-modal');
    else:
        $this->load->view('_connection-modal2');
    endif;
    echo template_footer();
    echo load_plugin('js',array('datatables','select'));

    if($arrconnections[0]['is_file'] != 1):
        echo '<script src="assets/js/custom/_connection-js.js"></script>';
    else:
        echo '<script src="assets/js/custom/_connection_filesources-js.js"></script>';
    endif;
?>
