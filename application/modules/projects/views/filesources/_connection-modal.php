<!-- begin add connection modal -->
<div class="modal fade bs-modal" id="connection-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Connection</h4>
            </div>
            <?=form_open('projects/add_new_connection/'.$this->uri->segment(3), array('class'=>'form-horizontal','role'=>'form','method'=>'post'))?>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Select from existing connection</label>
                                    <div class="col-md-7">
                                        <select class="form-control bs-select" id="selexconn" name="selexconn">
                                            <?php if(count($arrsources)>0):?>
                                                <option value="">-- SELECT CONNECTION --</option>
                                                <?php foreach($arrsources as $source): ?>
                                                <option value="<?=$source['source_id']?>"><?=$source['connname']?></option>
                                                <?php endforeach; else: ?>
                                                <option value="">-- NO EXISTING CONNECTIONS --</option>
                                            <?php endif; ?>
                                        </select>
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                
                                <!-- begin new connection -->
                                <div id="new-connection">
                                <hr>
                                <h5 class="font-green uppercase">Create new connection</h5>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Connection Name</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="selconnname">
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Database Type</label>
                                    <div class="col-md-7">
                                        <select class="form-control bs-select" <?=$action=='view'?'disabled':''?> id="seldbtype" name="seldbtype">
                                            <option value="">-- SELECT DB TYPE --</option>
                                            <?php foreach($arrdbTypes as $dbtype): ?>
                                                <option value="<?=$dbtype['type_id']?>"><?=$dbtype['name']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Database Name</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtdbname" id="txtdbname">
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Database Username</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtuname" id="txtuname">
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Database Password</label>
                                    <div class="col-md-7">
                                        <input type="password" class="form-control" <?=$action=='view'?'disabled':''?> name="txtpass" id="txtpass">
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Database Re-type Password</label>
                                    <div class="col-md-7">
                                        <input type="password" class="form-control" <?=$action=='view'?'disabled':''?>>
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                </div>
                                <!-- end new connection -->
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                            <div class="col-md-8">
                                <!-- The table listing the files available for upload/download -->
                                <table id="tblpresentation" class="table table-striped clearfix">
                                    <tbody class="files">
                                    </tbody>
                                </table>
                            </div>
                            <input type="hidden" id="txtfcancel" name="txtfcancel">
                            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                            <div class="row fileupload-buttonbar">
                                <div class="col-lg-9" style="display: inline-block; float: left; width: 42% !important;">
                                    <!-- The fileinput-button span is used to style the file input field as button -->
                                    <span class="btn blue fileinput-button">
                                        <i class="fa fa-plus"></i>
                                        <span> Add files... </span>
                                        <input type="file" name="files[]" id="btnfupload" multiple=""> </span>
                                    <button type="submit" class="btn green delete">
                                        <i class="fa fa-check"></i>
                                        <span> Submit&nbsp;&nbsp; </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?=form_close()?>
        </div>
    </div>
</div>
<!-- end add connection modal -->

<!-- begin edit project name modal -->
<div class="modal fade bs-modal-sm" id="edit_projectname-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <?=form_open('projects/edit_projectname/'.$this->uri->segment(3), array('method'=>'post'))?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Name</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" value="<?=$this->uri->segment(2)?>" name="txtaction">
                <input type="text" class="form-control" name="txtprojname" value="<?=$arrProject['report_name']?>">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn green">Submit</button>
            </div>
        </div>
        <?=form_close()?>
    </div>
</div>
<!-- end edit project name modal -->

<!-- begin delete modal -->
<div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <?=form_open('projects/remove_connection/'.$this->uri->segment(3), array('method'=>'post'))?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Connection</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="txtdelid" id="txtdelid">
                Are you sure you want to delete this connection?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green">Yes</button>
            </div>
        </div>
        <?=form_close()?>
    </div>
</div>
<!-- end delete modal -->
