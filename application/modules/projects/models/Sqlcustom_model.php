<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sqlcustom_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->customsql = 'tblcustom_sql';
	}

	function getCustomSQL($reportid)
	{
		return $this->db->get_where($this->customsql, array('cust_repid'=>$reportid))->result_array();
	}

	function addCustomSQL($arrData)
	{
		$this->db->insert($this->customsql, $arrData);
		$id = $this->db->insert_id();
		return $id;
	}

	function editCustomSQL($arrData, $id)
	{
		$this->db->where('cust_repid',$id);
		$this->db->update($this->customsql, $arrData);
		return $this->db->affected_rows();
	}

	function removesql($id)
	{
		$json_data = json_encode($this->getCustomSQL($id));
		$this->db->where('cust_repid',$id);
		$this->db->delete($this->customsql);
		log_action($this->db->last_query(),'Clear Custom Query',$json_data,2);
		return $this->db->affected_rows();
	}

	function checksql($query)
	{
		$res = array();
		$this->db->db_debug = false;
		if(!@$this->db->query($query)) {
			$res = $this->db->error();
		} else {
			$res = array('status' => 'success');
		}

		$this->db->db_debug = true;
		return $res;
	}

	function suggested_custom_sql($arrTableList)
	{
		$not_connected_table = 1;
		$suggested_custom_query = '';

		if(count($arrTableList) > 0) {
			$total_join_table = array_sum(array_map('count',array_column($arrTableList,'join_details')));
			$not_connected_table = count($arrTableList) - $total_join_table;

			// no join table
			if(count($arrTableList) == $not_connected_table) {
				$suggested_custom_query = selectfrom($arrTableList[0]['dbname'],$arrTableList[0]['table_name'],'*',0);
				foreach($arrTableList as $key => $tbl){
				    if($key > 0){
				        $p1 = json_decode($arrTableList[$key-1]['field_names'],true);
				        $pkey1 = array_search(1,array_column($p1,'primary_key'));
				        $pkey1 = $p1[$pkey1]['name'];
				        $p2 = json_decode($arrTableList[$key]['field_names'],true);
				        $pkey2 = array_search(1,array_column($p2,'primary_key'));
				        $pkey2 = $p2[$pkey2]['name'];
				        $suggested_custom_query .= jointbl('left join',
				                                $arrTableList[$key-1]['dbname'],
				                                $arrTableList[$key-1]['table_name'],
				                                $pkey1,
				                                $arrTableList[$key]['dbname'],
				                                $arrTableList[$key]['table_name'],
				                                $pkey2,
				                                0);
				    }
				}
			} else {
				$jd = 0;
				foreach($arrTableList as $key => $tbl){
				    if(count($tbl['join_details']) > 0){
				    	$jd++;
				    	$join_details = $tbl['join_details'][0];
				    	if($jd == 1) {
				    		$suggested_custom_query = selectfrom($arrTableList[$key]['dbname'],$arrTableList[$key]['table_name'],'*',0);
				    	}
				    	$p1 = json_decode($tbl['field_names'],true);
				        $pkey1 = array_search(1,array_column($p1,'primary_key'));
				        $pkey1 = $p1[$pkey1]['name'];
				        $suggested_custom_query .= jointbl($join_details['join_name'],
				        						$tbl['dbname'],
				        						$tbl['table_name'],
				        						$pkey1,
				        						$join_details['dbname'],
				        						$join_details['table_name'],
				        						$join_details['table2_pk'],
				                                0);
				    }
				}
			}
		}
		$res = array('sql' => $suggested_custom_query);
		return $res;
	}
	

}