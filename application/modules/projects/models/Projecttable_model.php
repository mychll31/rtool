<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Projecttable_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'tblproject_tables';
		$this->datasources = 'tbldata_sources';
		$this->joins = 'tbljoins';
	}

	function getData($table_id=0)
	{
		if($table_id==0):
			$this->db->join($this->datasources, $this->datasources.'.source_id='.$this->table.'.source_id', 'right');
			return $this->db->get_where($this->table, $cond1)->result_array();
		else:
			$this->db->join($this->datasources, $this->datasources.'.source_id='.$this->table.'.source_id', 'right');
			$result = $this->db->get_where($this->table, array('table_id' => $table_id))->result_array();
			return $result[0];
		endif;
	}

	function table_details($table_id=0,$reportid=0,$orderid=0)
	{
		if($table_id) {
			$this->db->where($this->table.'.table_id',$table_id);
		}
		if($reportid) {
			$this->db->where($this->table.'.report_id',$reportid);
		}
		if($orderid) {
			$this->db->where($this->table.'.table_order',$orderid);
		}
		$this->db->join($this->datasources, $this->datasources.'.source_id='.$this->table.'.source_id', 'right');
		$result = $this->db->get($this->table)->result_array();
		return count($result) ? $result[0] : null;
	}

	function delete_table($table_id)
	{
		$this->db->where('table_id', $table_id);
		$this->db->delete($this->table);
		return $this->db->affected_rows();	
	}

	function getjoins($joinid=0)
	{
		if($joinid==0):
			return $this->db->get($this->joins)->result_array();
		else:
			$res = $this->db->get_where($this->joins, array($this->joins.'.join_id' => $joinid))->result_array();
			return $res[0];
		endif;
	}

	// function getData($reportid=null, $isactive=null, $isdelete=null)
	// {
	// 	$conditions = array();
	// 	if($reportid!==null):
	// 		array_push($conditions, $this->table.'.report_id='.$reportid);
	// 	endif;

	// 	if($isactive!==null):
	// 		array_push($conditions, $this->table.'.is_active='.$isactive);
	// 	endif;
		
	// 	if($isdelete!==null):
	// 		array_push($conditions, $this->table.'.is_delete='.$isdelete);
	// 	endif;
		
	// 	$str_conditions = implode(' AND ', $conditions);

	// 	if($str_conditions!=''):
	// 		return $this->db->order_by('report_id')->get_where($this->table, $str_conditions)->result_array();
	// 	endif;
	// }

	// function getDataByReport($reportid,$isdelete=null)
	// {
	// 	$conditions = array();
	// 	if($isdelete!==null):
	// 		array_push($conditions, $this->projtables.'.is_delete='.$isdelete);
	// 	endif;
	// 	array_push($conditions, $this->projtables.'.report_id='.$reportid);

	// 	$str_conditions = implode(' AND ', $conditions);

	// 	$this->db->join($this->datasources, $this->datasources.'.source_id='.$this->projtables.'.source_id', 'right');
	// 	return $this->db->get_where($this->projtables, $str_conditions)->result_array();

	// }

	// function add($arrData)
	// {
	// 	$this->db->insert($this->table, $arrData);
	// 	$id = $this->db->insert_id();
	// 	return $id;		
	// }

	// function add_newconn($arrData)
	// {
	// 	$this->db->insert($this->tableconn, $arrData);
	// 	$id = $this->db->insert_id();
	// 	return $id;		
	// }

	// function getConnections($reportid)
	// {
	// 	$this->db->join($this->datasources, $this->datasources.'.source_id='.$this->tableconn.'.source_id', 'right');
	// 	return $this->db->get_where($this->tableconn, array($this->tableconn.'.report_id' => $reportid))->result_array();
	// }


	// function edit($arrData, $id)
	// {
	// 	$this->db->where('type_id',$id);
	// 	$this->db->update($this->table, $arrData);
	// 	return $this->db->affected_rows();
	// }

	// function delete_conn($conn_id)
	// {
	// 	$this->db->where('conn_id', $conn_id);
	// 	$this->db->delete($this->tableconn);
	// 	return $this->db->affected_rows();	
	// }

	// function delete_table($table_id)
	// {
	// 	$this->db->where('table_id', $table_id);
	// 	$this->db->delete($this->projtables);
	// 	return $this->db->affected_rows();	
	// }

	// function getData($id=0, $module=null)
	// {
	// 	$cond1 = array();
	// 	if($module!=null):
	// 		$cond1 = array('module' => $module);
	// 	endif;

	// 	if($id==0):
	// 		return $this->db->get_where($this->table, $cond1)->result_array();
	// 	else:
	// 		$result = $this->db->get_where($this->table, array('id' => $id))->result_array();
	// 		return $result[0];
	// 	endif;
	// }

}