<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sharedproject_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->tableshared = 'tblprojects_shared';
		$this->tablemembers = 'tbluser_group';
	}

	function manage_user($arrData, $report_id, $shared_to)
	{
		$exists = $this->db->get_where($this->tableshared,array('report_id' => $report_id, 'shared_to' => $shared_to))->result_array();
		if($exists) {
			$this->edit($arrData, $report_id, $shared_to);
			if($exists[0]['edit_report']!=$arrData['edit_report']) {
				set_notification(1,$shared_to,'reports/report_shared','@'.$_SESSION['user_name'].' '.notification_message(3).'.</u>');
			}
		} else {
			$this->add($arrData);
			set_notification(1,$shared_to,'reports/report_shared','@'.$_SESSION['user_name'].' '.notification_message(1).'.</u>');
		}
	}

	function manage_group($arrData, $report_id, $shared_to_groupid)
	{
		$group_members = $this->db->get_where($this->tablemembers,array('group_id' => $shared_to_groupid))->result_array();
		$exists = $this->db->get_where($this->tableshared,array('report_id' => $report_id, 'shared_to_groupid' => $shared_to_groupid))->result_array();
		if($exists) {
			$this->edit_group($arrData, $report_id, $shared_to_groupid);
			if($exists[0]['edit_report']!=$arrData['edit_report']) {
				foreach ($group_members as $member) {
					set_notification(1,$member['user_id'],'reports/report_shared','@'.$_SESSION['user_name'].' '.notification_message(3).'.</u>');
				}
			}
		} else {
			$this->add($arrData);
			foreach ($group_members as $member) {
				set_notification(1,$member['user_id'],'reports/report_shared','@'.$_SESSION['user_name'].' '.notification_message(1).'.</u>');
			}
		}
	}

	function add($arrData)
	{
		$this->db->insert($this->tableshared, $arrData);
		$id = $this->db->insert_id();
		return $id;		
	}

	function edit($arrData, $report_id, $shared_to)
	{
		$this->db->where('report_id',$report_id);
		$this->db->where('shared_to',$shared_to);
		$this->db->update($this->tableshared, $arrData);
		return $this->db->affected_rows();
	}

	function edit_group($arrData, $report_id, $shared_to_groupid)
	{
		$this->db->where('report_id',$report_id);
		$this->db->where('shared_to_groupid',$shared_to_groupid);
		$this->db->update($this->tableshared, $arrData);
		return $this->db->affected_rows();
	}

	function delete($projectid,$rem_id,$status)
	{
		if($rem_id!=null){
			$this->db->where('report_id', $projectid);
			if($status) {
				$this->db->where('shared_to_groupid', $rem_id);
			} else {
				$this->db->where('shared_to', $rem_id);
			}
			$this->db->delete($this->tableshared);
			return $this->db->affected_rows();	
		}
	}

}