<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Project_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'tblprojects';
		$this->tableshared = 'tblprojects_shared';
		$this->tableconn = 'tblproject_connections';
		$this->datasources = 'tbldata_sources';
		$this->dbtype = 'tbldata_source_type';
		$this->fsources = 'tblfile_sources';
		$this->projtables = 'tblproject_tables';
		$this->projjoin = 'tblproject_tjoins';
		$this->joins = 'tbljoins';

		$this->profiles = 'tblprofiles';
	}

	function tables_selected($reportid,$sourceid=0,$tableid=0)
	{
		$this->db->where('report_id',$reportid);
		if($sourceid){
			$this->db->where('source_id',$sourceid);	
		}
		if($tableid){
			$this->db->where('table_id',$tableid);	
		}
		
		$res = $this->db->get($this->projtables)->result_array();
		return $res;
	}

	function getDataByReport($reportid,$isdelete=null)
	{
		$conditions = array();
		if($isdelete!==null):
			array_push($conditions, $this->projtables.'.is_delete='.$isdelete);
		endif;
		array_push($conditions, $this->projtables.'.report_id='.$reportid);

		$str_conditions = implode(' AND ', $conditions);
		$this->db->order_by('table_order','asc');
		$this->db->join($this->datasources, $this->datasources.'.source_id='.$this->projtables.'.source_id', 'right');
		return $this->db->get_where($this->projtables, $str_conditions)->result_array();
	}

	function check_project_name($projectname)
	{
		$arrData = $this->db->get_where($this->table, array('report_name'=>$projectname))->result_array();
		return count($arrData) > 0 ? 1 : 0;
	}

	function getProject($reportid=0)
	{
		if($reportid==0):
			return $this->db->get($this->table)->result_array();
		else:
			$arrData = $this->db->get_where($this->table, array('report_id'=>$reportid))->result_array();
			return count($arrData) > 0 ? $arrData[0] : array();
		endif;
	}

	function getAllProjects($userid=0,$owned=0,$shared_to=0,$active=1)
	{
		$user_groups = $this->db->get_where('tbluser_group',array('user_id' => $_SESSION['userid']))->result_array();
		$tables_fields = array('report_id','report_name','added_date','added_by');
		foreach($tables_fields as $tables_field){
			$this->db->select($this->table.'.'.$tables_field);
		}
		$profile_fields = array('firstname','surname','middlename');
		foreach($profile_fields as $profile_field){
			$this->db->select($this->profiles.'.'.$profile_field);
		}
		// owned and shared
		if($userid){
			$shared_user = $shared_to == 0 ? $userid : $shared_to;
			$shared_fields = array('shared_to','view_report','edit_report');
			foreach($shared_fields as $shared_field){
				$this->db->select($this->tableshared.'.'.$shared_field);
			}
			$this->db->where('('.$this->table.'.added_by = '.$userid.' OR '.
								$this->tableshared.'.shared_to = '.$shared_user.' OR '.
								$this->tableshared.'.shared_to_groupid IN ('.implode(',',array_column($user_groups,'group_id')).'))');
			$this->db->join($this->tableshared, $this->tableshared.'.report_id  = '.$this->table.'.report_id ', 'left');
		}
		// owned only
		if($owned){
			$this->db->where($this->table.'.added_by',$owned);
		}
		// shared only
		if($shared_to){
			$shared_fields = array('shared_to','view_report','edit_report');
			foreach($shared_fields as $shared_field){
				$this->db->select($this->tableshared.'.'.$shared_field);
			}
			$this->db->where('('.$this->tableshared.'.shared_to = '.$shared_to.' OR '.
								$this->tableshared.'.shared_to_groupid IN ('.implode(',',array_column($user_groups,'group_id')).'))');
			$this->db->join($this->tableshared, $this->tableshared.'.report_id  = '.$this->table.'.report_id ', 'left');
		}
		$this->db->where($this->table.'.is_active',$active);
		$this->db->where($this->table.'.is_delete',0);
		$this->db->join($this->profiles, $this->profiles.'.userid = '.$this->table.'.added_by', 'left');
		$this->db->group_by($this->table.'.report_id');
		$projects = $this->db->get($this->table)->result_array();
		
		return $projects;
	}

	function check_access($userid,$projectid)
	{
		$user_groups = $this->db->get_where('tbluser_group',array('user_id' => $_SESSION['userid']))->result_array();
		$this->db->where('('.$this->table.'.added_by='.$userid.' OR '.$this->tableshared.'.shared_to='.$userid.' OR '.$this->tableshared.'.shared_to_groupid IN ('.implode(',',array_column($user_groups,'group_id')).'))');
		$this->db->where($this->table.'.report_id',$projectid);
		$this->db->join($this->tableshared, $this->tableshared.'.report_id  = '.$this->table.'.report_id ', 'left');
		$projects = $this->db->get($this->table)->result_array();
		
		if(count($projects) > 0) {
			if($projects[0]['added_by'] == $_SESSION['userid']) {
				return 0;
			} else {
				return !$projects[0]['edit_report'];
			}
		}
		return 1;
	}

	// function getProjectWFType()
	// {
	// 	$this->db->join($this->profiles, $this->profiles.'.userid = '.$this->table.'.added_by', 'left');
	// 	$projects = $this->db->get_where($this->table, array($this->table.'.is_active' => 1, $this->table.'.is_delete' => 0))->result_array();
	// 	return $projects;
	// 	// // $this->db->group_by('tblprojects.report_id');
	// 	// $this->db->join($this->profiles, $this->profiles.'.userid ='.$this->table.'.added_by ', 'left');
	// 	// $projects = $this->db->get_where($this->table, array($this->table.'.is_delete' => 0, $this->table.'.is_active' => 1))->result_array();

	// 	// $this->db->join($this->datasources, $this->datasources.'.source_id='.$this->tableconn.'.source_id', 'right');
	// 	// $conns = $this->db->get($this->tableconn)->result_array();
	// 	// // $conns = array_column($conn, 'report_id','is_file');
	// 	// // return $projects;

	// 	echo '<pre>';
	// 	// $keys = $conns[array_search('8', array_column($conns, 'report_id'))]['is_file'];
	// 	// print_r($keys);
	// 	// print_r($conns);
	// 	print_r($projects);
	// 	die();
	// 	// $projects = $this->db->get($this->table)->result_array();
	// 	// // print_rd(0);
	// 	// $arrProjects = array();
	// 	// foreach($projects as $key => $proj):
	// 	// 	$conn = $this->getConnections($proj['report_id']);
	// 	// 	if(count($conn) > 0):
	// 	// 		$projects[$key]['withfile'] = $conn[0]['is_file'];
	// 	// 	endif;
	// 	// endforeach;
	// 	// return $projects;
	// 	// // print_r($projects);
	// 	// // die();
	// }

	function add($arrData)
	{
		$this->db->insert($this->table, $arrData);
		$id = $this->db->insert_id();
		return $id;		
	}

	function edit($arrData, $id)
	{
		$this->db->where('report_id',$id);
		$this->db->update($this->table, $arrData);
		return $this->db->affected_rows();
	}

	// function edit_select($arrData, $tableid, $reportid)
	// {
	// 	$this->db->where('report_id',$reportid);
	// 	$this->db->update($this->projtables, array('is_select' => 0));
	// 	$this->db->where('table_id',$tableid);
	// 	$this->db->update($this->projtables, $arrData);
	// 	return $this->db->affected_rows();
	// }
	
	function add_newconn($arrData)
	{
		$this->db->insert($this->tableconn, $arrData);
		$id = $this->db->insert_id();
		return $id;		
	}

	function getConnections($reportid,$source_id=0)
	{
    	$this->db->join($this->datasources, $this->datasources.'.source_id = '.$this->tableconn.'.source_id','left');
    	$this->db->join($this->dbtype, $this->dbtype.'.type_id = '.$this->datasources.'.dbtype','left');
    	$this->db->join($this->fsources, $this->fsources.'.resource_id = '.$this->datasources.'.source_id','left');
    	if($source_id == 0):
    		$res = $this->db->get_where($this->tableconn,array($this->tableconn.'.report_id' => $reportid))->result_array();
    		return $res;
    	else:
    		$res = $this->db->get_where($this->tableconn,array($this->tableconn.'.report_id' => $reportid,$this->tableconn.'.source_id' => $source_id))->result_array();
    		return count($res) > 0 ? $res[0] : array();
    	endif;
	}

	function getfile_sources($reportid)
	{
		$filesource = $this->db->get_where($this->tableconn, array('report_id' => $reportid))->result_array();
		foreach($filesource as $key => $fsource):
			$filesource[$key]['fdata'] = $this->Datasource_model->getfileSource($fsource['source_id']);
		endforeach;
		return $filesource;
	}

	function getJoin($reportid,$delete=0,$table=0)
	{
		if($table > 0) {
			$this->db->where('table1_id',$table);
			$this->db->join($this->projtables,$this->projtables.'.table_id = '.$this->projjoin.'.table2_id','left');
			$this->db->join($this->datasources,$this->datasources.'.source_id = '.$this->projtables.'.source_id','left');
		}
		$this->db->join($this->joins,$this->joins.'.join_id = '.$this->projjoin.'.join_name','left');
		$arrCond = array($this->projjoin.'.report_id' => $reportid, $this->projjoin.'.is_delete' => $delete);
		$this->db->order_by('report_join_id');
		return $this->db->get_where($this->projjoin, $arrCond)->result_array();
	}

	function addtable($arrData)
	{
		$this->db->insert($this->projtables, $arrData);
		$id = $this->db->insert_id();
		return $id;		
	}

	function edittable($arrData,$tableid=0,$reportid=0,$orderid=0)
	{
		if($tableid){
			$this->db->where('table_id',$tableid);	
		}
		if($reportid){
			$this->db->where('report_id',$reportid);	
		}
		if($orderid){
			$this->db->where('table_order',$orderid);	
		}
		$this->db->update($this->projtables, $arrData);

		return $this->db->affected_rows();
	}

	function addjoin($arrData)
	{
		$this->db->insert($this->projjoin, $arrData);
		$id = $this->db->insert_id();
		return $id;		
	}

	function editjoin($arrData,$joinid)
	{
		$this->db->where('report_join_id',$joinid);
		$this->db->update($this->projjoin, $arrData);

		return $this->db->affected_rows();
	}

	function delete_join($joinid)
	{
		$this->db->where('report_join_id', $joinid);
		$this->db->delete($this->projjoin);
		return $this->db->affected_rows();	
	}

	function delete_conn($conn_id)
	{
		$this->db->where('conn_id', $conn_id);
		$this->db->delete($this->tableconn);
		return $this->db->affected_rows();	
	}

	function check_connection($db_type,$hostname,$username,$password,$database,$conndata='',$tblname='',$encrypt=0)
	{
		error_reporting(0);
		$dbpass = $encrypt == 1? c_decrypt($password) : $password;
        $arrData = array();
		#Begin mysql connection
        if($db_type == 1):
            $config['hostname'] = $hostname;
            $config['username'] = $username;
            $config['password'] = $dbpass;
            $config['database'] = $database;
            $config['dbdriver'] = 'mysqli';
            $db_obj = $this->load->database($config, TRUE);
            $connected = $db_obj->initialize();
            if ($connected) {
                $arrData['message'] = 'Connected';
                if($tblname==''):
	                if($conndata == 'tables'):
	                    $arrData['tables'] = $db_obj->list_tables();
	                endif;
	            else:
	            	$arrData['fields'] = $db_obj->field_data($tblname);
	            endif;
            }else{
                $error = $db_obj->error();
                $arrData['message'] = $error['message'];
            }
            return $arrData;
        endif;
        #End mysql connection
	}

	function getJoinList($reportid)
	{
		$arrTableList = $this->getDataByReport($reportid,0);
        $arrJoinList = $this->getJoin($reportid,0);

        foreach($arrJoinList as $key=>$jl):
        	$joindesc = $this->db->get_where($this->joins, array('join_id' => $jl['join_name']))->result_array();
        	$arrJoinList[$key]['join_desc'] = count($joindesc) ? $joindesc[0]['join_name'] : '';
            $ktable1 = array_search($jl['table1_id'], array_column($arrTableList, 'table_id'));
            $arrJoinList[$key]['table1_details'] = $arrTableList[$ktable1];

            $ktable2 = array_search($jl['table2_id'], array_column($arrTableList, 'table_id'));
            $arrJoinList[$key]['table2_details'] = $arrTableList[$ktable2];
        endforeach;

        return $arrJoinList;
	}

	function runSQL($strQuery,$reportid)
	{
		//TODO:: FETCH DATA USING PYTHON
		
		$dbconn   = $this->Project_model->getConnections($reportid);
		$db_type  = $dbconn[0]['dbtype'];
		$hostname = $dbconn[0]['dbhost'];
		$username = $dbconn[0]['dbusername'];
		$password = $dbconn[0]['dbpass'];
		$database = $dbconn[0]['dbname'];
		
		$dbpass = c_decrypt($password);
        $arrData = array();
		#Begin mysql connection
        if($db_type == 1):
            $config['hostname'] = $hostname;
            $config['username'] = $username;
            $config['password'] = $dbpass;
            $config['database'] = $database;
            $config['dbdriver'] = 'mysqli';
            
            $db_obj = $this->load->database($config, TRUE);
            $connected = $db_obj->initialize();
            if ($connected) {
            	$arrData['arrData'] = $db_obj->query($strQuery)->result_array();
            }else{
                $error = $db_obj->error();
                $arrData['dberr'] = $error['message'];
            }
            return $arrData;
        endif;
        #End mysql connection
	}

	function getactiveprojectbyuserid($userid,$status)
	{
		return $this->db->get_where($this->table,array('added_by' => $userid, 'is_active' => $status, 'is_delete' => 0))->result_array();
	}

	function getsharedproject($userid)
	{
		return $this->db->get_where($this->tableshared,array('shared_to' => $userid))->result_array();
	}

	function sharedprojectByProject($projectid)
	{
		$this->db->join('tblgroups','tblgroups.group_id = tblprojects_shared.shared_to_groupid', 'left');
		$this->db->join('tblprofiles','tblprofiles.userid = tblprojects_shared.shared_to','left');
		return $this->db->get_where($this->tableshared,array('report_id' => $projectid))->result_array();
	}

	function add_shared_project($arrData)
	{
		$this->db->insert($this->tableshared, $arrData);
		$id = $this->db->insert_id();
		return $id;		
	}

	function delete_shared_project($projectid,$rem_id,$status)
	{
		if($rem_id!=null){
			$this->db->where('report_id', $projectid);
			if($status) {
				$this->db->where('shared_to_groupid', $rem_id);
			} else {
				$this->db->where('shared_to', $rem_id);
			}
			$this->db->delete($this->tableshared);
			return $this->db->affected_rows();	
		}
	}

}