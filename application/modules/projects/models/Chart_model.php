<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Chart_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'tblcharts';
	}

	function getcharts()
	{
		return $this->db->get($this->table)->result_array();
	}

}