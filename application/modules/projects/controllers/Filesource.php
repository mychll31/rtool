<?php 
class Filesource extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        checkSession();
        $this->data = array();
        $this->data['no'] = 1;
        $this->load->model(array('settings/Datasourcetype_model','datasources/Datasource_model','Project_model','Projecttable_model','Sqlcustom_model'));
    }

    # Begin step 2 - Connection
    public function fconnection()
    {
        $projectid = $this->uri->segment(3);
        $datasources = $this->Project_model->getfile_sources($projectid);
        $this->data['arrDataSource'] = $datasources;
        // $this->data['arrconnections'] = $this->Project_model->getConnections($this->uri->segment(3));
        $fileext = strtoupper(str_replace('.','',$datasources[0]['fdata'][0]['fileext']));
        $this->data['arrsources'] = $this->Datasource_model->getDataByField('name',$fileext);
        $this->data['arrdbTypes'] = $this->Datasourcetype_model->getDataByField('name',$fileext);
        $this->data['arrProject'] = $this->Project_model->getProject($projectid);

        $this->data['project_status'] = 'Add new Project';
        $this->data['action'] = 'add';
        $this->template->load('template/main-template', 'Filesources/_connection_list', $this->data);
    }

    public function fconnection22()
    {
        $projectid = $this->uri->segment(3);
        # get project data
        $this->data['arrProject'] = $this->Project_model->getProject($projectid);

        # get data source
        $datasources = $this->Project_model->getfile_sources($projectid);
        $this->data['arrDataSource'] = $datasources;

        print_rd($this->data);

        # read files
        $arrFileData = array();
        foreach($datasources as $source):
            
            foreach($source['fdata'] as $fdata):
                if($fdata['fileext'] == '.csv'):
                    // echo FCPATH.'uploads/fsources/'.$fdata['filename'];
                    $file = fopen(FCPATH.'uploads/fsources/'.$fdata['filename'],'r');
                    while (! feof($file)) {
                        $line = fgetcsv($file);
                        if(count($line) > 1):
                            // echo '<br>';
                            array_push($arrFileData, implode('', $line));
                            // print_r(fgetcsv($file));
                        endif;
                    }
                    fclose($file);

                    
                endif;
            endforeach;
        endforeach;
        print_r($arrFileData);
        // exec('python '.FCPATH.'pyFiles/readcsv.py');

        
        // echo $projectid;
    }


    
}