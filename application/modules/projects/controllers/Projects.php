<?php 
class Projects extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        checkSession();
        $this->data = array();
        $this->data['no'] = 1;
        $this->load->model(array('settings/Datasourcetype_model','datasources/Datasource_model','Project_model','Projecttable_model','Sqlcustom_model','Chart_model','settings/User_model','reports/Report_model'));
    }
    
    public function project_all()
    {
        $this->data['project_status'] = 'All Projects';
        $this->data['data_status'] = 'all';
        $this->data['arrProjects'] = $this->Project_model->getAllProjects($_SESSION['userid']);

        $this->template->load('template/main-template', 'project/view_all', $this->data);
    }

    public function edit_project()
    {
        $reportid = $this->uri->segment(3);
        $this->Project_model->edit(array('is_edit' => 1),$reportid);
        redirect('projects/connection_list/'.$reportid);
    }

    public function project_all_owned()
    {
        $this->data['project_status'] = 'All Projects';
        $this->data['data_status'] = 'owned';
        $this->data['arrProjects'] = $this->Project_model->getAllProjects(0,$_SESSION['userid']);
        $this->template->load('template/main-template', 'project/view_all', $this->data);
    }

    public function project_all_shared()
    {
        $this->data['project_status'] = 'All Projects';
        $this->data['data_status'] = 'shared with you';
        $this->data['arrProjects'] = $this->Project_model->getAllProjects(0,0,$_SESSION['userid']);
        $this->template->load('template/main-template', 'project/view_all', $this->data);
    }

    public function inactive_projects()
    {
        $this->data['project_status'] = 'Inactive Projects';
        $this->data['data_status'] = 'all';
        $this->data['arrProjects'] = $this->Project_model->getAllProjects($_SESSION['userid'],0,0,0);
        $this->template->load('template/main-template', 'project/view_all', $this->data);
    }

    public function inactive_projects_owned()
    {
        $this->data['project_status'] = 'Owned';
        $this->data['data_status'] = 'owned';
        $this->template->load('template/main-template', 'project/view_all_inactive', $this->data);
    }

    public function inactive_projects_shared()
    {
        $this->data['project_status'] = 'Inactive Projects';
        $this->data['data_status'] = 'shared with you';
        $this->template->load('template/main-template', 'project/view_all_inactive', $this->data);
    }

    // public function edit_project()
    // {
    //     $this->template->load('template/main-template', 'template_test', $this->data);
    // }

    public function edit_projectname()
    {
        # edit project
        $arrpost = $this->input->post();
        $arrData = array(
                'report_name'       => $arrpost['txtprojname'],
                'last_updated_by'   => $_SESSION['userid'],
                'last_updated_date' => date('Y-m-d H:i:s'));
        $this->Project_model->edit($arrData, $this->uri->segment(3));
    }

    public function new_project()
    {
        $this->data['arrconnections'] = $this->Datasource_model->getData(1,0);
        $this->data['arrdbTypes'] = $this->Datasourcetype_model->getData(1,0);

        $arrpost = $this->input->post();
        if(!empty($arrpost)):
            # add project
            $arrData = array(
                    'report_name' => $arrpost['txtreportname'],
                    'file_name' => uniqid().time(),
                    'added_by'    => $_SESSION['userid'],
                    'added_date'  => date('Y-m-d H:i:s'));
            $projectid = $this->Project_model->add($arrData);
            $this->session->set_flashdata('strSuccessMsg','New project added successfully.');

            $sourceid = $arrpost['txtsource_id'];
            # add new connection
            if($sourceid == '') {
                if($arrpost['dbwfupload']) {
                    $dbtype_details = $this->Datasourcetype_model->getData(1,$arrpost['seldbtype']);
                    $upload_file = $_FILES['files']['name'];
                    $config['upload_path']          = $dbtype_details[0]['file_loc'];
                    $config['allowed_types']        = '*';

                    $ext = explode('.',$_FILES['files']['name']);
                    $config['file_name'] = uniqid().".".$ext[count($ext)-1]; 
                    $config['overwrite'] = FALSE;
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!is_dir($config['upload_path'])) {
                        mkdir($config['upload_path'], 0777, TRUE);
                    }

                    if ( ! $this->upload->do_upload('files'))
                    {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('strErrorMsg','Please try again!');
                    }
                    else
                    {
                        $data = $this->upload->data();
                        $password = isset($arrpost['txtpass']) ? ($arrpost['txtpass']=='' ? '' : c_encrypt($arrpost['txtpass'])) : '';
                        $arrData = array(
                                'connname'  => $arrpost['selconnname'],
                                'dbtype'    => $arrpost['seldbtype'],
                                'dbname'    => isset($arrpost['txtdbname']) ? $arrpost['txtdbname'] : '',
                                'dbusername'=> isset($arrpost['txtuname']) ? $arrpost['txtuname'] : '',
                                'is_file'   => 1,
                                'dbpass'    => $password,
                                'filename'  => $config['file_name'],
                                'actual_filename' => $upload_file,
                                'added_by'  => $_SESSION['userid'],
                                'added_date'=> date('Y-m-d H:i:s'));
                        $sourceid = $this->Datasource_model->add($arrData);
                    }
                } else {
                    $password = isset($arrpost['txtpass']) ? c_encrypt($arrpost['txtpass']) : '';
                    $arrData = array(
                            'connname'  => $arrpost['selconnname'],
                            'dbhost'    => isset($arrpost['txthost']) ? $arrpost['txthost'] : '',
                            'dbtype'    => $arrpost['seldbtype'],
                            'dbname'    => isset($arrpost['txtdbname']) ? $arrpost['txtdbname'] : '',
                            'dbusername'=> isset($arrpost['txtuname']) ? $arrpost['txtuname'] : '',
                            'dbpass'    => $password,
                            'added_by'  => $_SESSION['userid'],
                            'added_date'=> date('Y-m-d H:i:s'));
                    $sourceid = $this->Datasource_model->add($arrData);
                }
            }
            # add source to the project
            $arrData = array(
                    'report_id' => $projectid,
                    'source_id' => $sourceid,
                    'added_by'  => $_SESSION['userid'],
                    'added_date'=> date('Y-m-d H:i:s'));
            $this->Project_model->add_newconn($arrData);
            
            redirect('projects/connection_list/'.$projectid);
        endif;

        $this->data['project_status'] = 'Add new Project';
        $this->data['action'] = 'add';
        $this->template->load('template/main-template', 'project/_form', $this->data);
    }

    # Begin step 2 - Connection
    public function connection_list()
    {
        $arrProject = $this->Project_model->getProject($this->uri->segment(3));
        $access = $this->Project_model->check_access($_SESSION['userid'],$this->uri->segment(3));
        if($access) {
            redirect('general/error_404');
        }
        $this->data['arrProject'] = $arrProject;
        $this->data['arrconnections'] = $this->Project_model->getConnections($this->uri->segment(3));
        $this->data['arrsources'] = $this->Datasource_model->getData(1,0);
        // $this->data['arrsources'] = $this->Datasource_model->getDataByFieldFilter($this->uri->segment(3));
        // $this->data['arrsources'] = $this->Datasource_model->getDataByField('wfileupload',0,null,null);
        // $this->data['arrdbTypes'] = $this->Datasourcetype_model->getDataByField('','',1,0);
        $this->data['arrdbTypes'] = $this->Datasourcetype_model->getData(1,0);

        $this->data['project_status'] = ($arrProject['is_edit'] ? 'Edit Project' : 'Add new Project');
        $this->data['action'] = 'add';
        $this->template->load('template/main-template', 'project/_connection_list', $this->data);
    }

    // public function connection_listf()
    // {
    //     $this->data['arrconnections'] = $this->Project_model->getConnections($this->uri->segment(3),1);
    //     $this->data['arrsources'] = $this->Datasource_model->getDataByField('wfileupload',1,null,null);
    //     $this->data['arrdbTypes'] = $this->Datasourcetype_model->getDataByField('wfileupload',1,null,null);
    //     $this->data['arrProject'] = $this->Project_model->getProject($this->uri->segment(3));

    //     $this->data['project_status'] = 'Add new Project';
    //     $this->data['action'] = 'add';
    //     $this->template->load('template/main-template', 'project/_connection_list', $this->data);
    // }

    public function add_new_connection()
    {
        $arrpost = $this->input->post();
        if(!empty($arrpost)):
            $exconn = isset($arrpost['selexconn']) ? $arrpost['selexconn'] : '';
            if($exconn!=''):
                $source = $arrpost['selexconn'];
                $source_wf = $this->Datasource_model->getData(null,null,$source);
                $source_wf = $source_wf[0]['wfileupload'];
            else:
                # add datasource first
                $password = isset($arrpost['txtpass']) ? c_encrypt($arrpost['txtpass']) : '';
                $arrData = array(
                        'connname'  => $arrpost['selconnname'],
                        'dbhost'    => isset($arrpost['txthost']) ? $arrpost['txthost'] : '',
                        'dbtype'    => isset($arrpost['seldbtype']) ? $arrpost['seldbtype'] : '',
                        'dbname'    => isset($arrpost['txtdbname']) ? $arrpost['txtdbname'] : '',
                        'dbusername'=> isset($arrpost['txtuname']) ? $arrpost['txtuname'] : '',
                        'dbpass'    => $password,
                        'added_by'  => $_SESSION['userid'],
                        'added_date'=> date('Y-m-d H:i:s'));
                $source = $this->Datasource_model->add($arrData);

                $source_wf = $this->Datasourcetype_model->getData(null,null,$arrpost['seldbtype']);
                $source_wf = $source_wf[0]['wfileupload'];
            endif;

            # add connection
            $arrData = array(
                    'report_id' => $this->uri->segment(3),
                    'source_id' => $source,
                    'added_by'  => $_SESSION['userid'],
                    'added_date'=> date('Y-m-d H:i:s'));
            $this->Project_model->add_newconn($arrData);

            $conn_details = $this->Project_model->getConnections($this->uri->segment(3),$source);
            echo json_encode($conn_details);

            // $this->session->set_flashdata('strSuccessMsg','New data source added successfully.');
            // redirect('projects/connection_list/'.$this->uri->segment(3));
        endif;
    }

    # check connections
    public function check_connections()
    {
        error_reporting(0); # hide Error message
        $conn_data = array();
        $arrpost = $this->input->post();

        if($arrpost['dbwfupload']) {
            $conn_data['message'] = 'Connected';
            // if(file_exists($arrpost['file_loc'].$arrpost['filename'])) {
            //     $conn_data['message'] = 'Connected';
            // } else {
            //     $conn_data['message'] = 'File not exists.';
            // }
        } else {
            $conn_data = $this->Project_model->check_connection($arrpost['db_type'],$arrpost['hostname'],$arrpost['username'],$arrpost['password'],$arrpost['database'],'','',0); 
        }

        // $action = isset($arrpost['action']) ? $arrpost['action'] : 'add';
        // $sourceid = isset($arrpost['sourceid']) ? $arrpost['sourceid'] : '';
        
        // $check_connname = null;
        // $check_connname['status'] = 0;
        // if($action == 'add') {
        //     # check if connection name is already existing
        //     $check_connname = $this->Datasource_model->conn_name_exists($arrpost['conn_name']);    
        // }
        

        // if($arrpost['dbwfupload'])
        // {
        //     if($check_connname['status']){
        //         $conn_data['message'] = $check_connname['message'];
        //     } else {
        //         $dbtype_details = $this->Datasourcetype_model->getData(1,$arrpost['db_type']);
        //         $upload_file = $_FILES['files']['name'];
        //         $config['upload_path']          = $dbtype_details[0]['file_loc'];
        //         $config['allowed_types']        = '*';

        //         $ext = explode('.',$_FILES['files']['name']);
        //         $config['file_name'] = uniqid().".".$ext[count($ext)-1]; 
        //         $config['overwrite'] = FALSE;
                
        //         $this->load->library('upload', $config);
        //         $this->upload->initialize($config);
                
        //         if (!is_dir($config['upload_path'])) {
        //             mkdir($config['upload_path'], 0777, TRUE);
        //         }

        //         if ( ! $this->upload->do_upload('files'))
        //         {
        //             $error = array('error' => $this->upload->display_errors());
        //             $conn_data['message'] = $error['error'].'. Please try again!';
        //         }
        //         else
        //         {
        //             $data = $this->upload->data();
        //             $password = isset($arrpost['password']) ? ($arrpost['password']=='' ? '' : c_encrypt($arrpost['password'])) : '';
        //             $arrData = array(
        //                     'connname'  => $arrpost['database'],
        //                     'dbtype'    => $arrpost['db_type'],
        //                     'dbname'    => isset($arrpost['database']) ? $arrpost['database'] : '',
        //                     'dbusername'=> isset($arrpost['username']) ? $arrpost['username'] : '',
        //                     'is_file'   => 1,
        //                     'dbpass'    => $password,
        //                     'filename'  => $config['file_name'],
        //                     'actual_filename' => $upload_file);
        //             if($action == 'add') {
        //                 $arrData['added_by'] = $_SESSION['userid'];
        //                 $arrData['added_date'] = date('Y-m-d H:i:s');
        //                 $sourceid = $this->Datasource_model->add($arrData);
        //             } else {
        //                 $arrData['last_updated_by'] = $_SESSION['userid'];
        //                 $arrData['last_updated_date'] = date('Y-m-d H:i:s');
        //                 $this->Datasource_model->edit($arrData,$sourceid);
        //             }
        //             $conn_data['source_id'] = $sourceid;
        //             $conn_data['message'] = "Connected";
        //         }
        //     }
        // } else {
        //     if($check_connname['status']){
        //         $conn_data['message'] = $check_connname['message'];
        //     } else {
        //         $conn_data = $this->Project_model->check_connection($arrpost['db_type'],$arrpost['hostname'],$arrpost['username'],$arrpost['password'],$arrpost['database'],'','',0);
        //         if($conn_data['message'] == 'Connected'){
        //             $password = isset($arrpost['password']) ? $password : '';
        //             $arrData = array(
        //                     'connname'  => $arrpost['database'],
        //                     'dbhost'    => isset($arrpost['hostname']) ? $arrpost['hostname'] : '',
        //                     'dbtype'    => $arrpost['db_type'],
        //                     'dbname'    => isset($arrpost['database']) ? $arrpost['database'] : '',
        //                     'dbusername'=> isset($arrpost['username']) ? $arrpost['username'] : '',
        //                     'dbpass'    => $password!='' ? c_encrypt($password) : '');
        //             if($action == 'add') {
        //                 $arrData['added_by'] = $_SESSION['userid'];
        //                 $arrData['added_date'] = date('Y-m-d H:i:s');
        //                 $sourceid = $this->Datasource_model->add($arrData);
        //             } else {
        //                 $arrData['last_updated_by'] = $_SESSION['userid'];
        //                 $arrData['last_updated_date'] = date('Y-m-d H:i:s');
        //                 $this->Datasource_model->edit($arrData,$sourceid);
        //                 $conn_data['source_id'] = $sourceid;
        //             }
        //         }
        //     }
        // }
        // $conn_data['action'] = $action;
        echo json_encode($conn_data);
    }

    public function check_exists_connname()
    {
        error_reporting(0); # hide Error message
        $arrData = $this->Datasource_model->conn_name_exists($_GET['conn_name']);
        echo json_encode($arrData);
    }

    public function check_exists_connections()
    {
        error_reporting(0); # hide Error message
        $dbsource = $this->Datasource_model->getData(null,null,$_POST['selexconn']);

        $dbsource = $dbsource[0];
        if($dbsource['is_file']) {
            if(file_exists($dbsource['file_loc'].$dbsource['filename'])) {
                $arrData['message'] = "Connected";
            } else {
                $arrData['message'] = "File doesn't exists";
            }
        } else {
            $arrData = $this->Project_model->check_connection($dbsource['dbtype'],$dbsource['dbhost'],$dbsource['dbusername'],$dbsource['dbpass'],$dbsource['dbname'],'','',1);
        }
        $arrData['source_details'] = $dbsource;
        echo json_encode($arrData);
    }

    public function check_exists_reportname()
    {
        error_reporting(0); # hide Error message
        $exists_reportname = $this->Project_model->check_project_name($_GET['reportname']);
        if($exists_reportname) {
            $arrData['message'] = notification_message(6);
        } else {
            $arrData['message'] = "Connected";
        }
        echo json_encode($arrData);
    }

    public function remove_connection()
    {
        // print_r($_POST);
        $this->Project_model->delete_conn($_POST['connid']);
        // echo 'Connection removed successfully.';
        // $this->session->set_flashdata('strSuccessMsg','Connection removed successfully.');
        // redirect('projects/connection_list/'.$this->uri->segment(3));
    }
    # End step 2 - Connection

    # Begin step 3 - Tables
    public function select_tables()
    {
        $arrProject = $this->Project_model->getProject($this->uri->segment(3));
        $access = $this->Project_model->check_access($_SESSION['userid'],$this->uri->segment(3));
        if($access) {
            redirect('general/error_404');
        }
        $arrtables = $this->Project_model->getDataByReport($this->uri->segment(3),0);
        $this->data['arrProject'] = $arrProject;
        $arrconnections = $this->Project_model->getConnections($this->uri->segment(3));
        foreach($arrconnections as $key=>$dbsource):
            $source = $this->Project_model->check_connection($dbsource['dbtype'],$dbsource['dbhost'],$dbsource['dbusername'],$dbsource['dbpass'],$dbsource['dbname'],'tables','',1);
            $selected_tables = $this->Project_model->tables_selected($this->uri->segment(3),$dbsource['source_id']);
            $source['tables'] = array_diff($source['tables'], array_column($selected_tables,'table_name'));
            $source['connid'] = $dbsource['conn_id'];
            $arrconnections[$key]['table_info'] = $source;
        endforeach;
        $this->data['arrconnections'] = $arrconnections;

        $this->data['arrTables'] = $arrtables;
        $this->data['project_status'] = ($arrProject['is_edit'] ? 'Edit Project' : 'Add new Project');
        $this->data['action'] = 'add';
        $this->template->load('template/main-template', 'project/_select_tables', $this->data);
    }

    // public function check_table_exists($arrtable,$host,$dbname,$source_table)
    // {
    //     foreach($arrtable as $table)
    //     {
    //         if($table['dbhost'] == $host && $table['dbname'] == $dbname) {
    //             print_rd($source_table);
    //         }
    //     }
    // }

    // public function get_tables()
    // {
    //     error_reporting(0); # hide Error message
    //     $reportid = $_GET['reportid'];
    //     $arrTables = array();
    //     $datasources = $this->Project_model->getConnections($reportid);
    //     foreach($datasources as $dbsource):
    //         $source = $this->Project_model->check_connection($dbsource['dbtype'],$dbsource['dbhost'],$dbsource['dbusername'],$dbsource['dbpass'],$dbsource['dbname'],'tables','',1);
    //         $source['connid'] = $dbsource['conn_id'];
    //         $arrTables[] = $source;
    //     endforeach;
    //     echo json_encode($arrTables);
    // }

    public function save_table()
    {
        $reportid = $_POST['txtreportid'];
        $tables = json_decode($_POST['txtdetails'],true);
        $arrjson = array();
        foreach($tables as $table):
            $dbsource = $this->Project_model->getConnections($reportid,$table['sourceid']);
            $source = $this->Project_model->check_connection($dbsource['dbtype'],$dbsource['dbhost'],$dbsource['dbusername'],$dbsource['dbpass'],$dbsource['dbname'],'tables',$table['tablename'],1);
            $order = count($this->Project_model->tables_selected($reportid));
            $arrData = array(
                    'table_name' => $table['tablename'],
                    'table_order'=> ($order + 1),
                    'field_names'=> json_encode($source['fields']),
                    'report_id'  => $reportid,
                    'source_id'  => $table['sourceid'],
                    'added_by'   => $_SESSION['userid'],
                    'added_date' => date('Y-m-d H:i:s'));
            $tableid = $this->Project_model->addtable($arrData);
            $arrData['tableid'] = $tableid;
            $arrData['conn_id'] = $dbsource['conn_id'];
            $arrData['connname'] = $dbsource['connname'];
            $arrData['dbname'] = $dbsource['dbname'];
            $arrData['dbhost'] = $dbsource['dbhost'];
            $arrData['field_names'] = $source['fields'];
            array_push($arrjson,$arrData);
        endforeach;
        echo json_encode($arrjson);
    }

    // public function get_fields()
    // {
    //     error_reporting(0); # hide Error message
    //     $reportid = $_GET['reportid'];
    //     $arrTables = array();
        
    //     $datasources = $this->Project_model->getConnections($reportid,$_GET['sourceid']);
    //     foreach($datasources as $dbsource):
    //         $source = $this->Project_model->check_connection($dbsource['dbtype'],$dbsource['dbhost'],$dbsource['dbusername'],$dbsource['dbpass'],$dbsource['dbname'],'tables',$_GET['table']);
    //         $source['connid'] = $dbsource['conn_id'];
    //         $arrTables[] = $source;
    //     endforeach;
    //     echo json_encode($arrTables);
    // }

    public function view_fields()
    {
        error_reporting(0); # hide Error message
        $arrTable = $this->Projecttable_model->table_details($_POST['tableid'],$_POST['report_id']);
        $arrTable['field_names'] = json_decode($arrTable['field_names']);
        # table 2
        $arrTable['table_join'] = $this->Projecttable_model->table_details(0,$_POST['report_id'],$_POST['order_id']);
        $arrTable['table_join']['field_names'] = json_decode($arrTable['table_join']['field_names']);
        # join table
        $join_details = $this->Project_model->getJoin($_POST['report_id'],0,$_POST['tableid']);
        $arrTable['join_details'] = count($join_details) ? $join_details[0] : null;
        
        echo json_encode($arrTable);
    }

    public function remove_tables()
    {
        if($_POST['txtdelmode']=='table'):
            $this->Projecttable_model->delete_table($_POST['txtdelid']);
            // $this->session->set_flashdata('strSuccessMsg','Table removed successfully.');
        else:
            $this->Project_model->delete_join($_POST['txtdelid']);
            $this->session->set_flashdata('strSuccessMsg','Table join removed successfully.');
        endif;
        
        // redirect('projects/'.$_POST['txtmodaction'].'/'.$this->uri->segment(3));
    }
    # End step 3 - Tables

    # Begin step 4 - Tables
    public function join_list()
    {
        $reportid = $this->uri->segment(3);
        $arrProject = $this->Project_model->getProject($reportid);
        $access = $this->Project_model->check_access($_SESSION['userid'],$reportid);
        if($access) {
            redirect('general/error_404');
        }
        $this->data['arrProject'] = $arrProject;

        // $arrconnections = $this->Project_model->getConnections($this->uri->segment(3));
        // foreach($arrconnections as $key=>$dbsource):
        //     $source = $this->Project_model->check_connection($dbsource['dbtype'],$dbsource['dbhost'],$dbsource['dbusername'],$dbsource['dbpass'],$dbsource['dbname'],'tables','',1);
        //     $source['connid'] = $dbsource['conn_id'];
        //     $arrconnections[$key]['table_info'] = $source;
        // endforeach;
        // $this->data['arrconnections'] = $arrconnections;
        // $this->data['arrTables'] = $this->Project_model->getDataByReport($this->uri->segment(3),0);
        // echo '<pre>';
        $this->data['arrJoins'] = $this->Projecttable_model->getjoins();
        $arrTableList = $this->Project_model->getDataByReport($reportid,0);
        foreach($arrTableList as $key => $table) {
            $arrTableList[$key]['join_details'] = $this->Project_model->getJoin($reportid,0,$table['table_id']);
        }
        $this->data['arrTableList'] = $arrTableList;
        // print_r($arrTableList);
        // die();

        
        // $arrJoinList = $this->Project_model->getJoinList($reportid);
        // $this->data['arrJoinList'] = $arrJoinList;

        $sqlQuery = $this->Sqlcustom_model->getCustomSQL($reportid);
        $sql_is_show = 0;
        // custom query added by user
        $added_custom_query = '';
        if(count($sqlQuery)) {
            $sql_is_show = $sqlQuery[0]['is_show'];
            $added_custom_query = $sqlQuery[0]['cust_stmt'];
        }
        // suggested query if custom query is not yet added
        $suggested_custom_query = $this->Sqlcustom_model->suggested_custom_sql($arrTableList);
        // if(count($arrTableList) > 0) {
        //     $suggested_custom_query = selectfrom($arrTableList[0]['dbname'],$arrTableList[0]['table_name'],'*',0);
        //     foreach($arrTableList as $key => $tbl){
        //         if($key > 0){
        //             $p1 = json_decode($arrTableList[$key-1]['field_names'],true);
        //             $pkey1 = array_search(1,array_column($p1,'primary_key'));
        //             $pkey1 = $p1[$pkey1]['name'];
        //             $p2 = json_decode($arrTableList[$key]['field_names'],true);
        //             $pkey2 = array_search(1,array_column($p2,'primary_key'));
        //             $pkey2 = $p2[$pkey2]['name'];
        //             $suggested_custom_query .= jointbl('left join',
        //                                     $arrTableList[$key-1]['dbname'],
        //                                     $arrTableList[$key-1]['table_name'],
        //                                     $pkey1,
        //                                     $arrTableList[$key]['dbname'],
        //                                     $arrTableList[$key]['table_name'],
        //                                     $pkey2,
        //                                     0);
        //         }
        //     }
        // }
        $this->data['sql_is_show'] = $sql_is_show;
        $this->data['query'] = array('added' => $added_custom_query, 'suggested' => $suggested_custom_query['sql']);
        $this->data['custom_query'] = $added_custom_query == '' ? $suggested_custom_query['sql'] : $added_custom_query;
        // $sqlQuery = count($sqlQuery) > 0 ? $sqlQuery[0]['cust_stmt'] : '';
        // $this->data['sqlQuery'] = $sqlQuery;
        
        
        
        // # check custom query
        // if($sqlQuery == '') {
        //     # check if there is table join
        //     if(count($arrJoinList) > 0):
        //         $custom_query = selectfrom($arrJoinList[0]['table1_details']['dbname'],$arrJoinList[0]['table1_details']['table_name'],'*',0);
        //         foreach($arrJoinList as $sqljoin):
        //             $custom_query .= jointbl($sqljoin['join_desc'],
        //                             $sqljoin['table1_details']['dbname'],
        //                             $sqljoin['table1_details']['table_name'],
        //                             $sqljoin['table1_pk'],
        //                             $sqljoin['table2_details']['dbname'],
        //                             $sqljoin['table2_details']['table_name'],
        //                             $sqljoin['table2_pk'],0);
        //         endforeach;
        //     else:
        //         # check if there is table/s added
        //         if(count($arrTableList) > 0) {
        //             $custom_query = selectfrom($arrTableList[0]['dbname'],$arrTableList[0]['table_name'],'*',0);
        //             foreach($arrTableList as $key => $tbl){
        //                 if($key > 0){
        //                     $p1 = json_decode($arrTableList[$key-1]['field_names'],true);
        //                     $pkey1 = array_search(1,array_column($p1,'primary_key'));
        //                     $pkey1 = $p1[$pkey1]['name'];

        //                     $p2 = json_decode($arrTableList[$key]['field_names'],true);
        //                     $pkey2 = array_search(1,array_column($p2,'primary_key'));
        //                     $pkey2 = $p2[$pkey2]['name'];
        //                     $custom_query .= jointbl('left join',
        //                                             $arrTableList[$key-1]['dbname'],
        //                                             $arrTableList[$key-1]['table_name'],
        //                                             $pkey1,
        //                                             $arrTableList[$key]['dbname'],
        //                                             $arrTableList[$key]['table_name'],
        //                                             $pkey2,
        //                                             0);
        //                 }
        //             }
        //         }
        //     endif;
        // } else {
        //     $custom_query = $sqlQuery;
        // }

        
        $this->data['project_status'] = ($arrProject['is_edit'] ? 'Edit Project' : 'Add new Project');
        $this->data['action'] = 'add';
        $this->template->load('template/main-template', 'project/_join_list', $this->data);
    }

    public function add_join()
    {
        $action = $_POST['action'];
        $table_detail = '';
        if($action == 'delete') {
            $this->Project_model->delete_join($_POST['join_id']);
            $table_detail = $this->Project_model->tables_selected($_POST['report_id'],0,$_POST['table1_id']);
            if(count($table_detail)){
                $table_detail = count($table_detail) ? $table_detail[0] : null;
                $table_detail['table_pkey'] = getprimary_key($table_detail['field_names']);
                $table_detail['field_names'] = '';
            }
        } else {
            $arrData = array(
                        'report_id' => $_POST['report_id'],
                        'table1_id' => $_POST['table1_id'],
                        'table1_pk' => $_POST['table1_pk'],
                        'join_name' => $_POST['join_name'],
                        'table2_id' => $_POST['table2_id'],
                        'table2_pk' => $_POST['table2_pk']);  
            if($action == 'add') {
                $this->Project_model->addjoin($arrData); 
            } else {
                $this->Project_model->editjoin($arrData,$_POST['join_id']);
            }  
        }

        $sql = '';
        $formatted_sql = '';
        if($_POST['status'] == 'Generated'){
            $arrTableList = $this->Project_model->getDataByReport($_POST['report_id'],0);
            foreach($arrTableList as $key => $table) {
                $arrTableList[$key]['join_details'] = $this->Project_model->getJoin($_POST['report_id'],0,$table['table_id']);
            }
            $suggested_custom_query = $this->Sqlcustom_model->suggested_custom_sql($arrTableList);
            $sql = $suggested_custom_query['sql'];
            $formatted_sql = format_query(strip_tags($suggested_custom_query['sql']),$arrTableList);
        }
        echo json_encode(array('formatted_sql' => $formatted_sql, 'sql' => $sql, 'table_detail' => $table_detail));
    }

    // public function edit_select()
    // {
    //     $arrData = array('is_select'   => 1);
    //     $this->Project_model->edit_select($arrData, $_POST['id_select'], $_POST['reportid']);

    //     $arrTableList = $this->Project_model->getDataByReport($_POST['reportid'],0);
    //     $arrJoinList = $this->Project_model->getJoinList($_POST['reportid']);

    //     $sqlQuery = $this->Sqlcustom_model->getCustomSQL($_POST['reportid']);
    //     $sqlQuery = count($sqlQuery) > 0 ? $sqlQuery[0]['cust_stmt'] : '';
        
    //     $custom_query = '';
    //     # check custom query
    //     if($sqlQuery == '') {
    //         # check if there is table join
    //         if(count($arrJoinList) > 0):
    //             $custom_query = selectfrom($arrJoinList[0]['table1_details']['dbname'],$arrJoinList[0]['table1_details']['table_name'],'*',1);
    //             foreach($arrJoinList as $sqljoin):
    //                 $custom_query .= jointbl($sqljoin['join_desc'],
    //                                 $sqljoin['table1_details']['dbname'],
    //                                 $sqljoin['table1_details']['table_name'],
    //                                 $sqljoin['table1_pk'],
    //                                 $sqljoin['table2_details']['dbname'],
    //                                 $sqljoin['table2_details']['table_name'],
    //                                 $sqljoin['table2_pk'],1);
    //             endforeach;
    //         else:
    //             # check if there is table/s added
    //             if(count($arrTableList) > 0) {
    //                 $custom_query = selectfrom($arrTableList[0]['dbname'],$arrTableList[0]['table_name'],'*',1);
    //                 foreach($arrTableList as $key => $tbl){
    //                     if($key > 0){
    //                         $custom_query .= '<br>';
    //                         $p1 = json_decode($arrTableList[$key-1]['field_names'],true);
    //                         $pkey1 = array_search(1,array_column($p1,'primary_key'));
    //                         $pkey1 = $p1[$pkey1]['name'];

    //                         $p2 = json_decode($arrTableList[$key]['field_names'],true);
    //                         $pkey2 = array_search(1,array_column($p2,'primary_key'));
    //                         $pkey2 = $p2[$pkey2]['name'];
    //                         $custom_query .= jointbl('left join',
    //                                                 $arrTableList[$key-1]['dbname'],
    //                                                 $arrTableList[$key-1]['table_name'],
    //                                                 $pkey1,
    //                                                 $arrTableList[$key]['dbname'],
    //                                                 $arrTableList[$key]['table_name'],
    //                                                 $pkey2,
    //                                                 1);
    //                     }
    //                 }
    //             }
    //         endif;
    //     } else {
    //         $custom_query = $sqlQuery;
    //     }
        
    //     echo json_encode(array('query' => $custom_query, 'custom_query' => strip_tags($custom_query)));

    // }
    # end project actions
    # change order
    public function change_table_order()
    {
        $reportid = $_POST['report_id'];
        $tableid = $_POST['table_id'];
        $tr_index = $_POST['tr_index'];
        $tableid = $_POST['table_id'];

        if($_POST['move'] == 'down') {
            $this->Project_model->edittable(array('table_order' => $tr_index),0,$reportid,($tr_index+1));
            $this->Project_model->edittable(array('table_order' => ($tr_index+1)),$tableid,$reportid);
        } else {
            $this->Project_model->edittable(array('table_order' => ($tr_index+1)),0,$reportid,($tr_index));
            $this->Project_model->edittable(array('table_order' => ($tr_index)),$tableid,$reportid);
        }
        $sql = '';
        $formatted_sql = '';
        if($_POST['status'] == 'Generated'){
            $arrTableList = $this->Project_model->getDataByReport($reportid,0);
            foreach($arrTableList as $key => $table) {
                $arrTableList[$key]['join_details'] = $this->Project_model->getJoin($reportid,0,$table['table_id']);
            }
            $suggested_custom_query = $this->Sqlcustom_model->suggested_custom_sql($arrTableList);
            $sql = $suggested_custom_query['sql'];
            $formatted_sql = format_query(strip_tags($suggested_custom_query['sql']),$arrTableList);
        }
        echo json_encode(array('formatted_sql' => $formatted_sql, 'sql' => $sql));
    }
    # End step 4 - Tables

    public function view_report()
    {
        $reportid = $this->uri->segment(3);
        $project_details = $this->Project_model->getProject($reportid);
        $this->data['arrProject'] = $project_details;
        $this->data['arrcharts'] = $this->Chart_model->getcharts();
        $this->data['access'] = $this->Project_model->check_access($_SESSION['userid'],$reportid);
        $this->data['shared_with'] = $this->Project_model->sharedprojectByProject($reportid);
        $this->data['report_fname'] = preg_replace('/[^A-Za-z0-9]/','',ucwords($project_details['report_name']));

        // echo '<pre>';
        $generated_file = FCPATH.'/data/json/'.$project_details['file_name'].'.json';
        $json_data = file_get_contents($generated_file);
        if($json_data === false) {
            # redirect errors
        }

        $json_data = json_decode($json_data,true);
        if($json_data === false) {
            # redirect to errors
        } else {
            $this->data['arrdata'] = $json_data;
            $this->data['fields'] = array_keys($json_data[0]);
            // print_r(array_keys($json_data[0]));
        }

        $this->data['arrTableList'] = $this->Project_model->getDataByReport($reportid);
        // die();
        // echo '</pre>';

        // $arrProject = $this->Project_model->getProject($this->uri->segment(3));
        // $arrTableList = $this->Project_model->getDataByReport($this->uri->segment(3),0);
        // $arrJoinList = $this->Project_model->getJoinList($this->uri->segment(3));

        // $sqlQuery = $this->Sqlcustom_model->getCustomSQL($this->uri->segment(3));
        // $strQuery = $sqlQuery[0]['cust_stmt'];
        // $proj_data = $this->Project_model->runSQL($strQuery, $this->uri->segment(3));

        // $arrFields = array();
        // foreach($arrTableList as $tablelist):
        //     $fieldlist = json_decode($tablelist['field_names'], true);
        //     foreach($fieldlist as $field):
        //         $field['field'] = $tablelist['table_name'].'.'.$field['name'];
        //         array_push($arrFields, $field);
        //     endforeach;
        // endforeach;

        // $this->data['fields'] = $arrFields;
        // $this->data['arrProject'] = $arrProject;
        // $this->data['arrTableList'] = $arrTableList;
        // $this->data['arrJoinList'] = $arrJoinList;
        // $this->data['arrProjectData'] = $proj_data;
        // $this->data['arrcharts'] = $this->Chart_model->getcharts();

        // $this->data['users'] = $this->User_model->getData(1,null,null);
        // $this->data['groups'] = $this->User_model->getGroupByAuthor($_SESSION['userid']);
        // $this->data['access'] = $this->Project_model->check_access($_SESSION['userid'],$this->uri->segment(3));
        // $this->data['shared_with'] = $this->Project_model->sharedprojectByProject($this->uri->segment(3));

        // $this->data['report_fname'] = preg_replace('/[^A-Za-z0-9]/','',ucwords($arrProject['report_name']));
        // $arrpost = $this->input->post();
        // if(!empty($arrpost)):
        //     $this->data['chkpost'] = $arrpost;
        // endif;
        $this->template->load('template/main-template', 'project/view_report', $this->data);
    }

    # begin setting tables from files
    public function file_uploads()
    {
        # after file upload redirect to selecting table
        echo 'redirect to selecting tables<br>';
        echo '<a href="projects/fselect_tables">select tables</a>';
        die();
    }
    # end setting tables from files

    public function new_fsource()
    {
        $arrpost = $this->input->post();
        if(!empty($arrpost)):
            if($arrpost['selexconn']==''):
                $fcancel = explode(';',$arrpost['txtfcancel']);
                $arrData = array(
                        'connname'  => $arrpost['txtfconnname'],
                        'dbtype'    => isset($arrpost['seldbftype']) ? $arrpost['seldbftype'] : '',
                        'is_file'   => 1,
                        'added_by'  => $_SESSION['userid'],
                        'added_date'=> date('Y-m-d H:i:s'));
                $resourceid = $this->Datasource_model->add($arrData);

                $file_source = array();
                foreach(range(0, count($_FILES['files']['name'])-1) as $fid):
                    if(!in_array($fid, $fcancel) || $fid == ''):
                        $_FILES['file_source[]']['name'] = $_FILES['files']['name'][$fid];
                        $_FILES['file_source[]']['type'] = $_FILES['files']['type'][$fid];
                        $_FILES['file_source[]']['tmp_name'] = $_FILES['files']['tmp_name'][$fid];
                        $_FILES['file_source[]']['error'] = $_FILES['files']['error'][$fid];
                        $_FILES['file_source[]']['size'] = $_FILES['files']['size'][$fid];

                        $config['upload_path']  = './uploads/fsources/';
                        $config['allowed_types']= '*';
                        $config['max_size']     = '*';
                        $config['max_width']    = '*';
                        $config['max_height']   = '*';

                        $this->load->library('upload', $config);

                        if ( ! $this->upload->do_upload('file_source[]')):
                            echo $this->upload->display_errors();
                        else:
                            $fdata = $this->upload->data();
                            $arrData = array(
                                    'resource_id'=> $resourceid,
                                    'filename'   => $fdata['file_name'],
                                    'filetype'   => $fdata['file_type'],
                                    'filesize'   => $fdata['file_size'],
                                    'fileext'    => $fdata['file_ext'],
                                    'added_by'   => $_SESSION['userid'],
                                    'added_date' => date('Y-m-d H:i:s'));
                            $fsourceid = $this->Datasource_model->add_fsource($arrData);
                        endif;
                    endif;
                endforeach;
            else:
                $resourceid = $arrpost['selexconn'];
            endif;

            # add project
            if(!isset($arrpost['txtfreportid'])):
                $arrData = array(
                        'report_name' => $arrpost['txtfreportname'],
                        'added_by'    => $_SESSION['userid'],
                        'added_date'  => date('Y-m-d H:i:s'));
                $projectid = $this->Project_model->add($arrData);
            else:
                $projectid = $arrpost['txtfreportid'];
            endif;

            # add connection
            $arrData = array(
                    'report_id' => $projectid,
                    'source_id' => $resourceid,
                    'added_by'  => $_SESSION['userid'],
                    'added_date'=> date('Y-m-d H:i:s'));
            $this->Project_model->add_newconn($arrData);
            
            $this->session->set_flashdata('strSuccessMsg','New project added successfully.');
            redirect('projects/connection_listf/'.$projectid);
        endif;
    }

    # begin project actions
    public function project_activate()
    {
        $arrData = array(
                    'is_active'         => $this->uri->segment(3),
                    'last_updated_by'   => $_SESSION['userid'],
                    'last_updated_date' => date('Y-m-d H:i:s'));
        $this->Project_model->edit($arrData, $this->uri->segment(4));
        $this->session->set_flashdata('strSuccessMsg','Project '.($this->uri->segment(3)==1?'activated':'deactivated').' successfully.');
        redirect('projects/inactive_projects');
    }

    public function project_delete()
    {
        $arrData = array(
                    'is_delete'   => 1,
                    'delete_by'   => $_SESSION['userid'],
                    'delete_date' => date('Y-m-d H:i:s'));
        $this->Project_model->edit($arrData, $this->uri->segment(3));
        $this->session->set_flashdata('strSuccessMsg','Project deleted successfully.');
        redirect('projects/project_all');
    }
    # end project actions

    public function add_new_source()
    {
        $arrpost = $this->input->post();
        if(!empty($arrpost)) {
            $arrData = array(
                        'report_id' => $arrpost['project_id'],
                        'source_id' => $arrpost['source_id'],
                        'added_by'  => $_SESSION['userid'],
                        'added_date'=> date('Y-m-d H:i:s'));
            $newsource = $this->Project_model->add_newconn($arrData);
            echo json_encode(array('connid' => $newsource));
        }
    }

}