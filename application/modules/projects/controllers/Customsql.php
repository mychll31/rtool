<?php 
class Customsql extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        checkSession();
        $this->data = array();
        $this->data['no'] = 1;
        $this->load->model(array('settings/Datasourcetype_model','datasources/Datasource_model','Project_model','Projecttable_model','Sqlcustom_model','Chart_model','reports/Report_model'));
    }

    public function save_customsql()
    {
        $reportid = $_POST['reportid'];

        // $sqlQuery = $this->Sqlcustom_model->getCustomSQL($reportid);
        // $strQuery = count($sqlQuery) ? $sqlQuery[0]['cust_stmt'] : null;
        // $proj_data = $this->Project_model->runSQL($strQuery, $reportid);

        // $report_details = $this->Report_model->report_details($reportid);
        // $report_fname = $report_details['file_name'];

        $chckexists = $this->Sqlcustom_model->getCustomSQL($reportid);
        $isdrafted = isset($_POST['isdrafted']) ? $_POST['isdrafted'] : 0;
        
        $dberror = $this->Sqlcustom_model->checksql(strip_tags($_POST['sql']));
        if(isset($dberror['status'])) {
            if(count($chckexists) == 0):
                $arrData = array(
                        'is_drafted'  => $isdrafted,
                        'cust_repid' => $reportid,
                        'cust_stmt'  => strip_tags($_POST['sql']),
                        'added_by'   => $_SESSION['userid'],
                        'added_date' => date('Y-m-d H:i:s'));
                $this->Sqlcustom_model->addCustomSQL($arrData);
            else:
                $arrData = array(
                        'is_drafted'  => $isdrafted,
                        'cust_stmt'         => strip_tags($_POST['sql']),
                        'last_modified_by'  => $_SESSION['userid'],
                        'last_modified_date'=> date('Y-m-d H:i:s'));
                $this->Sqlcustom_model->editCustomSQL($arrData, $reportid);
            endif;
            $tablelist = $this->Project_model->getDataByReport($reportid,0);
            $formatted_sql = format_query(strip_tags($_POST['sql']),$tablelist);
            echo json_encode(array('message' => 'success', 'formatted_sql' => $formatted_sql));
        } else {
            echo json_encode($dberror);
        }

        // 
        // $sqlQuery = $this->Sqlcustom_model->getCustomSQL($reportid);
        // $strQuery = $sqlQuery[0]['cust_stmt'];
        // $proj_data = $this->Project_model->runSQL($strQuery, $reportid);

        // # Add csv file
        // foreach (array('csv','json') as $f) {
        //     $generated_file = FCPATH.'/data/'.$f.'/'.$report_fname;
        //     header('Content-Type: text/'.$f);
        //     header('Content-Disposition: attachment; filename='.$report_fname.'.'.$f);
        //     $fp = fopen($generated_file.'.'.$f,'w+');
        //     if($f=='csv') {
        //         foreach ($proj_data['arrData'] as $row) {
        //             fputcsv($fp,$row,"\t");
        //         }
        //     } else {
        //         fwrite($fp,json_encode($proj_data['arrData']));
        //     }
        //     fclose($fp);
        // }
        // echo 1;
        // $this->session->set_flashdata('strSuccessMsg','Custom Query '.($isdrafted ? 'created' : 'updated').' successfully.');
    }

    public function check_customsql()
    {
    	$dberror = $this->Sqlcustom_model->checksql($_POST['sql']);
    	if(isset($dberror['status'])) {
    		echo json_encode(array('message' => 'success'));
    	} else {
    		echo json_encode($dberror);
    	}
    }

    public function remove_customsql()
    {
        $reportid = $_POST['reportid'];
        $this->Sqlcustom_model->removesql($reportid);
        $tablelist = $this->Project_model->getDataByReport($reportid,0);
        $formatted_sql = format_query(strip_tags($_POST['sql']),$tablelist);
        echo json_encode(array('formatted_sql' => $formatted_sql));
    }

    public function hide_custom_area()
    {
        $reportid = $_POST['report_id'];
        $chckexists = $this->Sqlcustom_model->getCustomSQL($reportid);
        if(count($chckexists) > 0):
            $arrData = array(
                    'is_show'           => $_POST['is_show'],
                    'last_modified_by'  => $_SESSION['userid'],
                    'last_modified_date'=> date('Y-m-d H:i:s'));
            $this->Sqlcustom_model->editCustomSQL($arrData, $reportid);
        endif;
    }

}