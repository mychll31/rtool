<?php 
class Reports extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        checkSession();
        $this->data = array();
        $this->load->model(array('Project_model','settings/User_model','Sharedproject_model'));
    }

    public function shared_with()
    {
    	$arrpost = $this->input->post();
    	if(!empty($arrpost)) {
    		$arrpost['userlist'] = isset($arrpost['userlist']) ? $arrpost['userlist'] : array();
    		$arrpost['grouplist'] = isset($arrpost['grouplist']) ? $arrpost['grouplist'] : array();

    		$shares = $this->Project_model->sharedprojectByProject($arrpost['reportid']);
            /* share with users */
            // add / edit users
            foreach ($arrpost['userlist'] as $user) {
                $arrData = array('report_id'  => $arrpost['reportid'],
                                'view_report' => 1,
                                'edit_report' => $user['can_edit'],
                                'shared_to'   => $user['user'],
                                'shared_by'   => $_SESSION['userid'],
                                'shared_by_datetime' => date('Y-m-d H:i:s'));
                $this->Sharedproject_model->manage_user($arrData,$arrpost['reportid'],$user['user']);
            }
    		// remove users
            foreach(array_diff(array_column($shares,'shared_to'),array_column($arrpost['userlist'], 'user')) as $user_rem)
            {
            	if($user_rem!=NULL) {
            		set_notification(3,$user_rem,'reports/report_shared','@'.$_SESSION['user_name'].' '.notification_message(2).'.</u>');
	                $this->Sharedproject_model->delete($arrpost['reportid'],$user_rem,0);
	            }
            }
            
            /* share with groups */
            // add / edit groups
            foreach ($arrpost['grouplist'] as $grpid) {
                $arrData = array('report_id'  => $arrpost['reportid'],
                                'view_report' => 1,
                                'edit_report' => $grpid['can_edit'],
                                'shared_to_groupid'   => $grpid['grpid'],
                                'shared_by'   => $_SESSION['userid'],
                                'shared_by_datetime' => date('Y-m-d H:i:s'));
                $this->Sharedproject_model->manage_group($arrData,$arrpost['reportid'],$grpid['grpid']);
            }
    		// remove groups
            foreach(array_diff(array_column($shares,'shared_to_groupid'),array_column($arrpost['grouplist'], 'grpid')) as $group_rem)
            {
            	if($group_rem!=null) {
	                $this->Sharedproject_model->delete($arrpost['reportid'],$group_rem,1);

	                $group_data = $this->User_model->getGroupByGroupId($group_rem);
	                foreach ($group_data['members'] as $member) {
	                	set_notification(3,$member['user_id'],'reports/report_shared','@'.$_SESSION['user_name'].' '.notification_message(2).'.</u>');
	                }
	            }
            }

    	}
    }

    public function publish()
    {
        $this->Project_model->edit(array('is_published' => $_POST['publish'], 'is_public' => $_POST['public']),$_POST['reportid']);
    }

}