<?php 
class General extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->data = array();
        checkSession();
    }

    public function error_404()
    {
        $this->template->load('template/main-template', 'error_404', $this->data);
    }

    
    
}