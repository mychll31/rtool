<?=form_open($action=='add' ? 'datasources/new_source' : 'datasources/edit_source/'.$arrData['source_id'], array('method'=>'post', 'class'=>'form-horizontal'))?>
    <input type="hidden" id="connname" name="connname" value="<?=isset($arrData) ? $arrData['connname'] : ''?>">
    <input type="hidden" id="dbtype" name="dbtype" value="<?=isset($arrData) ? $arrData['dbtype'] : ''?>">
    <div class="form-group" <?=$action=='edit'? '' : 'hidden'?>>
        <label class="col-md-3 control-label"></label>
        <div class="note note-danger col-md-7" style="padding: 10px 10px;">
            <h5 class="font-red bold">WARNING!</h5>
            <small>
                Changing the database may affect your report view. Please fix your report after editing this source. Thank you!
            </small>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Host <span class="required">*</span></label>
        <div class="col-md-7">
            <input type="text" class="form-control" name="txthost" id="txthost"
                    value="<?=isset($arrData) ? $arrData['dbhost'] : ''?>"
                    <?=$action=='view' ? 'disabled' : ''?>>
            <span class="help-block"> </span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Database Name <span class="required">*</span></label>
        <div class="col-md-7">
            <input type="text" class="form-control" name="txtdbname" id="txtdbname"
                    value="<?=isset($arrData) ? $arrData['dbname'] : ''?>"
                    <?=$action=='view' ? 'disabled' : ''?>>
            <span class="help-block"> </span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Database Username <span class="required">*</span></label>
        <div class="col-md-7">
            <input type="text" class="form-control" name="txtuname" id="txtuname"
                    value="<?=isset($arrData) ? $arrData['dbusername'] : ''?>"
                    <?=$action=='view' ? 'disabled' : ''?>>
            <span class="help-block"> </span>
        </div>
    </div>
    <div class="form-group" <?=$action=='view'?'hidden':''?>>
        <label class="col-md-3 control-label">Database Password <span class="required">*</span></label>
        <div class="col-md-7">
            <input type="password" class="form-control" name="txtpass" id="txtpass">
            <span class="help-block"> </span>
        </div>
    </div>
    <div class="form-group" <?=$action=='view'?'hidden':''?>>
        <label class="col-md-3 control-label">Database Re-type Password <span class="required">*</span></label>
        <div class="col-md-7">
            <input type="password" class="form-control" name="txtretpass" id="txtretpass">
            <span class="help-block"> </span>
        </div>
    </div>

    <div class="row action-test-conn" <?=$action=='view'?'hidden':''?>>
        <div class="col-md-offset-3 col-md-7">
            <button type="button" class="btn red" id="btntestconn">Test Connection</button>
            <button type="button" class="btn default btn-clear-conn">Clear</button>
        </div>
    </div>
    <div class="row add-conn" hidden>
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn green"><?=ucfirst($action)?> Connection</button>
            <button type="button" class="btn default btn-clear-conn">Clear</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-3 col-md-7 conn-alert" hidden>
            <div class="alert alert-danger" style="margin-top: 20px;"></div>
        </div>
    </div>
<?=form_close()?>