<?=set_title('Datasource');load_plugin('css',array('datatables'))?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><?=$dsources_status?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li class="active"><?php echo str_replace('data source', '', strtolower($dsources_status)) ?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"> Data Souce List</span>
                </div>
            </div>
            <div class="portlet-body">
            	<div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblall-reports" style="visibility: hidden;">
                    <thead>
                        <tr>
                        	<th style="width: 50px;"> No </th>
                            <th> Connection Name </th>
                            <th> Datasource Type </th>
                            <th> Host </th>
                            <th> Database Name </th>
                            <th> File Name </th>
                            <th style="width: 80px;"> </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($arrdbsources) > 0): foreach($arrdbsources as $source): ?>
                        <tr class="odd gradeX">
                            <td><?=$no++?></td>
                            <td><?=$source['connname']?></td>
                            <td><?=$source['name']?></td>
                            <td><?=$source['dbhost']?></td>
                            <td><?=$source['dbname']?></td>
                            <td><?=$source['actual_filename']?></td>
                            <td align="center">
                                <a href="<?=base_url('datasources/view_source/').$source['source_id']?>" class="btn blue btn-sm">
                                    <i class="icon-magnifier"></i> View </a>
                            </td>
                        </tr>
                        <?php endforeach; endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?=template_footer()?>
<?=load_plugin('js',array('datatables'))?>

<script>
    $(document).ready(function() {
        $('#tblall-reports').dataTable( {
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblall-reports').css('visibility', 'visible');
            }} );
    });
</script>