<?=form_open_multipart($action=='edit' ? 'datasources/edit_fsource/'.$this->uri->segment(3) : 'datasources/new_fsource',
    array('method'=>'post','class'=>'form-horizontal'))?>
    <input type="hidden" id="fconnname" name="fconnname" value="<?=isset($arrData) ? $arrData['connname'] : ''?>">
    <input type="hidden" id="fdbtype" name="fdbtype" value="<?=isset($arrData) ? $arrData['dbtype'] : ''?>">
    <div class="form-group" <?=$action=='edit'? '' : 'hidden'?>>
        <label class="col-md-3 control-label"></label>
        <div class="note note-danger col-md-7" style="padding: 10px 10px;">
            <h5 class="font-red bold">WARNING!</h5>
            <small>
                Changing the file may affect your report view. Please fix your report after editing this source. Thank you!
            </small>
        </div>
    </div>
    <div class="form-group" <?=$action=='view'? ($arrData['dbname']==''?'hidden':'') : ''?>>
        <label class="col-md-3 control-label">Database Name</label>
        <div class="col-md-7">
            <input type="text" class="form-control" name="txtdbname"
                    value="<?=isset($arrData) ? $arrData['dbname'] : ''?>"
                    <?=$action=='view' ? 'disabled' : ''?>>
            <span class="help-block"> </span>
        </div>
    </div>
    <div class="form-group" <?=$action=='view'? ($arrData['dbusername']==''?'hidden':'') : ''?>>
        <label class="col-md-3 control-label">Database Username</label>
        <div class="col-md-7">
            <input type="text" class="form-control" name="txtuname"
                    value="<?=isset($arrData) ? $arrData['dbusername'] : ''?>"
                    <?=$action=='view' ? 'disabled' : ''?>>
            <span class="help-block"> </span>
        </div>
    </div>
    <div class="form-group" <?=$action=='view'?'hidden':''?>>
        <label class="col-md-3 control-label">Database Password</label>
        <div class="col-md-7">
            <input type="password" class="form-control" name="txtpass" id="txtfpass">
            <span class="help-block"> </span>
        </div>
    </div>
    <div class="form-group" <?=$action=='view'?'hidden':''?>>
        <label class="col-md-3 control-label">Database Re-type Password</label>
        <div class="col-md-7">
            <input type="password" class="form-control" name="txtretpass" id="txtfretpass">
            <span class="help-block"> </span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">File Upload <span class="required">*</span></label>
        <div class="col-md-7">
            <div class="row fileupload-buttonbar">
                <div class="col-lg-9">
                    <span class="btn dark fileinput-button" <?=$action=='view'?'style="display:none;"':''?>>
                        <i class="fa fa-upload"></i>
                        <span> <?=$action=='add'?'Select file...':'Change file...'?> </span>
                        <input type="file" name="files" id="btnfupload"
                                <?=isset($arrData) ? 'accept="'.$arrData['file_type'].'"' : ''?>
                                > </span>
                    <span class="font-blue-madison span-fname">
                        <a href="<?=base_url($arrData['file_loc'].$arrData['filename'])?>"
                            title="Click to download">
                                <?=file_ext_icon($arrData['filename'],$arrData['actual_filename'])?></a>
                    </span>
                    <span class="help-block font-red"> </span>
                </div>
            </div>
            <span class="help-block"> </span>
        </div>
    </div>
    <div class="row" <?=$action=='view'?'hidden':''?>>
        <div class="col-md-offset-3 col-md-9">
            <button type="button" class="btn red" id="btn-test-datasource">Test Datasource</button>
            <button type="submit" class="btn <?=$action=='add'?'blue':'green'?> hidden" id="btn-add-datasource"><?=ucfirst($action)?> Datasource</button>
            <button type="reset" class="btn default <?=$action=='edit'?'hidden':''?>" id="btn-clear-datasource">Clear</button>
            <a class="btn default <?=$action!='edit'?'hidden':''?>"
                href="<?=base_url('datasources/view_source/').$this->uri->segment(3)?>">Cancel</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-3 col-md-7 conn-alert" hidden>
            <div class="alert alert-danger" style="margin-top: 20px;"></div>
        </div>
    </div>
<?=form_close()?>