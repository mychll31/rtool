<?=set_title('Datasource');load_plugin('css',array('select','jquery-fileupload'))?>

<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><?=str_replace('connection', 'Datasource', strtolower($dsources_status))?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?=base_url('datasources/datasources_all')?>">Home</a>
        </li>
        <li class="active"><?=str_replace('connection', '', strtolower($dsources_status))?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-blue sbold uppercase">Form</span>
                </div>

                <?php if($action=='view'): ?>
                    <div class="actions">
                        <a href="<?=base_url('datasources/edit_source/').$this->uri->segment(3)?>"
                            class="btn green"
                            style="margin-right: 3px;">
                                <i class="fa fa-edit"></i> Edit</a>
                        <?php if($arrData['is_active'] == 1): ?>
                            <a class="btn grey-cascade"
                                data-toggle="modal"
                                href="#modal-confirm"
                                id="btndeactivate"
                                data-id="<?=$arrData['source_id']?>">
                                    <i class="icon-ban"></i> Deactivate</a>
                        <?php else: ?>
                            <a class="btn yellow"
                                data-toggle="modal"
                                href="#modal-confirm"
                                id="btnactivate"
                                data-id="<?=$arrData['source_id']?>">
                                    <i class="icon-check"></i> Activate</a>
                        <?php endif; ?>
                        <a class="btn red"
                            data-toggle="modal"
                            href="#modal-confirm"
                            id="btndelete"
                            data-id="<?=$arrData['source_id']?>">
                                <i class="icon-trash"></i> Delete</a>
                    </div>
                <?php endif;?>
            </div>

            <div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
            <div class="portlet-body form div-form" style="visibility: hidden;">
                <div class="form-horizontal">
                    <div class="form-body">
                        <input type="hidden" id="txtbaseurl" value="<?=base_url()?>">
                        <input type="hidden" value="<?=$action?>" id="txtaction">
                        <input type="hidden" value="<?=isset($arrData['source_id']) ? $arrData['source_id'] : ''?>" id="txtsourceid">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Datasource Name <span class="required">*</span></label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> id="txtconnname" value="<?=isset($arrData['connname']) ? $arrData['connname'] : ''?>">
                                <span class="help-block"> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Database Type <span class="required">*</span></label>
                            <div class="col-md-7">
                                <select class="form-control bs-select" <?=$action=='view'?'disabled':''?> id="seldbtype" name="seldbtype">
                                    <option value="">-- SELECT DB TYPE --</option>
                                    <?php if(count($arrdbtype) > 0): foreach($arrdbtype as $dbtype): ?>
                                        <option value="<?=$dbtype['type_id']?>"
                                                data-ftype="<?=$dbtype['file_type']?>"
                                                data-wfupload="<?=$dbtype['wfileupload']?>"
                                                    <?=isset($arrData['dbtype']) ? $arrData['dbtype'] == $dbtype['type_id'] ? 'selected' : '' : ''?>>
                                                    <?=$dbtype['name']?>
                                        </option>
                                    <?php endforeach; endif; ?>
                                </select>
                                <span class="help-block"> </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="div-fsource" <?=$action=='add'?'hidden':($arrData['wfileupload']?'hidden':'')?>>
                    <?=$this->load->view('_form_rdbms')?>
                </div>
                <div id="div-fload" <?=$action=='add'?'hidden':($arrData['wfileupload']?'':'hidden')?>>
                    <?=$this->load->view('_form_upload')?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?=$this->load->view('_datasource_modal')?>

<?php
    echo template_footer();
    echo load_plugin('js',array('select','modals','jquery-fileupload','jquery-validation')); ?>
<script src="<?=base_url('assets/js/custom/_datasource-js.js')?>"></script>