<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Datasource_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'tbldata_sources';
		$this->projects = 'tblprojects';
		$this->table_type = 'tbldata_source_type';
		$this->proj_conn = 'tblproject_connections';
		$this->shared = 'tblprojects_shared';
		$this->fsource = 'tblfile_sources';
		$this->user_group = 'tbluser_group';
	}

	function getData($isactive=null, $isdelete=null, $dsource_id=null)
	{
		$conditions = array();
		if($isactive!==null):
			array_push($conditions, $this->table.'.is_active='.$isactive);
		endif;
		
		if($isdelete!==null):
			array_push($conditions, $this->table.'.is_delete='.$isdelete);
		endif;

		if($dsource_id!==null):
			array_push($conditions, $this->table.'.source_id='.$dsource_id);
			$this->db->limit(1);
		endif;
		
		$str_conditions = implode(' AND ', $conditions);

		if($str_conditions==''):
			$this->db->join($this->table_type, $this->table_type.'.type_id='.$this->table.'.dbtype', 'left');
			return $this->db->get($this->table)->result_array();
		else:
			$this->db->join($this->table_type, $this->table_type.'.type_id='.$this->table.'.dbtype', 'left');
			return $this->db->get_where($this->table, $str_conditions)->result_array();
		endif;
	}

	# check if connection name is existing
	function conn_name_exists($conn_name)
	{
		$data = $this->db->get_where($this->table,array('LOWER(connname)' => $conn_name))->result_array();
		$data_stat = array();
		if(count($data)) {
			if($data[0]['is_active']) {
				$data_stat = array('status' => 1, 'message' => notification_message(5));
			} else {
				$data_stat = array('status' => 1, 'message' => notification_message(4));
			}
		} else {
			$data_stat = array('status' => 0, 'message' => '');
		}
		return $data_stat;
	}

	# get data by given field and value
	function getDataByField($field,$value,$isactive=null)
	{
		$conditions = array();
		if($isactive!==null):
			array_push($conditions, $this->table.'.is_active='.$isactive);
		endif;

		array_push($conditions, $field.'='."'".$value."'");
		
		$str_conditions = implode(' AND ', $conditions);

		$this->db->select($this->table.'.*, name, wfileupload');
		$this->db->join($this->table_type, $this->table_type.'.type_id='.$this->table.'.dbtype', 'left');
		return $this->db->get_where($this->table, $str_conditions)->result_array();
	}

	# get data by given without reportid connections
	function getDataByFieldFilter($reportid)
	{
		$proj_sources = $this->db->select('source_id')->where('report_id',$reportid)->get($this->proj_conn)->result_array();
		if(count($proj_sources)) {
			$this->db->where($this->table.'.source_id NOT IN('.implode(',',array_column($proj_sources,'source_id')).')');
		}
		$this->db->join($this->table_type, $this->table_type.'.type_id='.$this->table.'.dbtype', 'left');
		$res = $this->db->get($this->table)->result_array();
		return $res;
	}

	function add($arrData)
	{
		$this->db->insert($this->table, $arrData);
		$id = $this->db->insert_id();
		log_action($this->db->last_query(),'added new data source : '.$arrData['connname'],'',1);
		return $id;
	}

	function add_fsource($arrData)
	{
		$this->db->insert($this->fsource, $arrData);
		$id = $this->db->insert_id();
		return $id;		
	}

	function edit($arrData, $id)
	{
		$data = $this->db->get_where($this->table,array('source_id' => $id))->result_array();
		$this->db->where('source_id',$id);
		$this->db->update($this->table, $arrData);
		log_action($this->db->last_query(),'modify data source : '.$arrData['connname'],json_encode($data),2);
		$this->notify_user($id);
		return $this->db->affected_rows();
	}

	function getfileSource($resource_id)
	{
		return $this->db->get_where($this->fsource, array('resource_id' => $resource_id, 'is_delete' => 0))->result_array();
	}

	function edit_fsource($arrData, $id)
	{
		$this->db->where('fsource_id',$id);
		$this->db->update($this->fsource, $arrData);
		return $this->db->affected_rows();
	}

	# notify user when editing source
	function notify_user($source_id)
	{
		# get all projects with the given source id
		$projects = $this->db->group_by('report_id')
							 ->get_where($this->proj_conn,array('source_id' => $source_id))
							 ->result_array();
		
		if(count($projects)) {
			$users = $this->db->join($this->projects,$this->projects.'.report_id = '.$this->shared.'.report_id','left')
								 ->where_in($this->shared.'.report_id',array_column($projects,'report_id'))
								 ->where('edit_report',1)
								 ->get($this->shared)
								 ->result_array();
			
			foreach($users as $user)
			{
				$session_fullname = fixfullname($_SESSION['fname'],$_SESSION['lname'],$_SESSION['mname']);

				if($user['shared_to'] == '') {
					$users_group = $this->db->get_where($this->user_group,array('group_id' => $user['shared_to_groupid']))->result_array();
					foreach($users_group as $u)
					{
						if($u['user_id'] != $_SESSION['userid']) {
							set_notification(6,$u['user_id'],'projects/view_report/'.$user['report_id'],$session_fullname.' modify data source in the project '.$user['report_name']);
						}
					}
				} else {
					if($user['shared_to'] != $_SESSION['userid']) {
						set_notification(6,$user['shared_to'],'projects/view_report/'.$user['report_id'],$session_fullname.' modify data source in the project '.$user['report_name']);
					}
				}
			}
		}
		
	}

}