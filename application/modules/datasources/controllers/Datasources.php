<?php 
class Datasources extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        checkSession();
        $this->data = array();
        $this->data['no'] = 1;
        $this->data['arrData'] = null;
        $this->load->model(array('Datasource_model', 'settings/Datasourcetype_model'));
    }

    public function datasources_all()
    {
        $this->data['dsources_status'] = 'All Data source';
        $this->data['arrdbsources'] = $this->Datasource_model->getData(1,0);
        $this->template->load('template/main-template', 'dbsources/view_all', $this->data);
    }

    public function view_source()
    {
        $arrData = $this->Datasource_model->getData(null,null,$this->uri->segment(3));
        $this->data['arrData'] = $arrData[0];

        $this->data['arrdbtype'] = $this->Datasourcetype_model->getData(1,0);
        $this->data['dsources_status'] = 'View connection';
        $this->data['action'] = 'view';
        $this->template->load('template/main-template', 'dbsources/_form', $this->data);
    }

    public function new_source()
    {
        $arrpost = $this->input->post();
        if(!empty($arrpost)):
            $password = isset($arrpost['txtpass']) ? c_encrypt($arrpost['txtpass']) : '';
            $arrData = array(
                    'connname'  => $arrpost['connname'],
                    'dbhost'    => isset($arrpost['txthost']) ? $arrpost['txthost'] : '',
                    'dbtype'    => $arrpost['dbtype'],
                    'dbname'    => isset($arrpost['txtdbname']) ? $arrpost['txtdbname'] : '',
                    'dbusername'=> isset($arrpost['txtuname']) ? $arrpost['txtuname'] : '',
                    'dbpass'    => $password,
                    'added_by'  => $_SESSION['userid'],
                    'added_date'=> date('Y-m-d H:i:s'));
            $newid = $this->Datasource_model->add($arrData);
            $this->session->set_flashdata('strSuccessMsg','New data source added successfully.');
            redirect('datasources/view_source/'.$newid);
        endif;
        $this->data['arrdbtype'] = $this->Datasourcetype_model->getData(1,0);

        $this->data['dsources_status'] = 'Add new connection';
        $this->data['action'] = 'add';
        $this->template->load('template/main-template', 'dbsources/_form', $this->data);
    }

    public function edit_source()
    {
        $sourceid = $this->uri->segment(3);
        $arrData = $this->Datasource_model->getData(null,null,$sourceid);
        $this->data['arrData'] = $arrData[0];

        $arrpost = $this->input->post();
        if(!empty($arrpost)):
            $password = isset($arrpost['txtpass']) ? c_encrypt($arrpost['txtpass']) : '';
            $arrData = array(
                    'connname'  => $arrpost['connname'],
                    'dbhost'    => isset($arrpost['txthost']) ? $arrpost['txthost'] : '',
                    'dbtype'    => $arrpost['dbtype'],
                    'dbname'    => isset($arrpost['txtdbname']) ? $arrpost['txtdbname'] : '',
                    'dbusername'=> isset($arrpost['txtuname']) ? $arrpost['txtuname'] : '',
                    'dbpass'    => $password,
                    'last_updated_by'  => $_SESSION['userid'],
                    'last_updated_date'=> date('Y-m-d H:i:s'));
            $this->Datasource_model->edit($arrData, $sourceid);
            $this->Datasource_model->notify_user($sourceid);
            $this->session->set_flashdata('strSuccessMsg','Data source updated successfully.');
            redirect('datasources/view_source/'.$sourceid);
        endif;
        $this->data['arrdbtype'] = $this->Datasourcetype_model->getData(1,0);

        $this->data['dsources_status'] = 'Edit connection';
        $this->data['action'] = 'edit';
        $this->template->load('template/main-template', 'dbsources/_form', $this->data);
    }

    public function new_fsource()
    {
        $arrpost = $this->input->post();

        if(!empty($arrpost)):
            $dbtype_details = $this->Datasourcetype_model->getData(1,$arrpost['fdbtype']);
            $upload_file = $_FILES['files']['name'];
            $config['upload_path']          = $dbtype_details[0]['file_loc'];
            $config['allowed_types']        = '*';

            $ext = explode('.',$_FILES['files']['name']);
            $config['file_name'] = uniqid().".".$ext[count($ext)-1]; 
            $config['overwrite'] = FALSE;
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, TRUE);
            }

            if ( ! $this->upload->do_upload('files'))
            {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('strErrorMsg','Please try again!');
            }
            else
            {
                $data = $this->upload->data();
                $password = isset($arrpost['txtretpass']) ? ($arrpost['txtretpass']=='' ? '' : c_encrypt($arrpost['txtretpass'])) : '';
                $arrData = array(
                        'connname'  => $arrpost['fconnname'],
                        'dbtype'    => $arrpost['fdbtype'],
                        'dbname'    => isset($arrpost['txtdbname']) ? $arrpost['txtdbname'] : '',
                        'dbusername'=> isset($arrpost['txtuname']) ? $arrpost['txtuname'] : '',
                        'is_file'   => 1,
                        'dbpass'    => $password,
                        'filename'  => $config['file_name'],
                        'actual_filename' => $upload_file,
                        'added_by'  => $_SESSION['userid'],
                        'added_date'=> date('Y-m-d H:i:s'));
                $newid = $this->Datasource_model->add($arrData);
                $this->session->set_flashdata('strSuccessMsg','New data source added successfully.');
                redirect('datasources/view_source/'.$newid);
            }
        endif;
    }

    public function edit_fsource()
    {
        $arrpost = $this->input->post();
        $sourceid = $this->uri->segment(3);
        if(!empty($arrpost)):
            $password = isset($arrpost['txtretpass']) ? ($arrpost['txtretpass']=='' ? '' : c_encrypt($arrpost['txtretpass'])) : '';
            $upload_file = $_FILES['files']['name'];
            $arrData = array(
                    'connname'  => $arrpost['fconnname'],
                    'dbtype'    => $arrpost['fdbtype'],
                    'dbname'    => isset($arrpost['txtdbname']) ? $arrpost['txtdbname'] : '',
                    'dbusername'=> isset($arrpost['txtuname']) ? $arrpost['txtuname'] : '',
                    'dbpass'    => $password,
                    'is_file'   => 1,
                    'last_updated_by'  => $_SESSION['userid'],
                    'last_updated_date'=> date('Y-m-d H:i:s'));
            
            if($upload_file!='')
            {
                $dbtype_details = $this->Datasourcetype_model->getData(1,$arrpost['fdbtype']);
                $config['upload_path']          = $dbtype_details[0]['file_loc'];
                $config['allowed_types']        = '*';

                $ext = explode('.',$_FILES['files']['name']);
                $config['file_name'] = uniqid().".".$ext[count($ext)-1]; 
                $config['overwrite'] = FALSE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, TRUE);
                }

                if ( ! $this->upload->do_upload('files'))
                {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('strErrorMsg','Please try again!');
                }
                else
                {
                    $arrData['filename']  = $config['file_name'];
                    $arrData['actual_filename'] = $upload_file;
                } 
            }

            $this->Datasource_model->edit($arrData,$sourceid);
            $this->session->set_flashdata('strSuccessMsg','Data source updated successfully.');
            redirect('datasources/view_source/'.$sourceid);
        endif;
    }

    public function add_new_source()
    {
        $arrpost = $this->input->post();
        if(!empty($arrpost)){
            if($arrpost['wfileupload']) {
                # upload file
                $dbtype_details = $this->Datasourcetype_model->getData(1,$arrpost['dbtype']);
                $upload_file = $_FILES['files']['name'];
                $config['upload_path']          = $dbtype_details[0]['file_loc'];
                $config['allowed_types']        = '*';

                $ext = explode('.',$_FILES['files']['name']);
                $config['file_name'] = uniqid().".".$ext[count($ext)-1]; 
                $config['overwrite'] = FALSE;
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0777, TRUE);
                }

                if ( ! $this->upload->do_upload('files'))
                {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('strErrorMsg','Please try again!');
                }
                else
                {
                    $data = $this->upload->data();
                    $password = isset($arrpost['txtpass']) ? ($arrpost['txtpass']=='' ? '' : c_encrypt($arrpost['txtpass'])) : '';
                    $arrData = array(
                            'connname'  => $arrpost['connname'],
                            'dbtype'    => $arrpost['dbtype'],
                            'dbname'    => isset($arrpost['txtdbname']) ? $arrpost['txtdbname'] : '',
                            'dbusername'=> isset($arrpost['txtuname']) ? $arrpost['txtuname'] : '',
                            'is_file'   => 1,
                            'dbpass'    => $password,
                            'filename'  => $config['file_name'],
                            'actual_filename' => $upload_file,
                            'added_by'  => $_SESSION['userid'],
                            'added_date'=> date('Y-m-d H:i:s'));
                    $newid = $this->Datasource_model->add($arrData);
                    $dbsource = $this->Datasource_model->getData(null,null,$newid);
                    echo json_encode(array('message' => 'Connected', 'source_details' => $dbsource[0]));
                }
            } else {
                $password = isset($arrpost['txtpass']) ? c_encrypt($arrpost['txtpass']) : '';
                $arrData = array(
                        'connname'  => $arrpost['connname'],
                        'dbhost'    => isset($arrpost['txthost']) ? $arrpost['txthost'] : '',
                        'dbtype'    => $arrpost['dbtype'],
                        'dbname'    => isset($arrpost['txtdbname']) ? $arrpost['txtdbname'] : '',
                        'dbusername'=> isset($arrpost['txtuname']) ? $arrpost['txtuname'] : '',
                        'dbpass'    => $password,
                        'added_by'  => $_SESSION['userid'],
                        'added_date'=> date('Y-m-d H:i:s'));
                $newid = $this->Datasource_model->add($arrData);
                $dbsource = $this->Datasource_model->getData(null,null,$newid);
                echo json_encode(array('message' => 'Connected', 'source_details' => $dbsource[0]));
            }
        }
    }

    public function dbsource_activate()
    {
        $arrData = array(
                    'is_active'         => $this->uri->segment(3),
                    'last_updated_by'   => $_SESSION['userid'],
                    'last_updated_date' => date('Y-m-d H:i:s'));
        $this->Datasource_model->edit($arrData, $this->uri->segment(4));
        $this->session->set_flashdata('strSuccessMsg','Data source '.($this->uri->segment(3)==1?'activated':'deactivated').' successfully.');
        redirect('datasources/view_source/'.$this->uri->segment(4));
    }

    // public function remove_fsource()
    // {
    //     $arrData = array(
    //                 'is_delete'   => 1,
    //                 'delete_by'   => $_SESSION['userid'],
    //                 'delete_date' => date('Y-m-d H:i:s'));
    //     $this->Datasource_model->edit_fsource($arrData, $this->uri->segment(3));
    //     $this->session->set_flashdata('strSuccessMsg','File removed successfully.');
    //     redirect('datasources/view_source/'.$this->uri->segment(4));
    // }

    public function delete()
    {
        $arrData = array(
                    'is_delete'   => 1,
                    'delete_by'   => $this->session->userdata('user_id'),
                    'delete_date' => date('Y-m-d H:i:s'));
        $this->Datasource_model->edit($arrData, $this->uri->segment(3));
        $this->session->set_flashdata('strSuccessMsg','Data source deleted successfully.');
        redirect('datasources/datasources_all');
    }

    public function inactive_datasources()
    {
        $this->data['dsources_status'] = 'Inactive Data Source';
        $this->data['arrdbsources'] = $this->Datasource_model->getData(0);
        $this->template->load('template/main-template', 'dbsources/view_all', $this->data);
    }



    
    
}