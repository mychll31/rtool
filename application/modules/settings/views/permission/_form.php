<?=load_plugin('css',array('select'))?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><?=$p_status?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?=base_url('datasources/datasources_all')?>">Home</a>
        </li>
        <li class="active"><?php echo str_replace('permission', '', strtolower($p_status)) ?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-blue sbold uppercase"><?=$action?> User's Permission</span>
                </div>
                <?php if($action=='view'): ?>
                <div class="actions">
                    <a href="<?=base_url('settings/permissions/edit_permission')?>" class="btn green" style="margin-right: 3px;">
                        <i class="fa fa-edit"></i> Edit User Type</a>
                    <a class="btn red" data-toggle="modal" href="#delete-modal">
                        <i class="icon-trash"></i> Delete User</a>
                </div>
                <?php endif;?>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal" role="form">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Name of User</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" disabled>
                                <span class="help-block"> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">User Type</label>
                            <div class="col-md-7">
                                <select class="form-control bs-select" <?=$action=='view'?'disabled':''?>>
                                    <option value="">-- SELECT USER TYPE --</option>
                                    <option value="1">TEST</option>
                                </select>
                                <span class="help-block"> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-7">
                                <div class="checkbox-list">
                                    <label>
                                        <input type="checkbox" <?=$action=='view'?'disabled':''?>> Active User </label>
                                </div>
                            </div>
                        </div>
                        <?php if($action == 'view'): ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label sbold">Roles</label>
                                <div class="col-md-7">&nbsp;</div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3">&nbsp;</label>
                                <div class="col-md-8">
                                    <label class="col-md-4"><input type="checkbox" name="" disabled> Role a</label>
                                    <label class="col-md-4"><input type="checkbox" name="" disabled> Role a</label>
                                    <label class="col-md-4"><input type="checkbox" name="" disabled> Role a</label>
                                    <label class="col-md-4"><input type="checkbox" name="" disabled> Role a</label>
                                    <label class="col-md-4"><input type="checkbox" name="" disabled> Role a</label>
                                    <label class="col-md-4"><input type="checkbox" name="" disabled> Role a</label>
                                    <label class="col-md-4"><input type="checkbox" name="" disabled> Role a</label>
                                    <label class="col-md-4"><input type="checkbox" name="" disabled> Role a</label>
                                    <label class="col-md-4"><input type="checkbox" name="" disabled> Role a</label>
                                    <label class="col-md-4"><input type="checkbox" name="" disabled> Role a</label>
                                    <label class="col-md-4"><input type="checkbox" name="" disabled> Role a</label>
                                    <label class="col-md-4"><input type="checkbox" name="" disabled> Role a</label>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="form-actions" <?=$action=='view'?'hidden':''?>>
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- BEGIN DELETE MODAL -->
<div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete User</h4>
            </div>
            <div class="modal-body"> Are you sure you want to delete this user? </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="button" class="btn green">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- END DELETE MODAL -->

<?=template_footer()?>
<?=load_plugin('js',array('select','modals'))?>
