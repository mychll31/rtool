<?=set_title('Settings-Source Type');load_plugin('css',array('datatables'))?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><?=$dt_status?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li class="active"><?php echo str_replace('data source type', '', strtolower($dt_status)) ?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"> Data Souce Type List</span>
                </div>
            </div>
            <div class="portlet-body">
            	<div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblall-dbtype" style="visibility: hidden;">
                    <thead>
                        <tr>
                        	<th style="width: 80px;"> No </th>
                            <th> Name </th>
                            <th> Description </th>
                            <th style="width: 200px;"> </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($arrdbtype) > 0): foreach($arrdbtype as $type): ?>
                        <tr class="odd gradeX">
                            <td><?=$no++?></td>
                            <td><?=$type['name']?></td>
                            <td><?=$type['type_description']?></td>
                            <td align="center">
                                <?php if($type['is_active']==0): ?>
                                    <a data-toggle="modal" id="btnactivate" href="#modal-confirm" class="btn blue btn-sm" data-id="<?=$type['type_id']?>">
                                        <i class="icon-check"></i> Activate </a>
                                <?php else: ?>
                                    <a data-toggle="modal" id="btndeactivate" href="#modal-confirm" class="btn grey-cascade btn-sm" data-id="<?=$type['type_id']?>">
                                        <i class="icon-ban"></i> Deactivate </a>
                                <?php endif; ?>
                                <!-- <a data-toggle="modal" id="btndelete" href="#modal-confirm" class="btn red btn-sm" data-id="<?=$type['type_id']?>">
                                    <i class="icon-trash"></i> Delete </a> -->
                            </td>
                        </tr>
                        <?php endforeach; endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- BEGIN CONFIRM MODAL -->
<div class="modal fade bs-modal-sm" id="modal-confirm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form id="frmconfirm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"> </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green">Yes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONFIRM MODAL -->

<?=template_footer()?>
<?=load_plugin('js',array('datatables'))?>

<script>
    $(document).ready(function() {
        $('#tblall-dbtype').dataTable( {
            "pageLength": 5,
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblall-dbtype').css('visibility', 'visible');
            }} );

        $('#tblall-dbtype').on('click', 'a#btnactivate', function() {
            $('.modal-title').html('Activate Datasource Type');
            $('.modal-body').html('Are you sure you want to activate this data source type?');
            $('#frmconfirm').attr('action', 'dbsourcetype/edit_dbtype/1/'+$(this).data('id'))
        });

        $('#tblall-dbtype').on('click', 'a#btndeactivate', function() {
            $('.modal-title').html('Deactivate Datasource Type');
            $('.modal-body').html('Are you sure you want to deactivate this data source type?');
            $('#frmconfirm').attr('action', 'dbsourcetype/edit_dbtype/0/'+$(this).data('id'))
        });

        $('#tblall-dbtype').on('click', 'a#btndelete', function() {
            $('.modal-title').html('Delete Datasource Type');
            $('.modal-body').html('Are you sure you want to delete this data source type?');
            $('#frmconfirm').attr('action', 'dbsourcetype/delete_dbtype/'+$(this).data('id'))
        });

    });
</script>