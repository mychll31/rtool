<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="<?=$this->uri->segment(4)=='personal_info'?'active':''?>">
                            <a href="<?=base_url('settings/users/account_settings/personal_info')?>">Personal Info</a>
                        </li>
                        <li class="<?=$this->uri->segment(4)=='avatar'?'active':''?>">
                            <a href="<?=base_url('settings/users/account_settings/avatar')?>">Change Avatar</a>
                        </li>
                        <li class="<?=$this->uri->segment(4)=='change_password'?'active':''?>">
                            <a href="<?=base_url('settings/users/account_settings/change_password')?>">Change Password</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <!-- PERSONAL INFO TAB -->
                        <div class="tab-pane <?=$this->uri->segment(4)=='personal_info'?'active':''?>">
                            <?=form_open('settings/users/personal_info'); ?>
                                <div class="form-group">
                                    <label class="control-label">First Name<i class="required">*</i></label>
                                    <input type="text" id="txtfname" name="txtfname" class="form-control" value="<?=$user_details['firstname']?>" />
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Sur Name<i class="required">*</i></label>
                                    <input type="text" id="txtsname" name="txtsname" class="form-control" value="<?=$user_details['surname']?>" />
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Middle Name</label>
                                    <input type="text" id="txtmname" name="txtmname" class="form-control" value="<?=$user_details['middlename']?>" />
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Birthday<i class="required">*</i></label>
                                    <input type="text" class="form-control date-picker" data-date-format="dd-M-yyyy" data-date-viewmode="years" name="txtbday" id="txtbday" value="<?=$user_details['birthday']==''?'':date('d-M-Y',strtotime($user_details['birthday']))?>" readonly style="background-color: #fff;">
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Gender<i class="required">*</i></label>
                                    <div class="radio-list" id="optgender">
                                        <label class="radio-inline">
                                            <input type="radio" name="radgender" id="radfemale" value="f" <?=isset($user_details) ? $user_details['gender'] == 'f' ? 'checked' : '' : set_value('radgender') == 'f' ? 'checked' : ''?>> Female </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="radgender" id="radmale" value="m" <?=isset($user_details) ? $user_details['gender'] == 'm' ? 'checked' : '' : set_value('radgender') == 'm' ? 'checked' : ''?>> Male </label>
                                    </div>
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Email<i class="required">*</i></label>
                                    <input type="text" class="form-control" id="txtemail" name="txtemail" value="<?=$user_details['email']?>" />
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Username</label>
                                    <input type="text" class="form-control" id="txtusername" name="txtusername" value="<?=$user_details['username']?>" readonly />
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Security Question 1<i class="required">*</i></label>
                                    <select name="selquestion1" id="selquestion1" class="bs-select form-control">
                                    <option value="">Select Question</option>
                                        <?php foreach($questions as $q): ?>
                                            <option value="<?=$q['id']?>" <?=isset($user_details) ? $user_details['sec_ques_1'] == $q['id'] ? 'selected' : '' : set_value('selquestion') == $q['id'] ? 'selected' : ''?>><?=$q['question']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Security Answer 1<i class="required">*</i></label>
                                    <input type="text" class="form-control" id="txtansw1" name="txtanswer1" value="<?=$user_details['sec_answer_1']?>" />
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Security Question 2<i class="required">*</i></label>
                                    <select name="selquestion2" id="selquestion2" class="bs-select form-control">
                                    <option value="">Select Question</option>
                                        <?php foreach($questions as $q): ?>
                                            <option value="<?=$q['id']?>" <?=isset($user_details) ? $user_details['sec_ques_2'] == $q['id'] ? 'selected' : '' : set_value('selquestion') == $q['id'] ? 'selected' : ''?>><?=$q['question']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Security Answer 2<i class="required">*</i></label>
                                    <input type="text" class="form-control" id="txtansw2" name="txtanswer2" value="<?=$user_details['sec_answer_2']?>" />
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Security Question 3<i class="required">*</i></label>
                                    <select name="selquestion3" id="selquestion3" class="bs-select form-control">
                                    <option value="">Select Question</option>
                                        <?php foreach($questions as $q): ?>
                                            <option value="<?=$q['id']?>" <?=isset($user_details) ? $user_details['sec_ques_3'] == $q['id'] ? 'selected' : '' : set_value('selquestion') == $q['id'] ? 'selected' : ''?>><?=$q['question']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Security Answer 3<i class="required">*</i></label>
                                    <input type="text" class="form-control" id="txtansw3" name="txtanswer3" value="<?=$user_details['sec_answer_3']?>" />
                                    <span class="help-block"></span> </div>
                                <div class="margiv-top-10">
                                    <button type="submit" id="submit_profile" class="btn green"> Save Changes </button>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            <?=form_close()?>
                        </div>
                        <!-- END PERSONAL INFO TAB -->
                        <!-- CHANGE AVATAR TAB -->
                        <div class="tab-pane <?=$this->uri->segment(4)=='avatar'?'active':''?>">
                            <?=form_open_multipart('settings/users/upload_profile')?>
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="<?=base_url('assets/images/no_image.png')?>" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="profile_image" id="profile_image"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                    <div class="clearfix margin-top-10">
                                        <span class="label label-danger">NOTE! </span>
                                        <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                    </div>
                                </div>
                                <div class="margin-top-10">
                                    <button type="submit" class="btn green" id="btnsubmit-img" disabled> Submit </button>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                        <!-- END CHANGE AVATAR TAB -->
                        <!-- CHANGE PASSWORD TAB -->
                        <div class="tab-pane <?=$this->uri->segment(4)=='change_password'?'active':''?>">
                            <?=form_open('settings/users/change_password'); $err_pw = $this->session->flashdata('error'); ?>
                                <div class="form-group <?=$err_pw==''?'':'has-error'?>">
                                    <label class="control-label">Current Password<i class="required">*</i></label>
                                    <input type="password" id="txtcurpass" name="txtcurpass" class="form-control" value="<?=$this->session->flashdata('currpass')?>" /> 
                                    <span class="help-block"><?=$err_pw?></span> </div>
                                <div class="form-group">
                                    <label class="control-label">New Password<i class="required">*</i></label>
                                    <input type="password" id="txtnewpass" name="txtnewpass" class="form-control" /> 
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Re-type New Password<i class="required">*</i></label>
                                    <input type="password" id="txtretpass" name="txtretpass" class="form-control" /> 
                                    <span class="help-block"></span> </div>
                                <div class="margin-top-10">
                                    <button type="submit" id="submit_change_pass" class="btn green"> Change Password </button>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            <?=form_close()?>
                        </div>
                        <!-- END CHANGE PASSWORD TAB -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->