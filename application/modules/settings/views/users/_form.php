<?php
echo set_title('Settings-Users');
echo load_plugin('css',array('select','datetime-picker'));
$viewactions = false;
$isdeleted = false;
if($action == 'view'){
    $viewactions = isset($arrData) ? $arrData['is_delete'] != 1 ? true : false : false;
    $isdeleted = isset($arrData) ? $arrData['is_delete'] == 1 ? true : false : false;
}
?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><?=$isdeleted?'Deleted User':$u_status?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?=base_url('settings/users')?>">Home</a>
        </li>
        <li>
            <a href="<?=base_url($isdeleted?'settings/users/deleted_users':'settings/users')?>">Users</a>
        </li>
        <li class="active"><?=$isdeleted?'Deleted':str_replace('user', '', strtolower($u_status))?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-blue sbold uppercase"><?=$isdeleted?'Deleted':$action?> User</span>
                </div>
                <?php if($viewactions): ?>
                <div class="actions">
                    <a href="<?=base_url('settings/users/edit_user/').$this->uri->segment(4)?>" class="btn green" style="margin-right: 3px;">
                        <i class="fa fa-edit"></i> Edit</a>
                    <a class="btn red" data-toggle="modal" href="#delete-modal">
                        <i class="icon-trash"></i> Delete</a>
                </div>
                <?php endif;?>
            </div>
            <div class="portlet-body form">
                <?=form_open($action=='add'?'settings/users/new_user':'settings/users/edit_user/'.$this->uri->segment(4), array('method' => 'post', 'role' => 'form', 'class' => 'form-horizontal'))?>
                    <input type="hidden" id="txtaction" value="<?=$action?>">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">First Name</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtfname" id="txtfname"
                                    value="<?=isset($arrData)?$arrData['firstname']:set_value('txtfname')?>">
                                <span class="help-block"> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Surname</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtsname" id="txtsname"
                                    value="<?=isset($arrData)?$arrData['surname']:set_value('txtsname')?>">
                                <span class="help-block"> </span>
                            </div>
                        </div>
                        <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                            <label class="col-md-3 control-label">Middle Name</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtmname" id="txtmname"
                                    value="<?=isset($arrData)?$arrData['middlename']:set_value('txtmname')?>">
                                <span class="help-block"> </span>
                            </div>
                        </div>
                        <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                            <label class="col-md-3 control-label">Birthday</label>
                            <div class="col-md-7 input-group input-medium date date-picker" id="dtbdate"
                                data-date-format="dd-M-yyyy" data-date-viewmode="years" style="padding-left: 16px;">
                                <input type="text" class="form-control" name="txtbday" id="txtbday" value="<?=isset($arrData)? date('d-M-Y',strtotime($arrData['birthday'])) :set_value('txtbday')?>" readonly <?=$action=='edit'?'style="background-color: #fff;"':''?>>
                                <span class="input-group-btn">
                                    <button class="btn default" type="button" id="btnbdate">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                                <span class="help-block" style="display: table-row;line-height: 2;"></span>
                            </div>
                        </div>
                        <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                            <label class="col-md-3 control-label">Gender</label>
                            <div class="radio-list col-md-7" id="optgender">
                                <label class="radio-inline">
                                    <input type="radio" name="radgender" id="radfemale" value="f" <?=isset($arrData) ? $arrData['gender'] == 'f' ? 'checked' : '' : set_value('radgender') == 'f' ? 'checked' : ''?> <?=$action=='view' ? 'disabled' : ''?>> Female </label>
                                <label class="radio-inline">
                                    <input type="radio" name="radgender" id="radmale" value="m" <?=isset($arrData) ? $arrData['gender'] == 'm' ? 'checked' : '' : set_value('radgender') == 'm' ? 'checked' : ''?> <?=$action=='view' ? 'disabled' : ''?>> Male </label>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group <?=isset($error_email) ? 'has-error' : ''?>">
                            <label class="col-md-3 control-label">Email</label>
                            <div class="col-md-7">
                                <input type="email" class="form-control" <?=$action=='view'?'disabled':''?> name="txtemail" id="txtemail"
                                    value="<?=isset($arrData)?$arrData['email']:set_value('txtemail')?>">
                                <span class="help-block"><?=isset($error_email) ? $error_email : ''?></span>
                            </div>
                        </div>
                        <div class="form-group <?=isset($error) ? 'has-error' : ''?>">
                            <label class="col-md-3 control-label">Username</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" <?=$action!='add'?'disabled':''?> name="txtuname" id="txtuname"
                                    value="<?=isset($arrData)?$arrData['username']:set_value('txtuname')?>">
                                <span class="help-block"><?=isset($error) ? $error : ''?></span>
                            </div>
                        </div>

                        <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                            <label class="col-md-3 control-label">Security Question 1</label>
                            <div class="col-md-7">
                                <select name="selquestion1" id="selquestion1" class="bs-select form-control" <?=$action=='view' ? 'disabled' : ''?>>
                                    <option value="">Select Question</option>
                                    <?php foreach($questions as $q): ?>
                                        <option value="<?=$q['id']?>" <?=isset($arrData) ? $arrData['sec_ques_1'] == $q['id'] ? 'selected' : '' : set_value('selquestion') == $q['id'] ? 'selected' : ''?>><?=$q['question']?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"> </span>
                            </div>
                        </div>
                        <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                            <label class="col-md-3 control-label">Security Answer 1</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtanswer1" id="txtanswer1"
                                    value="<?=isset($arrData)?$arrData['sec_answer_1']:set_value('txtanswer1')?>">
                                <span class="help-block"> </span>
                            </div>
                        </div>

                        <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                            <label class="col-md-3 control-label">Security Question 2</label>
                            <div class="col-md-7">
                                <select name="selquestion2" id="selquestion2" class="bs-select form-control" <?=$action=='view' ? 'disabled' : ''?>>
                                    <option value="">Select Question</option>
                                    <?php foreach($questions as $q): ?>
                                        <option value="<?=$q['id']?>" <?=isset($arrData) ? $arrData['sec_ques_2'] == $q['id'] ? 'selected' : '' : set_value('selquestion') == $q['id'] ? 'selected' : ''?>><?=$q['question']?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"> </span>
                            </div>
                        </div>
                        <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                            <label class="col-md-3 control-label">Security Answer 2</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtanswer2" id="txtanswer2"
                                    value="<?=isset($arrData)?$arrData['sec_answer_2']:set_value('txtanswer2')?>">
                                <span class="help-block"> </span>
                            </div>
                        </div>

                        <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                            <label class="col-md-3 control-label">Security Question 3</label>
                            <div class="col-md-7">
                                <select name="selquestion3" id="selquestion3" class="bs-select form-control" <?=$action=='view' ? 'disabled' : ''?>>
                                    <option value="">Select Question</option>
                                    <?php foreach($questions as $q): ?>
                                        <option value="<?=$q['id']?>" <?=isset($arrData) ? $arrData['sec_ques_3'] == $q['id'] ? 'selected' : '' : set_value('selquestion') == $q['id'] ? 'selected' : ''?>><?=$q['question']?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"> </span>
                            </div>
                        </div>
                        <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                            <label class="col-md-3 control-label">Security Answer 3</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" <?=$action=='view'?'disabled':''?> name="txtanswer3" id="txtanswer3"
                                    value="<?=isset($arrData)?$arrData['sec_answer_3']:set_value('txtanswer3')?>">
                                <span class="help-block"> </span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-md-3 control-label">User Type</label>
                            <div class="col-md-7">
                                <select class="form-control bs-select" <?=$action=='view'?'disabled':''?> name="selutype" id="selutype">
                                    <?php 
                                        if(count($arruserType)>0):
                                            echo '<option value="">-- SELECT USER TYPE --</option>';
                                            foreach($arruserType as $utype):
                                                $selected = isset($arrData) ? $utype['access_level_id'] == $arrData['access_level_id'] ? 'selected' : '' : (set_value('selutype') == $utype['access_level_id'] ? 'selected' : '');
                                                echo '<option value="'.$utype['access_level_id'].'" '.$selected.'>'.strtoupper($utype['access_level_name']).'</option>';
                                            endforeach;
                                        else:
                                            echo '<option value="">-- NO USER TYPE --</option>';
                                        endif;
                                     ?>
                                </select>
                                <span class="help-block"> </span>
                            </div>
                        </div>

                        <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-7">
                                <div class="checkbox-list">
                                    <label>
                                        <input type="checkbox" <?=$action=='view'?'disabled':''?> name="chkisactive"
                                            <?=isset($arrData)?$arrData['is_active']==1?'checked':'':''?>> is Active </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions" <?=$action=='view'?'hidden':''?>>
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" id="btnsubmit-user" class="btn green">Submit</button>
                                <a href="<?=base_url('settings/users/view_user/').$this->uri->segment(4)?>" class="btn default">Cancel</a>
                            </div>
                        </div>
                    </div>
                <?=form_close()?>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- BEGIN DELETE MODAL -->
<div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <?=form_open('settings/users/delete_user/'.$this->uri->segment(4))?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete User</h4>
            </div>
            <div class="modal-body"> Are you sure you want to delete this user? </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green">Yes</button>
            </div>
        </div>
        <?=form_close()?>
    </div>
</div>
<!-- END DELETE MODAL -->

<?=template_footer()?>
<?=load_plugin('js',$action != 'view' ? array('select','datetime-picker','jquery-validation') : array('modals'))?>

<script src="<?=base_url('assets/js/settings/users/validation.js')?>"></script>