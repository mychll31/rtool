<?=set_title('Settings-Users');load_plugin('css',array('datatables'))?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><?=$u_status?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">users</a>
        </li>
        <li class="active"><?php echo str_replace('users', '', strtolower($u_status)) ?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"> User List</span>
                </div>
            </div>
            <div class="portlet-body">
            	<div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblall-reports" style="visibility: hidden;">
                    <thead>
                        <tr>
                            <th style="width:7% !important"> No </th>
                            <th> User ID </th>
                            <th> Name </th>
                            <th> Last Login </th>
                            <th> Email </th>
                            <th> User Type </th>
                            <th style="width: 60px;" <?=isset($action) ? $action=='deleted' ? 'hidden' : '' : ''?>> </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($arrUsers) > 0): foreach($arrUsers as $user): ?>
                            <tr class="odd gradeX" data-id="<?=$user['userid']?>">
                                <td><?=$no++?></td>
                                <td><?=$user['custom_userid']?></td>
                                <td><?=getFullname($user['firstname'], $user['surname'], $user['middlename'])?></td>
                                <td><?=$user['last_login']?></td>
                                <td><?=$user['email']?></td>
                                <td><?=strtoupper($user['access_level_name'])?></td>
                                <td align="center" <?=isset($action) ? $action=='deleted' ? 'hidden' : '' : ''?>>
                                    <a href="<?=base_url('settings/users/view_user/').$user['userid']?>" class="btn blue btn-sm">
                                        <i class="icon-magnifier"></i> View </a>
                                </td>
                            </tr>
                        <?php endforeach; endif;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?=template_footer()?>
<?=load_plugin('js',array('datatables'))?>

<script>
    $(document).ready(function() {
        $('#tblall-reports').dataTable( {
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblall-reports').css('visibility', 'visible');
            }} );
    });
</script>