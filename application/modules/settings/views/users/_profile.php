<?=load_plugin('css',array('profile','select','select2','datetime-picker'))?>
<style type="text/css">
    .page-content { background-color: #f3f3f3 !important; }
    .feeds li .col1 { width: 84%; }
    .feeds li .col2 { width: 25%; }
</style>
<!-- BEGIN PAGE TITLE-->
<h3 class="page-title"> Profile
    <small></small>
</h3>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="<?=base_url('uploads/profiles/'.$_SESSION['image'])?>" class="img-responsive" alt=""> </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> <?=fixfullname($user_details['firstname'], $user_details['surname'], $user_details['middlename'])?> </div>
                    <div class="font-blue-madison"> @<?=$user_details['username']?> </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="<?=$content_page == '_content_log' ? 'active' : ''?>">
                            <a href="<?=base_url('settings/users/profile')?>">
                                <i class="icon-home"></i> Overview </a>
                        </li>
                        <li class="<?=$content_page == '_content_acct_settings' ? 'active' : ''?>">
                            <a href="<?=base_url('settings/users/account_settings/personal_info')?>">
                                <i class="icon-settings"></i> Account Settings </a>
                        </li>
                        <li class="<?=$content_page == '_content_help' ? 'active' : ''?>">
                            <a href="<?=base_url('settings/users/help')?>">
                                <i class="icon-info"></i> Help </a>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- BEGIN PORTLET -->
            <div class="portlet light " <?=$content_page == '_content_log' ? '' : 'hidden'?>>
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="uppercase profile-stat-title"> <?=$total_projects?> </div>
                        <div class="uppercase profile-stat-text"> Projects </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="uppercase profile-stat-title"> <?=$total_shared?> </div>
                        <div class="uppercase profile-stat-text"> Shared </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="uppercase profile-stat-title"> <?=$total_drafts?> </div>
                        <div class="uppercase profile-stat-text"> Drafts </div>
                    </div>
                </div>
                <!-- END STAT -->
            </div>
            <!-- END PORTLET -->
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <?=$this->load->view($content_page) ?>
    </div>
</div>
<?=load_plugin('js',array('profile','select','select2','datetime-picker','jquery-validation'))?>
<script src="<?=base_url('assets/js/settings/users/account_settings_validation.js')?>"></script>