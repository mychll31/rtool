<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET -->
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Groups</span>
                    </div>
                    <div class="btn-group pull-right">
                        <a href="<?=base_url('settings/users/new_group')?>" class="btn blue btn-sm">
                            <i class="icon-plus"></i> Create New
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-pane active" id="tab_1_1">
                        <div class="scroller" style="height: 490px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                            <table class="table table-bordered" id="tblgroup">
                                <thead>
                                    <tr>
                                        <th>NAME</th>
                                        <th>MEMBERS</th>
                                        <th width="10px;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($groups as $group) {
                                        echo '<tr><td><b>'.$group['group_name'].'</b><br><small>Created by: <i>'.getProfileName($group['created_by']).'</i></small></td><td>';
                                            foreach($group['members'] as $member) {
                                                echo '<li>'.getFullname($member['firstname'], $member['surname'], $member['middlename']).'</li>';
                                            }
                                        echo '</td><td nowrap align="center">';
                                        if($group['created_by'] == $_SESSION['userid']){
                                            echo '<a class="btn btn-sm green" href="'.base_url('settings/users/group_edit/'.$group['group_id']).'">
                                                    <i class="fa fa-pencil"></i> Edit</a>

                                                    <a class="btn btn-sm red group-remove" href="javascript:;" data-groupid="'.$group['group_id'].'">
                                                    <i class="fa fa-remove"></i> Remove</a>';
                                        }
                                        echo '</td></tr>'; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PORTLET -->
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->

<!-- begin error message modal -->
<div class="modal fade bs-modal-sm" id="error-message-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="error-message-modal-title">
                    Remove Group
                </h4>
            </div>
            <div class="modal-body" id="error-message-modal-body">
                Are you sure you want to remove this group?
            </div>
            <div class="modal-footer">
                <?=form_open('settings/users/remove_group')?>
                    <input type="hidden" id="txtgroupid" name="txtgroupid">
                    <button type="submit" class="btn blue">Okay</button>
                    <a type="submit" class="btn dark" data-dismiss="modal">Close</a>
                <?=form_close()?>
            </div>
        </div>
    </div>
</div>
<!-- end error message modal -->