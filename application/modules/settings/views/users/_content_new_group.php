<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET -->
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Groups</span>
                    </div>
                    <div class="btn-group pull-right">
                        <a href="<?=base_url('settings/users/group')?>" class="btn grey-cascade btn-sm">
                            <i class="icon-users"></i> List
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-pane active" id="tab_1_1">
                        <div class="scroller" style="height: 490px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                            <?=form_open(); ?>
                                <textarea id="txtuser" name="txtuser" hidden></textarea>
                                <div class="form-group">
                                    <label class="control-label">Name <i class="required">*</i></label>
                                    <input type="text" id="txtgroupname" name="txtgroupname" class="form-control" value="<?=isset($group_details) ? $group_details['group_name'] : ''?>" />
                                    <span class="help-block"></span> </div>
                                <div class="form-group">
                                    <label class="control-label">Members</label>
                                    <ul id="list-members">
                                        <?php 
                                            if(isset($group_details)) {
                                                foreach($group_details['members'] as $member) {
                                                    echo '<li>'.getFullname($member['firstname'], $member['surname'], $member['middlename']).' <a class="seluser-close font-red" data-userid="'.$member['userid'].'"><i class="fa fa-close"></i></a></li>';
                                                }
                                            } ?>
                                    </ul>
                                    <select id="seluser" class="form-control select2">
                                        <option value="0">Nothing to Select Yet</option>
                                        <?php
                                            foreach($users as $user) {
                                                if($user['userid'] != $_SESSION['userid']) { 
                                                    echo '<option value="'.$user['userid'].'">'.getFullname($user['firstname'], $user['surname'], $user['middlename']).'</option>'; }
                                            } ?>
                                    </select>
                                </div>
                                <div class="margiv-top-10">
                                    <button type="submit" id="submit_group" class="btn blue"> Save </button>
                                    <a href="<?=base_url('settings/users/group')?>" class="btn default"> Cancel </a>
                                </div>
                            <?=form_close()?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PORTLET -->
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->