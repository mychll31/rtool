<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET -->
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Logs</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-pane active" id="tab_1_1">
                        <div class="scroller" style="height: 490px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                            <ul class="feeds">
                                <?php foreach($logs as $log): ?>
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <?php switch ($log['log_status']) {
                                                        case 1:?>
                                                    <div class="label label-sm label-success">
                                                        <i class="fa fa-info"></i>
                                                    </div>
                                                <?php break;
                                                         case 2: ?>
                                                    <div class="label label-sm label-warning">
                                                        <i class="fa fa-warning"></i>
                                                    </div>
                                                <?php break;
                                                         case 3: ?>
                                                    <div class="label label-sm label-danger">
                                                        <i class="fa fa-bolt"></i>
                                                    </div>
                                                <?php break; } ?>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc">
                                                    <?=$_SESSION['userid']==$log['log_usr_id'] ? 'You ' : ''?>
                                                    <?=$log['log_desc']?>
                                                    <small class="text-warning pull-right"><?=date('F d, Y h:i:s A', strtotime($log['log_date']))?></small>        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PORTLET -->
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->