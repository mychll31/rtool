<style type="text/css">
    li.notif-unread {
        background-color: #cacaca;
        color: #575757;
    }
    .feeds li .col2>.date
    {
        color: #575757;
        width: 102%;
    }
</style>
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET -->
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">NOTIFICATIONS</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div style="margin-bottom: 10px;"><a href="<?=base_url('settings/users/read_notif?id=0')?>" class="btn btn-xs btn-info"> <i class="glyphicon glyphicon-envelope"></i> Read all</a></div>
                    <div class="tab-pane active" id="tab_1_1">
                        <div class="scroller" style="height: 490px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                            <ul class="feeds">
                                <?php foreach(notifications(2) as $notif): ?>
                                <li class="<?=$notif['is_unread'] ? 'notif-unread' : ''?>">
                                    <?php if($notif['is_unread']): ?>
                                        <a href="<?=base_url('settings/users/read_notif?id='.$notif['notif_id'])?>"><?php endif ?>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-<?=$notif['alert_type']?>">
                                                            <i class="<?=$notif['type_icon']?>"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> <?=$notif['notif_message']?> </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> <?=date('F d, Y h:i:s A', strtotime($notif['datetime']))?>
                                                    <?php 
                                                        if($notif['is_unread']){
                                                            echo '<i class="fa fa-envelope"></i>';
                                                        } else {
                                                            echo '<i class="icon-envelope-open"></i>';
                                                        }
                                                     ?>
                                                </div>
                                            </div>
                                        <?=$notif['is_unread'] ? '</a>' : ''?>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PORTLET -->
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->