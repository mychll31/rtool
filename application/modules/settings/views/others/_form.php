<?=set_title('Settings-Security Questions');load_plugin('css',array('select'))?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1><?=$q_status?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?=base_url('settings/security_questions')?>">Home</a>
        </li>
        <li class="active"><?php echo str_replace('question', '', strtolower($q_status)) ?></li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-blue sbold uppercase"><?=$action?> Question</span>
                </div>
                <?php if($action=='view'): ?>
                <div class="actions">
                    <a href="<?=base_url('settings/edit_question/').$this->uri->segment(3)?>" class="btn green" style="margin-right: 3px;">
                        <i class="fa fa-edit"></i> Edit</a>
                    <a class="btn red" data-toggle="modal" href="#delete-modal">
                        <i class="icon-trash"></i> Delete</a>
                </div>
                <?php endif;?>
            </div>
            <div class="portlet-body form">
                <?=form_open($action == 'add' ? '' : 'settings/edit_question/'.$this->uri->segment(3), array('class' => 'form-horizontal'))?>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Question</label>
                            <div class="col-md-7">
                                <textarea class="form-control" <?=$action=='view'?'disabled':''?> name="txtquestion" id="txtquestion"><?=isset($arrData) ? $arrData['question'] : set_value('txtquestion')?></textarea>
                                <span class="help-block"> </span>
                            </div>
                        </div>
                        <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-7">
                                <div class="checkbox-list">
                                    <label>
                                        <input type="checkbox" <?=$action=='view'?'disabled':''?> <?=isset($arrData) ? $arrData['is_active'] == 1 ? 'checked' : '' : ''?> name="chkisactive">
                                            is Active </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions" <?=$action=='view'?'hidden':''?>>
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" id="btnsubmit-question" class="btn green">Submit</button>
                                <a href="<?=base_url('settings/security_questions')?>" class="btn default">Cancel</a>
                            </div>
                        </div>
                    </div>
                <?=form_close()?>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- BEGIN DELETE MODAL -->
<div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <?=form_open('system_config/questions/delete/'.$this->uri->segment(3))?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Question</h4>
            </div>
            <div class="modal-body"> Are you sure you want to delete this question? </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green">Yes</button>
            </div>
        </div>
        <?=form_close()?>
    </div>
</div>
<!-- END DELETE MODAL -->

<?=template_footer()?>
<?=load_plugin('js',array('select','modals'))?>

<script>
    $(document).ready(function() {
        $('#txtquestion').on('keyup change', function(){
            check_null($('#txtquestion'));
        });

        $('#btnsubmit-question').click(function(e){
            var errctr = 0;
            errctr = errctr + check_null($('#txtquestion'));

            if(errctr > 0){
                e.preventDefault();
            }
        });
    });
</script>