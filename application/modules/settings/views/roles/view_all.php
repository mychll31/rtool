<?=set_title('Settings-Roles');load_plugin('css',array('datatables'))?>
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
    <h1>All</h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li class="active">All</li>
    </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"> Role List</span>
                </div>
            </div>
            <div class="portlet-body scrollable" style="height: 300px;">
            	<div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblall-roles" style="visibility: hidden;">
                    <thead>
                        <tr>
                            <th style="width: 250px;"> Module name </th>
                            <th> Role name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($usertypes as $utype): ?>
                        <tr>
                            <td> <?=strtoupper($utype['access_level_name'])?> </td>
                            <td>
                                <label class="col-md-4"><input type="checkbox" checked disabled> <?=$utype['access_level_id']?></label>
                                <label class="col-md-4"><input type="checkbox" checked disabled> Role e</label>
                                <label class="col-md-4"><input type="checkbox" checked disabled> Role c</label>
                                <label class="col-md-4"><input type="checkbox" checked disabled> Role c</label>
                                <label class="col-md-4"><input type="checkbox" checked disabled> Role a</label>
                                <label class="col-md-4"><input type="checkbox" checked disabled> Role c</label>
                                <label class="col-md-4"><input type="checkbox" checked disabled> Role b</label>
                                <label class="col-md-4"><input type="checkbox" checked disabled> Role a</label>
                                <label class="col-md-4"><input type="checkbox" checked disabled> Role b</label>
                                <label class="col-md-4"><input type="checkbox" checked disabled> Role e</label>
                                <label class="col-md-4"><input type="checkbox" checked disabled> Role e</label>
                                <label class="col-md-4"><input type="checkbox" checked disabled> Role a</label>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?=template_footer()?>
<?=load_plugin('js',array('datatables'))?>

<script>
    $(document).ready(function() {
        $('#tblall-roles').dataTable( {
            "pageLength": 5,
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblall-roles').css('visibility', 'visible');
            }} );
    });
</script>