<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Securityquestions_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'tblsecurity_questions';
	}

	function getData($isactive=null, $isdelete=null, $quesid=null)
	{
		$conditions = array();
		if($isactive!==null):
			array_push($conditions, $this->table.'.is_active='.$isactive);
		endif;

		if($isdelete!==null):
			array_push($conditions, $this->table.'.is_delete='.$isdelete);
		endif;

		if($quesid!==null):
			array_push($conditions, $this->table.'.id='.$quesid);
		endif;
		
		$str_conditions = implode(' AND ', $conditions);

		if($str_conditions!=''):
			$arrRes = $this->db->order_by('question')->get_where($this->table, $str_conditions)->result_array();
		else:
			$arrRes = $this->db->order_by('question')->get($this->table)->result_array();
		endif;
		
		if($quesid!=null):
			return $arrRes[0];
		else:
			return $arrRes;
		endif;
	}

	function isQuestionExist($question,$quesid=0)
	{
		if($quesid==0):
			$arrRes = $this->db->get_where($this->table, array('question' => $question, 'is_active' => 1, 'is_delete' => 0))->result_array();
		else:
			$arrRes = $this->db->get_where($this->table, array('question' => $question, 'is_active' => 1, 'is_delete' => 0, 'id!=' => $quesid))->result_array();
		endif;
		return count($arrRes);
	}

	function addquestion($arrData)
	{
		$this->db->insert($this->table, $arrData);
		return $this->db->insert_id();
	}

	function editquestion($arrData, $id)
	{
		$this->db->where('id',$id);
		$this->db->update($this->table, $arrData);
		return $this->db->affected_rows();
	}

	# get data by field
	// function getDataByField($field, $value, $isactive=null, $isdelete=null)
	// {
	// 	$conditions = array();
	// 	if($isactive!==null):
	// 		array_push($conditions, $this->table.'.is_active='.$isactive);
	// 	endif;
		
	// 	if($isdelete!==null):
	// 		array_push($conditions, $this->table.'.is_delete='.$isdelete);
	// 	endif;

	// 	array_push($conditions, $field.'='.$value);
		
	// 	$str_conditions = implode(' AND ', $conditions);

	// 	if($str_conditions!=''):
	// 		return $this->db->order_by('name')->get_where($this->table, $str_conditions)->result_array();
	// 	endif;
	// }

	// // function add($arrData)
	// // {
	// // 	$this->db->insert($this->table, $arrData);
	// // 	$id = $this->db->insert_id();
	// // 	log_action($this->db->last_query(), 'Add Question');
	// // 	return $id;		
	// // }

	// function edit($arrData, $id)
	// {
	// 	$this->db->where('type_id',$id);
	// 	$this->db->update($this->table, $arrData);
	// 	return $this->db->affected_rows();
	// }

	// // function delete($id)
	// // {
	// // 	$json = json_encode($this->db->get_where($this->table, array('id' => $id))->result_array());
	// // 	$this->db->where('id', $id);
	// // 	$this->db->delete($this->table);
	// // 	log_action($this->db->last_query(), 'Delete Question', $json);
	// // 	return $this->db->affected_rows();	
	// // }

	// // function getData($id=0, $module=null)
	// // {
	// // 	$cond1 = array();
	// // 	if($module!=null):
	// // 		$cond1 = array('module' => $module);
	// // 	endif;

	// // 	if($id==0):
	// // 		return $this->db->get_where($this->table, $cond1)->result_array();
	// // 	else:
	// // 		$result = $this->db->get_where($this->table, array('id' => $id))->result_array();
	// // 		return $result[0];
	// // 	endif;
	// // }

}