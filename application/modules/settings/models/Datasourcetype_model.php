<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Datasourcetype_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'tbldata_source_type';
	}

	function getData($isactive=null, $dbtypeid=null)
	{
		$conditions = array();
		if($isactive!==null):
			array_push($conditions, $this->table.'.is_active='.$isactive);
		endif;

		if($dbtypeid!==null && $dbtypeid!==0):
			array_push($conditions, $this->table.'.type_id='.$dbtypeid);
		endif;
		
		$str_conditions = implode(' AND ', $conditions);

		if($str_conditions!=''):
			return $this->db->order_by('name')->get_where($this->table, $str_conditions)->result_array();
		else:
			return $this->db->order_by('name')->get($this->table)->result_array();
		endif;
	}

	# get data by field
	function getDataByField($field, $value, $isactive=null)
	{
		$conditions = array();
		if($isactive!==null):
			array_push($conditions, $this->table.'.is_active='.$isactive);
		endif;

		if($field!= '' && $value!= ''):
			array_push($conditions, $field.'='."'".$value."'");
		endif;
		
		$str_conditions = implode(' AND ', $conditions);

		if($str_conditions!=''):
			return $this->db->order_by('name')->get_where($this->table, $str_conditions)->result_array();
		endif;
	}

	// function add($arrData)
	// {
	// 	$this->db->insert($this->table, $arrData);
	// 	$id = $this->db->insert_id();
	// 	log_action($this->db->last_query(), 'Add Question');
	// 	return $id;		
	// }

	function edit($arrData, $id)
	{
		$this->db->where('type_id',$id);
		$this->db->update($this->table, $arrData);
		return $this->db->affected_rows();
	}

	// function delete($id)
	// {
	// 	$json = json_encode($this->db->get_where($this->table, array('id' => $id))->result_array());
	// 	$this->db->where('id', $id);
	// 	$this->db->delete($this->table);
	// 	log_action($this->db->last_query(), 'Delete Question', $json);
	// 	return $this->db->affected_rows();	
	// }

	// function getData($id=0, $module=null)
	// {
	// 	$cond1 = array();
	// 	if($module!=null):
	// 		$cond1 = array('module' => $module);
	// 	endif;

	// 	if($id==0):
	// 		return $this->db->get_where($this->table, $cond1)->result_array();
	// 	else:
	// 		$result = $this->db->get_where($this->table, array('id' => $id))->result_array();
	// 		return $result[0];
	// 	endif;
	// }

}