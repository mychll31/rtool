<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->users = 'tblusers';
		$this->profile = 'tblprofiles';
		$this->question = 'tblsecurity_questions';
		$this->access = 'tblaccess_level';
		$this->logs = 'tbllogs';
		$this->user_group = 'tbluser_group';
		$this->groups = 'tblgroups';
		$this->tableshared = 'tblprojects_shared';
	}

	function checkemail_exist($email)
	{
		$result = $this->db->get_where($this->profile, array('email' => $email))->result_array();
		return count($result) > 0 ? $result[0]['userid'] : 0;
	}
	
	function adduser($arrData)
	{
		$this->db->insert($this->users, $arrData);
		$id = $this->db->insert_id();
		return $id;		
	}

	function addprofile($arrData)
	{
		$this->db->insert($this->profile, $arrData);
		return $this->db->insert_id();		
	}

	function getUserByUsername($username)
	{
		$result = $this->db->get_where($this->users, array('username' => $username))->result_array();
		return count($result) > 0 ? $result[0] : 0;
	}

	function getLog($userid)
	{
		return $this->db->order_by('log_date','desc')->get_where($this->logs, array('log_usr_id' => $userid))->result_array();
	}

	function getUserByEmail($email)
	{
		$result = $this->db->get_where($this->profile, array('email' => $email))->result_array();
		return count($result) > 0 ? $result[0] : 0;
	}

	function checkUserByEmail($email,$userid)
	{
		$this->db->where('email',$email);
		$this->db->where('userid!=',$userid);
		$res = $this->db->get($this->profile)->result_array();
		return $res;
	}
	
	function getData($isactive=null, $isdelete=null, $userid=null)
	{
		$conditions = array();
		if($isactive!==null):
			array_push($conditions, $this->users.'.is_active='.$isactive);
		endif;
		
		if($isdelete!==null):
			array_push($conditions, $this->profile.'.is_delete='.$isdelete);
		endif;

		if($userid!==null):
			array_push($conditions, $this->users.'.userid='.$userid);
			$this->db->limit(1);
		endif;
		
		$str_conditions = implode(' AND ', $conditions);
		
		if($str_conditions==''):
			$this->db->join($this->access, $this->access.'.access_level_id='.$this->users.'.access_level', 'left');
			$this->db->join($this->profile, $this->profile.'.userid='.$this->users.'.userid', 'left');
			return $this->db->get($this->users)->result_array();
		else:
			if($userid!==null):
				$this->db->select($this->users.'.*,'.$this->profile.'.*,access_level_id');
			endif;
			$this->db->join($this->access, $this->access.'.access_level_id='.$this->users.'.access_level', 'left');
			$this->db->join($this->profile, $this->profile.'.userid='.$this->users.'.userid', 'left');
			$arrRes = $this->db->get_where($this->users, $str_conditions)->result_array();
			if($userid!==null):
				return $arrRes[0];
			else:
				return $arrRes;
			endif;
		endif;
	}

	function getQuestionData($id=0)
	{
		if($id==0):
			return $this->db->get($this->question)->result_array();
		else:
			$result = $this->db->get_where($this->question, array('id' => $id))->result_array();
			return $result[0];
		endif;
	}

	function getuserType()
	{
		return $this->db->get($this->access)->result_array();
	}

	function edituser($arrData, $id)
	{
		$this->db->where('userid',$id);
		$this->db->update($this->users, $arrData);
		return $this->db->affected_rows();
	}

	function editprofile($arrData, $id)
	{
		$json_data = json_encode($this->getData(null,null,$id));
		$this->db->where('userid',$id);
		$this->db->update($this->profile, $arrData);
		$rows = $this->db->affected_rows();
		$rows1 = $this->db->error();
		if($rows > 0) {
			log_action($this->db->last_query(),'Update Profile',$json_data,1);
		}
		return true;
	}

	function delete($id)
	{
		$json = json_encode($this->db->get_where($this->users, array('id' => $id))->result_array());
		$this->db->where('id', $id);
		$this->db->delete($this->users);
		return $this->db->affected_rows();	
	}

	function deleteProfile($id)
	{
		$json = json_encode($this->db->get_where($this->profile, array('userid' => $id))->result_array());
		$this->db->where('userid', $id);
		$this->db->delete($this->profile);
		return $this->db->affected_rows();	
	}

	function getGroupByGroupId($groupid)
	{
		$arrgroups = array();
		$groups = $this->db->get_where($this->groups,array('group_id' => $groupid))->result_array();
		foreach($groups as $key=>$group)
		{
			$this->db->join($this->profile,$this->profile.'.userid = '.$this->user_group.'.user_id');
			$groups[$key]['members'] = $this->db->get_where($this->user_group,array('group_id' => $group['group_id']))->result_array();
		}

		return count($groups) > 0 ? $groups[0] : array();
	}

	function getGroupByAuthor($userid)
	{
		$this->db->where($this->groups.'.created_by',$userid);
		$this->db->or_where($this->user_group.'.user_id',$userid);
		$this->db->group_by('tblgroups.group_id');
		$this->db->join($this->groups,$this->groups.'.group_id='.$this->user_group.'.group_id');
		$groups = $this->db->get($this->user_group)->result_array();
		
		foreach($groups as $key=>$group)
		{
			$this->db->join($this->profile,$this->profile.'.userid = '.$this->user_group.'.user_id');
			$groups[$key]['members'] = $this->db->get_where($this->user_group,array('group_id' => $group['group_id']))->result_array();
		}

		return $groups;
	}

	function check_user_group_exist($groupid,$userid)
	{
		return $this->db->get_where($this->user_group,array('group_id' => $groupid, 'user_id' => $userid))->result_array();
	}

	function addgroup($arrData)
	{
		$this->db->insert($this->groups,$arrData);
		$inserted_id = $this->db->insert_id();
		log_action($this->db->last_query(),'Created a group','',1);
		return $inserted_id;		
	}

	function editgroup($arrData, $groupid)
	{
		$json_data = json_encode($this->db->get_where($this->groups,array('group_id' => $groupid))->result_array());
		$this->db->where('group_id',$groupid);
		$this->db->update($this->groups, $arrData);
		$rows = $this->db->affected_rows();
		$rows1 = $this->db->error();
		if($rows > 0) {
			log_action($this->db->last_query(),'Update a Group',$json_data,1);
		}
		return true;
	}

	function deletegroup_user($groupid,$userid=0)
	{
		if($userid > 0){
			$this->db->where('user_id', $userid);	
		}
		$this->db->where('group_id', $groupid);
		$this->db->delete($this->user_group);
		return $this->db->affected_rows();	
	}

	function remove_group($groupid)
	{
		$last_query = '';
		$json_data = array();

		$json_data[0] = $this->db->get_where($this->tableshared,array('shared_to_groupid' => $groupid))->result_array();
		$this->db->where('shared_to_groupid', $groupid);
		$this->db->delete($this->tableshared);
		$last_query .= $this->db->last_query();

		$json_data[1] = $this->db->get_where($this->user_group,array('group_id' => $groupid))->result_array();
		$this->deletegroup_user($groupid);
		$last_query .= $this->db->last_query();

		$json_data[2] = $this->db->get_where($this->groups,array('group_id' => $groupid))->result_array();
		$this->db->where('group_id', $groupid);
		$this->db->delete($this->groups);
		$last_query .= $this->db->last_query();

		log_action($last_query,'Remove a group',json_encode($json_data),1);

		return true;
	}

	function adduser_group($arrData)
	{
		$this->db->insert($this->user_group,$arrData);
		return $this->db->insert_id();
	}

	function notification_read($notifid)
	{
		if($notifid > 0)
		{
			$this->db->where('notif_id',$notifid);
			$this->db->update('tblnotifications', array('is_unread' => 0));
			return $this->db->affected_rows();
		} else {
			$this->db->update('tblnotifications', array('is_unread' => 0));
			return $this->db->affected_rows();
		}
	}
		
}
/* End of file User_model.php
 * Location: ./application/modules/libraries/models/User_model.php */