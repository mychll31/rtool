<?php 
class Users extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->data = array();
        checkSession();
        $this->data['no'] = 1;
        $this->load->model(array('User_model','projects/Project_model','user_login/Login_model'));
    }

    public function index()
    {
        $this->data['u_status'] = 'All Users';
        
        $this->data['arrUsers'] = $this->User_model->getData(1,0);
        $this->template->load('template/main-template', 'users/view_all', $this->data);
    }

    public function view_user()
    {
        $userid = $this->uri->segment(4);
        $this->data['arrData'] = $this->User_model->getData(null,null,$userid);
        $this->data['arruserType'] = $this->User_model->getuserType();

        $this->data['u_status'] = 'View user';
        $this->data['action'] = 'view';
        $this->data['questions'] = $this->User_model->getQuestionData();
        $this->template->load('template/main-template', 'users/_form', $this->data);
    }

    public function new_user()
    {
        $arrpost = $this->input->post();

        if(!empty($arrpost)):
            $isUnameExist = $this->User_model->getUserByUsername($arrpost['txtuname']);
            $isEmailExist = $this->User_model->checkemail_exist($arrpost['txtemail']);
            if($isUnameExist == 0):
                if($isEmailExist == 0):
                    $options = ['cost' => 12];
                    $password = default_password();

                    $userData = array(
                            'username'    => isset($arrpost['txtuname']) ? $arrpost['txtuname'] : '',
                            'password'    => password_hash($password, PASSWORD_BCRYPT, $options),
                            'access_level'=> isset($arrpost['selutype']) ? $arrpost['selutype'] : '');
                    $userid = $this->User_model->adduser($userData);
                    $custom_userid = $this->User_model->edituser(array('custom_userid' => set_custom_userid($userid)), $userid);
                    $profileData = array(
                            'userid'    => $userid,
                            'firstname' => isset($arrpost['txtfname']) ? $arrpost['txtfname'] : '',
                            'surname'   => isset($arrpost['txtsname']) ? $arrpost['txtsname'] : '',
                            'email'     => isset($arrpost['txtemail']) ? $arrpost['txtemail'] : '',
                            'added_by'  => $this->session->userdata('userid'),
                            'added_date'=> date('Y-m-d H:i:s'));
                    $this->User_model->addprofile($profileData);
                    # todo::send password to email
                    $this->session->set_flashdata('strSuccessMsg','New user added successfully. ['.$password.']');
                    redirect('settings/users/view_user/'.$userid);
                else:
                    $this->data['error_email'] = 'Email address already exists';
                endif;
            else:
                $this->session->set_flashdata('strErrorMsg','Username already exists.');
                $this->data['error'] = 1;
            endif;
        endif;

        $this->data['arruserType'] = $this->User_model->getuserType();
        $this->data['u_status'] = 'Add new user';
        $this->data['action'] = 'add';
        $this->template->load('template/main-template', 'users/_form', $this->data);
    }

    public function edit_user()
    {
        $userid = $this->uri->segment(4);
        $this->data['arrData'] = $this->User_model->getData(null,null,$userid);
        $this->data['arruserType'] = $this->User_model->getuserType();

        $arrpost = $this->input->post();
        if(!empty($arrpost)):
            $isEmailExist = $this->User_model->checkUserByEmail($arrpost['txtemail'],$userid);
            if(count($isEmailExist) == 0):
                $bday = date('Y-m-d', strtotime($arrpost['txtbday']));
                $userData['is_active'] = isset($arrpost['chkisactive']) ? 1 : 0;
                $userData['access_level'] = isset($arrpost['selutype']) ? $arrpost['selutype'] : '';
                $this->User_model->edituser($userData, $userid);

                $profileData = array(
                        'firstname'   => isset($arrpost['txtfname']) ? $arrpost['txtfname'] : '',
                        'surname'     => isset($arrpost['txtsname']) ? $arrpost['txtsname'] : '',
                        'middlename'  => isset($arrpost['txtmname']) ? $arrpost['txtmname'] : '',
                        'email'       => isset($arrpost['txtemail']) ? $arrpost['txtemail'] : '',
                        'birthday'    => isset($arrpost['txtbday']) ? $bday : '',
                        'gender'      => isset($arrpost['radgender']) ? $arrpost['radgender'] : '',
                        'sec_ques_1'       => isset($arrpost['selquestion1']) ? $arrpost['selquestion1'] : '',
                        'sec_answer_1'     => isset($arrpost['txtanswer1'])   ? $arrpost['txtanswer1'] : '',
                        'sec_ques_2'       => isset($arrpost['selquestion2']) ? $arrpost['selquestion2'] : '',
                        'sec_answer_2'     => isset($arrpost['txtanswer2'])   ? $arrpost['txtanswer2'] : '',
                        'sec_ques_3'       => isset($arrpost['selquestion3']) ? $arrpost['selquestion3'] : '',
                        'sec_answer_3'     => isset($arrpost['txtanswer3'])   ? $arrpost['txtanswer3'] : '',
                        'last_updated_by'  => $_SESSION['userid'],
                        'last_updated_date'=> date('Y-m-d H:i:s'));
                $this->User_model->editprofile($profileData, $userid);
                $this->Login_model->add_session($_SESSION['userid'], $_SESSION['user_level'], $_SESSION['user_name'], $profileData['surname'], $profileData['firstname'], $profileData['middlename'], $_SESSION['user_cid'], $_SESSION['image']);
                $this->session->set_flashdata('strSuccessMsg','User updated successfully.');
                redirect('settings/users/view_user/'.$userid);
            else:
                $this->data['error_email'] = 'Email address already exists';
            endif;
        endif;

        $this->data['u_status'] = 'Edit User';
        $this->data['action'] = 'edit';
        $this->data['questions'] = $this->User_model->getQuestionData();
        $this->template->load('template/main-template', 'users/_form', $this->data);
    }

    public function delete_user()
    {
        $userid = $this->uri->segment(4);
        $profileData = array(
                'is_delete'  => 1,
                'delete_by'  => 1,
                'delete_date'=> date('Y-m-d H:i:s'));
        $this->User_model->editprofile($profileData, $userid);
        $this->User_model->edituser(array('is_active' => 0), $userid);

        $this->session->set_flashdata('strSuccessMsg','User deleted successfully.');
        redirect('settings/users');
    }

    public function inactive_users()
    {
        $this->data['u_status'] = 'Inactive Users';
        $this->data['arrUsers'] = $this->User_model->getData(0,0);
        $this->template->load('template/main-template', 'users/view_all', $this->data);
    }

    public function deleted_users()
    {
        $this->data['u_status'] = 'Deleted Users';
        $this->data['arrUsers'] = $this->User_model->getData(null,1);
        $this->template->load('template/main-template', 'users/view_all', $this->data);
    }

    public function profile()
    {
        # active projects
        $this->data['total_projects'] = count($this->Project_model->getactiveprojectbyuserid($_SESSION['userid'],1));
        # shared projects
        $this->data['total_shared'] = count($this->Project_model->getsharedproject($_SESSION['userid']));
        # inactive projects
        $this->data['total_drafts'] = count($this->Project_model->getactiveprojectbyuserid($_SESSION['userid'],0));
        # logs
        $this->data['logs'] = $this->User_model->getLog($_SESSION['userid']);
        $this->data['user_details'] = $this->User_model->getData(null,null,$_SESSION['userid']);
        $this->data['content_page'] = '_content_log';
        $this->template->load('template/main-template', 'users/_profile', $this->data);
    }

    public function account_settings()
    {
        $this->data['questions'] = $this->User_model->getQuestionData();
        $this->data['user_details'] = $this->User_model->getData(null,null,$_SESSION['userid']);
        $this->data['content_page'] = '_content_acct_settings';
        $this->template->load('template/main-template', 'users/_profile', $this->data);
    }

    public function group()
    {
        $this->data['user_details'] = $this->User_model->getData(null,null,$_SESSION['userid']);
        $this->data['groups'] = $this->User_model->getGroupByAuthor($_SESSION['userid']);
        $this->data['content_page'] = '_content_group';
        $this->template->load('template/main-template', 'users/_profile', $this->data);
    }

    public function new_group()
    {
        $arrpost = $this->input->post();
        if(!empty($arrpost)) {
            $lists = explode(',',$arrpost['txtuser']);
            $groupid = $this->User_model->addgroup(
                            array(
                                'group_name' => $arrpost['txtgroupname'],
                                'created_by' => $_SESSION['userid'],
                                'created_date' => date('Y-m-d H:i:s')));

            foreach($lists as $list)
            {
                $this->User_model->adduser_group(
                    array(
                        'group_id' => $groupid,
                        'user_id' => $list,
                        'added_by' => $_SESSION['userid'],
                        'added_date' => date('Y-m-d H:i:s')));
                // set notification
                set_notification(1,$list,'settings/users/group','@'.$_SESSION['user_name'].' added you in group '.$arrpost['txtgroupname']);
            }
            $this->session->set_flashdata('strSuccessMsg','Group added successfully.');
            redirect('settings/users/group');
        }

        $this->data['user_details'] = $this->User_model->getData(null,null,$_SESSION['userid']);
        $this->data['users'] = $this->User_model->getData(1,null,null);
        $this->data['content_page'] = '_content_new_group';
        $this->template->load('template/main-template', 'users/_profile', $this->data);
    }

    public function group_edit()
    {
        $groupid = $this->uri->segment(4);
        $arrpost = $this->input->post();
        $group_details = $this->User_model->getGroupByGroupId($groupid);

        if(!empty($arrpost)) {
            $user_list = explode(',',$arrpost['txtuser']);
            $this->User_model->editgroup(
                        array(
                            'group_name' => $arrpost['txtgroupname'],
                            'created_by' => $_SESSION['userid'],
                            'created_date' => date('Y-m-d H:i:s')),$groupid);
            $current_members = array_column($group_details['members'],'user_id');
            // to Add
            foreach(array_diff($user_list,$current_members) as $user_add)
            {
                $this->User_model->adduser_group(
                    array(
                        'group_id' => $groupid,
                        'user_id' => $user_add,
                        'added_by' => $_SESSION['userid'],
                        'added_date' => date('Y-m-d H:i:s')));
                set_notification(1,$user_add,'settings/users/group','@'.$_SESSION['user_name'].' added you in group <u>'.$arrpost['txtgroupname'].'</u>');
            }
            // to Remove
            foreach(array_diff($current_members,$user_list) as $user_rem)
            {
                $this->User_model->deletegroup_user($groupid,$user_rem);
                set_notification(3,$user_rem,'settings/users/group','@'.$_SESSION['user_name'].' removed you in group <u>'.$arrpost['txtgroupname'].'</u>');
            }
            
            $this->session->set_flashdata('strSuccessMsg','Group updated successfully.');
            redirect('settings/users/group');
        }
        $this->data['user_details'] = $this->User_model->getData(null,null,$_SESSION['userid']);

        $users = $this->User_model->getData(1,null,null);
        foreach($users as $key=>$user) 
        { 
            if(isset($group_details))
            {
                if(in_array($user['userid'], array_column($group_details['members'], 'user_id'))) {
                    unset($users[$key]);
                }
            }
        }
        $this->data['users'] = $users;
        $this->data['group_details'] = $group_details;
        $this->data['content_page'] = '_content_new_group';
        $this->template->load('template/main-template', 'users/_profile', $this->data);
    }

    public function remove_group()
    {
        $group_details = $this->User_model->getGroupByGroupId($_POST['txtgroupid']);
        foreach ($group_details['members'] as $member)
        {
            set_notification(3,$member['user_id'],'settings/users/group','@'.$_SESSION['user_name'].' removed group <u>'.$group_details['group_name'].'</u>');
        }

        $this->User_model->remove_group($_POST['txtgroupid']);
        $this->session->set_flashdata('strSuccessMsg','Group removed successfully.');
        redirect('settings/users/group');
    }

    public function personal_info()
    {
        $arrpost = $this->input->post();
        if(!empty($arrpost)) {
            $bday = date('Y-m-d', strtotime($arrpost['txtbday']));

            $profileData = array(
                    'firstname'   => isset($arrpost['txtfname']) ? $arrpost['txtfname'] : '',
                    'surname'     => isset($arrpost['txtsname']) ? $arrpost['txtsname'] : '',
                    'middlename'  => isset($arrpost['txtmname']) ? $arrpost['txtmname'] : '',
                    'email'       => isset($arrpost['txtemail']) ? $arrpost['txtemail'] : '',
                    'birthday'    => isset($arrpost['txtbday']) ? $bday : '',
                    'gender'      => isset($arrpost['radgender']) ? $arrpost['radgender'] : '',
                    'sec_ques_1'       => isset($arrpost['selquestion1']) ? $arrpost['selquestion1'] : '',
                    'sec_answer_1'     => isset($arrpost['txtanswer1'])   ? $arrpost['txtanswer1'] : '',
                    'sec_ques_2'       => isset($arrpost['selquestion2']) ? $arrpost['selquestion2'] : '',
                    'sec_answer_2'     => isset($arrpost['txtanswer2'])   ? $arrpost['txtanswer2'] : '',
                    'sec_ques_3'       => isset($arrpost['selquestion3']) ? $arrpost['selquestion3'] : '',
                    'sec_answer_3'     => isset($arrpost['txtanswer3'])   ? $arrpost['txtanswer3'] : '',
                    'last_updated_by'  => $_SESSION['userid'],
                    'last_updated_date'=> date('Y-m-d H:i:s'));
            
            $this->User_model->editprofile($profileData, $_SESSION['userid']);
            $this->Login_model->add_session($_SESSION['userid'], $_SESSION['user_level'], $_SESSION['user_name'], $profileData['surname'], $profileData['firstname'], $profileData['middlename'], $_SESSION['user_cid'], $_SESSION['image']);
            $this->session->set_flashdata('strSuccessMsg','Profile updated successfully.');
            redirect('settings/users/account_settings/personal_info');
        }
    }

    public function upload_profile()
    {
        $config['upload_path']          = 'uploads/profiles/';
        $config['allowed_types']        = 'jpg|jpeg|png';

        $ext = explode('.',$_FILES['profile_image']['name']);

        $config['file_name'] = uniqid().".".$ext[1]; 
        $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0777, TRUE);
        }

        if ( ! $this->upload->do_upload('profile_image'))
        {
            $error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('strErrorMsg','Please try again!');
        }
        else
        {
            $data = $this->upload->data();
            $arrData = array(
                        'image_fname' => $config['file_name'],
                        'upload_datetime' => date('Y-m-d H:i:s'));
            $this->User_model->editProfile($arrData, $_SESSION['userid']);
            $this->session->set_flashdata('strSuccessMsg','Upload successfully saved.');
            $this->Login_model->add_session($_SESSION['userid'], $_SESSION['user_level'], $_SESSION['user_name'], $_SESSION['lname'], $_SESSION['fname'], $_SESSION['mname'], $_SESSION['user_cid'], $config['file_name']);
            redirect('settings/users/profile');
        }
    }

    public function change_password()
    {
        $arrpost = $this->input->post();
        if(!empty($arrpost)) {
            $user = $this->Login_model->getUserByUsername($_SESSION['user_name']);
            if(password_verify($arrpost['txtcurpass'], $user['password'])):
                $options = ['cost' => 12];
                $password = password_hash($arrpost['txtnewpass'], PASSWORD_BCRYPT, $options);
                $arrData = array(
                            'password' => $password,
                            'last_pass_changed' => date('Y-m-d H:i:s'));
                $this->User_model->edituser($arrData, $_SESSION['userid']);
                $this->session->set_flashdata('strSuccessMsg','Change password successfully.');
                redirect('settings/users/account_settings/personal_info');
            else:
                $this->session->set_flashdata('currpass',$arrpost['txtnewpass']);
                $this->session->set_flashdata('error','Wrong Password.');
                redirect('settings/users/account_settings/change_password');
            endif;
        }
    }
    
    public function notification()
    {
        $this->data['user_details'] = $this->User_model->getData(null,null,$_SESSION['userid']);
        $this->data['content_page'] = '_content_notifications';
        $this->template->load('template/main-template', 'users/_profile', $this->data);
    }

    public function read_notif()
    {
        $notifid = $_GET['id'];
        $this->User_model->notification_read($notifid);
        redirect('settings/users/notification');
    }

    public function help()
    {
        $this->data['user_details'] = $this->User_model->getData(null,null,$_SESSION['userid']);
        $this->data['content_page'] = '_content_help';
        $this->template->load('template/main-template', 'users/_profile', $this->data);
    }

}