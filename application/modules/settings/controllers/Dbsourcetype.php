<?php 
class Dbsourcetype extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        checkSession();
        $this->data = array();
        $this->data['no'] = 1;
        $this->load->model('Datasourcetype_model');
    }

    public function index()
    {
        $this->data['dt_status'] = 'All Data source type';
        $this->data['arrdbtype'] = $this->Datasourcetype_model->getData(null,0);
        $this->template->load('template/main-template', 'settings/dbtype/view_all', $this->data);
    }

    public function delete_dbtype()
    {
        $this->Datasourcetype_model->edit(array('is_delete'=>1), $this->uri->segment(4));
        redirect('settings/dbsourcetype');
    }

    public function edit_dbtype()
    {
        $this->Datasourcetype_model->edit(array('is_active'=>$this->uri->segment(4)), $this->uri->segment(5));
        redirect('settings/dbsourcetype');
    }



    
    
}