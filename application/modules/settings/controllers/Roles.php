<?php 
class Roles extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        checkSession();
        $this->data = array();
        $this->load->model(array('settings/User_model'));
    }

    public function index()
    {
        $this->data['usertypes'] = $this->User_model->getuserType();
        $this->template->load('template/main-template', 'roles/view_all', $this->data);
    }


    
    
}