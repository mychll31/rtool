<?php 
class Settings extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        checkSession();
        $this->data = array();
        $this->data['no'] = 1;
        $this->load->model(array('Securityquestions_model'));
    }

    public function security_questions()
    {
        $this->data['arrquestions'] = $this->Securityquestions_model->getData(null,0);

        $this->data['q_status'] = 'All Questions';
        $this->template->load('template/main-template', 'others/view_all', $this->data);
    }

    public function view_question()
    {
        $this->data['arrData'] = $this->Securityquestions_model->getData(null,null,$this->uri->segment(3));

        $this->data['q_status'] = 'View Question';
        $this->data['action'] = 'view';
        $this->template->load('template/main-template', 'others/_form', $this->data);
    }

    public function add_question()
    {
        $arrpost = $this->input->post();
        if(!empty($arrpost)):
            $isQuestionExist = $this->Securityquestions_model->isQuestionExist($arrpost['txtquestion']);
            if($isQuestionExist < 1):
                $arrData = array(
                        'question'  => isset($arrpost['txtquestion']) ? $arrpost['txtquestion'] : '',
                        'added_by'  => 1, #$this->session->userdata('user_id'),
                        'added_date'=> date('Y-m-d H:i:s'));
                $this->Securityquestions_model->addquestion($arrData);
                $this->session->set_flashdata('strSuccessMsg','New question added successfully.');
                redirect('settings/security_questions');
            else:
                $this->session->set_flashdata('strErrorMsg','Question already exists.');
            endif;
        endif;

        $this->data['q_status'] = 'Add Question';
        $this->data['action'] = 'add';
        $this->template->load('template/main-template', 'others/_form', $this->data);
    }

    public function edit_question()
    {
        $arrpost = $this->input->post();
        if(!empty($arrpost)):
            $isQuestionExist = $this->Securityquestions_model->isQuestionExist($arrpost['txtquestion'], $this->uri->segment(3));
            if($isQuestionExist < 1):
                $arrData = array(
                        'question'  => isset($arrpost['txtquestion']) ? $arrpost['txtquestion'] : '',
                        'added_by'  => 1, #$this->session->userdata('user_id'),
                        'added_date'=> date('Y-m-d H:i:s'));
                $this->Securityquestions_model->editquestion($arrData, $this->uri->segment(3));
                $this->session->set_flashdata('strSuccessMsg','Question updated successfully.');
                redirect('settings/security_questions');
            else:
                $this->session->set_flashdata('strErrorMsg','Question already exists.');
            endif;
        endif;

        $this->data['arrData'] = $this->Securityquestions_model->getData(null,null,$this->uri->segment(3));

        $this->data['q_status'] = 'Edit Question';
        $this->data['action'] = 'edit';
        $this->template->load('template/main-template', 'others/_form', $this->data);
    }
    
    
}