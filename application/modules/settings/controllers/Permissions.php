<?php 
class Permissions extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        checkSession();
        $this->data = array();
        // $this->load->model();
    }

    public function index()
    {
        $this->data['p_status'] = 'All Permissions';
        $this->template->load('template/main-template', 'permission/view_all', $this->data);
    }

    public function view_permission()
    {
        $this->data['p_status'] = 'View Permission';
        $this->data['action'] = 'view';
        $this->template->load('template/main-template', 'permission/_form', $this->data);
    }

    public function edit_permission()
    {
        $this->data['p_status'] = 'Edit Permission';
        $this->data['action'] = 'edit';
        $this->template->load('template/main-template', 'permission/_form', $this->data);
    }
    
    
}