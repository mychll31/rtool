<?php
    echo load_plugin('css', array('datetime_picker','select'));
    # SQLite
    $showSQLite = 1;
    if(isset($arrData)):
        $showSQLite = $arrData['db_type'] == 3 ? 0 : 1;
    else:
        if(set_value('seltype') != null):
            $showSQLite = set_value('seltype') == 3 ? 0 : 1;
        endif;
    endif;
?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=base_url('home')?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="<?=base_url('config')?>">Configuration</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span><span><?=ucfirst($action)?></span>
        </li>
    </ul>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> <?=$action?> Configuration</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" <?=$action == 'view' ? '' : 'hidden'?>>
                                <a href="<?=base_url('config/database/edit/').$this->uri->segment(4)?>" class="btn btn-sm blue">Edit</a>
                                <a href="#" id="btndelete" class="btn btn-sm red">Delete</a>
                                <a href="<?=base_url('config/database')?>" class="btn btn-sm default">Cancel</a>
                            </div>
                            <?php $form = $action == 'add' ? 'config/database/add' : 'config/database/edit/'.$this->uri->segment(4); ?>
                            <form role="form" action="<?=base_url($form)?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <!-- begin database -->
                                <div class="portlet light bordered col-sm-12">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-database font-dark"></i>
                                            <span class="caption-subject font-dark bold uppercase">Database</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Type </label>
                                            <div class="col-sm-8">
                                                <select name="seltype" id="seldbtype" class="form-required <?=$action == 'view' ? '' : 'bs-select'?> form-control" <?=$action == 'view' ? 'disabled' : ''?>>
                                                    <option value="">Select Database Type</option>
                                                    <?php foreach(getDatabase_type() as $key=>$type): ?>
                                                        <option value="<?=$key?>" <?=isset($arrData) ? $arrData['db_type'] == $key ? 'selected' : '' : set_value('seltype') == $key ? 'selected' : ''?>>
                                                            <?=$type?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <small class="help-block"></small>
                                            </div>
                                        </div>
                                        <div class="form-group <?=isset($err['label'])!='' ? 'has-error' : ''?>">
                                            <label class="control-label col-sm-2">Label </label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control form-required" name="txtlabel"
                                                    value="<?=isset($arrData) ? $arrData['db_label'] : set_value('txtlabel')?>" 
                                                    <?=$action=='view' ? 'disabled' : ''?>>
                                                <small class="help-block"><?=isset($err['label'])!='' ? $err['label'] : ''?></small>
                                            </div>
                                        </div>
                                        <div class="div-sqlite-hide form-group">
                                            <label class="control-label col-sm-2">Host </label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control form-required" name="txthost"
                                                    value="<?=isset($arrData) ? $arrData['db_host'] : set_value('txthost')?>" 
                                                    <?=$action=='view' ? 'disabled' : ''?>>
                                                <small class="help-block"></small>
                                            </div>
                                        </div>
                                        <div class="div-sqlite-hide form-group">
                                            <label class="control-label col-sm-2">Database Name </label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control form-required" name="txtdbname"
                                                    value="<?=isset($arrData) ? $arrData['db_name'] : set_value('txtdbname')?>" 
                                                    <?=$action=='view' ? 'disabled' : ''?>>
                                                <small class="help-block"></small>
                                            </div>
                                        </div>
                                        <div class="div-sqlite-show form-group">
                                            <label class="control-label col-sm-2"> </label>
                                            <div class="col-sm-8">
                                                <input type="file" class="form-control <?=$action == 'add' ? 'form-required' : ''?>"
                                                    name="sqlitefile" <?=$action=='view' ? 'style="display: none;"' : ''?>>
                                                <input type="hidden" name="txtsqlitefile" id="txtsqlitefile">
                                                <span <?=$action=='add' ? 'hidden' : ''?> style="<?=$action=='edit' ? 'font-size: smaller;' : ''?>">
                                                    <?=isset($arrData) ? $arrData['db_sqlite_file_loc'] : ''?></span>
                                                <small class="help-block"><?=isset($err['db'])!='' ? $err['db'] : ''?></small>
                                            </div>
                                        </div>
                                        <div class="div-sqlite-hide form-group">
                                            <label class="control-label col-sm-2">Username </label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control form-required" name="txtuname"
                                                    value="<?=isset($arrData) ? $arrData['db_username'] : set_value('txtuname')?>" 
                                                    <?=$action=='view' ? 'disabled' : ''?>>
                                                <small class="help-block"></small>
                                            </div>
                                        </div>
                                        <div class="div-sqlite-hide form-group
                                            <?=isset($err['pword'])!='' ? 'has-error' : ''?>"
                                            <?=$action == 'view' ? 'hidden' : ''?> >
                                            <label class="control-label col-sm-2">Password </label>
                                            <div class="col-sm-8">
                                                <input type="password" class="form-control" name="txtpass"
                                                    <?=$action=='view' ? 'disabled' : ''?>>
                                                <small class="help-block"><?=isset($err['pword'])!='' ? $err['pword'] : ''?></small>
                                            </div>
                                        </div>
                                        <div class="div-sqlite-hide form-group
                                            <?=isset($err['pword'])!='' ? 'has-error' : ''?>"
                                            <?=$action == 'view' ? 'hidden' : ''?> >
                                            <label class="control-label col-sm-2">Re-Type Password </label>
                                            <div class="col-sm-8">
                                                <input type="password" class="form-control" name="txtretype"
                                                    <?=$action=='view' ? 'disabled' : ''?>>
                                                <small class="help-block"><?=isset($err['pword'])!='' ? $err['pword'] : ''?></small>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input" <?=$action=='add' ? 'hidden' : ''?>>
                                            <div class="col-md-offset-2 col-md-4">
                                                <div class="md-checkbox-list">
                                                    <div class="md-checkbox">
                                                        <input type="checkbox" id="chkisactive" name="chkisactive" class="md-check" 
                                                        <?=$arrData['isactive'] == 1 ? 'checked' : ''?>
                                                        <?=$action=='view' ? 'disabled' : ''?>>
                                                        <label for="chkisactive">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span> Active </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($action == 'view'): ?>
                                        <div class="form-group form-md-line-input">
                                            <div class="col-md-offset-2">
                                                <small>
                                                    <b>Added by</b>: <?=getProfileName($arrData['created_by'])?> <i><?=$arrData['created_date']?></i>
                                                    <br>
                                                    <b>Last updated by</b>: <?=getProfileName($arrData['updated_by'])?> <i><?=$arrData['updated_date']?></i>
                                                </small>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <!-- end database -->
                                <div class="form-actions" <?=$action=='view' ? 'hidden' : ''?>> 
                                    <div class="col-sm-10 pull-right">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?=$action=='edit' ? base_url('config/database/view/').$this->uri->segment(4) : base_url('config/database')?>" class="btn default">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- begin delete modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title bold uppercase" id="deleteModal">Delete Configuration</h5>
            </div>
            <form action="<?=base_url('config/database/delete')?>" method="post">
                <div class="modal-body">
                    <input type="hidden" name="txtid" value="<?=$this->uri->segment(4)?>">
                    Are you sure you want to delete this data?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">okay</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end DEdelete modal -->

<?=load_plugin('js', array('datetime_picker','select','custom-form-validation')) ?>
<script>
    $(document).ready(function() {
        // show file upload
        if("<?=$showSQLite?>" == 1){
            $('.div-sqlite-show').hide();
            $('.div-sqlite-hide').find('.form-control :not("password")').addClass('form-required');
        }
        // not sqlite hide other fields
        if("<?=$showSQLite?>" == "0"){
            $('.div-sqlite-hide').hide();
            $('.div-sqlite-hide').find('.form-control').removeClass('form-required');
        }
        if("<?=$action?>" == 'view'){
            $('.form-required').removeClass('form-required');
        }
        
        $('a#btndelete').click(function () {
            $('#deleteModal').modal('show');
        });
    });
</script>