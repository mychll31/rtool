<?=set_title($action.' Permission')?>
<?=load_plugin('css', array('select2')) ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=base_url('home')?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="<?=base_url('config')?>">Permission</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span><span><?=ucfirst($action)?></span>
        </li>
    </ul>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> <?=$action?> Permission<span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" <?=$action == 'view' ? '' : 'hidden'?>>
                                <a href="<?=base_url('permissions/edit/').$this->uri->segment(3)?>" class="btn btn-sm blue">Edit</a>
                                <a href="#" id="btndelete" class="btn btn-sm red">Delete</a>
                                <a href="<?=base_url('permissions')?>" class="btn btn-sm default">Cancel</a>
                            </div>
                            <?php $form = $action == 'add' ? 'permissions/add' : 'permissions/edit/'.$this->uri->segment(3); ?>
                            <form role="form" action="<?=base_url($form)?>" method="post" class="form-horizontal">
                                <div class="portlet-body">
                                    <div class="form-group ">
                                        <label class="control-label col-sm-2">User </label>
                                        <div class="col-sm-8">
                                            <?php if($action != 'view'): ?>
                                                <select name="selusers" data-placeholder="Choose User" id="selusers" class="form-control select2">
                                                    <option value=""></option>
                                                    <?php foreach($users as $user): ?>
                                                        <option value="<?=$user['user_id']?>"
                                                            <?=isset($arrData) ? $arrData[0]['userid'] == $user['user_id'] ? 'selected' : '' : ''?>><?=getFullname($user['first_name'], $user['last_name'], $user['middle_name'])?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            <?php else: ?>
                                                <input type="text" class="form-control" disabled value="<?=getFullname($arrData[0]['first_name'], $arrData[0]['last_name'], $arrData[0]['middle_name'])?>">
                                            <?php endif; ?>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="portlet light bordered col-sm-4">
                                            <i class="fa fa-database font-dark"></i>
                                            <span class="caption-subject font-dark bold uppercase">Database</span>
                                            <hr>
                                            <div class="portlet-body">
                                                <?php foreach($dbconfig_roles as $role): ?>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label title="<?=ucfirst($role['description'])?>">
                                                                <input type="checkbox" name="chkrole[]" value="<?=$role['id']?>" <?=$action == 'view' ? 'disabled' : ''?>
                                                                    <?=$action == 'add' ? 'checked' : array_search($role['id'], array_column($arrData, 'roleid')) >-1 ? 'checked' : ''?>> <?=$role['name']?></label>
                                                            <small class="help-block"></small>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                        <div class="portlet light bordered col-sm-4">
                                            <i class="fa fa-users font-dark"></i>
                                            <span class="caption-subject font-dark bold uppercase">Users</span>
                                            <hr>
                                            <div class="portlet-body">
                                                <?php foreach($user_roles as $role): ?>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label title="<?=ucfirst($role['description'])?>">
                                                                <input type="checkbox" name="chkrole[]" value="<?=$role['id']?>" <?=$action == 'view' ? 'disabled' : ''?>
                                                                    <?=$action == 'add' ? 'checked' : array_search($role['id'], array_column($arrData, 'roleid')) >-1 ? 'checked' : ''?>> <?=$role['name']?></label>
                                                            <small class="help-block"></small>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="portlet light bordered col-sm-8">
                                            <i class="fa fa-gears font-dark"></i>
                                            <span class="caption-subject font-dark bold uppercase">Others</span>
                                            <hr>
                                            <div class="col-sm-4">
                                                <?php foreach($oth_secques as $role): ?>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label title="<?=ucfirst($role['description'])?>">
                                                                <input type="checkbox" name="chkrole[]" value="<?=$role['id']?>" <?=$action == 'view' ? 'disabled' : ''?>
                                                                    <?=$action == 'add' ? 'checked' : array_search($role['id'], array_column($arrData, 'roleid')) >-1 ? 'checked' : ''?>> <?=$role['name']?></label>
                                                            <small class="help-block"></small>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                            <div class="col-sm-4">
                                                **Others
                                            </div>
                                            <div class="col-sm-4">
                                                **Others
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-8">
                                            <label><input type="checkbox" name="chkisactive"
                                                        <?=isset($arrData) ? $arrData[0]['isactive'] == 1 ? 'checked' : '' : ''?>
                                                        <?=$action=='view' ? 'disabled' : ''?>> Is active</label>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions" <?=$action=='view' ? 'hidden' : ''?>> 
                                    <div class="col-sm-10 pull-right">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?=$action=='edit' ? base_url('others/questions/view/').$this->uri->segment(3) : base_url('others/questions')?>" class="btn default">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?=load_plugin('js', array('form-validation', 'select2')) ?>

<!-- begin delete modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title bold uppercase" id="deleteModal">Delete Configuration</h5>
            </div>
            <form action="<?=base_url('others/questions/delete')?>" method="post">
                <div class="modal-body">
                    <input type="hidden" name="txtid" value="<?=$this->uri->segment(4)?>">
                    Are you sure you want to delete this data?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">okay</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end DEdelete modal -->

<script>
    $('select#selusers').select2({
        minimumResultsForSearch: -1,
        placeholder: function(){
            $(this).data('placeholder');
        }
    });
    $(document).ready(function() {
        $('a#btndelete').click(function () {
            $('#deleteModal').modal('show');
        });
    });
</script>