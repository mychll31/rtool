<?=set_title('Security Question')?>
<?=load_plugin('css', array('datatables')) ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=base_url('home')?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Permission</span>
        </li>
    </ul>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Managed Permission</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<?=base_url('permissions/add')?>" class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblpermissions" style="visibility: hidden;">
                    <thead>
                        <tr>
                            <th style="width:7% !important"> No </th>
                            <th> User </th>
                            <th> Role </th>
                            <th> Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($users) > 0): $no=1;
                                foreach($users as $user):
                                    $uid = $user['userid'];
                                    $userRole = array_filter($permissions, function($arr) use($uid) { return ($arr['userid'] == $uid); });?>
                                    <tr class="odd gradeX" data-id="<?=$uid?>">
                                        <td><?=$no++?></td>
                                        <td><?=getFullname($user['first_name'], $user['last_name'], $user['middle_name'])?></td>
                                        <td><?php foreach($userRole as $role): ?>
                                                <li><?=$role['description']?></li>
                                            <?php endforeach; ?>
                                        </td>
                                        <td>
                                        <?=$user['isactive'] == 1 ? '<span class="small label label-primary">Active</span>' : '<span class="small label label-danger">Inactive</span>'?></td>
                                    </tr>
                        <?php endforeach; endif;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?=load_plugin('js', array('datatables')) ?>
<script>
    $(document).ready(function() {
        $('#tblpermissions').dataTable( {
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblpermissions').css('visibility', 'visible');
            }} );
        $('#tblpermissions').on('click', 'tr', function () {
            window.location.href = "<?=base_url('permissions/view').'/'?>"+$(this).data('id');
        });
    });
</script>