<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=base_url('home')?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="<?=base_url('config/database_type')?>">Type of Database</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span><span><?=ucfirst($action)?></span>
        </li>
    </ul>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> <?=$action?> Type of Database</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" <?=$action == 'view' ? '' : 'hidden'?>>
                                <a href="<?=base_url('config/database_type/edit/').$this->uri->segment(4)?>" class="btn btn-sm blue">Edit</a>
                                <!-- <a href="#" id="btndelete" class="btn btn-sm red">Delete</a> -->
                                <a href="<?=base_url('config/database_type')?>" class="btn btn-sm default">Cancel</a>
                            </div>
                            <?php $form = $action == 'add' ? 'config/database_type/add' : 'config/database_type/edit/'.$this->uri->segment(4); ?>
                            <form role="form" action="<?=base_url($form)?>" method="post" class="form-horizontal">
                                <!-- begin database -->
                                <div class="form-group <?=isset($err['label'])!='' ? 'has-error' : ''?>">
                                    <label class="control-label col-sm-2">Type <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="txtname"
                                            value="<?=isset($arrData) ? $arrData['name'] : set_value('txtname')?>" 
                                            <?=$action=='view' ? 'disabled' : ''?>>
                                        <small class="help-block"><?=isset($err['label'])!='' ? $err['label'] : ''?></small>
                                    </div>
                                </div>
                                <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                                    <label class="control-label col-sm-2"></label>
                                    <div class="col-sm-8">
                                        <div class="md-checkbox-list">
                                            <div class="md-checkbox">
                                                <input type="checkbox" id="chkisactive" name="chkisactive" class="md-check" 
                                                <?=isset($arrData) ? $arrData['isactive'] == 1 ? 'checked' : '' : ''?>
                                                <?=$action=='view' ? 'disabled' : ''?>>
                                                <label for="chkisactive">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Active </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end database -->
                                <div class="form-actions" <?=$action=='view' ? 'hidden' : ''?>> 
                                    <div class="col-sm-10 pull-right">
                                        <button type="submit" class="btn green">Submit</button>
                                        <a href="<?=$action=='edit' ? base_url('config/database_type/view/').$this->uri->segment(4) : base_url('config/database_type')?>" class="btn default">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- begin delete modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title bold uppercase" id="deleteModal">Delete type of database</h5>
            </div>
            <form action="<?=base_url('config/database_type/delete')?>" method="post">
                <div class="modal-body">
                    <input type="hidden" name="txtid" value="<?=$this->uri->segment(4)?>">
                    Are you sure you want to delete this data?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">okay</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end DEdelete modal -->

<?=load_plugin('js', array('form-validation')) ?>
<script>
    $(document).ready(function() {
        $('a#btndelete').click(function () {
            $('#deleteModal').modal('show');
        });
    });
</script>