<?=load_plugin('css', array('datatables')) ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=base_url('home')?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Database Type</span>
        </li>
    </ul>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Managed Database Type</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<?=base_url('config/database_type/add')?>" class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblconfig" style="visibility: hidden;">
                    <thead>
                        <tr>
                            <th style="width:10% !important"> No </th>
                            <th> Database Type </th>
                            <th> Added </th>
                            <th> Last Modified </th>
                            <th> Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($arrData) > 0): $no=1; foreach($arrData as $data): ?>
                            <tr class="odd gradeX" data-id="<?=$data['id']?>">
                                <td><?=$no++?></td>
                                <td><?=$data['name']?></td>
                                <td><span class="small">
                                        <?=getProfileName($data['created_by'])?> <i><?=$data['created_date']?></i>
                                    </span></td>
                                <td><span class="small">
                                        <?=getProfileName($data['updated_by'])?> <i><?=$data['updated_date']?></i>
                                    </span></td>
                                <td><label class="label label-<?=$data['isactive'] == 1 ? 'primary' : 'danger'?>">
                                        <span class="small"><?=$data['isactive'] == 1 ? 'Active' : 'Inactive'?></span>
                                    </label></td>
                            </tr>
                        <?php endforeach; endif;?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<?=load_plugin('js', array('datatables')) ?>
<script>
    $(document).ready(function() {
        $('#tblconfig').dataTable( {
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblconfig').css('visibility', 'visible');
            }} );
        $('#tblconfig').on('click', 'tr', function () {
            window.location.href = "<?=base_url('config/database_type/view').'/'?>"+$(this).data('id');
        });
    });
</script>