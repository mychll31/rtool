<?=set_title('Security Question')?>
<?=load_plugin('css', array('datatables')) ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=base_url('home')?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Security Question</span>
        </li>
    </ul>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Managed Security Question</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<?=base_url('others/questions/add')?>" class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblconfig" style="visibility: hidden;">
                    <thead>
                        <tr>
                            <th style="width:7% !important"> No </th>
                            <th> Question </th>
                            <th> Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($questions) > 0): $no=1; foreach($questions as $q): ?>
                            <tr class="odd gradeX" data-id="<?=$q['id']?>">
                                <td><?=$no++?></td>
                                <td><?=$q['question']?></td>
                                <td><label class="label label-<?=$q['isactive'] == 1 ? 'primary' : 'danger'?>">
                                        <span class="small"><?=$q['isactive'] == 1 ? 'Active' : 'Inactive'?></span>
                                    </label></td>
                            </tr>
                        <?php endforeach; endif;?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<?=load_plugin('js', array('datatables')) ?>
<script>
    $(document).ready(function() {
        $('#tblconfig').dataTable( {
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblconfig').css('visibility', 'visible');
            }} );
        $('#tblconfig').on('click', 'tr', function () {
            window.location.href = "<?=base_url('others/questions/view').'/'?>"+$(this).data('id');
        });
    });
</script>