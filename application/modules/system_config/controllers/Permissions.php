<?php 
class Permissions extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->data = array();
        $this->load->model(array('Role_model','Permission_model','users/User_model'));
    }

    public function index()
    {
        $permissions = $this->Permission_model->getData();
        $this->data['permissions'] = $permissions;
        // $this->data['users'] = array_unique(array_column($permissions, 'userid'));
        $this->data['users'] = unique_multidimensional_array($permissions, 'userid');
        $this->template->load('template/main-template', 'permissions/_view', $this->data);
    }

    public function add()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost)):
            foreach($arrPost['chkrole'] as $roleid):
                $arrData = array(
                        'roleid' => $roleid,
                        'userid' => $arrPost['selusers'],
                        'added_by' => $this->session->userdata('userid'),
                        'added_date' => date('Y-m-d H:i:s'));
                $this->Permission_model->add($arrData);
            endforeach;
            $this->session->set_flashdata('strSuccessMsg','Permission added successfully.');
            redirect('permissions');
        endif;
        $this->data['action'] = 'add';
        $this->data['dbconfig_roles'] = $this->Role_model->getData(0, 'dbconfig');
        $this->data['user_roles'] = $this->Role_model->getData(0, 'users');
        $this->data['oth_secques'] = $this->Role_model->getData(0, 'oth_secques');
        $this->data['users'] = $this->User_model->getData(0,1);
        $this->template->load('template/main-template', 'permissions/_form', $this->data);
    }

    public function edit($id)
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost)):
            if(isset($arrPost['chkisactive'])):
                $this->Permission_model->delete($id);
                foreach($arrPost['chkrole'] as $roleid):
                    $arrData = array(
                            'roleid' => $roleid,
                            'userid' => $arrPost['selusers'],
                            'added_by' => $this->session->userdata('userid'),
                            'added_date' => date('Y-m-d H:i:s'));
                    $this->Permission_model->add($arrData);
                endforeach;
            else:
               foreach($arrPost['chkrole'] as $roleid):
                    $arrData = array(
                            'isactive' => 0,
                            'last_updated_by'=> $_SESSION['userid'],
                            'last_updated_date' => date('Y-m-d H:i:s'));
                    $this->Permission_model->edit($arrData, $id);
                endforeach;
            endif;
            $this->session->set_flashdata('strSuccessMsg','Permission updated successfully.');
            redirect('permissions/view/'.$id);
        endif;
        $this->data['action'] = 'edit';
        $this->data['arrData'] = $this->Permission_model->getData($id);
        $this->data['dbconfig_roles'] = $this->Role_model->getData(0, 'dbconfig');
        $this->data['user_roles'] = $this->Role_model->getData(0, 'users');
        $this->data['oth_secques'] = $this->Role_model->getData(0, 'oth_secques');
        $this->data['users'] = $this->User_model->getData(0,1);
        $this->template->load('template/main-template', 'permissions/_form', $this->data);
    }

    public function view($id)
    {
        $this->data['action'] = 'view';
        $this->data['arrData'] = $this->Permission_model->getData($id);
        $this->data['dbconfig_roles'] = $this->Role_model->getData(0, 'dbconfig');
        $this->data['user_roles'] = $this->Role_model->getData(0, 'users');
        $this->data['oth_secques'] = $this->Role_model->getData(0, 'oth_secques');
        $this->template->load('template/main-template', 'permissions/_form', $this->data);
    }

    public function delete()
    {
        $this->Question_model->delete($_POST['txtid']);
        $this->session->set_flashdata('strSuccessMsg','Permission deleted successfully.');
       redirect('others/questions');
    }


}