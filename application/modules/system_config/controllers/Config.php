<?php 
class Config extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->data = array();
        $this->load->model(array('Config_model'));
    }

    public function index()
    {
        $this->data['arrData'] = $this->Config_model->getData();
        $this->template->load('template/main-template', 'systemconfig/_view', $this->data);
    }

    public function add()
    {
        $arrPost = $this->input->post();
        $arrError = array();
        $dbtype = '';
        $dblabel = '';
        $dbErr = '';
        if(!empty($arrPost)):
            if($arrPost['txtpass'] !=$arrPost['txtretype']){
                $this->data['err']['pword'] = 'Password not match!';
            }
            #sqlite
            if($arrPost['seltype'] == 3):
                $config['upload_path'] = 'attachments/dbase_sqlite/';
                $config['allowed_types'] = '*';
                $dbtype = 'sqlite';
                $this->load->library('upload', $config);

                if(! $this->upload->do_upload('sqlitefile')):
                    $dbErr = $this->upload->display_errors();
                else:
                    $this->upload->data();
                endif;
            endif;

            $arrData = array(
                        'db_type' => $arrPost['seltype'],
                        'db_label' => $arrPost['txtlabel'],
                        'db_host' => $arrPost['txthost'],
                        'db_name' => $arrPost['txtdbname'],
                        'db_username' => $arrPost['txtuname'],
                        'db_password' => $arrPost['txtpass'],
                        'db_sqlite_file_loc' => $arrPost['seltype'] == 3 ? $config['upload_path'].$_FILES['sqlitefile']['name'] : '',
                        'created_by' => $this->session->userdata('userid'),
                        'created_date' => date('Y-m-d H:i:s'));

            if($this->Config_model->isExists($arrPost['seltype'], $arrPost['txtlabel']) > 0):
                $dblabel = 'Label already exists.';
            endif;

            if($dbErr == '' && $dblabel == ''):
                $this->Config_model->add($arrData);
                $this->session->set_flashdata('strSuccessMsg','Database added successfully.');
                redirect('config/database');
            else:
                $arrError = array('db' => $dbErr, 'label' => $dblabel, 'dbtype' => $dbtype);
            endif;
        endif;

        $this->data['err'] = $arrError;
        $this->data['action'] = 'add';
        $this->template->load('template/main-template', 'systemconfig/_form', $this->data);
    }

    public function view($id)
    {
        $this->data['action'] = 'view';
        $this->data['arrData'] = $this->Config_model->getData($id);
        $this->template->load('template/main-template', 'systemconfig/_form', $this->data);
    }

    public function edit($id)
    {
        $arrPost = $this->input->post();
        $this->data['arrData'] = $this->Config_model->getData($id);
        
        $arrError = array();
        $dbtype = '';
        $dblabel = '';
        $dbErr = '';
        $fileLocation = $this->data['arrData']['db_sqlite_file_loc'];
        if(!empty($arrPost)):
            if($arrPost['txtpass'] !=$arrPost['txtretype']){
                $this->data['err']['pword'] = 'Password not match!';
            }
            #sqlite
            if($arrPost['seltype'] == 3 and $_FILES['sqlitefile']['tmp_name'] != ''):
                $config['upload_path'] = 'attachments/dbase_sqlite/';
                $config['allowed_types'] = '*';
                $dbtype = 'sqlite';
                $this->load->library('upload', $config);

                if(! $this->upload->do_upload('sqlitefile')):
                    $dbErr = $this->upload->display_errors();
                else:
                    $this->upload->data();
                endif;
                $fileLocation = $config['upload_path'].$_FILES['sqlitefile']['name'];
            endif;

            $arrData = array(
                        'db_type' => $arrPost['seltype'],
                        'db_label' => $arrPost['txtlabel'],
                        'db_host' => $arrPost['seltype'] == 3 ? '' : $arrPost['txthost'],
                        'db_name' => $arrPost['seltype'] == 3 ? '' : $arrPost['txtdbname'],
                        'db_username' => $arrPost['seltype'] == 3 ? '' : $arrPost['txtuname'],
                        'db_password' => $arrPost['seltype'] == 3 ? '' : $arrPost['txtpass'],
                        'db_sqlite_file_loc' => $arrPost['seltype'] == 3 ? $fileLocation : '',
                        'created_by' => $this->session->userdata('userid'),
                        'created_date' => date('Y-m-d H:i:s'));

            if($this->Config_model->isExists($arrPost['seltype'], $arrPost['txtlabel'], $id) > 1):
                $dblabel = 'Label already exists.';
            endif;

            if($dbErr == '' && $dblabel == ''):
                $this->Config_model->edit($arrData, $id);
                $this->session->set_flashdata('strSuccessMsg','Database updated successfully.');
                redirect('config/database/view/'.$id);
            else:
                $arrError = array('db' => $dbErr, 'label' => $dblabel, 'dbtype' => $dbtype);
            endif;
        endif;

        $this->data['err'] = $arrError;
        $this->data['action'] = 'edit';
        $this->template->load('template/main-template', 'systemconfig/_form', $this->data);
    }

    public function delete()
    {
        $this->Config_model->delete($_POST['txtid']);
        $this->session->set_flashdata('strSuccessMsg','Database deleted successfully.');
        redirect('config/database');
    }

    public function check_connection()
    {
        error_reporting(0); # hide Error message
        $arrDb = $this->Config_model->getData($_GET['db']);
        $dbpass = c_decrypt($arrDb['db_password']);
        $arrData = array();

        #Begin mysql connection
        if($arrDb['db_type'] == 2):
            $config['hostname'] = $arrDb['db_host'];
            $config['username'] = $arrDb['db_username'];
            $config['password'] = $dbpass;
            $config['database'] = $arrDb['db_name'];
            $config['dbdriver'] = 'mysqli';
            $db_obj = $this->load->database($config, TRUE);
            $connected = $db_obj->initialize();
            if ($connected) {
                $arrData['message'] = 'Connected';
                $arrData['tables'] = $db_obj->list_tables();
            }else{
                $error = $db_obj->error();
                $arrData['message'] = $error['message'];
            }
            echo json_encode($arrData);
        endif;
        #End mysql connection
    }

    public function getFieldsList()
    {
        error_reporting(0); # hide Error message
        $arrDb = $this->Config_model->getData($_GET['db']);
        $dbpass = c_decrypt($arrDb['db_password']);
        $arrData = array();

        #Begin mysql connection
        if($arrDb['db_type'] == 2):
            $config['hostname'] = $arrDb['db_host'];
            $config['username'] = $arrDb['db_username'];
            $config['password'] = $dbpass;
            $config['database'] = $arrDb['db_name'];
            $config['dbdriver'] = 'mysqli';
            $db_obj = $this->load->database($config, TRUE);
            $connected = $db_obj->initialize();
            if ($connected) {
                $arrData['message'] = 'Connected';
                $arrData['fields'] = $db_obj->field_data($_GET['tbl']);
            }else{
                $error = $db_obj->error();
                $arrData['message'] = $error['message'];
            }
            echo json_encode($arrData);
        endif;
        #End mysql connection
    }

}