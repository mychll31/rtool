<?php 
class Questions extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->data = array();
        $this->load->model(array('Question_model'));
    }

    public function index()
    {
        $this->data['questions'] = $this->Question_model->getData();
        $this->template->load('template/main-template', 'questions/ques_view', $this->data);
    }

    public function add()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost)):
            $arrData = array(
                        'question' => ucfirst($arrPost['txtquestion']),
                        'added_by' => $this->session->userdata('userid'),
                        'added_date' => date('Y-m-d H:i:s'));
            $this->Question_model->add($arrData);
            $this->session->set_flashdata('strSuccessMsg','Question added successfully.');
            redirect('others/questions');
        endif;
        $this->data['action'] = 'add';
        $this->template->load('template/main-template', 'questions/ques_add', $this->data);
    }

    public function edit($id)
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost)):
            $arrData = array(
                        'question' => ucfirst($arrPost['txtquestion']),
                        'isactive' => isset($arrPost['chkisactive']) ? 1 : 0,
                        'last_modified_by' => $this->session->userdata('userid'),
                        'last_modified_date' => date('Y-m-d H:i:s'));
            $this->Question_model->edit($arrData, $id);
            $this->session->set_flashdata('strSuccessMsg','Question updated successfully.');
            redirect('others/questions');
        endif;
        $this->data['action'] = 'edit';
        $this->data['arrData'] = $this->Question_model->getData($id);
        $this->template->load('template/main-template', 'questions/ques_add', $this->data);
    }

    public function view($id)
    {
        $this->data['action'] = 'view';
        $this->data['arrData'] = $this->Question_model->getData($id);
        $this->template->load('template/main-template', 'questions/ques_add', $this->data);
    }

    public function delete()
    {
        $this->Question_model->edit(
                                array('is_delete'  => 1,
                                      'delete_by'  => $this->session->userdata('userid'),
                                      'delete_date'=> date('Y-m-d H:i:s')), 
                                $this->uri->segment(4));
        $this->session->set_flashdata('strSuccessMsg','Question deleted successfully.');
        redirect('settings/security_questions');
    }


}