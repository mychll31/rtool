<?php 
class Database_type extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->data = array();
        $this->load->model(array('Database_type_model'));
    }

    public function index()
    {
        $this->data['arrData'] = $this->Database_type_model->getData();
        $this->template->load('template/main-template', 'database_type/_view', $this->data);
    }

    public function add()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost)):
            $arrData = array(
                        'name' => $arrPost['txtname'],
                        'created_by' => $this->session->userdata('userid'),
                        'created_date' => date('Y-m-d H:i:s'));
            if(!$this->Database_type_model->isExists($arrPost['txtname'])):
                $this->Database_type_model->add($arrData);
                $this->session->set_flashdata('strSuccessMsg','Database type added successfully.');
                redirect('config/database_type');
            else:
                $this->data['err']['label'] = 'Type already exists.';
            endif;
        endif;
        $this->data['action'] = 'add';
        $this->template->load('template/main-template', 'database_type/_form', $this->data);
    }

    public function view($id)
    {
        $this->data['action'] = 'view';
        $this->data['arrData'] = $this->Database_type_model->getData($id);
        $this->template->load('template/main-template', 'database_type/_form', $this->data);
    }

    public function edit($id)
    {
        $arrPost = $this->input->post();
        $this->data['arrData'] = $this->Database_type_model->getData($id);
        if(!empty($arrPost)):
            $arrData = array(
                            'name' => $arrPost['txtname'],
                            'isactive' => isset($arrPost['chkisactive']) ? 1 : 0,
                            'updated_by' => $this->session->userdata('userid'),
                            'updated_date' => date('Y-m-d H:i:s'));
            if(!$this->Database_type_model->isExists($arrPost['txtname'], $id)):
                $this->Database_type_model->edit($arrData, $id);
                $this->session->set_flashdata('strSuccessMsg','Database type updated successfully.');
                redirect('config/database_type');
            else:
                $this->data['err']['label'] = 'Type already exists.';
            endif;
        endif;
        $this->data['action'] = 'edit';
        $this->template->load('template/main-template', 'database_type/_form', $this->data);
    }

    public function delete()
    {
        $this->Database_type_model->delete($_POST['txtid']);
        $this->session->set_flashdata('strSuccessMsg','Database type deleted successfully.');
        redirect('config/database_type');
    }

}