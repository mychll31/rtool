<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Database_type_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'database_types';
	}

	function add($arrData)
	{
		$this->db->insert($this->table, $arrData);
		$id = $this->db->insert_id();
		log_action($this->db->last_query(), 'Add database type','',1);
		return $id;		
	}

	function edit($arrData, $id)
	{
		$json = json_encode($this->db->get_where($this->table, array('id' => $id))->result_array());
		$this->db->where('id',$id);
		$this->db->update($this->table, $arrData);
		log_action($this->db->last_query(), 'Update database type', $json,1);
		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$json = json_encode($this->db->get_where($this->table, array('id' => $id))->result_array());
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		log_action($this->db->last_query(), 'Delete database type', $json,1);
		return $this->db->affected_rows();	
	}

	function getData($id=0)
	{
		if($id==0):
			return $this->db->get($this->table)->result_array();
		else:
			$result = $this->db->get_where($this->table, array('id' => $id))->result_array();
			return $result[0];
		endif;
	}

	function isExists($code, $id=0)
	{
		$result = $this->db->get_where($this->table, array('name' => $code, 'id !=' => $id))->result_array();
		if(count($result) > 0):
			return true;
		else:
			return false;
		endif;
	}

}