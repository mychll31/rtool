<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Role_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'roles';
	}

	function add($arrData)
	{
		$this->db->insert($this->table, $arrData);
		$id = $this->db->insert_id();
		log_action($this->db->last_query(), 'Add Question','',1);
		return $id;		
	}

	function edit($arrData, $id)
	{
		$json = json_encode($this->db->get_where($this->table, array('id' => $id))->result_array());
		$this->db->where('id',$id);
		$this->db->update($this->table, $arrData);
		log_action($this->db->last_query(), 'Update Question', $json,1);
		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$json = json_encode($this->db->get_where($this->table, array('id' => $id))->result_array());
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		log_action($this->db->last_query(), 'Delete Question', $json,1);
		return $this->db->affected_rows();	
	}

	function getData($id=0, $module=null)
	{
		$cond1 = array();
		if($module!=null):
			$cond1 = array('module' => $module);
		endif;

		if($id==0):
			return $this->db->get_where($this->table, $cond1)->result_array();
		else:
			$result = $this->db->get_where($this->table, array('id' => $id))->result_array();
			return $result[0];
		endif;
	}

}