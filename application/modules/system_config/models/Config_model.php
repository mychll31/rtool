<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Config_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'dbaseconfig';
		$this->tblType = 'database_types';
	}

	function add($arrData)
	{
		$this->db->insert($this->table, $arrData);
		$id = $this->db->insert_id();
		log_action($this->db->last_query(), 'Add database', '',1);
		return $id;		
	}

	function edit($arrData, $id)
	{
		$json = json_encode($this->db->get_where($this->table, array('id' => $id))->result_array());
		$this->db->where('id',$id);
		$this->db->update($this->table, $arrData);
		log_action($this->db->last_query(), 'Update database', $json,'',1);
		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$json = json_encode($this->db->get_where($this->table, array('id' => $id))->result_array());
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		log_action($this->db->last_query(), 'Delete database', $json,2);
		return $this->db->affected_rows();	
	}

	function getData($id=0)
	{
		if($id==0):
			return $this->db->get($this->table)->result_array();
		else:
			$result = $this->db->get_where($this->table, array($this->table.'.id' => $id))->result_array();
			return $result[0];
		endif;
	}

	function isExists($dbtype, $code, $id=0)
	{
		$result = $this->db->get_where($this->table, array('db_type' => $dbtype, 'db_label' => $code, 'id !=' => $id))->result_array();
		return count($result);
	}

}