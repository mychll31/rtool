<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Permission_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->table = 'permissions';
		$this->tblProfiles = 'profiles';
		$this->tblRoles = 'roles';
	}

	function add($arrData)
	{
		$this->db->insert($this->table, $arrData);
		$id = $this->db->insert_id();
		log_action($this->db->last_query(), 'Add Permission','',1);
		return $id;		
	}

	function edit($arrData, $id)
	{
		$json = json_encode($this->db->get_where($this->table, array('userid' => $id))->result_array());
		$this->db->where('userid',$id);
		$this->db->update($this->table, $arrData);
		log_action($this->db->last_query(), 'Update Permission', $json,1);
		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$json = json_encode($this->db->get_where($this->table, array('userid' => $id))->result_array());
		$this->db->where('userid', $id);
		$this->db->delete($this->table);
		log_action($this->db->last_query(), 'Delete Permission', $json,1);
		return $this->db->affected_rows();	
	}

	function getData($id=0)
	{
		if($id==0):
			return $this->db->join($this->tblProfiles, $this->tblProfiles.'.user_id='.$this->table.'.userid')
					->join($this->tblRoles, $this->tblRoles.'.id='.$this->table.'.roleid')
					->get($this->table)->result_array();
		else:
			return $this->db->join($this->tblProfiles, $this->tblProfiles.'.user_id='.$this->table.'.userid')
					->join($this->tblRoles, $this->tblRoles.'.id='.$this->table.'.roleid')
					->get_where($this->table, array('userid' => $id))->result_array();
		endif;
	}

}