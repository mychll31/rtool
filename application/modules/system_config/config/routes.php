<?php 
$route['users'] = 'Users/index';
$route['users/add'] = 'Users/add';

// Roles
$route['roles'] = 'users/roles';

// Security Question
$route['others/questions'] = 'system_config/questions';
$route['others/questions/add'] = 'system_config/questions/add';
$route['others/questions/edit/(:any)'] = 'system_config/questions/edit/$1';
$route['others/questions/view/(:any)'] = 'system_config/questions/view/$1';
$route['others/questions/delete'] = 'system_config/questions/delete';

// Permissions
$route['permissions'] = 'system_config/permissions';
$route['permissions/add'] = 'system_config/permissions/add';
$route['permissions/edit/(:any)'] = 'system_config/permissions/edit/$1';
$route['permissions/view/(:any)'] = 'system_config/permissions/view/$1';
$route['permissions/delete'] = 'system_config/permissions/delete';

// Config
$route['config/database'] = 'system_config/config';
$route['config/database/add'] = 'system_config/config/add';
$route['config/database/edit/(:any)'] = 'system_config/config/edit/$1';
$route['config/database/view/(:any)'] = 'system_config/config/view/$1';
$route['config/database/delete'] = 'system_config/config/delete';

// database type
$route['config/database_type'] = 'system_config/database_type';
$route['config/database_type/add'] = 'system_config/database_type/add';
$route['config/database_type/edit/(:any)'] = 'system_config/database_type/edit/$1';
$route['config/database_type/view/(:any)'] = 'system_config/database_type/view/$1';
$route['config/database_type/delete'] = 'system_config/database_type/delete';