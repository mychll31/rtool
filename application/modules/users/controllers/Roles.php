<?php 
class Roles extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->data = array();
        $this->load->model(array('Role_model'));
    }

    public function index()
    {
        $this->data['permissions'] = $this->Role_model->getPermissionData();
        $this->template->load('template/main-template', 'users/user_view', $this->data);
    }
}