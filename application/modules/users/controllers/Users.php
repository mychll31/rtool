<?php 
class Users extends MX_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->data = array();
        $this->load->model(array('users/User_model','user_login/Login_model'));
    }

    public function index()
    {
        $this->data['users'] = $this->User_model->getData();
        $this->template->load('template/main-template', 'users/user_view', $this->data);
    }

    public function registration()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost)):
            $isUserExist = $this->User_model->getUserByUsername($arrPost['txtuname']);
            $isEmailExist = $this->User_model->checkemail_exist($arrPost['txtemail']);
            $options = ['cost' => 12];
            $password = password_hash($arrPost['txtpword'], PASSWORD_BCRYPT, $options);
            if(count($isUserExist) == 0):
                if($isEmailExist == 0):
                    #add users
                    $arrData = array(
                                'username' => $arrPost['txtuname'],
                                'password' => $password,
                                'confirmation_code' => uniqid().date('Ymd'),
                                'added_date' => date('Y-m-d H:i:s'),
                                'added_by' => 0,
                                'added_by_ip' => $_SERVER['REMOTE_ADDR']);
                    $id = $this->User_model->addUser($arrData);
                    $custom_userid = $this->User_model->edit(array('custom_userid' => set_custom_userid($id)), $id);
                    #add profile
                    $arrData = array(
                                'userid' => $id,
                                'firstname' => $arrPost['txtfname'],
                                'surname' => $arrPost['txtmname'],
                                'middlename' => $arrPost['txtlname'],
                                'birthday' => date('Y-m-d', strtotime($arrPost['txtbdate'])),
                                'gender' => $arrPost['radgender'],
                                'email' => $arrPost['txtemail'],
                                'sec_ques_1' => $arrPost['selquestion'],
                                'sec_answer_1' => $arrPost['txtanswer'],
                                'added_date' => date('Y-m-d H:i:s'),
                                'added_by' => 0);
                    $this->User_model->addProfile($arrData);
                    //TODO:: Send Confirmation to Email
                    redirect('login');
                else:
                    $this->data['email_error'] = 'Email address already exists';
                endif;
            else:
                $this->data['error'] = 'Username already exists';
            endif;
        endif;
        $this->data['questions'] = $this->User_model->getQuestionData();
        $this->template->load('template/user-template', 'users/register', $this->data);
    }

    public function add()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost)):
            $arrPost['txtbday'] = date('Y-m-d', strtotime($arrPost['txtbday']));
            $arrData = array(
                        'username' => $arrPost['txtuname'],
                        'password' => $arrPost['txtpword'],
                        'added_date' => $this->session->userdata('userid'),
                        'added_by' => date('Y-m-d H:i:s'));
            $userid = $this->User_model->addUser($arrData);
            $arrData = array(
                        'userid' => $userid,
                        'first_name' => $arrPost['txtfname'],
                        'last_name' => $arrPost['txtmname'],
                        'middle_name' => $arrPost['txtlname'],
                        'birthday' => date('Y-m-d', strtotime($arrPost['txtbday'])),
                        'gender' => $arrPost['radgender'],
                        'email' => $arrPost['txtemail'],
                        'sec_question_id' => $arrPost['selquestion'],
                        'sec_answer' => $arrPost['txtanswer']);
            $this->User_model->addProfile($arrData);
            $this->session->set_flashdata('strSuccessMsg','User added successfully.');
            redirect('users');
        endif;
        $this->data['action'] = 'add';
        $this->data['questions'] = $this->User_model->getQuestionData();
        $this->template->load('template/main-template', 'users/user_add', $this->data);
    }

    public function view($id)
    {
        $this->data['action'] = 'view';
        $this->data['user'] = $this->User_model->getData($id);
        $this->data['questions'] = $this->User_model->getQuestionData();
        $this->template->load('template/main-template', 'users/user_add', $this->data);
    }

    public function edit($id)
    {
        $arrPost = $this->input->post();
        $this->data['user'] = $this->User_model->getData($id);

        if(!empty($arrPost)):
            $arrPost['txtbday'] = date('Y-m-d', strtotime($arrPost['txtbday']));
            // $options = ['cost' => 12];
            // $password = password_hash($arrPost['txtpword'], PASSWORD_BCRYPT, $options);
            $arrData = array(
                        'username' => $arrPost['txtuname'],
                        // 'password' => $password,
                        'isconfirmed' => isset($arrPost['chkisconfirmed']) ? 1 : 0,
                        'isactive' => isset($arrPost['chkisactive']) ? 1 : 0,
                        'last_modified_by' => $this->session->userdata('userid'),
                        'last_modified_date' => date('Y-m-d H:i:s'),
                        'last_modified_by_ip' => $_SERVER['REMOTE_ADDR']);
            $this->User_model->edit($arrData, $id);
            $arrData = array(
                        'first_name' => $arrPost['txtfname'],
                        'last_name' => $arrPost['txtmname'],
                        'middle_name' => $arrPost['txtlname'],
                        'birthday' => date('Y-m-d', strtotime($arrPost['txtbday'])),
                        'gender' => $arrPost['radgender'],
                        'email' => $arrPost['txtemail'],
                        'sec_question_id' => $arrPost['selquestion'],
                        'sec_answer' => $arrPost['txtanswer']);
            $this->User_model->editProfile($arrData, $id);
            $this->session->set_flashdata('strSuccessMsg','User updated successfully.');
            redirect('users/view/'.$id);
        endif;
        $this->data['action'] = 'edit';
        $this->data['questions'] = $this->User_model->getQuestionData();
        $this->data['user'] = $this->User_model->getData($id);
        $this->template->load('template/main-template', 'users/user_add', $this->data);
    }

    public function delete()
    {
        $this->User_model->delete($_POST['txtuserid']);
        $this->User_model->deleteProfile($_POST['txtuserid']);
        $this->session->set_flashdata('strSuccessMsg','User deleted successfully.');
        redirect('users');
    }

    public function forget_password()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost)):
            # check if email is exists
            $userid = $this->User_model->checkemail_exist($arrPost['txtemail']);
            if($userid > 0) :
                $hash_code = password_hash(date('Ymdh'), PASSWORD_BCRYPT, ['cost' => 12]);
                $url = base_url('change_password?u='.$userid.'&c='.$hash_code);
                echo 'userid='.$userid.'<br>';
                echo 'url='.$url.'<br>';
                $action = $this->User_model->edit(array('confirmation_code' => $hash_code), $userid);
                log_action($action,'Forget password confirmation code','',1);
                $this->data['success'] = 1;
            elseif($userid == -1):
                $this->data['error'] = 'Email is deleted please contact administrator.';
            elseif($userid == 0):
                $this->data['error'] = 'Email does not exists.';
            else:
            endif;
        endif;

        $this->template->load('template/user-template', 'user_login/forget_password', $this->data);
    }

    public function change_password()
    {
        $userid = $_GET['u'];
        $code = $_GET['c'];
        $user = $this->User_model->getData($userid);
        $arrPost = $this->input->post();
        if(!empty($arrPost)):
            $arrData = array(
                        'password' => password_hash($arrPost['txtpass'], PASSWORD_BCRYPT, ['cost' => 12]),
                        'last_pass_changed' => date('Y-m-d H:i:s'));
            $action = $this->User_model->edit($arrData, $userid);
            log_action($action,'Change Password',json_encode($user),2);
            $this->Login_model->add_session($user['userid'], $user['access_level'], $user['username'], $user['surname'], $user['firstname'], $user['middlename'], $user['custom_userid'], $user['image_fname']);
            redirect('reports/report_all');
        else:
            echo $userid;
            echo $code;
            echo '<br>';
            print_r($user);
            if(!password_verify(date('Ymdh'),$code)):
                $this->data['error'] = 1;
            endif;
        endif;

        $this->template->load('template/user-template', 'user_login/retype_password', $this->data);
    }


}