<pre><?php print_r($_SESSION) ?></pre>
<?=set_title('Users')?>
<?=load_plugin('css', array('datatables')) ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=base_url('home')?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>User Management</span>
        </li>
    </ul>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Managed User</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<?=base_url('users/add')?>" id="sample_editable_1_new" class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="loading-image"><center><img src="<?=base_url('assets/images/spinner-blue.gif')?>"></center></div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblusers" style="visibility: hidden;">
                    <thead>
                        <tr>
                            <th style="width:7% !important"> No </th>
                            <th> Name </th>
                            <th> Birthday </th>
                            <th> Gender </th>
                            <th> Email </th>
                            <th> Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($users) > 0): $no=1; foreach($users as $user): ?>
                            <tr class="odd gradeX" data-id="<?=$user['userid']?>">
                                <td><?=$no++?></td>
                                <td><?=getFullname($user['first_name'], $user['last_name'], $user['middle_name'])?></td>
                                <td><?=$user['birthday']?></td>
                                <td><?=$user['gender'] == 'f' ? 'Female' : 'Male'?></td>
                                <td><?=$user['email']?></td>
                                <td>
                                    <?=$user['isconfirmed'] == 1 ? '' : '<span class="small label label-warning">Unconfirmed</span>'?>
                                    <?=$user['isactive'] == 1 ? '<span class="small label label-primary">Active</span>' : '<span class="small label label-danger">Inactive</span>'?></td>
                            </tr>
                        <?php endforeach; endif;?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<?=load_plugin('js', array('datatables')) ?>
<script>
    $(document).ready(function() {
        $('#tblusers').dataTable( {
            "initComplete": function(settings, json) {
                $('.loading-image').hide();
                $('#tblusers').css('visibility', 'visible');
            }} );
        $('#tblusers').on('click', 'tbody > tr', function () {
            window.location.href = "<?=base_url('users/view').'/'?>"+$(this).data('id');
        });
    });
</script>