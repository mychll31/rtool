<h3 class="form-title font-green">Sign In</h3>
<div class="alert alert-danger" <?=isset($err_uname) ? '' : 'hidden'?>>
    <button class="close" data-close="alert"></button>
    <span> <?=isset($err_uname) ? $err_uname : 'Enter username and password'?> </span>
</div>
<form class="login-form" action="<?=base_url('login')?>" method="post">
	<div class="form-group <?=isset($err_uname) ? 'has-error' : ''?>">
    	<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    	<label class="control-label">Username</label>
    	<input class="form-control" type="text" autocomplete="off" 
    		name="username" id="username" value="<?=set_value('username')?>" />
    </div>
	<div class="form-group <?=isset($err_uname) ? 'has-error' : ''?>">
    	<label class="control-label">Password</label>
    	<input class="form-control" type="password" autocomplete="off" 
    		name="password" id="password" />
    </div>
	<div class="form-actions">
    	<button type="submit" class="btn green" id="btnlogin">Login</button>
    	<label class="rememberme check">
        	<input type="checkbox" name="remember" value="1" />Remember </label>
	</div>
	<div class="create-account">
		<p><a href="<?=base_url('registration')?>" id="register-btn" class="uppercase">Create an account</a></p>
	</div>
</form>

<?=load_plugin('js', array('select','datetime_picker','login-validation')) ?>