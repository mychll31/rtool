<br><h4>Registration</h4>
<form class="form" action="<?=base_url('registration')?>" method="post">
    <div class="form-group">
        <label class="control-label">Firstname</label>
        <input type="text" class="form-control" name="txtfname" id="txtfname" value="<?=set_value('txtfname')?>">
        <span class="help-block"></span>
    </div>
    <div class="form-group">
        <label class="control-label">Middlename</label>
        <input type="text" class="form-control" name="txtmname" id="txtmname" value="<?=set_value('txtmname')?>">
        <span class="help-block"></span>
    </div>
    <div class="form-group">
        <label class="control-label">Lastname</label>
        <input type="text" class="form-control" name="txtlname" id="txtlname" value="<?=set_value('txtlname')?>">
        <span class="help-block"></span>
    </div>
    <div class="form-group">
        <label class="control-label">Birthday</label>
        <div class="input-group input-medium date date-picker" id="dtbdate"
            data-date-format="dd-M-yyyy" data-date-viewmode="years" style="width: 100% !important;">
            <input type="text" class="form-control" name="txtbdate" id="txtbdate" value="<?=set_value('txtbdate')?>" readonly>
            <span class="input-group-btn">
                <button class="btn default" type="button" id="btnbdate">
                    <i class="fa fa-calendar"></i>
                </button>
            </span>
        </div>
        <span class="help-block"></span>
    </div>
    <div class="form-group">
        <label class="control-label">Gender</label>
        <div class="radio-list" id="optgender">
            <label class="radio-inline">
                <input type="radio" name="radgender" id="radfemale" value="f" <?=set_value('radgender') == 'f' ? 'checked' : ''?>> Female </label>
            <label class="radio-inline">
                <input type="radio" name="radgender" id="radmale" value="m" <?=set_value('radgender') == 'm' ? 'checked' : ''?>> Male </label>
        </div>
        <span class="help-block"></span>
    </div>
    <div class="form-group <?=isset($email_error) ? 'has-error' : ''?>"">
        <label class="control-label">Email</label>
        <input type="text" class="form-control" name="txtemail" id="txtemail" value="<?=set_value('txtemail')?>">
        <span class="help-block"><?=isset($email_error) ? $email_error : ''?></span>
    </div>
    <div class="form-group <?=isset($error) ? 'has-error' : ''?>">
        <label class="control-label">Username</label>
        <input type="text" class="form-control" name="txtuname" id="txtuname" value="<?=set_value('txtuname')?>">
        <span class="help-block"><?=isset($error) ? $error : ''?></span>
    </div>
    <div class="form-group">
        <label class="control-label">Password</label>
        <input type="password" class="form-control" name="txtpword" id="txtpword" value="<?=set_value('txtpword')?>">
        <span class="help-block"></span>
    </div>
    <div class="form-group">
        <label class="control-label">Retype Password</label>
        <input type="password" class="form-control" name="txtretpword" id="txtretpword" value="<?=set_value('txtretpword')?>">
        <span class="help-block"></span>
    </div>
    <div class="form-group">
        <label class="control-label">Security Question</label>
        <select name="selquestion" id="selquestion" class="bs-select form-control">
            <option value="">Select Question</option>
            <?php foreach($questions as $q): ?>
                <option value="<?=$q['id']?>" <?=set_value('selquestion') == $q['id'] ? 'selected' : ''?>><?=$q['question']?></option>
            <?php endforeach; ?>
        </select>
        <span class="help-block"></span>
    </div>
    <div class="form-group">
        <label class="control-label">Answer</label>
        <input type="text" class="form-control" name="txtanswer" id="txtanswer" value="<?=set_value('txtanswer')?>">
        <span class="help-block"></span>
    </div>
    <div class="form-actions">
        <button type="submit" id="submit_register" class="btn btn-success">Submit</button>
        <a href="login" class="btn btn-default">Back to Login</a>
    </div>
</form>
<script src="<?=base_url('assets/js/registration-validation.js')?>"></script>