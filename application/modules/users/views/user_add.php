<?=set_title('Users - '.$action)?>
<?=load_plugin('css', array('datetime_picker')) ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?=base_url('home')?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="<?=base_url('users')?>">User Management</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span><?=ucfirst($action)?></span>
        </li>
    </ul>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ADD COUNTRY-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> <?=$action?> User</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" <?=$action == 'view' ? '' : 'hidden'?>>
                                <a href="<?=base_url('users/edit/').$this->uri->segment(3)?>" class="btn btn-sm blue">Edit</a>
                                <a href="#" id="btndelete" class="btn btn-sm red">Delete</a>
                                <a href="<?=base_url('users')?>" class="btn btn-sm default">Cancel</a>
                            </div>
                            <?php $form = $action == 'add' ? 'users/add' : 'users/edit/'.$this->uri->segment(3); ?>
                            <form role="form" action="<?=base_url($form)?>" method="post" class="form-horizontal">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Firstname</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="txtfname" id="txtfname"
                                                value="<?=isset($user) ? $user['first_name'] : ''?>"
                                                <?=$action == 'view' ? 'disabled' : ''?>>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Middlename</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="txtmname" id="txtmname"
                                                value="<?=isset($user) ? $user['middle_name'] : ''?>"
                                                <?=$action == 'view' ? 'disabled' : ''?>>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Lastname</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="txtlname" id="txtlname"
                                                value="<?=isset($user) ? $user['last_name'] : ''?>"
                                                <?=$action == 'view' ? 'disabled' : ''?>>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Birthday</label>
                                        <div class="col-sm-8">
                                            <div class="input-group input-medium date date-picker" id="dtbdate" data-date-format="yyyy-M-dd" data-date-viewmode="years">
                                                <input type="text" class="form-control" name="txtbday" id="txtdate"
                                                    value="<?=isset($user) ? date('Y-M-d', strtotime($user['birthday'])) : ''?>"
                                                    <?=$action == 'view' ? 'disabled' : ''?>>
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Gender</label>
                                        <div class="col-sm-8">
                                            <div class="radio-list">
                                                <label class="radio-inline">
                                                    <input type="radio" name="radgender" id="radfemale" <?=$action == 'view' ? 'disabled' : ''?> value="f" <?=isset($user) ? $user['gender'] == 'f' ? 'checked' : '' : ''?>> Female </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="radgender" id="radmale" <?=$action == 'view' ? 'disabled' : ''?> value="m" <?=isset($user) ? $user['gender'] == 'm' ? 'checked' : '' : ''?>> Male </label>
                                            </div>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Email</label>
                                        <div class="col-sm-8">
                                            <input type="email" class="form-control" name="txtemail" id="txtemail"
                                                value="<?=isset($user) ? $user['email'] : ''?>"
                                                <?=$action == 'view' ? 'disabled' : ''?>>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Username</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="txtuname" id="txtuname"
                                                value="<?=isset($user) ? $user['username'] : ''?>"
                                                <?=$action == 'view' ? 'disabled' : ''?>>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="form-group" <?=$action != 'add' ? 'hidden' : ''?>>
                                        <label class="control-label col-sm-2">Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control" name="txtpword" id="txtpword"
                                                value="<?=isset($user) ? $user['password'] : ''?>">
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="form-group" <?=$action != 'add' ? 'hidden' : ''?>>
                                        <label class="control-label col-sm-2">Re-type Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control" name="txtretpword" id="txtretpword"
                                                value="<?=isset($user) ? $user['password'] : ''?>">
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Security Question</label>
                                        <div class="col-sm-8">
                                            <select name="selquestion" id="selquestion" class="bs-select form-control" <?=$action == 'view' ? 'disabled' : ''?>>
                                                <option value="">Select Question</option>
                                                <?php foreach($questions as $q): ?>
                                                    <option value="<?=$q['id']?>"
                                                        <?=isset($user) ? $user['sec_question_id'] == $q['id'] ? 'selected' : '' : set_value('selquestion')?> >
                                                        <?=$q['question']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Answer</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="txtanswer" id="txtanswer"
                                                value="<?=isset($user) ? $user['sec_answer'] : ''?>"
                                                <?=$action == 'view' ? 'disabled' : ''?>>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="form-group" <?=$action=='add' ? 'hidden' : ''?>>
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-8">
                                            <label><input type="checkbox" name="chkisactive"
                                                        <?=isset($user) ? $user['isactive'] == 1 ? 'checked' : '' : ''?>
                                                        <?=$action=='view' ? 'disabled' : ''?>> Is active</label>
                                            &nbsp;&nbsp;
                                            <label><input type="checkbox" name="chkisconfirmed"
                                                        <?=isset($user) ? $user['isconfirmed'] == 1 ? 'checked' : '' : ''?>
                                                        <?=$action=='view' ? 'disabled' : ''?>> Is confirmed</label>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="form-group" <?=$action == 'view' ? 'hidden' : ''?>>
                                        <div class="col-sm-10 pull-right">
                                            <button type="submit" id="submit_user" class="btn green">Submit</button>
                                            <a href="<?=$action=='edit' ? base_url('users/view/').$this->uri->segment(3) : base_url('users')?>" class="btn default">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END ADD COUNTRY-->
    </div>
</div>

<!-- BEGIN DELETE USER -->
<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="deleteUserModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title bold uppercase" id="deleteUserModal">Delete User</h5>
            </div>
            <form action="<?=base_url('users/delete')?>" method="post">
                <div class="modal-body">
                    <input type="hidden" name="txtuserid" value="<?=$this->uri->segment(3)?>">
                    Are you sure you want to delete this data?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">okay</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END DELETE USER -->

<?php if($action != 'view') { echo load_plugin('js', array('datetime_picker','form-validation')); } ?>
<script>
    $(document).ready(function() {
        $('a#btndelete').click(function () {
            $('#deleteUserModal').modal('show');
        });
    });
</script>