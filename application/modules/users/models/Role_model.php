<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Role_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->tblRole = 'roles';
		$this->tblPermission = 'permissions';
		$this->tblProfile = 'profiles';
	}

	// function addUser($arrData)
	// {
	// 	$this->db->insert($this->tblUser, $arrData);
	// 	$id = $this->db->insert_id();
	// 	log_action($this->db->last_query(), 'Add user account');
	// 	return $id;		
	// }

	// function addProfile($arrData)
	// {
	// 	$this->db->insert($this->tblProfile, $arrData);
	// 	log_action($this->db->last_query(), 'Add user profile');
	// 	return $this->db->insert_id();		
	// }

	// function getQuestionData($id=0)
	// {
	// 	if($id==0):
	// 		return $this->db->get($this->tblQuestions)->result_array();
	// 	else:
	// 		$result = $this->db->get_where($this->tblQuestions, array('id' => $id))->result_array();
	// 		return $result[0];
	// 	endif;
	// }

	// function getUserByUsername($username)
	// {
	// 	$result = $this->db->get_where($this->tblUser, array('username' => $username))->result_array();
	// 	return count($result) > 0 ? $result[0] : null;
	// }
	
	function getPermissionData($id=0)
	{
		if($id==0):
			return $this->db->join($this->tblRole, $this->tblRole.'.id = '.$this->tblPermission.'.roleid')
					->join($this->tblProfile, $this->tblProfile.'.userid = '.$this->tblPermission.'.userid')
					->get($this->tblPermission)->result_array();
		else:
			$result = $this->db->join($this->tblRole, $this->tblRole.'.id = '.$this->tblPermission.'.roleid')
						->join($this->tblProfile, $this->tblProfile.'.userid = '.$this->tblPermission.'.userid')
						->get_where($this->tblPermission, array('id' => $id))->result_array();
			return count($result) > 0 ? $result[0] : 0;
		endif;
	}

	function getRoleData($id=0)
	{
		if($id==0):
			return $this->db->get($this->tblRole)->result_array();
		else:
			$result = $this->db->get_where($this->tblRole, array('id' => $id))->result_array();
			return count($result) > 0 ? $result[0] : 0;
		endif;
	}

	// function getData($id=0)
	// {
	// 	if($id==0):
	// 		return $this->db->join($this->tblProfile, $this->tblProfile.'.userid = '.$this->tblUser.'.id')
	// 				->get($this->tblUser)->result_array();
	// 	else:
	// 		$result = $this->db->join($this->tblProfile, $this->tblProfile.'.userid = '.$this->tblUser.'.id')
	// 				->get_where($this->tblUser, array('id' => $id))->result_array();
	// 		return count($result) > 0 ? $result[0] : 0;
	// 	endif;
	// }

	// function edit($arrData, $id)
	// {
	// 	$json = json_encode($this->db->get_where($this->tblUser, array('id' => $id))->result_array());
	// 	$this->db->where('id',$id);
	// 	$this->db->update($this->tblUser, $arrData);
	// 	log_action($this->db->last_query(), 'Update user account', $json);
	// 	return $this->db->affected_rows();
	// }

	// function editProfile($arrData, $id)
	// {
	// 	$json = json_encode($this->db->get_where($this->tblProfile, array('userid' => $id))->result_array());
	// 	$this->db->where('userid',$id);
	// 	$this->db->update($this->tblProfile, $arrData);
	// 	log_action($this->db->last_query(), 'Update user profile', $json);
	// 	return $this->db->affected_rows();
	// }

	// function delete($id)
	// {
	// 	$json = json_encode($this->db->get_where($this->tblUser, array('id' => $id))->result_array());
	// 	$this->db->where('id', $id);
	// 	$this->db->delete($this->tblUser);
	// 	log_action($this->db->last_query(), 'Delete user account', $json);
	// 	return $this->db->affected_rows();	
	// }

	// function deleteProfile($id)
	// {
	// 	$json = json_encode($this->db->get_where($this->tblProfile, array('userid' => $id))->result_array());
	// 	$this->db->where('userid', $id);
	// 	$this->db->delete($this->tblProfile);
	// 	log_action($this->db->last_query(), 'Delete user profile', $json);
	// 	return $this->db->affected_rows();	
	// }
		
}
/* End of file User_model.php
 * Location: ./application/modules/libraries/models/User_model.php */