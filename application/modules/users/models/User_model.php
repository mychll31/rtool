<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {

	function __construct()
	{
		$this->load->database();
		$this->users = 'tblusers';
		$this->profile = 'tblprofiles';
		$this->question = 'tblsecurity_questions';
	}

	function addUser($arrData)
	{
		$this->db->insert($this->users, $arrData);
		$id = $this->db->insert_id();
		log_action($this->db->last_query(), 'Add user account','',1);
		return $id;		
	}

	function addProfile($arrData)
	{
		$this->db->insert($this->profile, $arrData);
		log_action($this->db->last_query(), 'Add user profile','',1);
		return $this->db->insert_id();		
	}

	function getQuestionData($id=0)
	{
		if($id==0):
			return $this->db->get($this->question)->result_array();
		else:
			$result = $this->db->get_where($this->question, array('id' => $id))->result_array();
			return $result[0];
		endif;
	}

	function getUserByUsername($username)
	{
		$result = $this->db->get_where($this->users, array('username' => $username))->result_array();
		return count($result) > 0 ? $result[0] : null;
	}

	function checkemail_exist($email)
	{
		$result = $this->db->get_where($this->profile, array('email' => $email))->result_array();
		return count($result) > 0 ? $result[0]['is_delete'] == 1 ? -1 : $result[0]['userid'] : 0;
	}
	
	function getData($id=0, $stat=null)
	{
		if($stat!=null){
			$cond1 = array($this->users.'.userid' => $id, 'is_active' => $stat);
			$cond2 = array('is_active' => $stat);
		}else{
			$cond1 = array($this->users.'.userid' => $id);
			$cond2 = array();
		}

		if($id==0):
			return $this->db->join($this->profile, $this->profile.'.userid = '.$this->users.'.userid')
					->get_where($this->users, $cond2)->result_array();
		else:
			$result = $this->db->join($this->profile, $this->profile.'.userid = '.$this->users.'.userid')
					->get_where($this->users, $cond1)->result_array();
			return count($result) > 0 ? $result[0] : 0;
		endif;
	}

	function edit($arrData, $userid)
	{
		// $json = json_encode($this->db->get_where($this->users, array('userid' => $userid))->result_array());
		$this->db->where('userid',$userid);
		$this->db->update($this->users, $arrData);
		// log_action($this->db->last_query(), 'Update user account', $json);
		return $this->db->last_query();
	}

	function editProfile($arrData, $id)
	{
		$json = json_encode($this->db->get_where($this->profile, array('userid' => $id))->result_array());
		$this->db->where('userid',$id);
		$this->db->update($this->profile, $arrData);
		log_action($this->db->last_query(), 'Update user profile', $json,1);
		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$json = json_encode($this->db->get_where($this->users, array('id' => $id))->result_array());
		$this->db->where('id', $id);
		$this->db->delete($this->users);
		log_action($this->db->last_query(), 'Delete user account', $json,2);
		return $this->db->affected_rows();	
	}

	function deleteProfile($id)
	{
		$json = json_encode($this->db->get_where($this->profile, array('userid' => $id))->result_array());
		$this->db->where('userid', $id);
		$this->db->delete($this->profile);
		log_action($this->db->last_query(), 'Delete user profile', $json,2);
		return $this->db->affected_rows();	
	}
		
}
/* End of file User_model.php
 * Location: ./application/modules/libraries/models/User_model.php */