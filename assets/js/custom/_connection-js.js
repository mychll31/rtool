/* Module name: Project; Page name: connection */
function clear_error(excon = 0)
{
    $('.form-group').removeClass('has-error');
    $('.help-block').text('');
    if(excon == 1){
        $('.testconnection').show();
        $('.addconnection,.div-host,.div-form,.div-upload').hide();
        $('#seldbtype').val('');
        $('#seldbtype').selectpicker('refresh');
    } else if(excon == 2) {
        $('input[type="text"]').val('');
        $('.span-fname').hide();
        $('.testconnection').show();
        $('.newdata-source,.addconnection,.div-host,.div-form,.div-upload').hide();
        $('#seldbtype').val('');
        $('#seldbtype').selectpicker('refresh');
    } else {
        //
    }
    $('.alert-danger').hide();
}
var new_connection = '';
$(document).ready(function()
{
    $('.alert-danger').hide();
    $('#tblconnection').dataTable( {
        "pageLength": 5,
        "initComplete": function(settings, json) {
            $('.loading-image').hide();
            $('#tblconnection').css('visibility', 'visible');
        }} );

    $('#btn-add-conn-list').click(function() {
        $('.span-fname').show().text('');
       $('#connection-modal').modal('show');
    });

    /* Begin selecting existing connection*/
    $('#selexconn').change(function() {
        if($(this).val()!=''){
            $('.new-connection').hide();
        }else{
            $('.new-connection').show();
        }
        clear_error(1);
    });
    /* End selecting existing connection*/

    /* Begin selecting dbtype*/
    $('#seldbtype').change(function () {
        var dbval = $(this).find(':selected').data('wfupload');
        var ftype = $(this).find(':selected').data('ftype');
        
        $('#seldbtype').val($(this).val());
        $('#seldbtype').selectpicker('refresh');
        
        if (dbval == 1) {
            $('#btnfupload').attr('accept',ftype);
            $('.div-upload').show();
            $('.div-host').hide();
        } else {
            $('.div-upload').hide();
            $('.div-host').show();
        }
        $('.div-form,.testconnection').show();
        checknull($(this),1);
        clear_error();
    });
    /* End selecting dbtype*/

    /* Begin Test Connection */
    $('#btnfupload').change(function (e) {
        $('.span-fname').show().text(e.target.files[0].name);
        $('.span-fname').next('span').text('');
        $('div.fileupload-buttonbar').parent('div').parent('div').removeClass('has-error');
    });

    $('#selconnname,#txthost,#txtdbname,#txtuname,#txtpass,#txtretpass').on('keyup',function() {
      checknull($(this),1);
    });
    $('#txtpass,#txtretpass').on('keyup',function() {
      validate_password($('#txtpass'),$('#txtretpass'),1);
    });

    $('#btntestconn').click(function () {
        var exconn = $('#selexconn').val();
        var dbwfupload = $('#seldbtype').find(':selected').data('wfupload');
        
        /* begin adding new connection */
        if (exconn == '') {
            var err = 0;
            err = err + checknull($('#selconnname'),1);
            err = err + checknull($('#seldbtype'),1);
            
            if(!dbwfupload) {
                err = err + checknull($('#txthost'),1);
                err = err + checknull($('#txtdbname'),1);
                err = err + checknull($('#txtuname'),1);
                if(!(checknull($('#txtpass'),1) + checknull($('#txtretpass'),1))) {
                    err = err + validate_password($('#txtpass'),$('#txtretpass'),1);
                }
            } else {
                if($('.span-fname').text() == '') {
                    $('div.fileupload-buttonbar').parent('div').parent('div').addClass('has-error');
                    $('.span-fname').next('span').text('This field is required.');
                    err = err + 1;
                }
                if($('#txtpass').val() != '' || $('#txtretpass').val() != '') {
                    if(!(checknull($('#txtpass'),1) + checknull($('#txtretpass'),1))) {
                        err = err + validate_password($('#txtpass'),$('#txtretpass'),1);
                    }
                }
            }

            if(err < 1) {
                var dbval = $('#seldbtype').find(':selected').data('wfupload');
                $.ajax ({type : 'post',
                        url: $("#txtbase_url").val()+'projects/check_connections',
                        dataType: 'json',
                        data: {
                                "dbwfupload" : dbval,
                                "file_loc" : $('#txtfileloc').val(),
                                "filename" : $('#txtfilename').text(),
                                "db_type" : $('#seldbtype').val(),
                                "hostname" : $('#txthost').val(),
                                "username" : $('#txtuname').val(),
                                "password" : $('#txtpass').val(),
                                "database" : $('#txtdbname').val()
                            },
                        success: function(res){
                            if(res.message == 'Connected'){
                              $('#btntestconn').next('button').next('.alert-danger').hide().html('');
                              $.ajax({type : 'GET',
                                      url: $("#txtbase_url").val()+'projects/check_exists_connname',
                                      dataType: 'json',
                                      data: {"conn_name" : $('#selconnname').val()},
                                      success: function(res) {
                                          if(res.status == 1){
                                              $('#btntestconn').next('button').next('.alert-danger').show().html(res.message);
                                          } else {
                                              $('.testconnection').hide();
                                              $('.newdata-source').show();
                                              // $('#dbwfupload').val(dbval);
                                          }
                                      }
                                  });
                            }else{
                                $('#btntestconn').next('button').next('.alert-danger').show().html(res.message);
                            }
                        }
                });
        
            }
        /* end adding new connection */
        } else {
            /* begin checking connection from existing source */
            $.ajax({
                type: 'POST',
                url: $('#txtbase_url').val()+'projects/check_exists_connections',
                dataType: 'json',
                data: { selexconn: $('#selexconn').val() },
                success: function (res) {
                    if (res.message == 'Connected') {
                        $('.testconnection').hide();
                        $('.addconnection').show();
                        new_connection = res.source_details; 
                    } else {
                        $('#btntestconn').next('button').next('.alert-danger').show().html(res.message);
                    }
                }
            });
        }
         /* end checking connection from existing source */
    });
    /* Begin Test Connection */

    /* Begin Connection */
    $('#btnaddconn').click(function() {
        var newsource = 0;
        $.ajax({
            type: 'POST',
            url: $('#txtbase_url').val()+'projects/add_new_source',
            dataType: 'json',
            data: { project_id: $('#txtreport_id').val(),
                    source_id: new_connection.source_id},
            success: function (res) {
                newsource = res.connid;
                if($.fn.DataTable.isDataTable('#tblconnection')) {
                    $('#tblconnection').DataTable().destroy();
                }
                $('#tblconnection > tbody').append('<tr data-id="'+ new_connection.source_id +'" data-connid="'+ newsource +'"><td>'+ (new_connection.dbhost!=null ? new_connection.dbhost : '') +
                                                        '</td><td>'+ (new_connection.dbname!=null ? new_connection.dbname : '') +
                                                        '</td><td><center>'+ (new_connection.name!=null ? new_connection.name : '') + '</center>'+
                                                        '</td><td>'+ (new_connection.connname!=null ? new_connection.connname : '') +
                                                        '</td><td>'+ (new_connection.actual_filename!=null ? new_connection.actual_filename : '') +
                                                        '</td><td align="center">'+
                                                                '<a class="btn red btn-sm delete-conn">'+
                                                                    '<i class="icon-trash"> </i> Remove </a>'+
                                                        '</td></tr>');
                $('#tblconnection').dataTable({"pageLength": 5});
                toastr.success('Project data source added successfully.');
                $('#connection-modal').modal('hide');
            }
        });
    });
    /* End Connection */

    /*Begin Remove Connection*/
    $('table#tblconnection').on('click', 'a.delete-conn', function() {
        var connid = $(this).closest('tr').data('connid');
        $('#txtsourceid').val(connid);
        $('#delete-modal').modal('show');
    });

    $('.btn-confirm').on('click', function() {
        var connid = $(this).closest('tr').data('connid');
        $.ajax ({type : 'POST', url: $('#txtbase_url').val()+'projects/remove_connection',
                data: { connid : $('#txtsourceid').val()},
                success: function(res){
                    if($.fn.DataTable.isDataTable('table#tblconnection')) {
                        $('table#tblconnection').DataTable().destroy();
                    }
                    tr = $('table#tblconnection').find('tr[data-connid="'+$('#txtsourceid').val()+'"]');
                    tr.remove();
                    $('#tblconnection').dataTable({"pageLength": 5});
                    $('#delete-modal').modal('hide');
                    toastr.success('Project data source removed successfully.');
                }
        });
    });
    /*End Remove Connection*/

    /*Begin add new data source*/
    $('#btnadd-newsource').click(function() {
        var form_data = new FormData();
        form_data.append('files',$('#btnfupload')[0].files[0]);
        form_data.append('connname',$('#selconnname').val());
        form_data.append('txthost',$('#txthost').val());
        form_data.append('dbtype',$('#seldbtype').val());
        form_data.append('txtdbname',$('#txtdbname').val());
        form_data.append('txtuname',$('#txtuname').val());
        form_data.append('txtpass',$('#txtpass').val());
        form_data.append('wfileupload',$('#seldbtype').find(':selected').data('wfupload'));

        $.ajax({
            type: 'POST',
            url: $('#txtbase_url').val()+'datasources/add_new_source',
            dataType: 'json',
            data: form_data,
            contentType: false,
            processData: false,
            success: function (res) {
                if(res.message == 'Connected'){
                    $('.newdata-source').hide();
                    $('.addconnection').show();
                    new_connection = res.source_details;
                    toastr.success('Data source added successfully.');
                }
            }
        });
    });
    /*End add new data source*/

    /* Begin close button*/
    $('.btn-close').click(function() {
        var val = $(this).data('id');
        if(val == 1) {
            // close connection
            $('#connection-modal').modal('hide');
        } else if(val == 2) {
            // cancel addition of data source
            clear_error(2);
            $('#connection-modal').modal('hide');
        } else {
            // cancel addition of connection
        }
    });
    /* End close button*/

    // activate project
    $('a#btnactivate').click(function() {
        $('input#txtaction').val('activate');
        $('.title-conf').html('Activate Project');
        $('.modal-conf').html('Are you sure you want to activate this project?');
        $('#confirm-modal').modal('show');
    });
    // deactivate project
    $('a#btndeactivate').click(function() {
        $('input#txtaction').val('deactivate');
        $('.title-conf').html('Deactivate Project');
        $('.modal-conf').html('Are you sure you want to deactivate this project?');
        $('#confirm-modal').modal('show');
    });

    // delete project
    $('a#btndelete-project').click(function() {
        var baseurl = $('#txtbase_url').val();
        $('input#txtaction').val('delete');
        $('#frmconfirm').attr('action', baseurl+'projects/project_delete/'+$('#txtreport_id').val());
        $('.title-conf').html('Delete Project');
        $('.modal-conf').html('Are you sure you want to delete this project?');
        $('#confirm-modal').modal('show');
    });

    // confirmation modal
    $('button.btn-confirm').click(function(e) {
        var baseurl = $('#txtbase_url').val();
        var action = $('#txtaction').val();
        var reportid = $('#txtreport_id').val();
        if(action=='delete-table'){
            e.preventDefault();
            $.ajax ({type : 'POST', url: baseurl+'projects/remove_tables',
                    data: {'txtdelid' : $('#txtdelid').val(),'txtdelmode' : $('#txtdelmode').val()},
                    success: function(res){
                        if($.fn.DataTable.isDataTable('table#tbldb-tables')) {
                            $('table#tbldb-tables').DataTable().destroy();
                        }
                        $('table#tbldb-tables').find('tr[data-id="'+$('#txtdelid').val()+'"]').remove();
                        $('#tbldb-tables').dataTable({"pageLength": 5});
                        $('#confirm-modal').modal('hide');
                    }
            });
        } else if(action=='deactivate'){
            e.preventDefault();
            $.ajax ({type : 'POST', url: baseurl+'projects/project_activate/0/'+reportid,
                    data: {'txtdelid' : $('#txtdelid').val()},
                    success: function(res){
                        $('a#btndeactivate').addClass('hidden');
                        $('a#btnactivate').removeClass('hidden');
                        $('#confirm-modal').modal('hide');
                    }
            });
        } else if(action=='activate'){
            e.preventDefault();
            $.ajax ({type : 'POST', url: baseurl+'projects/project_activate/1/'+reportid,
                    data: {'txtdelid' : $('#txtdelid').val()},
                    success: function(res){
                        $('a#btndeactivate').removeClass('hidden');
                        $('a#btnactivate').addClass('hidden');
                        $('#confirm-modal').modal('hide');
                    }
            });
        } else {
            
        } 
    });
    
});
