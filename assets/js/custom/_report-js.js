$(document).ready(function() {

	$('#tblall-reports').dataTable( {
	    "initComplete": function(settings, json) {
	        $('.loading-image').hide();
	        $('#tblall-reports').css('visibility', 'visible');
	    }} );

	// if checkall is checked
	$('div.portlet-db').find('#chkall').click(function() {
		var val_all = $(this).is(":checked");
		pbody = $(this).closest('div.portlet-title').next('div.portlet-body');
		chklist = pbody.find('div.list-checkbox').find('label');
		if(val_all){
			// checked
			chklist.find('div.checker span').addClass('checked');
			chklist.find('input.chktbl').prop('checked', true);
		}else{
			// unchecked
			chklist.find('div.checker span').removeClass('checked');
			chklist.find('input.chktbl').prop('checked', false);
		}
	});
	
	// chart
	$('li.lichart,.btn-chart').hide();
	$('a.chrt-layout').on('click', function() {
		var layout = $(this).data('id');
		$('li.lichart').hide();
		$('.btn-chart, li.lichart.'+layout).show();

	});

	// share with
	$('#sel-sharewith-user').change(function () {
	  var userid = $(this).val();
	  if(userid > 0)
	  {
	    $('#tblsharewith-user').append('<tr data-userid="'+userid+'">'+
	    									'<td class="tds-user">'+$('#sel-sharewith-user option:selected').text()+'</td>'+
	    									'<td align="center" nowrap>'+
                                                '<a href="javascript:;" title="This User Can View Report" class="btn btn-round blue"><i class="fa fa-eye"></i></a>'+
                                                '<a href="javascript:;" title="This User Can\'t Edit Report" class="btn btn-round btn-default btnedit-sw-user"><i class="fa fa-pencil"></i></a></td>'+
	    									'<td align="center"><a href="javascript:;" class="btn btn-xs red btnremove-sw-user"><i class="fa fa-remove"></i></a></td>'+
	    								'</tr>');
	    $("option[value='"+ userid +"']").remove();
	    $(this).select2('val',0);
	  }
	});
	// remove user in table users
	$('table#tblsharewith-user').on('click', 'a.btnremove-sw-user', function() {
	  var userid = $(this).closest('tr').data('userid');
	  var td_text = $(this).closest('tr').find('td.tds-user').text();
	  $('#sel-sharewith-user').append('<option value="'+ userid +'">'+ td_text +'</option>');
	  $(this).closest('tr').remove();
	});
	// allow/disallow user to edit report
	$('table#tblsharewith-user').on('click', 'a.btnedit-sw-user', function() {
		if($(this).hasClass('btn-default')) {
			can_edit = 1;
			$(this).attr("title","This User Can Edit Report.");
			$(this).removeClass('btn-default').addClass('green');
		} else {
			can_edit = 0;
			$(this).attr("title","This User Can't Edit Report.");
			$(this).removeClass('green').addClass('btn-default');
		}
	});
	// remove all users in table users
	$('a.btn-remove-all-user').on('click',function() {
		$('table#tblsharewith-user').find('tr').each(function(i,val) {
			userid = $(this).data('userid');
			td_text = $(this).find('td.tds-user').text();
			if(userid!=null) {
				$('#sel-sharewith-user').append('<option value="'+ userid +'">'+ td_text +'</option>');
				$(this).remove();
			}
		});
	});
	// allow/disallow all users to edit report
	$('a.btn-can-edit-all-user').on('click',function() {
		$('table#tblsharewith-user').find('tr').each(function(i,val) {
			if($(this).find('.btnedit-sw-user').hasClass('btn-default')) {
				$(this).find('.btnedit-sw-user').attr("title","This User Can Edit Report.");
				$(this).find('.btnedit-sw-user').removeClass('btn-default').addClass('green');
			} else {
				$(this).find('.btnedit-sw-user').attr("title","This User Can't Edit Report.");
				$(this).find('.btnedit-sw-user').removeClass('green').addClass('btn-default');
			}
		});
	});

	// allow/disallow group to edit report
	$('table#tblsharewith-group').on('click', 'a.btnedit-sw-grp', function() {
		if($(this).hasClass('btn-default')) {
			can_edit = 1;
			$(this).attr("title","This User Can Edit Report.");
			$(this).removeClass('btn-default').addClass('green');
		} else {
			can_edit = 0;
			$(this).attr("title","This User Can't Edit Report.");
			$(this).removeClass('green').addClass('btn-default');
		}
	});
	// add - share with group
	$('table#tblsharewith-group').on('click', 'a.btn-grp-add', function() {
		$(this).closest('td').prev('td').find('.btn-round').removeClass('disabled');
		$(this).closest('td').prev('td').find('.btnview-sw-grp').removeClass('default').addClass('blue');
	  	var groupid = $(this).data('grpid');
	  	$(this).removeClass('default').addClass('green');
	  	$(this).closest('td').find('a.btn-grp-rem').removeClass('default').addClass('red');
	});
	// remove - share with group
	$('table#tblsharewith-group').on('click', 'a.btn-grp-rem', function() {
		$(this).closest('td').prev('td').find('.btn-round').addClass('disabled');
		$(this).closest('td').prev('td').find('.btnview-sw-grp').removeClass('blue').addClass('btn-default');
		$(this).closest('td').prev('td').find('.btnedit-sw-grp').removeClass('green').addClass('btn-default');
	  	var groupid = $(this).data('grpid');
	  	$(this).removeClass('red').addClass('default');
	  	$(this).closest('td').find('a.btn-grp-add').removeClass('green').addClass('default');
	});
	// submit share with
	$('button#btnshare_with').click(function(e) {
		e.preventDefault();
		var groupnames = [];
		var grouplist = [];
		$('table#tblsharewith-group').find('a.btn-grp-add.green').each(function(i,val) {
		  	grpid = $(this).data('grpid');
		  	can_edit = ($(this).closest('td').prev('td').find('.btnedit-sw-grp').hasClass('green')) ? 1 : 0;
		  	if(grpid!=null) {
		  		grouplist.push({'grpid' : grpid, 'can_edit' : can_edit});
		  		groupnames.push($(this).closest('tr').find('td').text());
		  	}
		});

		var usernames = [];
		var userlist = [];
		$('table#tblsharewith-user').find('tr').each(function(i,val) {
			swuserid = $(this).data('userid');
			can_edit = ($(this).find('.btnedit-sw-user').hasClass('green')) ? 1 : 0;
			if(swuserid!=null) {
				userlist.push({'user' : swuserid, 'can_edit' : can_edit});
				usernames.push($(this).find('td').text());
			}
		});
		
		var reportid = window.location.href.split('/');
		reportid = reportid[reportid.length-1];
		$.ajax ({type : 'POST', url: $('#txtbaseurl').val() + "projects/reports/shared_with",
		        data: {"reportid" : reportid,
		    		   "userlist" : userlist, "grouplist" : grouplist},
			    success: function(res){
			        console.log(res);
			        swstr = usernames.join(',') + ',' + groupnames.join(',');
			        swstr = swstr.length > 150 ? swstr.substr(0,150-1) + '&hellip;' : swstr;
			        $('#share-with-container').html('<small class="font-blue">'+ swstr +'</small>');
			        $('#share-with-modal').modal('hide');
			    }
		});
	});

	// publish
	$('a#btnunpublish').click(function(e) {
		$('.publish-title').html('Unpublish Project');
		$('#txtpublish').val(0);
		$('.unpublish-body').show();
		$('.publish-body').hide();
	});
	// unpublish
	$('a#btnpublish').click(function(e) {
		$('.publish-title').html('Publish Project');
		$('#txtpublish').val(1);
		$('.publish-body').show();
		$('.unpublish-body').hide();
	});
	// submit Publish
	$('button#btnsubmit-publish').click(function(e) {
		e.preventDefault();
		var reportid = window.location.href.split('/');
		reportid = reportid[reportid.length-1];
		publish = $('#txtpublish').val();
		public = $('#chkpublish').closest('span').hasClass('checked') ? 1 : 0;
		public = publish == 0 ? 0 : public;
		$.ajax ({type : 'POST', url: $('#txtbaseurl').val() + "projects/reports/publish",
		        data: {"reportid" : reportid, "publish" : publish, "public" : public},
			    success: function(res){
			        if(publish == 1) {
			        	toastr.success('Report published successfully.');
			        	$('a#btnunpublish').removeClass('hidden');
			        	$('a#btnpublish').addClass('hidden');
			        } else {
			        	toastr.success('Report unpublished successfully.');
			        	$('#chkpublish').closest('span').removeClass('checked');
			        	$('#chkpublish').attr('checked',false);
			        	$('a#btnunpublish').addClass('hidden');
			        	$('a#btnpublish').removeClass('hidden');
			        }
			        $('#publish-modal').modal('hide');
			    }
		});
	});

	// Edit Name
	$('button#btnsubmit-name').click(function(e) {
		e.preventDefault();
		var reportid = window.location.href.split('/');
		reportid = reportid[reportid.length-1];
		console.log($('#txtbaseurl').val() + "projects/edit_projectname/"+reportid);
		$.ajax ({type : 'POST', url: $('#txtbaseurl').val() + "projects/edit_projectname/"+reportid,
		        data: {"txtprojname" : $('#txtprojname').val()},
			    success: function(res){
			        $('#project-name').html($('#txtprojname').val());
			        $('#edit_projectname-modal').modal('hide');
			    }
		});
	});

	// report select field
	$('button#btnselect-field').click(function(e) {
		e.preventDefault();
		$('div.list-checkbox').find('.chktbl').each(function(i,item) {
			header = $(this).val();
			if(!$(this).is(':checked')) {
				$('table#tblall-reports > thead > tr').find('th.th-'+header).hide();
				$('table#tblall-reports > tbody > tr').find('td.td-'+header).hide();
			} else {
				$('table#tblall-reports > thead > tr').find('th.th-'+header).show();
				$('table#tblall-reports > tbody > tr').find('td.td-'+header).show();
			}
		});
		$('#select-field-modal').modal('hide');
	});

	// filter field
	$('a#btnfilter-field').click(function() {
		var tablename = $(this).data('id');
		$('div.filter-container').addClass('hidden');
		$('div.add-condition, div[data-id="'+tablename+'"]').removeClass('hidden');
	});

	$('button#clear-condition').click(function() {
		// $('#condition-stmt').html('WHERE 1');
		$('div.filter-container, div.add-condition').addClass('hidden');
	});

	$('button#btn-add-condition').click(function() {
		$('div.filter-container').each(function(i,item) {
			if($(this).is(':visible')){
				tablename = $(this).data('id');
				$('input[data-table="'+tablename+'"]').each(function() {
					txtval = $(this).val();
					if(txtval!='') {
						selfield = $(this).data('field');
						selfilter = $(this).closest('tr').find('#selfilter').val();
						cond = $(this).closest('tr').find('#selcond').val();
						// console.log(sql_filter(cond,tablename,selfield,selfilter,txtval));
						// $('#condition-stmt').append(sql_filter(cond,tablename,selfield,selfilter,txtval));
					}
				});
			}
			
		});
	});


});