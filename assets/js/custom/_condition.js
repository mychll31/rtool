function sql_filter(cond,tbl,fld,op,string)
{
    switch(op) {
        case 'equal':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" = '"+string+"'";

        case 'higher':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" > '"+string+"'";

        case 'higher_equal':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" >= '"+string+"'";

        case 'less':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" < '"+string+"'";

        case 'less_equal':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" <= '"+string+"'";

        case 'not_equal':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" != '"+string+"'";

        case 'like':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" LIKE '"+string+"'";

        case 'w_like':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" LIKE '%"+string+"'";

        case 'like_w':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" LIKE '"+string+"%'";

        case 'w_like_w':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" LIKE '%"+string+"%'";

        case 'not_like':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" NOT LIKE '"+string+"'";

        case 'in_arr':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" IN ("+string+")";

        case 'not_in_arr':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" NOT IN ("+string+")";

        case 'between':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" BETWEEN '"+string+"'";

        case 'not_between':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" NOT BETWEEN '"+string+"'";

        case 'is_null':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" IS NULL";

        case 'is_not_null':
             return ' '+cond.toUpperCase()+" "+tbl+"."+fld+" IS NOT NULL";
    }
}
