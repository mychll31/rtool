$(document).ready(function() { 
    $('#tbldb-tables').dataTable( {
    "pageLength": 5,
    "initComplete": function(settings, json) {
        $('.loading-image').hide();
        $('#tbldb-tables').css('visibility', 'visible');
    }} );

    $('.tbltable-list').dataTable({
        "pageLength": 5,            
        "columnDefs": [{'orderable': false,'targets': [0]},{"searchable": false,"targets": [0]}],
        "order": [[1, "asc"]],
        "language": {"info": "_TOTAL_ Entries"}
    });

    // Begin Checkall
    $('table.tbltable-list').find('#chkall').click(function() {
        var val_all = $(this).is(":checked");
        var conn = $(this).val();

        if($.fn.DataTable.isDataTable('table[data-conn="'+conn+'"]')) {
            $('table[data-conn="'+conn+'"]').DataTable().destroy();
        }
                
        $('tbody[data-conn="'+conn+'"]').each(function() {
            if(val_all){
                $(this).find('tr > td > div.checker > span').addClass('checked');
            } else {
                $(this).find('tr > td > div.checker > span').removeClass('checked');
            }
        });

        $('table[data-conn="'+conn+'"]').dataTable({
            "pageLength": 5,            
            "columnDefs": [{'orderable': false,'targets': [0]},{"searchable": false,"targets": [0]}],
            "order": [[1, "asc"]],
            "language": {"info": "_TOTAL_ Entries"}
        });
    });
    // End Checkall

    // Begin checkbox
    $('table.tbltable-list').find('input[type="checkbox"]').on('click',function() {
        if($(this).is(":checked")){
            $(this).closest('div.checker > span').addClass('checked');
        } else {
            $(this).closest('div.checker > span').removeClass('checked');
        }
    });
    // End checkbox

    // add table submit
    $('#btnselect-tables').click(function() {
        $('#addTable-modal').modal('show');
    });
    $('#btnaddTable').click(function(e) {
        e.preventDefault();
        var arrtables = [];
        var baseurl = $('#txtbaseurl').val();
        var reportid = $('#txtreport_id').val();
        if($.fn.DataTable.isDataTable('table.tbltable-list')) {
            $('table.tbltable-list').DataTable().destroy();
        }
        $('table.tbltable-list').find('input[type="checkbox"]').each(function() {
            var item = $(this);
            if(item.is(':checked')){
                if(item.attr('id') != 'chkall') {
                    var tablename = item.val();
                    var sourceid = item.data('source');
                    arrtables.push({'tablename': tablename,'sourceid': sourceid});
                    $(this).closest('tr').remove();
                }
            }
        });
        $('table.tbltable-list').dataTable({"pageLength": 5});
        $.ajax ({type : 'POST', url: baseurl+'projects/save_table',
                data: {'txtdetails':JSON.stringify(arrtables), 'txtreportid':reportid, 'txtaction':'select'},
                success: function(res){
                    var data = JSON.parse(res);
                    if($.fn.DataTable.isDataTable('table#tbldb-tables')) {
                        $('table#tbldb-tables').DataTable().destroy();
                    }
                    $.each(data,function(i,f) {
                        $('table#tbldb-tables > tbody').append('<tr data-id="'+data[i].tableid+'" data-sourceid="'+data[i].source_id+'"><td hidden>'+
                                                                    '</td><td>'+data[i].connname+
                                                                    '</td><td>'+data[i].dbhost+
                                                                    '</td><td>'+data[i].dbname+
                                                                    '</td><td>'+data[i].table_name+
                                                                    '</td><td align="center" data-fields=\''+JSON.stringify(data[i].field_names)+'\' >'+
                                                                        '<a class="btn blue btn-sm" id="btnviewfields">'+
                                                                            '<i class="icon-list"></i>&nbsp; View Fields </a>'+
                                                                        '<a class="btn red btn-sm" id="btndelete">'+
                                                                            '<i class="icon-trash"></i>&nbsp; Remove </a>'+
                                                                    '</td></tr>');
                    });
                    $('table#tbldb-tables').dataTable({"pageLength": 5});

                    $('#addTable-modal').modal('hide');
                }
        });
    });
    
    // Edit Name
    $('button#btnsubmit-name').click(function(e) {
        e.preventDefault();
        var reportid = window.location.href.split('/');
        reportid = reportid[reportid.length-1];

        $.ajax ({type : 'POST', url: $('#txtbaseurl').val() + "projects/edit_projectname/"+reportid,
                data: {"txtprojname" : $('#txtprojname').val()},
                success: function(res){
                    $('#project-name').html($('#txtprojname').val());
                    $('#edit_projectname-modal').modal('hide');
                }
        });
    });

    // remove table
    $('table#tbldb-tables').on('click', 'a#btnviewfields', function() {
        var field_names = $(this).closest('td').data('fields');
        $.each(field_names,function(i,f) {
            $('table#tblfields > tbody').append('<tr><td>'+field_names[i].name+
                                                    '</td><td align="center">'+field_names[i].type+
                                                    '</td><td align="center">'+field_names[i].max_length+
                                                    '</td><td align="center">'+(field_names[i].primary_key ? 'Yes' : '')+
                                                    '</td></tr>');
        });
        $('#viewfield-modal').modal('show');
    });

    // remove table
    $('table#tbldb-tables').on('click', 'a#btndelete', function() {
        if($.fn.DataTable.isDataTable('table.tbltable-list')) {
            $('table.tbltable-list').DataTable().destroy();
        }
        sourceid = $(this).closest('tr').data('sourceid');
        tablename = $(this).closest('tr').find('td:eq(4)').text();
        $('table.tbltable-list[data-sourceid="'+sourceid+'"] > tbody ').append('<tr><td><input type="checkbox" data-source="'+sourceid+'" value="'+tablename+'"/></td><td>'+tablename+'</td></tr>');
        $('table.tbltable-list').dataTable({"pageLength": 5});
        $('input#txtaction').val('delete-table');
        $('.title-conf').html('Delete Table');
        $('.modal-conf').html('Are you sure you want to delete this Table?');
        $('#txtdelid').val($(this).closest('tr').data('id'));
        $('#confirm-modal').modal('show');
    });
    
    // activate project
    $('a#btnactivate').click(function() {
        $('input#txtaction').val('activate');
        $('.title-conf').html('Activate Project');
        $('.modal-conf').html('Are you sure you want to activate this project?');
        $('#confirm-modal').modal('show');
    });
    // deactivate project
    $('a#btndeactivate').click(function() {
        $('input#txtaction').val('deactivate');
        $('.title-conf').html('Deactivate Project');
        $('.modal-conf').html('Are you sure you want to deactivate this project?');
        $('#confirm-modal').modal('show');
    });

    // delete project
    $('a#btndelete-project').click(function() {
        var baseurl = $('#txtbaseurl').val();
        $('input#txtaction').val('delete');
        $('#frmconfirm').attr('action', baseurl+'projects/project_delete/'+$('#txtreport_id').val());
        $('.title-conf').html('Delete Project');
        $('.modal-conf').html('Are you sure you want to delete this project?');
        $('#confirm-modal').modal('show');
    });

    // confirmation modal
    $('button.btn-confirm').click(function(e) {
        var baseurl = $('#txtbaseurl').val();
        var action = $('#txtaction').val();
        var reportid = $('#txtreport_id').val();
        if(action=='delete-table'){
            e.preventDefault();
            $.ajax ({type : 'POST', url: baseurl+'projects/remove_tables',
                    data: {'txtdelid' : $('#txtdelid').val(),'txtdelmode' : $('#txtdelmode').val()},
                    success: function(res){
                        if($.fn.DataTable.isDataTable('table#tbldb-tables')) {
                            $('table#tbldb-tables').DataTable().destroy();
                        }
                        $('table#tbldb-tables').find('tr[data-id="'+$('#txtdelid').val()+'"]').remove();
                        $('#tbldb-tables').dataTable({"pageLength": 5});
                        $('#confirm-modal').modal('hide');
                    }
            });
        } else if(action=='deactivate'){
            e.preventDefault();
            $.ajax ({type : 'POST', url: baseurl+'projects/project_activate/0/'+reportid,
                    data: {'txtdelid' : $('#txtdelid').val()},
                    success: function(res){
                        $('a#btndeactivate').addClass('hidden');
                        $('a#btnactivate').removeClass('hidden');
                        $('#confirm-modal').modal('hide');
                    }
            });
        } else if(action=='activate'){
            e.preventDefault();
            $.ajax ({type : 'POST', url: baseurl+'projects/project_activate/1/'+reportid,
                    data: {'txtdelid' : $('#txtdelid').val()},
                    success: function(res){
                        $('a#btndeactivate').removeClass('hidden');
                        $('a#btnactivate').addClass('hidden');
                        $('#confirm-modal').modal('hide');
                    }
            });
        } else {
            
        } 
    });

});