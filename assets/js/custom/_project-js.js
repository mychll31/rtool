/* Module: Project; Page: New Project*/
$(document).ready(function () {
  $('#div-fload,.addconnection,.alert-danger').hide()

  /* Begin selecting dbtype*/
  $('#seldbtype').change(function () {
    var dbval = $(this).find(':selected').data('wfupload');
    var ftype = $(this).find(':selected').data('ftype');

    $('#seldbtype').val($(this).val());
    $('#seldbtype').selectpicker('refresh');

    if (dbval == 1) {
      $('#btnfupload').attr('accept',ftype);
      $('.div-upload').show();
      $('.div-host').hide();
    } else {
      $('.div-upload').hide();
      $('.div-host').show();
    }
    $('.div-form,.testconnection').show();
  });
  /* End selecting dbtype*/

  $('#selexconn').change(function () {
    $('.testconnection').show();
    $('.addconnection').hide();
    if ($(this).val() != '') {
      $('#new-connection').hide()
    } else {
      $('#new-connection').show()
    }
  })
  $('button.submitupload').click(function (e) {
    $('input#txtfreportname').val($('input#txtreportname').val())
  })
  $('#selconnname,#txtfconnname').change(function () {
    $('#selconnname,#txtfconnname').val($(this).val())
  })

  $('#btnfupload').change(function (e) {
    $('.span-fname').text(e.target.files[0].name);
    $('.span-fname').next('span').text('');
    $('div.fileupload-buttonbar').parent('div').parent('div').removeClass('has-error');
  });

  $('#tblpresentation').on('click', 'a.fcancel', function () {
    $(this).closest('tr').hide()
    $('#txtfcancel').val($('#txtfcancel').val() + ';' + $(this).closest('tr').index())
  })

  /* Begin Test Connection */
  $('#txtreportname,#selconnname,#txthost,#txtdbname,#txtuname,#txtpass,#txtretpass').on('keyup',function() {
    checknull($(this),1);
  });
  $('#txtpass,#txtretpass').on('keyup',function() {
    validate_password($('#txtpass'),$('#txtretpass'),1);
  });
  $('#seldbtype').change(function() {
    checknull($(this),1);
  });

  $('#btntestconn').click(function () {
    var exconn = $('#selexconn').val();
    var dbwfupload = $('#seldbtype').find(':selected').data('wfupload');
    /* begin adding new connection */
    if (exconn == '') {
      /*Validation*/
      var err = 0;
      err = err + checknull($('#txtreportname'),1);
      err = err + checknull($('#selconnname'),1);
      err = err + checknull($('#seldbtype'),1);
      if(!dbwfupload) {
        err = err + checknull($('#txthost'),1);
        err = err + checknull($('#txtdbname'),1);
        err = err + checknull($('#txtuname'),1);
        if(!(checknull($('#txtpass'),1) + checknull($('#txtretpass'),1))) {
          err = err + validate_password($('#txtpass'),$('#txtretpass'),1);
        }
      } else {
        if($('.span-fname').text() == '') {
            $('div.fileupload-buttonbar').parent('div').parent('div').addClass('has-error');
            $('.span-fname').next('span').text('This field is required.');
            err = err + 1;
        }
        if($('#txtpass').val() != '' || $('#txtretpass').val() != '') {
          if(!(checknull($('#txtpass'),1) + checknull($('#txtretpass'),1))) {
              err = err + validate_password($('#txtpass'),$('#txtretpass'),1);
          }
        }
      }

      if(err < 1) {
        $.ajax({
            type: 'GET',
            url: $('#txtbase_url').val()+'projects/check_exists_reportname',
            dataType: 'json',
            data: { reportname: $('#txtreportname').val() },
            success: function (res) {
              if (res.message != 'Connected') {
                $('#btntestconn').next('.alert-danger').show().html(res.message);
              } else {
                $('#btntestconn').next('.alert-danger').hide().html('');
                var dbval = $('#seldbtype').find(':selected').data('wfupload');
                $.ajax ({type : 'post',
                        url: $("#txtbase_url").val()+'projects/check_connections',
                        dataType: 'json',
                        data: {
                                "dbwfupload" : dbval,
                                "file_loc" : $('#txtfileloc').val(),
                                "filename" : $('#txtfilename').text(),
                                "db_type" : $('#seldbtype').val(),
                                "hostname" : $('#txthost').val(),
                                "username" : $('#txtuname').val(),
                                "password" : $('#txtpass').val(),
                                "database" : $('#txtdbname').val()
                            },
                        success: function(res){
                            if(res.message == 'Connected'){
                              $('#btntestconn').next('.alert-danger').hide().html('');
                              $.ajax({type : 'GET',
                                      url: $("#txtbase_url").val()+'projects/check_exists_connname',
                                      dataType: 'json',
                                      data: {"conn_name" : $('#selconnname').val()},
                                      success: function(res) {
                                        console.log(res);
                                          if(res.status == 1){
                                              $('#btntestconn').next('.alert-danger').show().html(res.message);
                                          } else {
                                              $('.testconnection').hide();
                                              $('.addconnection').show();
                                              $('#dbwfupload').val(dbval);
                                          }
                                      }
                                  });
                            }else{
                                $('#btntestconn').next('.alert-danger').show().html(res.message);
                            }
                        }
                });
              }
            }
        });
      }
    /* end adding new connection */
    } else {
    /* begin checking connection from existing source */
      $.ajax({
        type: 'POST',
        url: $('#txtbase_url').val()+'projects/check_exists_connections',
        dataType: 'json',
        data: { exconn: $('#selexconn').val() },
        success: function (res) {
          if (res.message == 'Connected') {
            var err = 0;
            err = err + checknull($('#txtreportname'),1);

            if(err < 1) {
              $.ajax({
                  type: 'GET',
                  url: $('#txtbase_url').val()+'projects/check_exists_reportname',
                  dataType: 'json',
                  data: { reportname: $('#txtreportname').val() },
                  success: function (res) {
                    if (res.message != 'Connected') {
                      $('#btntestconn').next('.alert-danger').show().html(res.message);
                    } else {
                      $('.testconnection').hide();
                      $('.addconnection').show();
                      $('#txtsource_id').val($('#selexconn').val());
                    }
                  }
              });
            }
          } else {
            $('#btntestconn').next('.alert-danger').show().html(res.message);
          }
        }
      });
    }
    /* end checking connection from existing source */
  });
  /* Begin Test Connection */
});
