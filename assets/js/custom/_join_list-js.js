$(document).ready(function() {
    $('#tbltables').dataTable( {
        "pageLength": 5,
        "initComplete": function(settings, json) {
            $('.loading-image').hide();
            $('#tbltables').css('visibility', 'visible');
        }} );
    $('div.divfunc_detail').hide();

    // set join
    $('#btnremovejoin').hide();
	$('table#tbltables').on('click', 'a#btnjoin', function() {
		var reportid = $('#txtreport_id').val();
		var tableid = $(this).closest('tr').data('tblid');
		var order_id = $(this).closest('tr').index();
		$('#txttableid').val(tableid);
	    $.ajax ({
	    		type : 'POST', url: $('#txtbaseurl').val() + "projects/view_fields",
			        data: {"tableid": tableid, "report_id" : reportid, "order_id" : (order_id+2)},
			        dataType: 'json',
				    success: function(res){
				    	if(res.join_details!=null){
				    		$('#seljoin').selectpicker('val',res.join_details.join_id);
				    		$('#txtjoinid').val(res.join_details.report_join_id);
				    		$('#btnremovejoin').show();
				    	} else {
				    		$('#seljoin').selectpicker('val','');
				    		$('#txtjoinid').val('');
				    		$('#btnremovejoin').hide();
				    	}
				    	$('#txtseltable').val(res['table_name']);
				    	$('#txtseltableid').val(res['table_id']);
				    	$('div#radtbl1').empty();
				    	$.each(res['field_names'], function(i, field) {
				    		set_checked = (res.join_details!=null) ? (field['name'] == res.join_details.table1_pk) ? 'checked' : '' : '';
				    		$('div#radtbl1').append('<label><input type="radio" name="radpk1" id="radpk1" value="'+field['name']+'" '+set_checked+'> '+field['name']+'</label>');
				    	});

				    	$('#txtseltable2').val(res['table_join']['table_name']);
				    	$('#txtseltableid2').val(res['table_join']['table_id']);
				    	$('div#radtbl2').empty();
				    	$.each(res['table_join']['field_names'], function(i, field) {
				    		set_checked = (res.join_details!=null) ? (field['name'] == res.join_details.table2_pk) ? 'checked' : '' : '';
				    		$('div#radtbl2').append('<label><input type="radio" name="radpk2" id="radpk2" value="'+field['name']+'" '+set_checked+'> '+field['name']+'</label>');
				    	});

				    	$('#join-modal').modal('show');
				    }
		});
	});

	// delete join
	$('a#btnremovejoin').on('click', function() {
	    $('#confirm-modal').modal('show');
	});
	$('button#submit-confirm-modal').click(function(e) {
		var reportid = $('#txtreport_id').val();
		$.ajax ({type : 'POST', url: $('#txtbaseurl').val() + "projects/add_join",
	    		data: {"join_id" : $('#txtjoinid').val(),
	    		 		"table1_id" : $('#txtseltableid').val(),
	    		 		"report_id" : reportid,
						"action" : 'delete',
						"status" : $('#lblstatus').text()},
				dataType: 'json',
	        success: function(res){
	        	if($('#lblstatus').text() == 'Generated') {
	        		$('#txtquery-suggested').html(res.sql);
	        		$('#txtcustom_query').html(res.sql);
	        		$('#div-query-format').html(res.formatted_sql);
	        	}
	        	from_table = $('#txtseltable').val() + '.' + res.table_detail.table_pkey;
	        	$('table#tbltables > tbody > tr[data-tblid="'+$('#txttableid').val()+'"] > td:eq(3)').html(from_table);
	        	$('table#tbltables > tbody > tr[data-tblid="'+$('#txttableid').val()+'"] > td:eq(4)').html('');
	        	$('table#tbltables > tbody > tr[data-tblid="'+$('#txttableid').val()+'"] > td:eq(5)').html('');
	        	$('#confirm-modal').modal('hide');
	        	$('#join-modal').modal('hide');
	        	toastr.success('Join table successfully deleted.');
	        }
	    });
	});

	// view fields
	$('table#tbltables').on('click', 'a#btnviewfields', function() {
	    $.ajax ({type : 'GET', url: $('#txtbaseurl').val() + "projects/view_fields", dataType: 'json',
	    		 data: {"tableid": $(this).data('id')},
	        success: function(res){
	        	csquery = $('#btncustom-query').data('value');
	        	$('#vfield-tablename').html(res['table_name']);
	        	$('#vfield-hostname').html(res['dbhost']);
				$('#vfield-databasename').html(res['dbname']);
	        	$('table#tblfields > tbody').empty();
	        	$.each(res['field_names'], function(i, field) {
                	tblfields_details = '<tr><td>'+field['name']
                										+'</td><td align="center">'+field['type']
                										+'</td><td align="center">'+field['max_length']
                										+'</td><td align="center">'+field['primary_key']
                										+'</td>';
                	if(csquery == 'show'){
                		$('table#tblfields th:last').show();
                		tblfields_details = tblfields_details + '<td class="tdaction" align="center"><a class="btn btn-xs default pull-right btnselect-table" data-tablename="'+ field['name'] +'"> Select </a>'
                										+'</td>';
                	} else {
                		$('table#tblfields th:last').hide();
                	}
                	tblfields_details = tblfields_details + '</tr>';
                	$('table#tblfields > tbody').append(tblfields_details);
                });

                $('#viewfield-modal').modal('show');
	        }
	    });
	});

	$('#seljoin').change(function () {
		checknull($('#seljoin'));
	})
	$('#seltable2').change(function () {
	  	checknull($('#seltable2'));
	})

	// proceed to join
	$('#alert_pkrad1').hide();
	$('#alert_pkrad2').hide();
	$('#btnproceed').click(function() {
		var reportid = $('#txtreport_id').val();

		var err = 0;
		err = err + checknull($('#seljoin'));

		if ($("input[name='radpk1']:checked").val() == null) {
			$('#alert_pkrad1').show();
		  	errmsg = 'Primary key is required.';
		  	err = err + 1;
		} else {
			$('#alert_pkrad1').hide();
		  	errmsg = '';
		}
		$('#alert_pkrad1').text(errmsg);

		if ($("input[name='radpk2']:checked").val() == null) {
			$('#alert_pkrad2').show();
		  	errmsg = 'Primary key is required.';
		  	err = err + 1;
		} else {
			$('#alert_pkrad2').hide();
		  	errmsg = '';
		}
		$('#alert_pkrad2').text(errmsg);
		// add join table
		if(err < 1) {
			tbl_pk1 = $("input[name='radpk1']:checked").val();
			tbl_pk2 = $("input[name='radpk2']:checked").val();
			action = $('#txtjoinid').val() == '' ? 'add' : 'edit';
			$.ajax ({type : 'POST', url: $('#txtbaseurl').val() + "projects/add_join",
		    		data: {"report_id" : reportid,
							"table1_id" : $('#txtseltableid').val(),
							"table1_pk" : tbl_pk1,
							"join_name" : $('#seljoin').val(),
							"table2_id" : $('#txtseltableid2').val(),
							"table2_pk" : tbl_pk2,
							"join_id" : $('#txtjoinid').val(),
							"action" : action,
							"status" : $('#lblstatus').text()},
					dataType: 'json',
		        success: function(res){
		        	if($('#lblstatus').text() == 'Generated') {
		        		$('#txtquery-suggested').html(res.sql);
		        		$('#txtcustom_query').html(res.sql);
		        		$('#div-query-format').html(res.formatted_sql);
		        	}
		        	from_table = $('#txtseltable').val() + '.' + tbl_pk1;
		        	$('table#tbltables > tbody > tr[data-tblid="'+$('#txttableid').val()+'"] > td:eq(3)').html(from_table);
		        	$('table#tbltables > tbody > tr[data-tblid="'+$('#txttableid').val()+'"] > td:eq(4)').html($('#seljoin option:selected').text());
		        	to_table = $('#txtseltable2').val() + '.' + tbl_pk2;
		        	$('table#tbltables > tbody > tr[data-tblid="'+$('#txttableid').val()+'"] > td:eq(5)').html(to_table);
		        	$('#join-modal').modal('hide');
		        	toastr.success('Join table successfully added.');
		        }
		    });
		}
	});
	
	// save and finish
	$('#btnfinish').click(function(e) {
		e.preventDefault();
		var reportid = $('#txtreport_id').val();
		sqlcustom = $('#txtsql').text();
		isdrafted = 0;
		csrf_test_name = $("input[name=csrf_test_name]").val();
		if($('#custom_query-div').is(':visible')) {
			isdrafted = 1;
		} else {
			isdrafted = 0;
		}
		
		if(sqlcustom != ''){
			$.ajax ({type : 'POST', url: $('#txtbaseurl').val() + "projects/customsql/save_customsql",
			        data: {"reportid" : reportid,
			    		   "sql" : sqlcustom, "isdrafted" : isdrafted, "csrf_test_name" : csrf_test_name},
				    success: function(res){
				        window.location.href = "../view_report/" + reportid;
				    }
			});
		}else{
			$('#error-message-modal-title').text('Error');
			$('#error-message-modal-body').text('Error occured in the system. Please try again!');
			$('#error-message-modal').modal('show');
		}
	});

	/*Begin Add Table*/
	$('.tbltable-list').dataTable({
	    "pageLength": 5,            
	    "columnDefs": [{'orderable': false,'targets': [0]},{"searchable": false,"targets": [0]}],
	    "order": [[1, "asc"]],
	    "language": {"info": "_TOTAL_ Entries"}
	});

	// Begin Checkall
	$('table.tbltable-list').find('#chkall').click(function() {
	    var val_all = $(this).is(":checked");
	    var conn = $(this).val();

	    if($.fn.DataTable.isDataTable('table[data-conn="'+conn+'"]')) {
	        $('table[data-conn="'+conn+'"]').DataTable().destroy();
	    }
	            
	    $('tbody[data-conn="'+conn+'"]').each(function() {
	        if(val_all){
	            $(this).find('tr > td > div.checker > span').addClass('checked');
	        } else {
	            $(this).find('tr > td > div.checker > span').removeClass('checked');
	        }
	    });

	    $('table[data-conn="'+conn+'"]').dataTable({
	        "pageLength": 5,            
	        "columnDefs": [{'orderable': false,'targets': [0]},{"searchable": false,"targets": [0]}],
	        "order": [[1, "asc"]],
	        "language": {"info": "_TOTAL_ Entries"}
	    });
	});
	// End Checkall

	// Begin checkbox
	$('table.tbltable-list').find('input[type="checkbox"]').on('click',function() {
	    if($(this).is(":checked")){
	        $(this).closest('div.checker > span').addClass('checked');
	    } else {
	        $(this).closest('div.checker > span').removeClass('checked');
	    }
	});
	// End checkbox

	// add table submit
	$('#btnaddTable').click(function() {
	    var arrtables = [];
	    if($.fn.DataTable.isDataTable('table.tbltable-list')) {
	        $('table.tbltable-list').DataTable().destroy();
	    }
	    $('table.tbltable-list').find('span.checked').each(function() {
	        var item = $(this).find('input[type="checkbox"]');
	        if(item.attr('id') != 'chkall') {
	            var tablename = item.val();
	            var sourceid = item.data('source');
	            arrtables.push({'tablename': tablename,'sourceid': sourceid});
	            $('.table-list-modal').hide();
	            $('#loading-image2').show();
	        }
	    });
	    $('#txtdetails').val(JSON.stringify(arrtables));
	});
	/*End Add Table*/

	/*Begin create custom query*/
	$('#btncustom-query').click(function() {
	    if($('#custom_query-div').is(':visible')) {
	    	$('#custom_query-div').hide();
	    	$(this).data('value','hide');
	    	$(this).text('Show Custom Query');
	    	$('#join-list').show();
	    	$('div.well').show();
	    	is_show = 0;
	    } else {
	    	$('#custom_query-div').show();
	    	$(this).data('value','show');
	    	$(this).text('Hide Custom Query');
	    	$('#join-list').hide();
	    	$('div.well').hide();
	    	is_show = 1;
	    }
	    reportid = $('#txtreport_id').val();
	    $.ajax ({
	    		type : 'POST', url: $('#txtbaseurl').val() + "projects/customsql/hide_custom_area",
			        data: {"report_id" : reportid, "is_show" : is_show},
				    success: function(res){
				    	// console.log(res);
				    }
		});
	});

	// join button
	$('.btnselect-join').click(function() {
		var join_value = $(this).data('joinname');
		sql = $('#txtcustom_query').val();
		sql = sql + join_value.toUpperCase()+' ';
		$('#txtcustom_query').val(sql);
	    $('#custom-query-modal').modal('hide');
	});

	// select fields
	$('table#tblfields tbody').on('click','td.tdaction a', function() {
		var table_name = $(this).data('tablename');
	    $('#txtcustom_query').append(table_name+' ');
	    $('#viewfield-modal').modal('hide');
	});

	// select clause
	$('.btn-cls').click(function() {
		var cls_value = $(this).data('cls');
		cls_value = cls_value == 'select_all' ? 'select *' : cls_value;
		sql = $('#txtcustom_query').val();
		sql = sql + cls_value+' ';
		$('#txtcustom_query').val(sql);
	});

	// function decription details
	$('a.btnfunc_desc').click(function() {
		var elfunc = $(this).next('div.divfunc_detail');
		if (elfunc.is(':hidden'))
		{
			elfunc.show();
		} else {
			elfunc.hide();
		}
	});

	// function details
	$('.list-function li').on('click','a.btnfunction', function() {
		var fname = $(this).data('name');
		sql = $('#txtcustom_query').val();

		if(fname!='WHERE') {
	    	sql = sql + fname+'() ';
	    } else {
	    	sql = sql + fname+' ';
	    }
		$('#txtcustom_query').val(sql);
	    $('#mysql_functions-modal').modal('hide');
	});

	// save generate
	$('#btn-query-generate').click(function(e) {
		$('#txtcustom_query').val($('#txtquery-suggested').text());
	});
	// save query
	$('#btn-query-save').click(function(e) {
		e.preventDefault();
		var reportid = $('#txtreport_id').val();
		sqlcustom = $('#txtcustom_query').val();
		csrf_test_name = $("input[name=csrf_test_name]").val();
		if(sqlcustom != ''){
			$.ajax ({type : 'POST', url: $('#txtbaseurl').val() + "projects/customsql/save_customsql",
			        data: {"reportid" : reportid,
			    		   "sql" : sqlcustom, "isdrafted" : 1, "csrf_test_name" : csrf_test_name},
			    	dataType: 'json',
				    success: function(res){
				        if(res.message == 'success') {
				        	$('#lblstatus').text('Custom');
				        	$('#txtquery-added').html(sqlcustom);
				        	$('#div-query-format').html(res.formatted_sql);
				        	$('#error-message-modal-title').html('<span class="font-green">Success</span>');
							$('#error-message-modal-body').text('Congratulations! Your query run successfully.');
							$('#error-message-modal').modal('show');
				        } else {
				        	$('#error-message-modal-title').html('<span class="font-red">Error ' + res.code + '</span>');
							$('#error-message-modal-body').text(res.message);
							$('#error-message-modal').modal('show');
				        }
				        toastr.success('Custom Query successfully saved.');
				    }
			});
		}else{
			$('#error-message-modal-title').text('Warning');
			$('#error-message-modal-body').text('Custom Query is empty.');
			$('#error-message-modal').modal('show');
		}
	});
	// check query
	$('#btn-query-check').click(function(e) {
		e.preventDefault();
		sqlcustom = $('#txtcustom_query').val();
		csrf_test_name = $("input[name=csrf_test_name]").val();
		if(sqlcustom != ''){
			$.ajax ({type : 'POST', url: $('#txtbaseurl').val() + "projects/customsql/check_customsql",
			        data: {"sql" : sqlcustom, "csrf_test_name" : csrf_test_name},dataType: 'json',
				    success: function(res){
				        if(res.message == 'success') {
				        	$('#error-message-modal-title').html('<span class="font-green">Success</span>');
							$('#error-message-modal-body').text('Congratulations! Your query run successfully.');
							$('#error-message-modal').modal('show');
				        } else {
				        	$('#error-message-modal-title').html('<span class="font-red">Error ' + res.code + '</span>');
							$('#error-message-modal-body').text(res.message);
							$('#error-message-modal').modal('show');
				        }
				    }
			});
		}else{
			$('#error-message-modal-title').text('Warning');
			$('#error-message-modal-body').text('Custom Query is empty.');
			$('#error-message-modal').modal('show');
		}
	});
	// clear query
	$('button#btn-clear-custom-query').click(function() {
		var reportid = $('#txtreport_id').val();
	    $.ajax ({
	    		type : 'POST', url: $('#txtbaseurl').val() + "projects/customsql/remove_customsql",
			        data: {"reportid" : reportid, "sql" : $('#txtquery-suggested').text()},
			        dataType: 'json',
				    success: function(res){
				    	// $('#div-query-format').html(res.formatted_sql).show();
				    	$('#lblstatus').text('Generated');
				    	$('#div-query-format').html(res.formatted_sql);
				    	// $('#custom_query-div').hide();
				    	$('#btncustom-query').data('value','hide');
				    	$('#btncustom-query').text('Show Custom Query');
				    	$('#txtcustom_query').val('').change();
				    	$('#clear_custom_query-modal').modal('hide');
				    	toastr.success('Custom Query successfully cleared.');
				    }
			});
	});
	/*End create custom query*/

	// Edit Name
	$('button#btnsubmit-name').click(function(e) {
	    e.preventDefault();
	    var reportid = $('#txtreport_id').val();
	    $.ajax ({type : 'POST', url: $('#txtbaseurl').val() + "projects/edit_projectname/"+reportid,
	            data: {"txtprojname" : $('#txtprojname').val()},
	            success: function(res){
	                $('#project-name').html($('#txtprojname').val());
	                $('#edit_projectname-modal').modal('hide');
	                toastr.success('Project name successfully changed.');
	            }
	    });
	});

	// begin button move up and down
	$('table#tbltables').on('click', 'a#btnmove-down', function() {
		var reportid = $('#txtreport_id').val();

		down_loc = $(this).closest('tr').index();
		down_loc = down_loc + 1;

	    $('table#tbltables').find('a#btnmove-up,a#btnmove-down,a#btnjoin').removeClass('disabled');

	    // change the position
	    $(this).closest('tr').insertAfter('table#tbltables > tbody > tr:nth-child('+ (down_loc+1) +')');
	    var all_tr = $('table#tbltables > tbody > tr').length;
	    $('table#tbltables > tbody > tr:nth-child('+all_tr+')').find('a#btnmove-down').addClass('disabled');
	    $('table#tbltables > tbody > tr:nth-child('+all_tr+')').find('a#btnjoin').addClass('disabled');
	    $('table#tbltables > tbody > tr:nth-child(1)').find('a#btnmove-up').addClass('disabled');

	    table_id = $(this).closest('tr').data('tblid');
	    $.ajax ({
	    		type : 'POST', url: $('#txtbaseurl').val() + "projects/change_table_order",
			    data: {"report_id" : reportid, 
			           "table_id" : table_id, 
			           "tr_index" : down_loc, 
			           "move" : "down",
			           "status" : $('#lblstatus').text()},
			    dataType: 'json',
				success: function(res){
					if($('#lblstatus').text() == 'Generated') {
						$('#txtquery-suggested').html(res.sql);
						$('#txtcustom_query').html(res.sql);
						$('#div-query-format').html(res.formatted_sql);
					}
				}
		});

	});
	$('table#tbltables').on('click', 'a#btnmove-up', function() {
		var reportid = $('#txtreport_id').val();

		up_loc = $(this).closest('tr').index();
		$('table#tbltables').find('a#btnmove-up,a#btnmove-down,a#btnjoin').removeClass('disabled');

		// change the position and disabled 0 index
	    if(up_loc == 1){
	        $(this).closest('tr').insertBefore('table#tbltables > tbody > tr:nth-child(1)');
	    }else{
	        $(this).closest('tr').insertAfter('table#tbltables > tbody > tr:nth-child('+ (up_loc-1) +')');
	    }

	    var all_tr = $('table#tbltables > tbody > tr').length;
	    $('table#tbltables > tbody > tr:nth-child('+all_tr+')').find('a#btnmove-down').addClass('disabled');
	    $('table#tbltables > tbody > tr:nth-child('+all_tr+')').find('a#btnjoin').addClass('disabled');
	    $('table#tbltables > tbody > tr:nth-child(1)').find('a#btnmove-up').addClass('disabled');

	    table_id = $(this).closest('tr').data('tblid');
	    $.ajax ({
	    		type : 'POST', url: $('#txtbaseurl').val() + "projects/change_table_order",
			        data: {"report_id" : reportid,
			        		"table_id" : table_id, 
			        		"tr_index" : up_loc, 
			        		"move" : "up",
			        		"status" : $('#lblstatus').text()},
			        dataType: 'json',
				    success: function(res){
				    	if($('#lblstatus').text() == 'Generated') {
			        		$('#txtquery-suggested').html(res.sql);
			        		$('#txtcustom_query').html(res.sql);
			        		$('#div-query-format').html(res.formatted_sql);
			        	}
				    }
		});
	});
	// end button move up and down

});

