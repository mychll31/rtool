$(document).ready(function() {
    $('#seldbtype').on('change', function() {
    	var wcreds = $(this).find(':selected').data('wcreds');
    	if(wcreds==0){
    		$('.wcreds').hide();
    	}else{
    		$('.wcreds').show();
    	}
    });

    $('#btnfupload').change(function(e) {
        // $('#tblpresentation').show();
        for (var i = 0; i < e.target.files.length; i++) {
            console.log(e.target.files[i].name);
            $('#tblpresentation > tbody').append('<tr><td>'+
                                        '<span class="name">'+e.target.files[i].name+'</span>'+
                                        '<a class="btn red btn-xs pull-right fcancel"><i class="fa fa-ban"></i> <span>Cancel</span></a>'+
                                        '</td></tr>');
        }
    });

    $('#tblpresentation').on('click','a.fcancel',function() {
        $(this).closest('tr').hide();
        $('#txtfcancel').val($('#txtfcancel').val()+';'+$(this).closest('tr').index());
    });

    $('#selexconn').change(function() {
        if($(this).val() == ''){
            $('#new-connection, .fileinput-button').show();
        }else{
            $('#new-connection, .fileinput-button').hide();
        }
    });

});