/* Modulename : Datasource*/
function clear_dataconn()
{
    $('#txthost,#txtdbname,#txtuname,#txtpass,#txtretpass').val('');
    $('#txtconnname,#txthost,#txtdbname,#txtuname,#txtpass,#txtretpass').parent().parent().removeClass('has-error');
    $('#txtconnname,#txthost,#txtdbname,#txtuname,#txtpass,#txtretpass').parent().parent().find('.help-block').html('');
}
function clear_fsource()
{
    $('#txtconnname').parent().parent().removeClass('has-error');
    $('#txtconnname').parent().parent().find('.help-block').html('');
    $('.span-fname').text('');
}

$(document).ready(function()
{
    $('.loading-image').hide();
    $('.div-form').css('visibility', 'visible');

    /*Begin Database Type*/
    $('#seldbtype').change(function() {
        var selval = $(this).val();
        var dbval = $(this).find(':selected').data('wfupload');
        var ftype = $(this).find(':selected').data('ftype');

        if(selval == '') {
            clear_dataconn();
            $('#div-fload').hide();
            $('#div-fsource').hide();
        } else {
            if(dbval == 1) {
                $('#btnfupload').attr('accept',ftype);
                $('#div-fload').show();
                $('#div-fsource').hide();
            } else {
                clear_dataconn();
                $('#div-fsource').show();
                $('#div-fload').hide();
            }
        }
    });
  /*End Database Type*/

    /*Begin Test Connection and Validation*/
    $('#txtconnname,#txthost,#txtdbname,#txtuname').on('keyup', function () {
        checknull($(this),1);
    });
    $('#txtpass,#txtretpass').on('keyup', function () {
        validate_password($('#txtpass'),$('#txtretpass'),1);
    });

    $('#btntestconn').click(function(e) {
        e.preventDefault();

        var err = 0;
        err = err + checknull($('#txtconnname'),1);
        err = err + checknull($('#txthost'),1);
        err = err + checknull($('#txtdbname'),1);
        err = err + checknull($('#txtuname'),1);
        if(!(checknull($('#txtpass'),1) + checknull($('#txtretpass'),1))) {
            err = err + validate_password($('#txtpass'),$('#txtretpass'),1);
        }

        if(err < 1) {
            var dbval = $('#seldbtype').find(':selected').data('wfupload');
            $.ajax ({type : 'post',
                    url: $("#txtbaseurl").val()+'projects/check_connections',
                    dataType: 'json',
                    data: {
                            "dbwfupload" : dbval,
                            "file_loc" : $('#txtfileloc').val(),
                            "filename" : $('.span-fname').text(),
                            "db_type" : $('#seldbtype').val(),
                            "hostname" : $('#txthost').val(),
                            "username" : $('#txtuname').val(),
                            "password" : $('#txtpass').val(),
                            "database" : $('#txtdbname').val()
                        },
                    success: function(res){
                        console.log('check_connections');
                        console.log(res);
                        if(res.message == 'Connected'){
                            if($('#txtaction').val() == 'add'){
                                $.ajax({type : 'GET',
                                        url: $("#txtbaseurl").val()+'projects/check_exists_connname',
                                        dataType: 'json',
                                        data: {"conn_name" : $('#txtconnname').val()},
                                        success: function(res) {
                                            console.log('check_exists_connname');
                                            console.log(res);
                                            if(res.status == 1){
                                                $('.conn-alert').show();
                                                $('.alert-danger').html(res.message);
                                            } else {
                                                $('.action-test-conn').hide();
                                                $('.add-conn').show();
                                                $('#connname').val($('#txtconnname').val());
                                                $('#dbtype').val($('#seldbtype').val());
                                                $('.conn-alert').hide();
                                                $('.alert-danger').html('');
                                            }
                                        }
                                    });
                            } else {
                                $('.action-test-conn').hide();
                                $('.add-conn').show();
                                $('#connname').val($('#txtconnname').val());
                                $('#dbtype').val($('#seldbtype').val());
                                $('.conn-alert').hide();
                                $('.alert-danger').html('');
                            }
                        }else{
                            $('.conn-alert').show();
                            $('.alert-danger').html(res.message);
                        }
                    }
            }); 
        }
    });

    $('.btn-clear-conn').click(function() {
        clear_dataconn();
        $('#txtconnname').val('');
        $('.action-test-conn').show();
        $('.add-conn').hide();
    });

    /*Begin File Datasource*/
    $('#btnfupload').change(function(e) {
        $('.span-fname').text(e.target.files[0].name);
        $('.span-fname').next('span').text('');
        $('div.fileupload-buttonbar').parent('div').parent('div').removeClass('has-error');
    });

    $('#txtfpass,#txtfretpass').on('keyup', function () {
        $('#txtfpass,#txtfretpass').parent().parent().removeClass('has-error');
        $('#txtfpass,#txtfretpass').parent().parent().find('.help-block').html('');
        if($('#txtfpass').val() != '' || $('#txtfretpass').val() != '') {
            if(!(checknull($('#txtfpass'),1) + checknull($('#txtfretpass'),1))) {
                validate_password($('#txtfpass'),$('#txtfretpass'),1);
            }
        }
    });

    $('#btn-test-datasource').click(function(e) {
        e.preventDefault();
        var err = 0;
        err = err + checknull($('#txtconnname'),1);
        if($('#txtaction').val() == 'add'){
            if($('#btnfupload').val() == '') {
                $('div.fileupload-buttonbar').parent('div').parent('div').addClass('has-error');
                $('.span-fname').next('span').text('This field is required.');
                err = err + 1;
            }
        }

        $('#txtfpass,#txtfretpass').parent().parent().removeClass('has-error');
        $('#txtfpass,#txtfretpass').parent().parent().find('.help-block').html('');
        if($('#txtfpass').val() != '' || $('#txtfretpass').val() != '') {
            if(!(checknull($('#txtfpass'),1) + checknull($('#txtfretpass'),1))) {
                err = err + validate_password($('#txtfpass'),$('#txtfretpass'),1);
            }
        }

        if(err < 1) {
            $('#fconnname').val($('#txtconnname').val());
            $('#fdbtype').val($('#seldbtype').val());
            if($('#txtaction').val() == 'add'){
                $.ajax({type : 'GET',
                    url: $("#txtbaseurl").val()+'projects/check_exists_connname',
                    dataType: 'json',
                    data: {"conn_name" : $('#txtconnname').val()},
                    success: function(res) {
                        console.log('check_exists_connname');
                        console.log(res);
                        if(res.status == 1){
                            $('.conn-alert').show();
                            $('.alert-danger').html(res.message);
                        } else {
                            $('#btn-test-datasource').hide();
                            $('#btn-add-datasource').removeClass('hidden');
                            $('#fconnname').val($('#txtconnname').val());
                            $('#fdbtype').val($('#seldbtype').val());
                            $('.conn-alert').hide();
                            $('.alert-danger').html('');
                        }
                    }
                });
            } else {
                $('#btn-test-datasource').hide();
                $('#btn-add-datasource').removeClass('hidden');
                $('#fconnname').val($('#txtconnname').val());
                $('#fdbtype').val($('#seldbtype').val());
                $('.conn-alert').hide();
                $('.alert-danger').html('');
            }
        }
    });

    $('#btn-clear-datasource').click(function() {
        $('#txtconnname').val('');
        clear_fsource();
    });
    /*End File Datasource*/

    /* Begin Action */
    $('a#btnactivate').click(function() {
        $('.modal-title').html('Activate Datasource');
        $('.modal-body').html('Are you sure you want to activate this data source?');
        $('#frmconfirm').attr('action', $('#txtbaseurl').val()+'datasources/dbsource_activate/1/'+$(this).data('id'))
    });

    $('a#btndeactivate').click(function() {
        $('.modal-title').html('Deactivate Datasource');
        $('.modal-body').html('Are you sure you want to deactivate this data source?');
        $('#frmconfirm').attr('action', $('#txtbaseurl').val()+'datasources/dbsource_activate/0/'+$(this).data('id'))
    });

    $('#btndelete').click(function() {
        $('.modal-title').html('Delete Datasource');
        $('.modal-body').html('Are you sure you want to delete this data source?');
        $('#frmconfirm').attr('action', $('#txtbaseurl').val()+'datasources/delete/'+$(this).data('id'))
    });
    /* End Action */

});