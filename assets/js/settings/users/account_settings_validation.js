$(document).ready(function () {
  $('#txtfname,#txtsname,#txtansw1,#txtansw2,#txtansw3,#txtcurpass,#txtgroupname').keyup(function () {
    checknull($(this));
  });

  $('#txtnewpass').on('keyup', function () {
    if (!checknull($(this))) {
      validate_password($('#txtnewpass'), $('#txtretpass'));
    }
  });

  $('#txtretpass').on('keyup', function () {
    if (!checknull($(this))) {
      validate_password($('#txtnewpass'), $('#txtretpass'));
    }
  });

  $('#selquestion1,#selquestion2,#selquestion3').change(function () {
    checknull($(this));
  });

  $('#txtemail').keyup(function () {
    checkemail($(this));
  });

  $('#selquestion').change(function () {
    checknull($(this));
  });

  $('#radfemale,#radmale').click(function () {
    checkopt_2('radgender', 'optgender');
  });

  $('.date-picker').on('changeDate', function () {
    if ($('.date-picker').datepicker('getDate') == '') {
      $('.date-picker').parent().addClass('has-error');
      errmsg = 'This field is required.';
      res = 1;
    } else {
      $('.date-picker').parent().removeClass('has-error');
      errmsg = '';
      res = 0;
    }
    $('.date-picker').parent().find('.help-block').html(errmsg);
    return res;
  });

  $('#submit_profile').click(function (e) {
    var res = 0;
    res = res + checknull($('#txtfname'));
    res = res + checknull($('#txtsname'));
    res = res + checknull($('#txtbday'));
    res = res + checkopt_2('radgender', 'optgender');
    res = res + checknull($('#txtemail'));
    res = res + checknull($('#selquestion1'));
    res = res + checknull($('#txtansw1'));
    res = res + checknull($('#selquestion2'));
    res = res + checknull($('#txtansw2'));
    res = res + checknull($('#selquestion3'));
    res = res + checknull($('#txtansw3'));

    if (res) {
      e.preventDefault();
    }
  });

  $('#submit_change_pass').click(function (e) {
    var res = 0;
    res = res + checknull($('#txtcurpass'));
    res = res + checknull($('#txtnewpass'));
    res = res + checknull($('#txtretpass'));

    if (checknull($('#txtnewpass')) + checknull($('#txtretpass')) > 0) {
      res = res + 1;
    } else {
      if (validate_password($('#txtnewpass'), $('#txtretpass'))) {
        res = res + 1;
      }
    }
    if (res) {
      e.preventDefault();
    }
  });


  // groups
  $('#seluser').change(function () {
    var userid = $(this).val();
    if(userid > 0)
    {
      $('#list-members').append('<li>'+$('#seluser option:selected').text()+' <a class="seluser-close font-red" data-userid="'+ userid +'"><i class="fa fa-close"></i></a></li>');
      $("option[value='"+ userid +"']").remove();
      $(this).select2('val',0);
    }
  });

  $('ul#list-members').on('click', 'a.seluser-close', function() {
    var li_id = $(this).data('userid');
    var li_text = $(this).closest('li').text();
    $('#seluser').append('<option value="'+ li_id +'">'+ li_text +'</option>');
    $(this).closest('li').remove();
  });

  $('#submit_group').click(function (e) {
    var userlist = [];
    $('ul#list-members li > a').each(function(i,val) {
      userlist.push($(this).data('userid'));
    });
    $('#txtuser').text(userlist);

    if (checknull($('#txtgroupname'))) {
      e.preventDefault();
    }
  });

  $('a.group-remove').click(function() {
    $('#txtgroupid').val($(this).data('groupid'));
    $('#error-message-modal').modal('show');
  });


});