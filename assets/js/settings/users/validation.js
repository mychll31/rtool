$(document).ready(function () {
  $('#txtfname,#txtsname,#txtuname,#txtanswer1,#txtanswer2,#txtanswer3').on('keyup', function () {
    checknull($(this), 1)
  })

  $('#radfemale,#radmale').click(function () {
    checkopt_2('radgender', 'optgender')
  })

  $('#txtemail').on('keyup', function () {
    checkemail($(this), 1)
  })

  $('#selutype,#selquestion1,#selquestion2,#selquestion3').on('change', function () {
    checknull($(this), 1)
  })

  $('.date-picker').on('changeDate', function () {
    if ($('.date-picker').datepicker('getDate') == '') {
      $('.date-picker').parent().addClass('has-error')
      errmsg = 'This field is required.'
      res = 1
    } else {
      $('.date-picker').parent().removeClass('has-error')
      errmsg = ''
      res = 0
    }
    $('.date-picker').parent().find('.help-block').html(errmsg)
    return res
  })

  $('#btnsubmit-user').click(function (e) {
    var res = 0
    res = res + checknull($('#txtfname'), 1)
    res = res + checknull($('#txtsname'), 1)
    res = res + checknull($('#txtuname'), 1)
    res = res + checkemail($('#txtemail'), 1)
    res = res + checknull($('#selutype'), 1)

    if ($('#txtaction').val() == 'edit') {
      res = res + checknull($('#txtbday'), 1)
      res = res + checknull($('#selquestion1'), 1)
      res = res + checknull($('#txtanswer1'), 1)
      res = res + checknull($('#selquestion2'), 1)
      res = res + checknull($('#txtanswer2'), 1)
      res = res + checknull($('#selquestion3'), 1)
      res = res + checknull($('#txtanswer3'), 1)
      res = res + checkopt_2('radgender', 'optgender')
    }

    if (res) {
      e.preventDefault()
    }
  })
})
