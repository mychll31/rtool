function checkElement(e, obj = '') {
	var res = 1;
	if(e.val() == ''){
		e.parent().parent().addClass('has-error');
		errmsg = 'This field is required.';
		res = 1;
	}else{
		if(obj == 'text'){
	        if(!e.val().replace(/\s/g, '').length){
	            e.parent().parent().addClass('has-error');
	            errmsg = 'Invalid input.';
	            res = 1;
	        }else{
	        	e.parent().parent().removeClass('has-error');
				errmsg = '';
				res = 0;
	        }
	    }else{
	    	e.parent().parent().removeClass('has-error');
			errmsg = '';
			res = 0;
	    }
	}
	e.next('.help-block').html(errmsg);
	e.next().next('.help-block').html(errmsg);
	return res;
}

function checkPassword(pass, retpass) {
	if(pass.val() == '' || retpass.val() == ''){
		if(pass.val() == ''){
			pass.parent().parent().addClass('has-error');
			errmsg = 'This field is required.';
			res = 1;
		}else{
			pass.parent().parent().removeClass('has-error');
			errmsg = '';
			res = 0;
		}
		if(retpass.val() == ''){
			retpass.parent().parent().addClass('has-error');
			errmsg = 'This field is required.';
			res = 1;
		}else{
			retpass.parent().parent().removeClass('has-error');
			errmsg = '';
			res = 0;
		}
	}else{
		if(pass.val() != retpass.val()){
			pass.parent().parent().addClass('has-error');
			retpass.parent().parent().addClass('has-error');
			errmsg = 'Password not Match.';
			res = 1;
		}else{
			pass.parent().parent().removeClass('has-error');
			retpass.parent().parent().removeClass('has-error');
			errmsg = '';
			res = 0;
		}
	}
	pass.next('.help-block').html(errmsg);
	retpass.next('.help-block').html(errmsg);
	return res;
}

function checkDate(e){
    var res = 1;
    if(e.val() == ''){
        e.parent().parent().parent().addClass('has-error');
        errmsg = 'This field is required.';
        res = 1;
    }else{
        e.parent().parent().parent().removeClass('has-error');
        errmsg = '';
        res = 0;
    }
    e.parent().parent().parent().find('.help-block').html(errmsg);
    return res;
}
$(document).ready(function() {

	$('form [type="text"]').keyup(function(e) {
		checkElement($(this), 'text');
	});

	$('form select').change(function(e) {
		checkElement($(this));
	});

	if($('form [type="password"]').length > 0){
		$('form [type="password"]').keyup(function(e) {
			checkElement($(this));
		});
	}

	if($('form [type="email"]').length > 0){
		$('form [type="email"]').keyup(function(e) {
			checkElement($(this), 'email')
		});
	}

	// if($('form [type="radio"]').length > 0){
	// 	$('form [type="radio"]').click(function(e) {
	// 		$('.radio-list').parent().parent().removeClass('has-error');
	// 		$('.radio-list').parent().parent().find('.help-block').html('');
	// 	});
	// }

	if($('.date-picker').length > 0){
		$('.date-picker').datepicker().on('changeDate', function(){
	        $('#txtbday').parent().parent().removeClass('has-error');
	        $(this).parent().parent().removeClass('has-error');
	        $(this).parent().parent().find('.help-block').html('');
	    });
	}

	$('form').submit(function(e) {
		e.preventDefault();
		console.log('validation');
		var resval = [];
		$('[type="text"]').each(function() {
			resval.push(checkElement($(this), 'text'));
		});

		$('select').each(function() {
			resval.push(checkElement($(this)));
		});

		if($('form [type="email"]').length > 0){
			$('[type="email"]').each(function() {
				resval.push(checkElement($(this), 'email'));
			});
		}

		if($('form [type="password"]').length > 0){
			resval.push(checkPassword($('#txtpword'),$('#txtretpword')));
		}

		// if($('form [type="radio"]').length > 0){
		// 	radchoice = [];
		// 	$('[type="radio"]').each(function() {
		// 		if($(this).is(':checked')){
		// 			radchoice.push($(this).val());
		// 		}
		// 	});
		// 	if(radchoice.length < 1){
		// 		$('[type="radio"]').each(function() {
		// 			$('.radio-list').parent().parent().addClass('has-error');
		// 			$('.radio-list').parent().parent().find('.help-block').html('This field is required.');
		// 		});
		// 	}
		// 	resval.push(radchoice.length > 0 ? 0 : 1);
		// }
		
		if($('#txtbday').length > 0){
			$('#txtbday').each(function() {
				resval.push(checkDate($(this)));
			});
		}

		// resval = resval.slice(1);
		// console.log(resval);
		// if(resval.includes(1)){
		// 	e.preventDefault();
		// }
	});
});