function checkElement(e){
	var res = 1;
	if(e.val() == ''){
		e.parent().addClass('has-error');
		errmsg = 'This field is required.';
		res = 1;
	}else{
		e.parent().removeClass('has-error');
		errmsg = '';
		res = 0;
	}
	e.next('.help-block').html(errmsg);
	return res;
}

$(document).ready(function() {
	// begin agency validation
	var res_agncy = []; res_agncy['code'] = 1; res_agncy['desc'] = 1;
	$('#agncy_code').keyup(function(){
		res_agncy['code'] = checkElement($('#agncy_code'));
	});
	$('#agncy_desc').keyup(function(){
		res_agncy['desc'] = checkElement($('#agncy_desc'));
	});
	$('#submit_agncy').click(function(e){
		res_agncy['code'] = checkElement($('#agncy_code'));
		res_agncy['desc'] = checkElement($('#agncy_desc'));
		if(res_agncy['code'] == 0 && res_agncy['desc'] == 0){
			// SUBMIT
		}else{
			e.preventDefault();
			if(res_agncy['code'] == 1){
				$('#agncy_code').parent().addClass('has-error');
				$('#agncy_code').next('.help-block').html('This field is required.');
			}
			if(res_agncy['desc'] == 1){
				$('#agncy_desc').parent().addClass('has-error');
				$('#agncy_desc').next('.help-block').html('This field is required.');
			}
		}
	});
	// end agency validation

	// begin agreement_type validation
	var res_at = []; res_at['code'] = 1; res_at['desc'] = 1;
	$('#at_code').keyup(function(){
		res_at['code'] = checkElement($('#at_code'));
	});
	$('#at_desc').keyup(function(){
		res_at['desc'] = checkElement($('#at_desc'));
	});
	$('#submit_at').click(function(e){
		res_at['code'] = checkElement($('#at_code'));
		res_at['desc'] = checkElement($('#at_desc'));
		if(res_at['code'] == 0 && res_at['desc'] == 0){
			// SUBMIT
		}else{
			e.preventDefault();
			if(res_at['code'] == 1){
				$('#at_code').parent().addClass('has-error');
				$('#at_code').next('.help-block').html('This field is required.');
			}
			if(res_at['desc'] == 1){
				$('#at_desc').parent().addClass('has-error');
				$('#at_desc').next('.help-block').html('This field is required.');
			}
		}
	});
	// end agreement_type validation

	// begin areas of cooperation validation
	var res_coop = []; res_coop['desc'] = 1;
	$('#aoc_desc').keyup(function(){
		res_coop['desc'] = checkElement($('#aoc_desc'));
	});
	$('#submit_aoc').click(function(e){
		res_coop['desc'] = checkElement($('#aoc_desc'));
		if(res_coop['desc'] == 0){
			// SUBMIT
		}else{
			e.preventDefault();
			if(res_coop['desc'] == 1){
				$('#aoc_desc').parent().addClass('has-error');
				$('#aoc_desc').next('.help-block').html('This field is required.');
			}
		}
	});

	var res_sub_aoc = []; res_sub_aoc['parent'] = 1; res_sub_aoc['abbrv'] = 1; res_sub_aoc['desc'] = 1;
	$('#selParentAoc').change(function(){
		res_sub_aoc['parent'] = checkElement($('#selParentAoc'));
	});
	$('#aoc_sub_desc').keyup(function(){
		res_sub_aoc['desc'] = checkElement($('#aoc_sub_desc'));
	});
	$('#submit_sub_aoc').click(function(e){
		res_sub_aoc['parent'] = checkElement($('#selParentAoc'));
		res_sub_aoc['desc'] = checkElement($('#aoc_sub_desc'));
		if(res_sub_aoc['parent'] == 0 && res_sub_aoc['desc'] == 0){
			// SUBMIT
		}else{
			e.preventDefault();
			if(res_sub_aoc['parent'] == 1){
				$('#selParentAoc').parent().addClass('has-error');
				$('#selParentAoc').next('.help-block').html('This field is required.');
			}
			if(res_sub_aoc['desc'] == 1){
				$('#aoc_sub_desc').parent().addClass('has-error');
				$('#aoc_sub_desc').next('.help-block').html('This field is required.');
			}
		}
	});
	// end area of cooperation validation

	// // begin area of attendee validation
	// var res_att = [1,1,1,1,1];
	// $('#att_agency').change(function(){
	// 	res_att[0] = checkElement($('#att_agency'));
	// });
	// $('#att_fname').keyup(function(){
	// 	res_att[1] = checkElement($('#att_fname'));
	// });
	// $('#att_mname').keyup(function(){
	// 	res_att[2] = checkElement($('#att_mname'));
	// });
	// $('#att_mi').keyup(function(){
	// 	res_att[3] = checkElement($('#att_mi'));
	// });
	// $('#att_lname').keyup(function(){
	// 	res_att[4] = checkElement($('#att_lname'));
	// });
	// $('#submit_att').click(function(e){
	// 	if(res_att.indexOf(1) > -1){
	// 		e.preventDefault();
	// 		if(res_att[0] == 1){
	// 			$('#att_agency').parent().addClass('has-error');
	// 			$('#att_agency').next('.help-block').html('This field is required.');
	// 		}
	// 		if(res_att[1] == 1){
	// 			$('#att_fname').parent().addClass('has-error');
	// 			$('#att_fname').next('.help-block').html('This field is required.');
	// 		}
	// 		if(res_att[2] == 1){
	// 			$('#att_mname').parent().addClass('has-error');
	// 			$('#att_mname').next('.help-block').html('This field is required.');
	// 		}
	// 		if(res_att[3] == 1){
	// 			$('#att_mi').parent().addClass('has-error');
	// 			$('#att_mi').next('.help-block').html('This field is required.');
	// 		}
	// 		if(res_att[4] == 1){
	// 			$('#att_lname').parent().addClass('has-error');
	// 			$('#att_lname').next('.help-block').html('This field is required.');
	// 		}
	// 	}else{
	// 		// SUBMIT
	// 	}
	// });
	// // end area of attendee validation

	// // begin area of budget validation
	// var res_budgt = [1,1,1,1];
	// $('#budgt_agency').change(function(){
	// 	res_budgt[0] = checkElement($('#budgt_agency'));
	// });
	// $('#budgt_type').change(function(){
	// 	res_budgt[1] = checkElement($('#budgt_type'));
	// });
	// $('#budgt_amt').keyup(function(){
	// 	res_budgt[2] = checkElement($('#budgt_amt'));
	// });
	// $('#budgt_year').keyup(function(){
	// 	res_budgt[3] = checkElement($('#budgt_year'));
	// });
	// $('#submit_budgt').click(function(e){
	// 	if(res_budgt.indexOf(1) > -1){
	// 		e.preventDefault();
	// 		if(res_budgt[0] == 1){
	// 			$('#budgt_agency').parent().addClass('has-error');
	// 			$('#budgt_agency').next('.help-block').html('This field is required.');
	// 		}
	// 		if(res_budgt[1] == 1){
	// 			$('#budgt_type').parent().addClass('has-error');
	// 			$('#budgt_type').next('.help-block').html('This field is required.');
	// 		}
	// 		if(res_budgt[2] == 1){
	// 			$('#budgt_amt').parent().addClass('has-error');
	// 			$('#budgt_amt').next('.help-block').html('This field is required.');
	// 		}
	// 		if(res_budgt[3] == 1){
	// 			$('#budgt_year').parent().addClass('has-error');
	// 			$('#budgt_year').next('.help-block').html('This field is required.');
	// 		}
	// 	}else{
	// 		// SUBMIT
	// 	}
	// });
	// // end area of budget validation

	// // begin budget_type validation
	// var res_bt = []; res_bt['code'] = 1; res_bt['desc'] = 1;
	// $('#bt_code').keyup(function(){
	// 	res_bt['code'] = checkElement($('#bt_code'));
	// });
	// $('#bt_desc').keyup(function(){
	// 	res_bt['desc'] = checkElement($('#bt_desc'));
	// });
	// $('#submit_bt').click(function(e){
	// 	if(res_bt['code'] == 0 && res_bt['desc'] == 0){
	// 		// SUBMIT
	// 	}else{
	// 		e.preventDefault();
	// 		if(res_bt['code'] == 1){
	// 			$('#bt_code').parent().addClass('has-error');
	// 			$('#bt_code').next('.help-block').html('This field is required.');
	// 		}
	// 		if(res_bt['desc'] == 1){
	// 			$('#bt_desc').parent().addClass('has-error');
	// 			$('#bt_desc').next('.help-block').html('This field is required.');
	// 		}
	// 	}
	// });
	// // end budget_type validation

	// begin country validation
	var res_ctry = []; res_ctry['code'] = 1; res_ctry['name'] = 1;
	$('#country_code').keyup(function(){
		res_ctry['code'] = checkElement($('#country_code'));
	});
	$('#country_name').keyup(function(){
		res_ctry['name'] = checkElement($('#country_name'));
	});
	$('#submit_country').click(function(e){
		res_ctry['code'] = checkElement($('#country_code'));
		res_ctry['name'] = checkElement($('#country_name'));
		if(res_ctry['code'] == 0 && res_ctry['name'] == 0){
			// SUBMIT
		}else{
			e.preventDefault();
			if(res_ctry['code'] == 1){
				$('#country_code').parent().addClass('has-error');
				$('#country_code').next('.help-block').html('This field is required.');
			}
			if(res_ctry['name'] == 1){
				$('#country_name').parent().addClass('has-error');
				$('#country_name').next('.help-block').html('This field is required.');
			}
		}
	});
	// end country validation

	// // begin engagement_type validation
	// var res_et = []; res_et['code'] = 1; res_et['name'] = 1;
	// $('#et_code').keyup(function(){
	// 	res_et['code'] = checkElement($('#et_code'));
	// });
	// $('#et_name').keyup(function(){
	// 	res_et['name'] = checkElement($('#et_name'));
	// });
	// $('#submit_et').click(function(e){
	// 	if(res_et['code'] == 0 && res_et['name'] == 0){
	// 		// SUBMIT
	// 	}else{
	// 		e.preventDefault();
	// 		if(res_et['code'] == 1){
	// 			$('#et_code').parent().addClass('has-error');
	// 			$('#et_code').next('.help-block').html('This field is required.');
	// 		}
	// 		if(res_et['name'] == 1){
	// 			$('#et_name').parent().addClass('has-error');
	// 			$('#et_name').next('.help-block').html('This field is required.');
	// 		}
	// 	}
	// });
	// // end engagement_type validation

	// // begin meeting_type validation
	// var res_mt = []; res_mt['code'] = 1; res_mt['name'] = 1;
	// $('#mt_code').keyup(function(){
	// 	res_mt['code'] = checkElement($('#mt_code'));
	// });
	// $('#mt_name').keyup(function(){
	// 	res_mt['name'] = checkElement($('#mt_name'));
	// });
	// $('#submit_mt').click(function(e){
	// 	if(res_mt['code'] == 0 && res_mt['name'] == 0){
	// 		// SUBMIT
	// 	}else{
	// 		e.preventDefault();
	// 		if(res_mt['code'] == 1){
	// 			$('#mt_code').parent().addClass('has-error');
	// 			$('#mt_code').next('.help-block').html('This field is required.');
	// 		}
	// 		if(res_mt['name'] == 1){
	// 			$('#mt_name').parent().addClass('has-error');
	// 			$('#mt_name').next('.help-block').html('This field is required.');
	// 		}
	// 	}
	// });
	// // end meeting_type validation

	// begin area of organization validation
	var res_org = []; res_org['abbrv'] = 1; res_org['name'] = 1;

	$('#org_abbrv').keyup(function(){
		res_org['abbrv'] = checkElement($('#org_abbrv'));
	});
	$('#org_name').keyup(function(){
		res_org['name'] = checkElement($('#org_name'));
	});
	$('#submit_org').click(function(e){
		res_org['abbrv'] = checkElement($('#org_abbrv'));
		res_org['name'] = checkElement($('#org_name'));
		if(res_org['abbrv'] == 0 && res_org['name'] == 0){
			// SUBMIT
		}else{
			e.preventDefault();
			if(res_org['abbrv'] == 1){
				$('#org_abbrv').parent().addClass('has-error');
				$('#org_abbrv').next('.help-block').html('This field is required.');
			}
			if(res_org['name'] == 1){
				$('#org_name').parent().addClass('has-error');
				$('#org_name').next('.help-block').html('This field is required.');
			}
		}
	});

	var res_sub_org = []; res_sub_org['parent'] = 1; res_sub_org['abbrv'] = 1; res_sub_org['name'] = 1;

	$('#selParentOrg').change(function(){
		res_sub_org['parent'] = checkElement($('#selParentOrg'));
	});
	$('#org_sub_abbrv').keyup(function(){
		res_sub_org['abbrv'] = checkElement($('#org_sub_abbrv'));
	});
	$('#org_sub_name').keyup(function(){
		res_sub_org['name'] = checkElement($('#org_sub_name'));
	});
	$('#submit_sub_org').click(function(e){
		res_sub_org['parent'] = checkElement($('#selParentOrg'));
		res_sub_org['abbrv'] = checkElement($('#org_sub_abbrv'));
		res_sub_org['name'] = checkElement($('#org_sub_name'));
		if(res_sub_org['abbrv'] == 0 && res_sub_org['name'] == 0){
			// SUBMIT
		}else{
			e.preventDefault();
			if(res_sub_org['parent'] == 1){
				$('#selParentOrg').parent().addClass('has-error');
				$('#selParentOrg').next('.help-block').html('This field is required.');
			}
			if(res_sub_org['abbrv'] == 1){
				$('#org_sub_abbrv').parent().addClass('has-error');
				$('#org_sub_abbrv').next('.help-block').html('This field is required.');
			}
			if(res_sub_org['name'] == 1){
				$('#org_sub_name').parent().addClass('has-error');
				$('#org_sub_name').next('.help-block').html('This field is required.');
			}
		}
	});
	// end area of organization validation

	// // begin scholarship_type validation
	// var res_st = []; res_st['code'] = 1; res_st['desc'] = 1;
	// $('#st_code').keyup(function(){
	// 	res_st['code'] = checkElement($('#st_code'));
	// });
	// $('#st_desc').keyup(function(){
	// 	res_st['desc'] = checkElement($('#st_desc'));
	// });
	// $('#submit_st').click(function(e){
	// 	if(res_st['code'] == 0 && res_st['desc'] == 0){
	// 		// SUBMIT
	// 	}else{
	// 		e.preventDefault();
	// 		if(res_st['code'] == 1){
	// 			$('#st_code').parent().addClass('has-error');
	// 			$('#st_code').next('.help-block').html('This field is required.');
	// 		}
	// 		if(res_st['desc'] == 1){
	// 			$('#st_desc').parent().addClass('has-error');
	// 			$('#st_desc').next('.help-block').html('This field is required.');
	// 		}
	// 	}
	// });
	// // end scholarship_type validation

	// begin status validation
	var res_stat = []; res_stat['code'] = 1; res_stat['desc'] = 1;
	$('#stat_code').keyup(function(){
		res_stat['code'] = checkElement($('#stat_code'));
	});
	$('#stat_desc').keyup(function(){
		res_stat['desc'] = checkElement($('#stat_desc'));
	});
	$('#submit_stat').click(function(e){
		res_stat['code'] = checkElement($('#stat_code'));
		res_stat['desc'] = checkElement($('#stat_desc'));
		if(res_stat['code'] == 0 && res_stat['desc'] == 0){
			// SUBMIT
		}else{
			e.preventDefault();
			if(res_stat['code'] == 1){
				$('#stat_code').parent().addClass('has-error');
				$('#stat_code').next('.help-block').html('This field is required.');
			}
			if(res_stat['desc'] == 1){
				$('#stat_desc').parent().addClass('has-error');
				$('#stat_desc').next('.help-block').html('This field is required.');
			}
		}
	});
	// end status validation

});