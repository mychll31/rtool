$(document).ready(function () {
  $('#txtfname,#txtlname,#txtuname,#txtpword,#txtretpword,#selquestion,#txtanswer').keyup(function () {
    checknull($(this))
  })

  $('#selquestion').change(function () {
    checknull($(this))
  })

  $('#txtemail').keyup(function () {
    checkemail($(this))
  })

  $('#radfemale,#radmale').click(function () {
    checkopt_2('radgender', 'optgender')
  })

  $('.date-picker').on('changeDate', function () {
    if ($('.date-picker').datepicker('getDate') == '') {
      $('.date-picker').parent().addClass('has-error')
      errmsg = 'This field is required.'
      res = 1
    } else {
      $('.date-picker').parent().removeClass('has-error')
      errmsg = ''
      res = 0
    }
    $('.date-picker').parent().find('.help-block').html(errmsg)
    return res
  })

  $('#txtpword').on('keyup', function () {
    if (!checknull($(this))) {
      validate_password($('#txtpword'), $('#txtretpword'))
    }
  })

  $('#txtretpword').on('keyup', function () {
    if (!checknull($(this))) {
      validate_password($('#txtpword'), $('#txtretpword'))
    }
  })

  $('#submit_register').click(function (e) {
   var res = 0
   res = res + checknull($('#txtfname'))
   res = res + checknull($('#txtlname'))
   res = res + checknull($('#txtuname'))
   res = res + checknull($('#selquestion'))
   res = res + checknull($('#txtanswer'))
   res = res + checkemail($('#txtemail'))
   res = res + checkopt_2('radgender', 'optgender')

    if (checknull($('#txtpword')) + checknull($('#txtretpword')) > 0) {
      res = res + 1
    } else {
      if (validate_password($('#txtpword'), $('#txtretpword'))) {
        res = res + 1
      }
    }

    if ($('.date-picker').datepicker('getDate') == null) {
      $('.date-picker').parent().addClass('has-error')
      errmsg = 'This field is required.'
      dpres = 1
    } else {
      $('.date-picker').parent().removeClass('has-error')
      errmsg = ''
      dpres = 0
    }
    $('.date-picker').parent().find('.help-block').html(errmsg)
    res = res + dpres

    if (res) {
      e.preventDefault()
    }
  })
})
