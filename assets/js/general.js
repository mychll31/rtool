function check_null(e){
	var res = 1;
	if(e.val() == ''){
		e.parent().parent().addClass('has-error');
		errmsg = 'This field is required.';
		res = 1;
    }else{
		e.parent().parent().removeClass('has-error');
		errmsg = '';
		res = 0;
	}
	e.parent().find('.help-block').html(errmsg);
	return res;
}

function check_email(e){
	var res = 1;
	if(e.val() == ''){
		e.parent().parent().addClass('has-error');
		errmsg = 'This field is required.';
		res = 1;
    }else{
    	var re = /\S+@\S+.\S+/;
    	if(re.test(e.val())){
			e.parent().parent().removeClass('has-error');
			errmsg = '';
			res = 0;
    	}else{
    		e.parent().parent().addClass('has-error');
			errmsg = 'Invalid email.';
			res = 1;
    	}
	}
	e.parent().find('.help-block').html(errmsg);
	return res;
}

function check_password(pword, retpword){
	var res = 1;
	if(pword.val() != retpword.val()){
		pword.parent().parent().addClass('has-error');
		retpword.parent().parent().addClass('has-error');
		errmsg = 'Password does not match.';
		res = 1;
    }else{
		pword.parent().parent().removeClass('has-error');
		retpword.parent().parent().removeClass('has-error');
		errmsg = '';
		res = 0;
	}
	pword.parent().find('.help-block').html(errmsg);
	retpword.parent().find('.help-block').html(errmsg);
	return res;
}