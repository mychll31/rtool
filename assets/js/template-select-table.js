function checkElement(e) {
	var res = 1;
	if(e.val() == ''){
		e.parent().parent().addClass('has-error');
		errmsg = 'This field is required.';
		res = 1;
	}else{
		e.parent().parent().removeClass('has-error');
		errmsg = '';
		res = 0;
	}
	e.parent().parent().find('.help-block').html(errmsg);
	return res;
}

$(document).ready(function() {
	var resval = [];

	$('select.form-required').change(function(e) {
		checkElement($(this));
	});

	$('#submit-select-tables').click(function() {
		resval.push(checkElement($('#seldbase')));
		resval.push(checkElement($('#seltable')));
	});
	console.log(resval);
});