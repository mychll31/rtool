function checkElement(e, obj){
	var res = 1;
	if(e.val() == ''){
		e.parent().parent().addClass('has-error');
		errmsg = 'This field is required.';
		res = 1;
    }else{
    	if(obj == 'input'){
	        if(!e.val().replace(/\s/g, '').length){
	            e.parent().addClass('has-error');
	            errmsg = 'Invalid input.';
	            res = 1;
	            e.parent().find('.help-block').html(errmsg);
	            return res;
	        }
	    }
		e.parent().parent().removeClass('has-error');
		errmsg = '';
		res = 0;
	}
	e.parent().find('.help-block').html(errmsg);
	return res;
}

function checkElement2(e, obj){
    var res = 1;
    if(e.val() == ''){
        e.parent().parent().parent().addClass('has-error');
        errmsg = 'This field is required.';
        res = 1;
    }else{
    	if(obj == 'input'){
	        if(!e.val().replace(/\s/g, '').length){
	            e.parent().parent().parent().addClass('has-error');
	            errmsg = 'Invalid input.';
	            res = 1;
	            e.parent().parent().parent().find('.help-block').html(errmsg);
	            return res;
	        }
	    }
        e.parent().parent().parent().removeClass('has-error');
        errmsg = '';
        res = 0;
    }
    
    e.parent().parent().find('.help-block').html(errmsg);
    return res;
}

function addError(e, obj=null){
	msg = 'This field is required.';
	if(obj=='email'){ msg = 'Invalid Email'; }
	if(obj=='pword'){ msg = 'Password not match'; }
	$(e).parent().parent().addClass('has-error');
	$(e).parent().parent().find('.help-block').html(msg);
	return 1;
}

function removeError(e){
	$(e).parent().parent().removeClass('has-error');
	$(e).parent().parent().find('.help-block').html('');
	return 0;
}

$(document).ready(function() {
	
    var errval = [];
    $('#txtfname').keyup(function(){
        errval['txtfname'] = checkElement($('#txtfname'), 'input');
    });
    $('#txtmname').keyup(function(){
        errval['txtmname'] = checkElement($('#txtmname'), 'input');
    });
    $('#txtlname').keyup(function(){
        errval['txtlname'] = checkElement($('#txtlname'), 'input');
    });
    $('#dtbdate').datepicker().on('changeDate', function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).parent().parent().find('.help-block').html('');
        errval['dfrom'] = 0;
    });
    $('#txtbdate').keyup(function(e){
        errval['txtbdate'] = checkElement2($('#txtbdate'), 'input');
    });
    $('#radfemale').click(function(){
    	errval['gender'] = removeError('.radio-list');
    });
    $('#radmale').click(function(){
    	errval['gender'] = removeError('.radio-list');
    });
    $('#txtbdate').keyup(function(e){
        errval['txtbdate'] = checkElement2($('#txtbdate'), 'input');
    });
    $('#txtemail').keyup(function(){
        errval['txtemail'] = checkElement($('#txtemail'), 'input');
    });
    $('#txtuname').keyup(function(){
        errval['txtuname'] = checkElement($('#txtuname'), 'input');
    });
    $('#txtpword').keyup(function(){
        errval['txtpword'] = checkElement($('#txtpword'), 'input');
    });
    $('#txtretpword').keyup(function(){
        errval['txtretpword'] = checkElement($('#txtretpword'), 'input');
    });
    $('#selquestion').change(function(){
        errval['selquestion'] = checkElement($('#selquestion'), '');
    });
    $('#txtanswer').keyup(function(){
        errval['txtanswer'] = checkElement($('#txtanswer'), 'input');
    });

	$('#submit_user').click(function(e){
        // check validation
        errval['txtfname'] = checkElement($('#txtfname'), 'input');
        errval['txtmname'] = checkElement($('#txtmname'), 'input');
        errval['txtlname'] = checkElement($('#txtlname'), 'input');
        errval['txtbday'] = checkElement2($('#txtbday'), 'input');
        if($('#radfemale').is(':checked') == false && $('#radmale').is(':checked') == false){
	        errval['gender'] = addError('.radio-list');
        }
        errval['txtemail'] = checkElement($('#txtemail'), 'input');
        if(errval['txtemail'] == 0){
        	var re = /\S+@\S+.\S+/;
        	if(re.test($('#txtemail').val())){
        		errval['invalidEmail'] = removeError($('#txtemail'));
        	}else{
        		errval['invalidEmail'] = addError($('#txtemail'), 'email');
        	}
        }
        errval['txtuname'] = checkElement($('#txtuname'), 'input');
        errval['txtpword'] = checkElement($('#txtpword'), 'input');
        errval['txtretpword'] = checkElement($('#txtretpword'), 'input');
        if(errval['txtpword'] == 0 && errval['txtretpword'] == 0){
        	if($('#txtpword').val() == $('#txtretpword').val()){
        		errval['pwnotmatch'] = removeError($('#txtpword'));
        		errval['pwnotmatch'] = removeError($('#txtretpword'));
        	}else{
        		errval['pwnotmatch'] = addError($('#txtpword'), 'pword');
        		errval['pwnotmatch'] = addError($('#txtretpword'), 'pword');
        	}
        }
        errval['selquestion'] = checkElement($('#selquestion'), '');
        errval['txtanswer'] = checkElement($('#txtanswer'), 'input');

        var cont = 0;
        for(var key in errval){ if(errval[key] == 1){ cont = 1; break; }}
        if(cont == 1){
            e.preventDefault();
        }
	});

});