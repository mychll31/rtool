function checkElement(e, obj = '') {
	var res = 1;
	if(e.val() == ''){
		e.parent().parent().addClass('has-error');
		errmsg = 'This field is required.';
		res = 1;
	}else{
		if(obj == 'text'){
	        if(!e.val().replace(/\s/g, '').length){
	            e.parent().parent().addClass('has-error');
	            errmsg = 'Invalid input.';
	            res = 1;
	        }else{
	        	e.parent().parent().removeClass('has-error');
				errmsg = '';
				res = 0;
	        }
	    }else{
	    	e.parent().parent().removeClass('has-error');
			errmsg = '';
			res = 0;
	    }
	}
	e.next('.help-block').html(errmsg);
	e.next().next('.help-block').html(errmsg);
	return res;
}

function checkPassword(pass, retpass) {
	if(pass.val() == '' || retpass.val() == ''){
		if(pass.val() == ''){
			pass.parent().parent().addClass('has-error');
			errmsg = 'This field is required.';
			res = 1;
		}else{
			pass.parent().parent().removeClass('has-error');
			errmsg = '';
			res = 0;
		}
		if(retpass.val() == ''){
			retpass.parent().parent().addClass('has-error');
			errmsg = 'This field is required.';
			res = 1;
		}else{
			retpass.parent().parent().removeClass('has-error');
			errmsg = '';
			res = 0;
		}
	}else{
		if(pass.val() != retpass.val()){
			pass.parent().parent().addClass('has-error');
			retpass.parent().parent().addClass('has-error');
			errmsg = 'Password not Match.';
			res = 1;
		}else{
			pass.parent().parent().removeClass('has-error');
			retpass.parent().parent().removeClass('has-error');
			errmsg = '';
			res = 0;
		}
	}
	pass.next('.help-block').html(errmsg);
	retpass.next('.help-block').html(errmsg);
	return res;
}

function checkFile(e, ext) {
	if(e.val() == ''){
		e.parent().parent().addClass('has-error');
		errmsg = 'This field is required.';
		res = 1;
	}else{
		ext = e.val().split('.');
		ext = ext[ext.length-1];
		if(ext != 'db'){
			e.parent().parent().addClass('has-error');
			errmsg = 'Invalid file.';
			res = 1;
	    }else{
	    	e.parent().parent().removeClass('has-error');
			errmsg = '';
			res = 0;
	    }
	}
	e.parent().parent().find('.help-block').html(errmsg);
	e.parent().parent().find('.help-block').html(errmsg);
	return res;
}

$(document).ready(function() {

	$('form [type="text"].form-required').keyup(function(e) {
		checkElement($(this), 'text');
	});

	$('form select.form-required').change(function(e) {
		checkElement($(this));
	});

	if($('form [type="password"].form-required').length > 0){
		$('form [type="password"].form-required').keyup(function(e) {
			checkElement($(this));
		});
	}

	if($('#seldbtype').length > 0){
		$('#seldbtype').change(function(e) {
			if($(this).val() == 3){
				$('.div-sqlite-hide').find('.form-control').removeClass('form-required');
				$('.div-sqlite-show').find('.form-control').addClass('form-required');
				$('.div-sqlite-hide').hide();
				$('.div-sqlite-show').show();
			}else{
				$('.div-sqlite-hide').find('.form-control').addClass('form-required');
				$('.div-sqlite-show').find('.form-control').removeClass('form-required');
				$('.div-sqlite-hide').show();
				$('.div-sqlite-show').hide();
			}
		});
	}

	$('form').submit(function(e) {
		// e.preventDefault();
		var resval = [];
		$('[type="text"].form-required').each(function() {
			resval.push(checkElement($(this), 'text'));
		});

		$('select.form-required').each(function() {
			resval.push(checkElement($(this)));
		});

		var arrpass = [];
		if($('form [type="password"].form-requireds').length > 0){
			$('[type="password"].form-required').each(function() {
				resval.push(checkElement($(this), 'text'));
				arrpass.push($(this));
			});
			resval.push(checkPassword(arrpass[0],arrpass[1]));
		}

		if($('form [type="password"].form-required').length > 0){
			$('[type="password"].form-required').each(function() {
				resval.push(checkElement($(this), 'text'));
				arrpass.push($(this));
			});
			resval.push(checkPassword(arrpass[0],arrpass[1]));
		}

		if($('form [type="file"].form-required').length > 0){
			$('[type="file"].form-required').each(function() {
				resval.push(checkFile($(this)));
				if($('#txtsqlitefile').length > 0){ $('#txtsqlitefile').val($(this).val());}
			});
		}

		console.log(resval);
		if(resval.includes(1)){
			e.preventDefault();
		}
	});
});