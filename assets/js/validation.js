function checknull (e, w = 0)
{
  var p = e.parent();
  if (w) {
    p = p.parent()
  }
  if (e.val() == '') {
    p.addClass('has-error')
    errmsg = 'This field is required.'
    res = 1
  } else {
    if (e.val().replace(/\s/g, '').length) {
      p.removeClass('has-error')
      errmsg = ''
      res = 0
    } else {
      p.addClass('has-error')
      errmsg = 'Invalid input.'
      p.find('.help-block').html(errmsg)
      res = 1
    }
  }
  p.find('.help-block').html(errmsg)
  return res
}

function checkemail (e, w = 0) {
  var p = e.parent()
  if (w) {
    p = p.parent()
  }
  if (e.val() == '') {
    p.addClass('has-error')
    errmsg = 'This field is required.'
    res = 1
  } else {
    var re = /\S+@\S+.\S+/
    if (re.test(e.val())) {
      p.removeClass('has-error')
      errmsg = ''
      res = 0
    } else {
      p.addClass('has-error')
      errmsg = 'Invalid input.'
      p.find('.help-block').html(errmsg)
      res = 1
    }
  }
  p.find('.help-block').html(errmsg)
  return res
}

function checkopt_2 (a, b) {
  if ($("input[name='" + a + "']:checked").val() == null) {
    $('#' + b).parent().addClass('has-error')
    errmsg = 'This field is required.'
    res = 1
  } else {
    $('#' + b).parent().removeClass('has-error')
    errmsg = ''
    res = 0
  }
  $('#' + b).parent().find('.help-block').html(errmsg)
  return res
}

function validate_password (pass, retpass, w = 0)
{
  vpass = pass.val();
  vretpass = retpass.val();
  errmsg = '';
  if(w) {
    pass = pass.parent();
    retpass = retpass.parent();
  }
  if(vpass!='' && vretpass!='') {
    if(vpass == vretpass) {
      pass.parent().removeClass('has-error');
      retpass.parent().removeClass('has-error');
      errmsg = '';
      res = 0;
    } else {
      pass.parent().addClass('has-error');
      retpass.parent().addClass('has-error');
      errmsg = 'password not match.';
      res = 1;
    }
  } else {
    res = 1;
  }
  pass.parent().find('.help-block').html(errmsg);
  retpass.parent().find('.help-block').html(errmsg);
  return res;
}
