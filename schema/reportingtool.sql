-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2020 at 02:49 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbrtool`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblaccess_level`
--

CREATE TABLE `tblaccess_level` (
  `access_level_id` int(11) NOT NULL,
  `access_level_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblaccess_level`
--

INSERT INTO `tblaccess_level` (`access_level_id`, `access_level_name`) VALUES
(1, 'admin'),
(2, 'system admin'),
(3, 'user'),
(4, 'guest');

-- --------------------------------------------------------

--
-- Table structure for table `tblcharts`
--

CREATE TABLE `tblcharts` (
  `id` int(11) NOT NULL,
  `layout_code` varchar(50) NOT NULL,
  `layout_name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `system` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcharts`
--

INSERT INTO `tblcharts` (`id`, `layout_code`, `layout_name`, `type`, `system`) VALUES
(1, 'amchart', 'amChart', 'BAR CHARTS (column and line mix)', 1),
(2, 'amchart', 'amChart', 'LINE & AREA (duration on value axis)', 1),
(3, 'amchart', 'amChart', 'LINE & AREA (with changing color)', 1),
(4, 'amchart', 'amChart', 'BAR & LINE (bar and line chart mix)', 1),
(5, 'amchart', 'amChart', '3D CHART (3d cylinder chart)', 1),
(6, 'amchart', 'amChart', 'SIMPLE PIE CHART', 1),
(7, 'amchart', 'amChart', '3D PIE CHART (bar and line chart mix)', 1),
(8, 'amchart', 'amChart', 'POLAR CHART', 1),
(9, 'amchart', 'amChart', 'RADAR CHART (bar and line chart mix)', 1),
(10, 'amchart', 'amChart', 'STOCK CHARTS (with stock events)', 1),
(12, 'flotchart', 'Flotchart', 'BASIC CHART', 1),
(13, 'flotchart', 'Flotchart', 'TRACKING CURVES', 1),
(14, 'flotchart', 'Flotchart', 'DYNAMIC CHART', 1),
(15, 'flotchart', 'Flotchart', 'STACK CHART CONTROLS', 1),
(16, 'flotchart', 'Flotchart', 'INTERACTIVE CHART', 1),
(17, 'flotchart', 'Flotchart', 'BAR CHART', 1),
(18, 'flotchart', 'Flotchart', 'HORIZONTAL BAR CHART', 1),
(19, 'flotchart', 'Flotchart', 'PIE CHART 1', 1),
(20, 'flotchart', 'Flotchart', 'PIE CHART 2', 1),
(21, 'flotchart', 'Flotchart', 'PIE CHART 3', 1),
(22, 'flotchart', 'Flotchart', 'PIE CHART 4', 1),
(23, 'flotchart', 'Flotchart', 'PIE CHART 5', 1),
(24, 'flotchart', 'Flotchart', 'PIE CHART 6', 1),
(25, 'flotchart', 'Flotchart', 'PIE CHART 7', 1),
(26, 'flotchart', 'Flotchart', 'PIE CHART 8', 1),
(27, 'flotchart', 'Flotchart', 'PIE CHART 9', 1),
(28, 'flotchart', 'Flotchart', 'PIE CHART 10', 1),
(29, 'flotchart', 'Flotchart', 'PIE CHART 11', 1),
(30, 'flotchart', 'Flotchart', 'PIE CHART 12', 1),
(31, 'googlecharts', 'Google Charts', 'COLUMN CHART 1', 1),
(32, 'googlecharts', 'Google Charts', 'COLUMN CHART 2', 1),
(33, 'googlecharts', 'Google Charts', 'LINE CHART 1', 1),
(34, 'googlecharts', 'Google Charts', 'PIE CHART 1', 1),
(35, 'googlecharts', 'Google Charts', 'PIE CHART 2', 1),
(36, 'echarts', 'eCharts', 'PIE CHART', 1),
(37, 'echarts', 'eCharts', 'BAR CHART', 1),
(38, 'echarts', 'eCharts', 'LINE CHART', 1),
(39, 'echarts', 'eCharts', 'SCATTER CHART', 1),
(40, 'echarts', 'eCharts', 'CANDLESTICK CHART', 1),
(41, 'morrischarts', 'Morris Chart', 'PIE CHART', 1),
(42, 'morrischarts', 'Morris Chart', 'LINE CHART', 1),
(43, 'morrischarts', 'Morris Chart', 'AREA CHART', 1),
(44, 'morrischarts', 'Morris Chart', 'BAR CHART', 1),
(46, 'highcharts', 'HighCharts', 'LINE CHART 1', 1),
(47, 'highcharts', 'HighCharts', 'LINE CHART 2', 1),
(51, 'highcharts', 'HighCharts', 'AREA CHART', 1),
(52, 'highcharts', 'HighCharts', 'BAR CHART', 1),
(53, 'highcharts', 'HighCharts', 'DONUT CHART', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblcustom_sql`
--

CREATE TABLE `tblcustom_sql` (
  `cust_id` int(11) NOT NULL,
  `cust_repid` int(11) NOT NULL,
  `cust_stmt` text NOT NULL,
  `is_drafted` int(11) NOT NULL DEFAULT '0',
  `is_show` int(11) NOT NULL DEFAULT '1',
  `added_date` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbldata_sources`
--

CREATE TABLE `tbldata_sources` (
  `source_id` int(11) NOT NULL,
  `connname` varchar(250) NOT NULL,
  `dbhost` varchar(100) DEFAULT NULL,
  `dbtype` int(11) NOT NULL,
  `dbname` varchar(250) DEFAULT NULL,
  `dbusername` varchar(250) DEFAULT NULL,
  `dbpass` varchar(250) DEFAULT NULL,
  `is_file` int(2) NOT NULL DEFAULT '0',
  `filename` varchar(150) DEFAULT NULL,
  `actual_filename` varchar(150) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbldata_source_type`
--

CREATE TABLE `tbldata_source_type` (
  `type_id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `type_description` varchar(150) DEFAULT NULL,
  `wfileupload` int(11) NOT NULL DEFAULT '0',
  `wcreds` int(11) NOT NULL DEFAULT '0',
  `file_type` varchar(500) DEFAULT NULL,
  `file_loc` varchar(150) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbldata_source_type`
--

INSERT INTO `tbldata_source_type` (`type_id`, `name`, `type_description`, `wfileupload`, `wcreds`, `file_type`, `file_loc`, `is_active`) VALUES
(1, 'MySQL', 'Relational Database based on SQL', 0, 1, NULL, NULL, 1),
(2, 'MSSQL', 'Relational Database Management System from Microsoft', 0, 1, NULL, NULL, 1),
(3, 'SQLite', 'File contains a database.', 1, 1, '.sqlite,.db,.db3,.sqlite3', 'data/filesource/sqlite/', 1),
(4, 'Oracle', 'Database from Oracle Corporation.', 0, 1, NULL, NULL, 1),
(5, 'EXCEL', 'Microsoft Excel spreadsheet', 1, 0, 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'data/filesource/excel/', 1),
(6, 'CSV', 'Comma-separated values', 1, 0, '.csv', 'data/filesource/csv/', 1),
(7, 'JSON', 'Javascript Object Notation', 1, 0, '.json', 'data/filesource/json/', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblfile_sources`
--

CREATE TABLE `tblfile_sources` (
  `fsource_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL COMMENT 'Resource id from tbldata_source',
  `filename` varchar(250) NOT NULL,
  `filetype` varchar(10) NOT NULL,
  `fileext` varchar(10) NOT NULL,
  `filesize` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblfsource_joins`
--

CREATE TABLE `tblfsource_joins` (
  `fsource_id` int(11) NOT NULL,
  `ftable1_id` int(11) NOT NULL,
  `ftable1_field` varchar(150) NOT NULL,
  `ftable2_id` int(11) NOT NULL,
  `ftable2_field` varchar(150) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblfsource_reports`
--

CREATE TABLE `tblfsource_reports` (
  `freport_id` int(11) NOT NULL,
  `freport_name` varchar(150) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblfsource_tables`
--

CREATE TABLE `tblfsource_tables` (
  `ftable_id` int(11) NOT NULL,
  `fsource_id` int(11) NOT NULL,
  `freport_id` int(11) NOT NULL,
  `is_select` int(11) NOT NULL DEFAULT '0',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblgroups`
--

CREATE TABLE `tblgroups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbljoins`
--

CREATE TABLE `tbljoins` (
  `join_id` int(11) NOT NULL,
  `join_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbljoins`
--

INSERT INTO `tbljoins` (`join_id`, `join_name`) VALUES
(1, 'inner join'),
(2, 'left join'),
(3, 'right join'),
(4, 'full join');

-- --------------------------------------------------------

--
-- Table structure for table `tbllogs`
--

CREATE TABLE `tbllogs` (
  `log_id` int(11) NOT NULL,
  `log_usr_id` int(11) DEFAULT NULL,
  `log_sql` text,
  `log_status` int(11) NOT NULL DEFAULT '1' COMMENT '1=normal,2=warning,3=danger',
  `log_desc` varchar(500) DEFAULT NULL,
  `log_json` text,
  `log_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblnotifications`
--

CREATE TABLE `tblnotifications` (
  `notif_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `notif_type` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `is_unread` int(11) NOT NULL DEFAULT '1',
  `notif_link` varchar(500) NOT NULL,
  `notif_message` varchar(500) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblnotification_types`
--

CREATE TABLE `tblnotification_types` (
  `type_id` int(11) NOT NULL,
  `type_icon` varchar(20) NOT NULL,
  `alert_type` varchar(10) NOT NULL COMMENT 'success;warning;info;danger'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblnotification_types`
--

INSERT INTO `tblnotification_types` (`type_id`, `type_icon`, `alert_type`) VALUES
(1, 'fa fa-plus', 'success'),
(2, 'fa fa-file', 'success'),
(3, 'fa fa-bolt', 'danger'),
(4, 'fa fa-bolt', 'success'),
(5, 'fa fa-pencil', 'success'),
(6, 'fa fa-info', 'warning');

-- --------------------------------------------------------

--
-- Table structure for table `tblpermissions`
--

CREATE TABLE `tblpermissions` (
  `perm_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblprofiles`
--

CREATE TABLE `tblprofiles` (
  `userid` int(11) NOT NULL,
  `firstname` varchar(250) DEFAULT NULL,
  `surname` varchar(250) DEFAULT NULL,
  `middlename` varchar(250) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `sec_ques_1` int(11) DEFAULT NULL COMMENT 'First security question',
  `sec_answer_1` varchar(250) DEFAULT NULL COMMENT 'First answer',
  `sec_ques_2` int(11) DEFAULT NULL COMMENT 'Second security question',
  `sec_answer_2` varchar(250) DEFAULT NULL COMMENT 'Second answer',
  `sec_ques_3` int(11) DEFAULT NULL COMMENT 'Third security question',
  `sec_answer_3` varchar(250) DEFAULT NULL COMMENT 'Third answer',
  `image_fname` varchar(1000) DEFAULT NULL,
  `upload_datetime` datetime DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblprojects`
--

CREATE TABLE `tblprojects` (
  `report_id` int(11) NOT NULL,
  `report_name` varchar(250) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_published` int(11) NOT NULL DEFAULT '0',
  `is_public` int(11) NOT NULL DEFAULT '0',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `is_edit` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(50) DEFAULT NULL,
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblprojects_shared`
--

CREATE TABLE `tblprojects_shared` (
  `shared_id` int(11) NOT NULL,
  `report_id` int(11) NOT NULL COMMENT 'Project id',
  `view_report` int(11) NOT NULL DEFAULT '1',
  `edit_report` int(11) NOT NULL DEFAULT '0',
  `shared_to` int(11) DEFAULT NULL,
  `shared_to_groupid` int(11) DEFAULT NULL,
  `shared_by` int(11) NOT NULL,
  `shared_by_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblproject_connections`
--

CREATE TABLE `tblproject_connections` (
  `conn_id` int(11) NOT NULL,
  `report_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL COMMENT 'Data source id',
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblproject_tables`
--

CREATE TABLE `tblproject_tables` (
  `table_id` int(11) NOT NULL,
  `table_name` varchar(250) DEFAULT NULL,
  `field_names` text,
  `report_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `table_order` int(11) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblproject_tjoins`
--

CREATE TABLE `tblproject_tjoins` (
  `report_join_id` int(11) NOT NULL,
  `join_order` int(11) NOT NULL DEFAULT '0',
  `report_id` int(11) NOT NULL,
  `table1_id` int(11) NOT NULL,
  `table1_pk` varchar(150) NOT NULL,
  `join_name` int(11) NOT NULL,
  `table2_id` int(11) NOT NULL,
  `table2_pk` varchar(150) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblroles`
--

CREATE TABLE `tblroles` (
  `role_id` int(11) NOT NULL,
  `user_access_level` int(11) NOT NULL,
  `module_name` varchar(100) DEFAULT NULL,
  `role_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsecurity_questions`
--

CREATE TABLE `tblsecurity_questions` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `added_date` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE `tblusers` (
  `userid` int(11) NOT NULL,
  `custom_userid` varchar(30) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `last_login` datetime NOT NULL,
  `last_pass_changed` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT '1 is for active, 0 for inactive',
  `is_confirmed` int(11) NOT NULL DEFAULT '0',
  `confirmation_code` varchar(250) DEFAULT NULL,
  `access_level` int(11) NOT NULL DEFAULT '4' COMMENT '4 is for guest, refer to tblaccess_level',
  `added_date` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `added_by_ip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbluser_group`
--

CREATE TABLE `tbluser_group` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblaccess_level`
--
ALTER TABLE `tblaccess_level`
  ADD PRIMARY KEY (`access_level_id`);

--
-- Indexes for table `tblcharts`
--
ALTER TABLE `tblcharts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblcustom_sql`
--
ALTER TABLE `tblcustom_sql`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indexes for table `tbldata_sources`
--
ALTER TABLE `tbldata_sources`
  ADD PRIMARY KEY (`source_id`);

--
-- Indexes for table `tbldata_source_type`
--
ALTER TABLE `tbldata_source_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `tblfile_sources`
--
ALTER TABLE `tblfile_sources`
  ADD PRIMARY KEY (`fsource_id`);

--
-- Indexes for table `tblfsource_joins`
--
ALTER TABLE `tblfsource_joins`
  ADD PRIMARY KEY (`fsource_id`);

--
-- Indexes for table `tblfsource_reports`
--
ALTER TABLE `tblfsource_reports`
  ADD PRIMARY KEY (`freport_id`);

--
-- Indexes for table `tblfsource_tables`
--
ALTER TABLE `tblfsource_tables`
  ADD PRIMARY KEY (`ftable_id`);

--
-- Indexes for table `tblgroups`
--
ALTER TABLE `tblgroups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `tbljoins`
--
ALTER TABLE `tbljoins`
  ADD PRIMARY KEY (`join_id`);

--
-- Indexes for table `tbllogs`
--
ALTER TABLE `tbllogs`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `tblnotifications`
--
ALTER TABLE `tblnotifications`
  ADD PRIMARY KEY (`notif_id`);

--
-- Indexes for table `tblnotification_types`
--
ALTER TABLE `tblnotification_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `tblpermissions`
--
ALTER TABLE `tblpermissions`
  ADD PRIMARY KEY (`perm_id`);

--
-- Indexes for table `tblprofiles`
--
ALTER TABLE `tblprofiles`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `tblprojects`
--
ALTER TABLE `tblprojects`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `tblprojects_shared`
--
ALTER TABLE `tblprojects_shared`
  ADD PRIMARY KEY (`shared_id`);

--
-- Indexes for table `tblproject_connections`
--
ALTER TABLE `tblproject_connections`
  ADD PRIMARY KEY (`conn_id`);

--
-- Indexes for table `tblproject_tables`
--
ALTER TABLE `tblproject_tables`
  ADD PRIMARY KEY (`table_id`);

--
-- Indexes for table `tblproject_tjoins`
--
ALTER TABLE `tblproject_tjoins`
  ADD PRIMARY KEY (`report_join_id`);

--
-- Indexes for table `tblroles`
--
ALTER TABLE `tblroles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `tblsecurity_questions`
--
ALTER TABLE `tblsecurity_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblusers`
--
ALTER TABLE `tblusers`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblaccess_level`
--
ALTER TABLE `tblaccess_level`
  MODIFY `access_level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblcharts`
--
ALTER TABLE `tblcharts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `tblcustom_sql`
--
ALTER TABLE `tblcustom_sql`
  MODIFY `cust_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbldata_sources`
--
ALTER TABLE `tbldata_sources`
  MODIFY `source_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbldata_source_type`
--
ALTER TABLE `tbldata_source_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tblfile_sources`
--
ALTER TABLE `tblfile_sources`
  MODIFY `fsource_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblfsource_joins`
--
ALTER TABLE `tblfsource_joins`
  MODIFY `fsource_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblfsource_reports`
--
ALTER TABLE `tblfsource_reports`
  MODIFY `freport_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblfsource_tables`
--
ALTER TABLE `tblfsource_tables`
  MODIFY `ftable_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblgroups`
--
ALTER TABLE `tblgroups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbljoins`
--
ALTER TABLE `tbljoins`
  MODIFY `join_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbllogs`
--
ALTER TABLE `tbllogs`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblnotifications`
--
ALTER TABLE `tblnotifications`
  MODIFY `notif_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblnotification_types`
--
ALTER TABLE `tblnotification_types`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tblpermissions`
--
ALTER TABLE `tblpermissions`
  MODIFY `perm_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblprojects`
--
ALTER TABLE `tblprojects`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblprojects_shared`
--
ALTER TABLE `tblprojects_shared`
  MODIFY `shared_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblproject_connections`
--
ALTER TABLE `tblproject_connections`
  MODIFY `conn_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblproject_tables`
--
ALTER TABLE `tblproject_tables`
  MODIFY `table_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblproject_tjoins`
--
ALTER TABLE `tblproject_tjoins`
  MODIFY `report_join_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblroles`
--
ALTER TABLE `tblroles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblsecurity_questions`
--
ALTER TABLE `tblsecurity_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblusers`
--
ALTER TABLE `tblusers`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
