###################
Business Intelligence Reporting Tool - BI-RTOOL
###################


This study will contribute to the list of open source reporting tools that will help users in generating
a report without the need of IT people. This study will help big and small organizations to make their data
more valuable and for them to make a good decision. Also, small entrepreneurs will be encouraged to set big
goals for their business by promoting innovations to develop strong industries. This study will also help to
show what the organization can contribute to the economy.













*******************
Objectives of the Study
*******************

The general objective of the study is to develop a web-based reporting tool.
Specifically, the study aims to:

1.	Design a system with the following features:

a. Has DSS predictive analytics and forecast that help users for anticipatory decision making and may lead to fact-based decision making.

b. Has Ability to export report using the most suitable format (HTML, PDF, XLS, XML, TXT, CSV, RTF)

c. Has ready-to-use charts according to single charts (e.g. histograms, pie charts, bar charts, etc)

d. Has Ad-hoc Reporting module that allows non-IT users to define and customize reports that meet user’s requirements.

2.	Create the system as designed using HTML for the front-end; PHP for back-end and; R and Python programming for data extraction.

3.	Test the project using system testing and compatibility testing, and improve the project based on the results of the tests conducted; and

4.	Evaluate the acceptability/performance of the project prototype using the ISO 25010 Quality Standard.












**************************
Scope and Limitations of the Study
**************************

There are many reporting tools to choose from but some of them are a little complex to use,
even the developers have to spend the time to learn how to use it and how to make it look good.
This study focuses on the development of a system that will allow non-IT people to generate reports with big data.












*******************
Server Requirements
*******************

Software Requirements

a.	Windows Operating System
b.	Web Server (Apache, NGINX, IIS)
c.	Text Editor (Sublime)
d.	GitBash
e.	Java Run Time
f.	MySQL; MSSQL
g.	PHP version 5.6 or newer is recommended.

Hardware Requirements

a.	Computer with minimum 2GHz Processor
b.	Minimum of 2GB RAM
